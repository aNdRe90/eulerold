

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Problem133 {
	/*
	 * A number consisting entirely of ones is called a repunit. 
	 * We shall define R(k) to be a repunit of length k; 
	 * for example, R(6) = 111111.
	 * 
	 * Let us consider repunits of the form R(10^n).
	 * 
	 * Although R(10), R(100), or R(1000) are not divisible by 17, 
	 * R(10000) is divisible by 17. 
	 * 
	 * Yet there is no value of n for which R(10n) will divide by 19. 
	 * In fact, it is remarkable that 11, 17, 41, and 73 
	 * are the only four primes below one-hundred 
	 * that can be a factor of R(10^n).
	 * 
	 * Find the sum of all the primes below one-hundred thousand 
	 * that will never be a factor of R(10^n).
	 */
	
	/*
	 * SOLUTION:   my solution from problem 132 the fact
	 * that if we operate on modulos operation some values
	 * may quickly repeat, for example we get modulos:
	 * 5, 3, 7, 18, 5 performing the same operations on them.
	 * And we may say that no modulo 1 will appear. I use it
	 * to determine whether modulo 1 will appear.
	 */
	public static void main(String[] args) {
		final long N = 100000;
		BigInteger tenToTen = new BigInteger("10000000000");
		long sum = 0L;
		
		for(long p=2L; p<N; p++) {
			if(MyStuff.MyMath.isPrime(p)) {
				System.out.println(p);
				
				BigInteger modular = BigInteger.valueOf(9L*p);
				BigInteger modulo = tenToTen.mod(modular);
				
				Set<BigInteger> modulos = new TreeSet<BigInteger>();
				modulos.add(modulo);
				
				while(true) {
					modulo = modulo.pow(10).mod(modular);
					if(modulo.equals(BigInteger.ONE)) {
						break;
					}
					
					if(modulos.contains(modulo)) {
						sum += p;
						break;
					}					
					
					modulos.add(modulo);
				}
			}
		}
		
		System.out.println(sum);
	}
}


public class Problem19 
{
	/*
	 * You are given the following information, but you may prefer to do some research for yourself.

    1 Jan 1900 was a Monday.
    Thirty days has September,
    April, June and November.
    All the rest have thirty-one,
    Saving February alone,
    Which has twenty-eight, rain or shine.
    And on leap years, twenty-nine.
    A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.

	How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?

	 */
	public static void main(String[] args) 
	{
		int nextYearsFirstDay = 1;
		int sumOfFirstSundays = 0;
		
		for(int i=1900; i<2001; i++)
		{
			Year y = new Year(i, nextYearsFirstDay);
			if(i>1900)
				sumOfFirstSundays += y.getSundaysOnFirstDayOfMonth();		
			
			
			if(y.isLeap())
				nextYearsFirstDay +=2;
			else
				nextYearsFirstDay +=1;
			
			if(nextYearsFirstDay>7)
				nextYearsFirstDay -= 7;
		}
		
		System.out.println(sumOfFirstSundays);
	}
}

class Year
{
	public Year(int year, int firstDayNumber)
	{
		this.year = year;
		firstDay = firstDayNumber;  //1 = monday, 2 = tuesday etc.
		if(year%400==0)
			leap = true;
		else if(year%100==0)
			leap = false;
		else if(year%4==0)
			leap = true;
		
		if(leap)
		{
			//monthDays[1]+=1;
			daysForward[1]+=1;
		}
	}
	public int getSundaysOnFirstDayOfMonth()
	{
		int counter = 0;
		int firstDayOfMonth = firstDay;
		if(firstDayOfMonth==7)
			counter++;
		
		for(int i=0; i<11; i++)
		{
			firstDayOfMonth += daysForward[i];
			if(firstDayOfMonth>7)
				firstDayOfMonth -=7;
			if(firstDayOfMonth==7)
				counter++;
		}
		
		return counter;
	}
	public boolean isLeap()
	{
		return leap;
	}
	
	private int year;
	private final int months = 12;
	private boolean leap;
	private int firstDay;
	//private int[] monthDays = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	private int[] daysForward = { 3, 0, 3, 2, 3, 2, 3, 3, 2, 3, 2, 3 };
}




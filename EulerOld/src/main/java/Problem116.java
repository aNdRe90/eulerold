

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

public class Problem116 {
	/*
	 * A row of five black square tiles is to have a number of its tiles 
	 * replaced with coloured oblong tiles chosen from red (length two), green (length three), 
	 * or blue (length four).
	 * 
	 * If red tiles are chosen there are exactly seven ways this can be done.	  
	 * If green tiles are chosen there are three ways.	  
	 * And if blue tiles are chosen there are two ways.
	 * 
	 * Assuming that colours cannot be mixed there are 7 + 3 + 2 = 12 ways of 
	 * replacing the black tiles in a row measuring five units in length.
	 * 
	 * How many different ways can the black tiles in a row measuring fifty units 
	 * in length be replaced if colours cannot be mixed and at least one coloured tile must be used?
	 * 
	 * NOTE: This is related to problem 117.
	 */
	public static void main(String[] args) {
		BigInteger result = BigInteger.ZERO;
		final int rowLength = 50;
		
		for(int blockLength=2; blockLength<=4; blockLength++) {
			for(int i=0; i<=rowLength; i++)
				solutions.put(i, new HashMap<Integer, BigInteger>());
			
			result = result.add(getSolutionForRow(rowLength, blockLength));
			result = result.subtract(BigInteger.ONE);  //row cannot be empty like in 114,115 problems.
		}
		
		System.out.println(result);
	}
	
	private static BigInteger getSolutionForRow(int rowLength, int blockLength) {
		BigInteger preResult = getPreResult(rowLength, -1);
		if(preResult!=null)
			return preResult;
		
		BigInteger result = BigInteger.ONE;		
		if(rowLength<blockLength)
			return result;		
		
		//for(int blockLength=minBlockLength; blockLength<=rowLength; blockLength++) {
			result = result.add(getSolutionForRowAndBlockLength(rowLength, blockLength, blockLength));
		//}
		
		solutions.get(rowLength).put(-1, result);
		return result;
	}

	private static BigInteger getSolutionForRowAndBlockLength(
										int rowLength, int blockLength, int minBlockLength) {
		BigInteger preResult = getPreResult(rowLength, blockLength);
		if(preResult!=null)
			return preResult;
		
		BigInteger result = BigInteger.ZERO;
		int maxStartIndex = rowLength - blockLength;
		
		for(int startIndex=0; startIndex<=maxStartIndex; startIndex++) {
			result = result.add(BigInteger.ONE.multiply(
								getSolutionForRow(maxStartIndex-startIndex, minBlockLength)));
		}
		
		solutions.get(rowLength).put(blockLength, result);
		return result;
	}
	
	
	private static BigInteger getPreResult(int rowLength, int blockLength) {
		BigInteger preResult = null;
		
		Map<Integer, BigInteger> temp = solutions.get(rowLength);
		if(temp!=null)
			preResult = temp.get(blockLength);
			
		return preResult;
	}

	private static Map<Integer, Map<Integer, BigInteger>> solutions 
				= new HashMap<Integer, Map<Integer, BigInteger>>();
}

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.StringTokenizer;


public class Problem59
{
	/*
	 * Each character on a computer is assigned a unique code and the preferred standard is ASCII 
	 * (American Standard Code for Information Interchange). 
	 * For example, uppercase A = 65, asterisk (*) = 42, and lowercase k = 107.
	 * 
	 * A modern encryption method is to take a text file, convert the bytes to ASCII, 
	 * then XOR each byte with a given value, taken from a secret key. 
	 * The advantage with the XOR function is that using the same encryption key on the cipher text, 
	 * restores the plain text; for example, 65 XOR 42 = 107, then 107 XOR 42 = 65.
	 * 
	 * For unbreakable encryption, the key is the same length as the plain text message, 
	 * and the key is made up of random bytes. 
	 * The user would keep the encrypted message and the encryption key in different locations, 
	 * and without both "halves", it is impossible to decrypt the message.
	 * 
	 * Unfortunately, this method is impractical for most users, 
	 * so the modified method is to use a password as a key. 
	 * If the password is shorter than the message, which is likely, 
	 * the key is repeated cyclically throughout the message. 
	 * The balance for this method is using a sufficiently long password key for security, 
	 * but short enough to be memorable.
	 * 
	 * Your task has been made easy, as the encryption key consists of three lower case characters. 
	 * Using cipher1.txt (right click and 'Save Link/Target As...'), a file containing the encrypted ASCII codes, 
	 * and the knowledge that the plain text must contain common English words, 
	 * decrypt the message and find the sum of the ASCII values in the original text.
	 */
	public static void main(String[] args) 
	{
		/*byte[] k = {(byte) 12, (byte) 13, (byte) 1 };
		byte[] x = {(byte) 80, (byte) 97, (byte) 117, (byte) 108, (byte) 105, (byte) 110, (byte) 97, };
		byte[] y = encryptXOR(x,k);
		byte[] z = decipherXOR(y,k);
		
		String orig = new String(x);
		String en = new String(y);
		String dec = new String(z);
		byte[] kk = decipherXOR(y, x);*/
		
		
		
		
		
		int minCommonWordsToFind = 30;
		long sum = 0L;
		
		
		String[] commonWordsSorted = getSortedCommonWords();
		byte[] encrypted = readMessage();	
		String en = new String(encrypted);
		
		byte[] key = new byte[3];
		for(key[0] = 'a'; key[0]<='z'; key[0]++)
			for(key[1] = 'a'; key[1]<='z'; key[1]++)
				for(key[2] = 'a'; key[2]<='z'; key[2]++)
				{
					byte[] decipheredBytes = decipherXOR(encrypted, key);
					String deciphered = new String(decipheredBytes);
					
					int commonFound = 0;
					for(int i=0; i<commonWordsSorted.length; i++)
						if(deciphered.contains(commonWordsSorted[i]))
						{
							commonFound++;
						}
					
					//if(commonFound>deciphered.length()/2)
					//{
					if(commonFound>minCommonWordsToFind)
					{
						String k = ""+(char)key[0]+(char)key[1]+(char)key[2];
						int length = deciphered.length();
						for(int i=0; i<length; i++)
							sum += deciphered.charAt(i);
						
						key[1] = key[0] = 'z'+1;
						break;
					}
					//}
				}
		
		System.out.println(sum);
	}
	
	
	
	public static byte[] readMessage()
	{
		BufferedReader f = null;
		LinkedList<Byte> encryptedMessage = new LinkedList<Byte>();
		
		try
		{
			f = new BufferedReader(new FileReader("cipher1.txt"));
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
		String line = null;
		do
		{
			try 
			{
				line = f.readLine();
			} 
			catch(IOException ee) { ee.printStackTrace(); }
			
			if(line==null)
				break;
			
			StringTokenizer st = new StringTokenizer(line, ",");
			
			while(st.hasMoreTokens())
				encryptedMessage.add( Byte.valueOf(st.nextToken()) );
		}
		while(true);
		
		int length = encryptedMessage.size();
		
		byte[] encrypted = new byte[length];
		Iterator<Byte> iter = encryptedMessage.iterator();
		int i = 0;
		while(iter.hasNext())
			encrypted[i++] = iter.next().byteValue();
		
		return encrypted;
	}
	
	public static byte[] decipherXOR(byte[] encrypted, byte[] key)
	{
		if(key==null || key.length==0 || encrypted==null || encrypted.length==0)
			return null;
		
		byte[] deciphered = new byte[encrypted.length];
		
		for(int i=0, j=0; i<deciphered.length; i++)
		{
			deciphered[i] = (byte) (encrypted[i] ^ key[j++]);
			if(j==key.length)
				j = 0;
		}
		
		return deciphered;
	}
	
	public static byte[] encryptXOR(byte[] original, byte[] key)
	{
		if(key==null || key.length==0 || original==null || original.length==0)
			return null;
		
		byte[] encrypted = new byte[original.length];
		
		for(int i=0, j=0; i<encrypted.length; i++)
		{
			encrypted[i] = (byte) (original[i] ^ key[j++]);
			if(j==key.length)
				j = 0;
		}
		
		return encrypted;
	}
	
	private static String[] getSortedCommonWords()
	{
		LinkedList<String> sorted = new LinkedList<String>();
		for(int i=0; i<commonWords.length; i++)
			sorted.add(commonWords[i]);
		
		Collections.sort(sorted);
		
		String[] sortedWords = new String[sorted.size()];
		Iterator<String> iter = sorted.iterator();
		int i = 0;
		while(iter.hasNext())
			sortedWords[i++] = iter.next();
			
		sorted = null;
		return sortedWords;
	}
	
	private static String[] commonWords = 
		{ 
		  "the", "be", "two", "of", "and", "a", "in", "that", "have", "i",
		  "it", "for", "not", "on", "with", "he", "as", "you", "do", "at",
		  "this", "but", "his", "by", "from", "they", "we", "say", "her", "she",
		  "or", "an", "will", "my", "one", "all", "would", "there", "their", "what",
		  "so", "up", "out", "if", "about", "who", "get", "which", "go", "me",
		  "when", "make", "can", "like", "time", "no", "just", "him", "know", "take",
		  "person", "into", "year", "your", "good", "some", "could", "them", "see", "other",
		  "than", "then", "now", "look", "only", "come", "its", "over", "think", "also", 
		  "back", "after", "use", "two", "how", "our", "work", "first", "well", "way",
		  "even", "new", "want", "beacause", "any", "these", "give", "day", "most", "us"
		};
}



import MyStuff.MyMath;
import MyStuff.Prime;

public class Problem234 {
	/*
	 * For an integer n >= 4, we define the lower prime square root of n, 
	 * denoted by lps(n), as the largest prime <= sqrt(n) and the upper prime 
	 * square root of n, ups(n), as the smallest prime >= sqrt(n).
	 * 
	 * So, for example, lps(4) = 2 = ups(4), lps(1000) = 31, ups(1000) = 37.
	 * Let us call an integer n >= 4 semidivisible, if one of lps(n) and ups(n) 
	 * divides n, but not both.
	 * 
	 * The sum of the semidivisible numbers not exceeding 15 is 30, 
	 * the numbers are 8, 10 and 12.
	 * 
	 * 15 is not semidivisible because it is a multiple of both lps(15) = 3 and ups(15) = 5.
	 * As a further example, the sum of the 92 semidivisible numbers up to 1000 is 34825.
	 * 
	 * What is the sum of all semidivisible numbers not exceeding 999966663333 ?
	 */
	public static void main(String[] args) {
		final long N = 999966663333L; //sqrtN must be prime!
		final long sqrtN = (long) Math.sqrt(N);
		final long maxPrime = Prime.isPrime(sqrtN) ? sqrtN : Prime
				.getNextPrime(sqrtN);
		long sum = 0L;

		long currentPrime = 2L;
		long currentPrimeSquare = 4L;
		long nextPrime = -1L;
		long nextPrimeSquare = -1L;

		//long bruteForceSum = bruteForce(N);

		while (nextPrime < maxPrime) {
			System.out.println(currentPrime);

			nextPrime = Prime.getNextPrime(currentPrime);
			nextPrimeSquare = nextPrime * nextPrime;

			sum += getSumForInterval(currentPrime, currentPrimeSquare,
					nextPrime, nextPrimeSquare);

			currentPrime = nextPrime;
			currentPrimeSquare = nextPrimeSquare;
		}

		System.out.println("RESULT = " + sum);
		//System.out.println("RESULT BRUTE = " + bruteForceSum);
	}

	private static long getSumForInterval(long a, long aSquare, long b,
			long bSquare) {
		long divisibleByA = (bSquare / a) - a;
		long divisibleByB = (b - 1L) - (aSquare / b);

		long ab = a * b;
		long divisibleByAB = (bSquare / ab) - (aSquare / ab);

		long sumOfDivisibleByA = (((aSquare + a) + ((bSquare / a) * a)) * divisibleByA) / 2L;
		long sumOfDivisibleByB = (((b * ((aSquare / b) + 1L)) + (bSquare - b)) * divisibleByB) / 2L;
		long sumOfDivisibleByAB = (((ab * ((aSquare / ab) + 1L)) + ((bSquare / ab) * ab)) * divisibleByAB) / 2L;

		return (sumOfDivisibleByA + sumOfDivisibleByB)
				- (2 * sumOfDivisibleByAB);
	}

	private static long bruteForce(long N) {
		long sum = 0L;

		for (long n = 4L; n <= N; n++) {
			long sqrtN = (long) Math.sqrt(n);
			if (Prime.isPrime(sqrtN) && MyMath.isSquareNumber(n)) {
				continue;
			}

			long a = Prime.getPreviousPrime(sqrtN + 1L);
			long b = Prime.getNextPrime(sqrtN);

			if ((((n % a) == 0L) && ((n % b) != 0L))
					|| (((n % b) == 0L) && ((n % a) != 0L))) {
				sum += n;
			}
		}

		return sum;
	}
}

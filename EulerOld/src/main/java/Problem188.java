

import java.math.BigInteger;

public class Problem188 {
	/*
	 * The hyperexponentiation or tetration of a number a by a positive integer b, 
	 * denoted by a↑↑b or ba, is recursively defined by:
	 * 
	 * a↑↑1 = a,
	 * a↑↑(k+1) = a(a↑↑k).
	 * 
	 * Thus we have e.g. 3↑↑2 = 33 = 27, hence 3↑↑3 = 327 = 7625597484987 
	 * and 3↑↑4 is roughly 103.6383346400240996*10^12.
	 * 
	 * Find the last 8 digits of 1777↑↑1855.
	 */

	/*SOLUTION
	 * 1777 is prime so 1777 and 10^8 are coprime thus 
	 * we can use Euler`s theorem:   (http://en.wikipedia.org/wiki/Euler%27s_theorem)
	 * a^phi(n) mod n = 1
	 * 
	 * So =>   1777^(40 000 000) mod 10^8 = 1
	 * 
	 * Process of solving
	 * 
	 * 1777^1777 mod 40 000 000 = x
	 * so 1777^(1777^1777) mod 10^8  = (1777^(something*40000000) * 1777^x) mod 10^8 =
	 * = 1777^x mod 10^8 = y
	 * 
	 * 1777^y mod 10^8 = 1777^(y mod 40000000) mod 10^8 = ...
	 */
	public static void main(final String[] args) {
		final int a = 1777;
		final BigInteger bigA = BigInteger.valueOf(a);
		final int b = 1855;

		final long totient = MyStuff.MyMath.getPhi((long) 1e8);
		final BigInteger modulo = BigInteger.valueOf(totient);

		BigInteger pow = BigInteger.valueOf(a);
		int limit = b - 1;
		for (int i = limit - 1; i >= 0; i--) {
			if (i > 0) {
				pow = bigA.modPow(pow, modulo);
			} else {
				pow = bigA.modPow(pow, BigInteger.valueOf((long) 1e8));
			}
		}

		System.out.println(pow);
	}
}

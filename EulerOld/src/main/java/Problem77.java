import java.util.Iterator;
import java.util.LinkedList;
import MyStuff.MyMath;

public class Problem77 {
	/*
	 * It is possible to write ten as the sum of primes in exactly five different ways:

		7 + 3
		5 + 5
		5 + 3 + 2
		3 + 3 + 2 + 2
		2 + 2 + 2 + 2 + 2

		What is the first value which can be written as the sum of primes in over five thousand different ways?
	 */
	
	public static void main(String[] args) {
		
		for(int i=10;  ;i++) {
			System.out.println(i);
			possibleWays = 0;
			primes = getPrimesLowerThan(i);
			for(int j=0; j<primes.length; j++) {
				LinkedList<Integer> values = new LinkedList<Integer>();
				values.add(primes[j]);
				searchNode(values, i - primes[j]);
			}
			
			if(possibleWays>5000) {
				System.out.println("SOLUTION = "+i);
				break;
			}
		}
		//System.out.print(possibleWays);
	}

	private static void searchNode(LinkedList<Integer> values, int sumLeft) {
		int[] primesPermittedInThisNode = getPrimesUpTo(values.getLast().intValue());
		
		for(int i=0; i<primesPermittedInThisNode.length; i++) {
			LinkedList<Integer> newValues = new LinkedList<Integer>(values);
			newValues.add(primesPermittedInThisNode[i]);
			
			int newSum = sumLeft - newValues.getLast();
			if(newSum<0)
				break;
			if(newSum==0) {
				possibleWays++;
				break;
			}
			
			searchNode(newValues, newSum);
		}
	}
	
	//private static int sumOf(LinkedList<Integer> v) {
	//	int sum = 0;
	//	for(int i : v)
	//		sum += i;
		
	//	return sum;
	//}
	
	private static int[] getPrimesLowerThan(int n) {
		LinkedList<Integer> primes = new LinkedList<Integer>();
		for(int i=2; i<n; i++)
			if(MyMath.isPrime(i))
				primes.add(i);
			
		int[] ret = new int[primes.size()];
		
		int k=0;
		for(int i : primes)
			ret[k++] = i;
		
		return ret;
	}
	
	private static int[] getPrimesUpTo(int n) {
		if(primes==null)
			throw new RuntimeException("Primes not initialized!!!");
		
		LinkedList<Integer> primesUpToN = new LinkedList<Integer>();
		
		for(int i=0; i<primes.length; i++) {
			int next = primes[i];
			if(next>n)
				break;
			
			primesUpToN.add(next);
		}
		
		int[] ret = new int[primesUpToN.size()];
		
		int k=0;
		for(int i : primesUpToN)
			ret[k++] = i;
		
		return ret;
	}
	
	private static int possibleWays = 0;
	private static int[] primes;
}

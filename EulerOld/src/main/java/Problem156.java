

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Problem156 {
	/*
	 * Starting from zero the natural numbers are written 
	 * down in base 10 like this:
	 * 
	 * 0 1 2 3 4 5 6 7 8 9 10 11 12....
	 * 
	 * Consider the digit d=1. After we write down each number n, 
	 * we will update the number of ones that have occurred and call this number f(n,1). 
	 * The first values for f(n,1), then, are as follows:
	 * 
	 * n	f(n,1)
	 * 0	0
	 * 1	1
	 * 2	1
	 * 3	1
	 * 4	1
	 * 5	1
	 * 6	1
	 * 7	1
	 * 8	1
	 * 9	1
	 * 10	2
	 * 11	4
	 * 12	5
	 * 
	 * Note that f(n,1) never equals 3.
	 * So the first two solutions of the equation f(n,1)=n are n=0 and n=1. 
	 * The next solution is n=199981.
	 * 
	 * In the same manner the function f(n,d) gives the total number of 
	 * digits d that have been written down after the number n has been written.
	 * 
	 * In fact, for every digit d != 0, 0 is the first solution of the equation f(n,d)=n.
	 * 
	 * Let s(d) be the sum of all the solutions for which f(n,d)=n.
	 * You are given that s(1)=22786974071.
	 * Find sum s(d) for 1 <= d <= 9.
	 * 
	 * Note: if, for some n, f(n,d)=n for more than one value of d 
	 * this value of n is counted again for every value of d for which f(n,d)=n.
	 */

	/*SOLUTION:
	 * By analysing f(n,1) I was able to deduce that:
	 * f(10^x - 1, d) = 10*f(10^(x-1) - 1, d) + 10^(x-1)
	 * Furthermore:
	 * f(10^x - 1, d) = x*10^(x-1)    (does not depend on d)
	 * 
	 * AND
	 * 
	 * f(122,d) = 122/100*f(99,d) + f(22,d) + X
	 * where X = 22+1 if d==122/100,  100 if d<122/100,  0 if d>122/100
	 * 
	 * f(22,d) = 22/10*f(9,d) + f(2,d) + Y
	 * where Y = 2+1 if d==22/10,  10 if d<22/10,  0 if d>22/10
	 * 
	 * f(2,d) = 1 if 2==d,  0 if 2!=d
	 * 
	 * Then we can say that:   [a(n) is the nth digit]
	 * f(a(n)a(n-1)a(n-2)...a(2)a(1), d) = 
	 * 		sum[i=2 n] (a(i)*(i-1)*10^(i-2)) + 
	 * 		f(a(1),d) +
	 * 		sum[i=2 n] (if a(i)==d then a(i-1)a(i-2)...a(2)a(1)) + 1
	 * 					if a(i)>d then 10^(n-1)
	 * 					if a(i)<d then 0)
	 * 
	 * However I thought that if:
	 * f(10^x - 1, d) = x*10^(x-1)
	 * then:
	 * f(n, d) > n  
	 * x*10^(x-1) > 10^x - 1    => x>=10
	 * 
	 * and 10 000 000 000 is the upper bound i need to look in.
	 * 
	 * My thoughts were wrong but i noticed that if I found solution 
	 * f(n, d) X  for n<=10 000 000 000 it repeats every 10 000 000 000  up to (not including)
	 * d*10 000 000 000.
	 * 
	 * So if f(28263827, 2) = 28263827    then f(10028263827, 2) = 10028263827
	 * and if f(371599983, 3) = 371599983   then f(10371599983, 3) = 10371599983
	 * 										and f(20371599983, 3) = 20371599983
	 */
	public static void main(String[] args) {
		long[] digitOccurences = new long[10];
		long[] s = new long[10];

		List<List<Long>> values = getInitialValues();
		long limit = (long) 1e10;

		for (long i = 1L; i < limit; i++) {
			if ((i % 10000000L) == 0L) {
				System.out.println(i);
			}

			long n = i;
			while (n > 0L) {
				int digit = (int) (n % 10L);
				digitOccurences[digit]++;
				n = n / 10L;
			}

			for (int j = 1; j < 10; j++) {
				if (digitOccurences[j] == i) {
					s[j] += i;
					values.get(j).add(i);
				}
			}
		}

		long sum = 0L;
		for (int i = 1; i < 10; i++) {
			sum += s[i];

			for (int j = 1; j <= (i - 1); j++) {
				sum += (values.get(i).size() + 1) * j * 10000000000L;
				sum += s[i];
			}
		}

		System.out.println("RESULT = " + sum);
	}

	private static List<List<Long>> getInitialValues() {
		List<List<Long>> values = new ArrayList<List<Long>>(10);
		values.add(null);
		for (int i = 1; i < 10; i++) {
			values.add(new LinkedList<Long>());
		}

		return values;
	}
}


public class Problem46 
{
	/*
	 * It was proposed by Christian Goldbach that every odd composite number 
	 * can be written as the sum of a prime and twice a square.
	 * 
	 * 9 = 7 + 2�1^2
	 * 15 = 7 + 2�2^2
	 * 21 = 3 + 2�3^2
	 * 25 = 7 + 2�3^2
	 * 27 = 19 + 2�2^2
	 * 33 = 31 + 2�1^2
	 * 
	 * It turns out that the conjecture was false.
	 * 
	 * What is the smallest odd composite that cannot be written 
	 * as the sum of a prime and twice a square?	 */
	
	public static void main(String[] args) 
	{		
		for(int i=0; i<twiceAsquares.length; i++)
			twiceAsquares[i] = (long) (i*i*2);
		
		long smallest = 0L;
		for(long i=35L; ;i+=2)
		{
			if(!MyMath.isPrime(i) && !canBeWrittenAsSumOfPrimeAndTwiceaSquare(i))
			{
				smallest = i;
				break;
			}
		}
		
		System.out.println(smallest);
	}
	
	public static boolean canBeWrittenAsSumOfPrimeAndTwiceaSquare(long v)
	{
		if(v==137)
		{
			int xx = 0;
		}
		long largestPrimeLowerThanV = MyMath.getLargestPrimeLowerThan(v);
		while(largestPrimeLowerThanV!=-1L)
		{
			long tempV = v - largestPrimeLowerThanV;
			
			for(long i=1; ;i++)
			{
				long temp = tempV - 2L*i*i;
				if(temp>0)
					continue;
				else if(temp<0)
					break;
				else //if(temp==0)
					return true;
			}
			
			largestPrimeLowerThanV = MyMath.getLargestPrimeLowerThan(largestPrimeLowerThanV);
		}
		return false;
	}
	
	private static long[] twiceAsquares = new long[1000]; 
}

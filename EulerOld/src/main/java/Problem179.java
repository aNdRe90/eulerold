

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import MyStuff.PrimeGenerator;

public class Problem179 {
	/*
	 * Find the number of integers 1 < n < 10^7, for which n and n + 1 
	 * have the same number of positive divisors. 
	 * 
	 * For example, 14 has the positive divisors 1, 2, 7, 14 
	 * while 15 has 1, 3, 5, 15.
	 */
	private static final long N = (long) 1e7;
	private static final long sqrtN = (long) Math.sqrt(N);
	private static final List<Long> primes = loadPrimesUpTo(sqrtN);

	public static void main(final String[] args) {

		int sum = 0;

		int currentDivs = 2;
		for (long i = 3; i < N; i++) {
			if ((i % 1024) == 0) {
				System.out.println(i);
			}
			int nextDivs = getNumberOfDivisors(i);

			if (currentDivs == nextDivs) {
				sum++;
			}

			currentDivs = nextDivs;
		}

		//int[] divisors = new int[(int) N];//getEmptyListOfSize(N);
		//		for (long prime : primes) {
		//			System.out.println(prime);
		//			long temp = prime;
		//
		//			while (true) {
		//				if (temp <= sqrtN) {
		//					divisors[(int) temp] += 2;
		//					if (temp == sqrtN) {
		//						divisors[(int) temp]--;
		//					}
		//				} else {
		//					break;
		//				}
		//
		//				if (prime == 1L) {
		//					break;
		//				}
		//				temp += prime;
		//			}
		//		}

		//		System.out.println("HIHI = " + divisors[454238]);
		//		int currentNumOfDivs = divisors[1];//divisors.get(0);
		//		for (int i = 2; i < N; i++) {
		//			//System.out.println(i + " = " + divisors[i]);
		//			int nextNumOfDivs = divisors[i];//divisors.get(i);
		//			if (currentNumOfDivs == nextNumOfDivs) {
		//				sum++;
		//			}
		//
		//			currentNumOfDivs = nextNumOfDivs;
		//		}

		System.out.println(sum);
	}

	private static int getNumberOfDivisors(final long i) {
		Set<Long> nextDivs = new HashSet<Long>();
		long sqrt = (long) Math.sqrt(i);

		for (long prime : primes) {
			long temp = prime;

			while (temp <= sqrt) {
				if (((i % temp) == 0)) {
					long a = i / temp;
					long b = i / a;
					nextDivs.add(a);
					nextDivs.add(b);
				}

				temp += prime;
			}
		}

		return nextDivs.size() + 2;
	}

	private static List<Long> loadPrimesUpTo(final long limit) {
		List<Long> primes = new LinkedList<Long>();
		PrimeGenerator.loadPrimes("primes1e0_1e9.txt", primes, limit);

		return primes;
	}
}



import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;

import MyStuff.Fibonacci;

public class Problem104 {
	/*
	 * The Fibonacci sequence is defined by the recurrence relation:
	 * 
	 *     F(n) = F(n-1) + F(n-2), where F(1) = 1 and F(2) = 1.
	 *     
	 *     It turns out that F(541), which contains 113 digits, is the first Fibonacci number 
	 *     for which the last nine digits are 1-9 pandigital (contain all the digits 1 to 9, 
	 *     but not necessarily in order). And F(2749), which contains 575 digits, 
	 *     is the first Fibonacci number for which the first nine digits are 1-9 pandigital.
	 *     
	 *     Given that F(k) is the first Fibonacci number for which the first nine digits 
	 *     AND the last nine digits are 1-9 pandigital, find k.
	 */
	public static void main(String[] args) {
		BigDecimal a = new BigDecimal("1.618033988749894848204586834365638117720309179");
		BigDecimal invertedSqrt5 = new BigDecimal("0.447213595499957939281834733746255247088123671");
		double logA = Math.log10( ( 1.0+Math.sqrt(5.0) ) /2.0);
		double temp = 0.5*Math.log10(5.0);
		
		int prevA = 1;
		int prevB = 1;
		int next = -1;
		
		for(int n=3; ;n++) {
			
			next = getLast9DigitsOf(prevA + prevB);
			prevA = prevB;
			prevB = next;
			
			//MY SOLUTION:
			if(n>39L) {  //F(40) is the first to have at least 9 digits
				if(MyStuff.MyMath.isPandigital(next)) {
					System.out.println(n);
					
					//MY SOLUTION:---------------------------------------------
					/*BigDecimal fibonacciRounded = 
									invertedSqrt5.multiply(a.pow(n, MathContext.DECIMAL64));
					
					String fibonnaciRoundedString = fibonacciRounded.toBigInteger().toString();
					
					if(hasFirst9DigitsPandigital(fibonnaciRoundedString)) {
						System.out.println("RESULT="+n);
						break;
					}	*/
					//---------------------------------------------------------
					//AFTER SOLVING - FROM EULER THREAD FORUM SOLUTION (FASTER, CLEVER):
					//a^n / sqrt(5) = F(n)    =>  log10(a^n / sqrt(5)) = log10(F(n)) = x
					//F(n) = 10^x
					//n*log10(a) - 0.5*log10(5) = x
					//F(n) = 10^(a.b)      a decides only about the position of starting digits so taking 10^((0.b)+k)
					//														we get first k digits of F(n)
					
					
					double xTemp = logA*(double)n - temp;
					double x = (xTemp - (int)xTemp);
					long fibRoundedStart = (long) (Math.pow(10.0, x)*100000000.0);
					if(MyStuff.MyMath.isPandigital(fibRoundedStart)) {
						System.out.println("RESULT="+n);
						break;
					}
					//---------------------------------------------------------
				}
			}
		}
	}

	private static int getLast9DigitsOf(int v) {
		String vString = Integer.toString(v);
		
		if(vString.length()<10)
			return v;
		
		int length = vString.length();
		String last9DigitsString = vString.substring(length-9, length);
		return Integer.parseInt(last9DigitsString);
	}

	private static boolean hasFirst9DigitsPandigital(String nextString) {
		long number = Long.parseLong(nextString.substring(0, 9));
		return MyStuff.MyMath.isPandigital(number);
	}
	
	private static boolean hasFirst9DigitsPandigital(long number) {
		return MyStuff.MyMath.isPandigital(number);
	}
}

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;


public class Problem84 {
	/*
	 * In the game, Monopoly, the standard board is set up in the following way:
	 * 
	 *  GO 	A1 	CC1 A2 	T1 	R1 	B1 	CH1 B2 	B3 	JAIL
		H2 	  									C1
		T2 	  									U1
		H1 	  									C2
		CH3 	  								C3
		R4 	  									R2
		G3 	  									D1
		CC3 	  								CC2
		G2 	  									D2
		G1 	  									D3
		G2J F3 	U2 	F2 	F1 	R3 	E3 	E2 	CH2 E1 	FP
		
	A player starts on the GO square and adds the scores on two 6-sided dice to determine 
	the number of squares they advance in a clockwise direction. 
	Without any further rules we would expect to visit each square with equal probability: 2.5%. 
	However, landing on G2J (Go To Jail), CC (community chest), and CH (chance) changes this distribution.
	
	In addition to G2J, and one card from each of CC and CH, that orders the player to go directly to jail, 
	if a player rolls three consecutive doubles, they do not advance the result of their 3rd roll. 
	Instead they proceed directly to jail.
	
	At the beginning of the game, the CC and CH cards are shuffled. 
	When a player lands on CC or CH they take a card from the top of the respective pile and, 
	after following the instructions, it is returned to the bottom of the pile. 
	There are sixteen cards in each pile, but for the purpose of this problem 
	we are only concerned with cards that order a movement; any instruction not concerned with movement 
	will be ignored and the player will remain on the CC/CH square.

    Community Chest (2/16 cards):
        Advance to GO
        Go to JAIL
    Chance (10/16 cards):
        Advance to GO
        Go to JAIL
        Go to C1
        Go to E3
        Go to H2
        Go to R1
        Go to next R (railway company)
        Go to next R
        Go to next U (utility company)
        Go back 3 squares.

	The heart of this problem concerns the likelihood of visiting a particular square. 
	That is, the probability of finishing at that square after a roll. 
	For this reason it should be clear that, with the exception of G2J for which the probability of finishing 
	on it is zero, the CH squares will have the lowest probabilities, as 5/8 request a movement to another square, 
	and it is the final square that the player finishes at on each roll that we are interested in. 
	We shall make no distinction between "Just Visiting" and being sent to JAIL, 
	and we shall also ignore the rule about requiring a double to "get out of jail", 
	assuming that they pay to get out on their next turn.

	By starting at GO and numbering the squares sequentially from 00 to 39 we can concatenate these two-digit numbers 
	to produce strings that correspond with sets of squares.

	Statistically it can be shown that the three most popular squares, 
	in order, are JAIL (6.24%) = Square 10, E3 (3.18%) = Square 24, and GO (3.09%) = Square 00. 
	So these three most popular squares can be listed with the six-digit modal string: 102400.

	If, instead of using two 6-sided dice, two 4-sided dice are used, find the six-digit modal string.
	 */
	
	public static void main(String[] args) {		
		initializeBoard();
		initializeCommunityChest();
		initializeChance();
		
		final int N = 100000000;
		for(int i=0; i<N; i++) {
			if(i%10000==0)
				System.out.println(i);
			move();
		}
		
		int[] temp = getThreeMostVisitedFields();
		System.out.println(Arrays.toString(temp));
	}
	
	private static void initializeCommunityChest() {
		for(int i=0; i<14; i++)
			communityChest.add(-1);
		
		communityChest.add(0); //GO
		communityChest.add(10); //JAIL
		
		Collections.shuffle(communityChest);
	}
	
	private static void initializeChance() {
		for(int i=0; i<6; i++)
			chance.add(-1);
		
		chance.add(0); //GO
		chance.add(10); //JAIL
		chance.add(11); //C1
		chance.add(24); //E3
		chance.add(39); //H2
		chance.add(5); //R1
		
		chance.add(-2); //go to next R
		chance.add(-2); //go to next R
		chance.add(-3); //go to next U
		chance.add(-4); //go back 3 steps
		
		Collections.shuffle(chance);
	}
	
	private static void goToNextR() {
		for(int i= (currentPosition+1); ;i++){
			if(i>39)
				i -= 40;
			
			if(board[i].charAt(0)=='R') {
				currentPosition = i;
				break;
			}
		}
	}
	
	private static void goToNextU() {
		for(int i= (currentPosition+1); ;i++){
			if(i>39)
				i -= 40;
			
			if(board[i].charAt(0)=='U') {
				currentPosition = i;
				break;
			}
		}
	}
	
	private static void initializeBoard() {
		currentPosition = 0;
		//updateStatistics();
		
		board[0] = "GO";
		board[1] = "A1";
		board[2] = "CC1";
		board[3] = "A2";
		board[4] = "T1";
		board[5] = "R1";
		board[6] = "B1";
		board[7] = "CH1";
		board[8] = "B2";
		board[9] = "B3";
		
		board[10] = "JAIL";
		board[11] = "C1";
		board[12] = "U1";
		board[13] = "C2";
		board[14] = "C3";
		board[15] = "R2";
		board[16] = "D1";
		board[17] = "CC2";
		board[18] = "D2";
		board[19] = "D3";
		
		board[20] = "FP";
		board[21] = "E1";
		board[22] = "CH2";
		board[23] = "E2";
		board[24] = "E3";
		board[25] = "R3";
		board[26] = "F1";
		board[27] = "F2";
		board[28] = "U2";
		board[29] = "F3";
		
		board[30] = "G2J";
		board[31] = "G1";
		board[32] = "G2";
		board[33] = "CC3";
		board[34] = "G3";
		board[35] = "R4";
		board[36] = "CH3";
		board[37] = "H1";
		board[38] = "T2";
		board[39] = "H2";
	}
	
	private static void move() {
		currentPosition = nextPosition();
		
		if(consecutiveDoubles==3) {
			consecutiveDoubles = 0;
			currentPosition = 10; //JAIL
		}
		else
			handleCurrentField();
		
		updateStatistics();
	}
	
	private static int[] getThreeMostVisitedFields() {
		int[] fields = new int[3];
		
		for(int j=0; j<3; j++) {
			
			int max = boardFieldsVisited[0];
			int maxIndex = 0;
			for(int i=1; i<40; i++)
				if(boardFieldsVisited[i]>max) {
					max = boardFieldsVisited[i];
					maxIndex = i;
				}
			fields[j] = maxIndex;
			boardFieldsVisited[maxIndex] = Integer.MIN_VALUE;
		}		
		
		return fields;
	}
	
	private static void handleCurrentField() {		
		String field = board[currentPosition];
		if(isCommunityChestField(field)) {
			int card = getNextCommunityChestCard();
			if(card!=-1)
				currentPosition = card;
		}
		
		else if(isChanceField(field)) {
			int card = getNextChanceCard();
			if(card>-1)
				currentPosition = card;
			else if(card==-2)
				goToNextR();
			else if(card==-3)
				goToNextU();
			else if(card==-4) {
				currentPosition -= 3;
				if(currentPosition<0)
					currentPosition = 40 + currentPosition;
			}
			else if(card==-1) {
				
			}
			else
				throw new RuntimeException("CHANCE CARD ERROR!");
		}
		
		else if(isGoToJailField(field)) {
			currentPosition = 10;
		}
	}
	
	private static int getNextCommunityChestCard() {
		int newCard = communityChest.removeFirst();
		communityChest.add(newCard);
		
		return newCard;
	}
	
	private static int getNextChanceCard() {
		int newCard = chance.removeFirst();
		chance.add(newCard);
		
		return newCard;
	}
	
	private static boolean isCommunityChestField(String field) {
		if(field.charAt(0)=='C' && field.charAt(1)=='C')
			return true;
		else
			return false;
	}
	
	private static boolean isChanceField(String field) {
		if(field.charAt(0)=='C' && field.charAt(1)=='H')
			return true;
		else
			return false;
	}
	
	private static boolean isGoToJailField(String field) {
		if(field.equals("G2J"))
			return true;
		else
			return false;
	}
	
	private static void updateStatistics() {
		boardFieldsVisited[currentPosition]++;
	}
	
	private static int nextPosition() {
		int newPosition = currentPosition+rollDices();
		if(newPosition>39)
			newPosition -= 40;
		
		return newPosition;
	}
	
	private static int rollDices() {
		int firstDice = (rand.nextInt(diceSides)+1);
		int secondDice = (rand.nextInt(diceSides)+1);
		int result = firstDice + secondDice;
		
		if(firstDice==secondDice)
			consecutiveDoubles++;
		else
			consecutiveDoubles = 0;
		
		return result;
	}
	
	private static final int diceSides = 4;
	private static String[] board = new String[40];
	private static int[] boardFieldsVisited = new int[40];
	private static int currentPosition;
	private static Random rand = new Random();
	private static LinkedList<Integer> communityChest = new LinkedList<Integer>();
	private static LinkedList<Integer> chance = new LinkedList<Integer>();
	private static int consecutiveDoubles = 0;
}




public class Problem164 {
	/*
	 * How many 20 digit numbers n (without any leading zero) exist such that 
	 * no three consecutive digits of n have a sum greater than 9?
	 */
	private static final int N = 20;
	private static final Long[][][] memory = new Long[N + 1][10][10];

	public static void main(final String[] args) {
		System.out.println(getResultFor(N, 0, 0) - getResultFor(N - 1, 0, 0));
	}

	private static long getResultFor(final int length, final int a, final int b) {
		if (memory[length][a][b] != null) {
			return memory[length][a][b];
		}

		long result = 0L;
		int limit = 10 - a - b;
		if (length == 1) {
			return limit;
		}

		for (int i = 0; i < limit; i++) {
			result += getResultFor(length - 1, b, i);
		}

		memory[length][a][b] = result;
		return result;
	}

	private static long getBruteForceResult(int n) {
		long result = 0L;

		while (n >= 0) {
			result += getBruteForceResultForLength(n);
			n--;
		}

		return result;
	}

	private static long getBruteForceResultForLength(final int n) {
		long limit = (long) Math.pow(10.0, n);
		long start = limit / 10L;
		long result = 0L;

		for (long i = start; i < limit; i++) {
			String s = Long.toString(i);
			if (isNumberValid(s)) {
				result++;
			}
		}

		return result;
	}

	private static boolean isNumberValid(final String s) {
		int size = s.length();
		for (int i = 0; i < size; i++) {
			int max = Math.min(i + 2, size - 1);
			int sum = 0;

			for (int j = i; j <= max; j++) {
				sum += (s.charAt(j) - '0');
			}

			if (sum > 9) {
				return false;
			}
		}

		return true;
	}
}

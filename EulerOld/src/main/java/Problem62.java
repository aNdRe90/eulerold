import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

import MyStuff.*;


public class Problem62
{
	/*
	 * The cube, 41063625 (345^3), can be permuted to produce two other cubes: 
	 * 56623104 (384^3) and 66430125 (405^3). 
	 * In fact, 41063625 is the smallest cube which has 
	 * exactly three permutations of its digits which are also cube.
	 * 
	 * Find the smallest cube for which exactly five permutations of its digits are cube.
	 */
	public static void main(String[] args) 
	{
		int maxDigitsInCube = 13;
		cubes = new ArrayList<ArrayList<BigInteger>>(maxDigitsInCube-2);
		
		for(int i=0; i<(maxDigitsInCube-2);i++)
			cubes.add(new ArrayList<BigInteger>(1000));
		
		//ASSUMING THAT RESULT WOULD BE MAX 8-digit-----------------------------
		
		//GENERATE CUBES OF LENGTH x IN cubes.get(x) ARRAYLIST
		for(int i=5; ; i++)   //1420
		{
			BigInteger current = new BigInteger(Integer.toString(i)).pow(3);
			int length = current.toString().length();
			if(length>maxDigitsInCube)
				break;
			
			cubes.get(length-3).add(current);
		}
		//-----------------------------------------------------------------------
		
		BigInteger smallest = null;
		for(int i=0; i<cubes.size(); i++)
		{
			ArrayList<BigInteger> temp = cubes.get(i);
			for(int j=0; j<(temp.size()-1); j++)
			{
				if(getNumberOfCubesByPermutating(temp.get(j), j+1)==5)
				{
					smallest = new BigInteger(temp.get(j).toString());
					i = cubes.size();
					break;
				}
				
			}
		}
		
		System.out.println(smallest.toString());
	}
	
	public static boolean isCube(BigInteger b)
	{
		double bd = b.doubleValue();
		double bdRoot3 = Math.pow(bd, 1.0/3.0);
		
		double difference = (Math.ceil(bdRoot3)-bdRoot3);
		if(difference>0.000000000001)
		{
			difference = (bdRoot3-Math.floor(bdRoot3));
			if(difference>0.000000000001)
				return false;
			else
				return true;
		}
		else
			return true;
	}
	
	public static int getNumberOfCubesByPermutating(BigInteger b, int startIndex)
	{
		int cubesNumber = 1;
		
		int length = b.toString().length() - 3;
		ArrayList<BigInteger> temp = cubes.get(length);
		
		for(int i=startIndex; i<temp.size(); i++)
		{
			if(isCreatedByPermutating(b, temp.get(i)))
				cubesNumber++;
		}
		
		return cubesNumber;
	}
	
	public static boolean isCreatedByPermutating(BigInteger a, BigInteger b)
	{
		String aS = a.toString();
		String bS = b.toString();
		
		if(aS.length()!=bS.length())
			return false;
		
		LinkedList<Character> aL = new LinkedList<Character>();
		LinkedList<Character> bL = new LinkedList<Character>();
		
		int length = aS.length();
		for(int i=0; i<length; i++)
		{
			aL.add(new Character(aS.charAt(i)));
			bL.add(new Character(bS.charAt(i)));
		}
		
		Collections.sort(aL);
		Collections.sort(bL);
		
		Iterator<Character> iterA = aL.iterator();
		Iterator<Character> iterB = bL.iterator();
		int counter = 0;
		while(iterA.hasNext())
		{
			if(iterA.next().equals(iterB.next()))
				counter++;
		}
		
		if(counter==length)
			return true;
		else
			return false;
	}
	
	public static int getCubePermutations(BigInteger b)
	{
		HashSet<BigInteger> cubes = new HashSet<BigInteger>(5);
		StringPermutationGenerator sGen = new StringPermutationGenerator(b.toString());
		String next = null;
		while(sGen.hasMore())
		{
			next = sGen.getNext();
			if(next.charAt(0)=='0')
				continue;
			if(isCube(new BigInteger(next)))
			{
				cubes.add(new BigInteger(next));
				if(cubes.size()>5)
					break;
			}
		}
		
		return cubes.size();
	}
	
	private static ArrayList<ArrayList<BigInteger>> cubes = null;
}

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.StringTokenizer;


public class Problem42 
{
	/*
	 * The nth term of the sequence of triangle numbers is given by, t(n) = 0.5*n(n+1); 
	 * so the first ten triangle numbers are:
	 * 
	 * 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...
	 * 
	 * By converting each letter in a word to a number corresponding to its 
	 * alphabetical position and adding these values we form a word value. 
	 * 
	 * For example, the word value for SKY is 19 + 11 + 25 = 55 = t(10). 
	 * If the word value is a triangle number then we shall call the word a triangle word.
	 * 
	 * Using words.txt (right click and 'Save Link/Target As...'), 
	 * a 16K text file containing nearly two-thousand common English words, 
	 * how many are triangle words?	 */
	
	
	/*
	 * SOLUTION:
	 * if [ t(n) = x ]
	 * 		then => 2x = n^2+n
	 * 			 => n^2 + n - 2x = 0
	 * 			 => delta = 1+8x
	 * 	
	 * So x is the triangle number if sqrt(1+8x) is natural number.
	 * Moreover, it is the [ 0.5*(sqrt(1+8x)-1) ]th  triangle number.
	 */
	public static void main(String[] args) 
	{
		readWords();
		
		Iterator<String> iter = words.iterator();
		
		int triangleWords = 0;
		while(iter.hasNext())
		{
			String temp = iter.next();
			int size = temp.length();
			int wordSum = 0;
			for(int j=0; j<size; j++)
				wordSum += (temp.charAt(j) - 0x40);				

			if(MyMath.isTriangleNumber(wordSum))
				triangleWords++;
		}

		System.out.println(triangleWords);
	}	
	
	
	
	public static void readWords()
	{
		BufferedReader f = null;
		
		try
		{
			f = new BufferedReader(new FileReader("words.txt"));
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
		String line = null;
		do
		{
			try 
			{
				line = f.readLine();
			} 
			catch(IOException ee) { ee.printStackTrace(); }
			
			if(line==null)
				break;
			
			StringTokenizer st = new StringTokenizer(line, ",");
			while(st.hasMoreTokens())
			{
				String temp = st.nextToken();
				words.add(temp.substring(1, temp.length()-1));
			}				
		}
		while(true);
	}
	private static LinkedList<String> words = new LinkedList<String>();
}

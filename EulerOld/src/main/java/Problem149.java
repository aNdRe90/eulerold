

public class Problem149 {
	/*
	 * Looking at the table below, it is easy to verify that the 
	 * maximum possible sum of adjacent numbers in any direction 
	 * (horizontal, vertical, diagonal or anti-diagonal) is 16 (= 8 + 7 + 1).
	 * 
	 * -2	5	3	2
	 * 9	-6	5	1
	 * 3	2	7	3
	 * -1	8	-4	  8
	 * 
	 * Now, let us repeat the search, but on a much larger scale:
	 * 
	 * First, generate four million pseudo-random numbers using a specific 
	 * form of what is known as a "Lagged Fibonacci Generator":
	 * 
	 * For 1 <= k <= 55, 
	 * sk = [100003 - 200003k + 300007k^3] (modulo 1000000) - 500000.
	 * 
	 * For 56 <= k <= 4000000, 
	 * sk = [sk-24 + sk-55 + 1000000] (modulo 1000000) - 500000.
	 * 
	 * Thus, s10 = -393027 and s100 = 86613.
	 * 
	 * The terms of s are then arranged in a 2000x2000 table, 
	 * using the first 2000 numbers to fill the first row (sequentially), 
	 * the next 2000 numbers to fill the second row, and so on.
	 * 
	 * Finally, find the greatest sum of (any number of) adjacent entries 
	 * in any direction (horizontal, vertical, diagonal or anti-diagonal).
	 */

	private static long[][] matrixTest = { new long[] { -2L, 5L, 3L, 2L },
			new long[] { 9L, -6L, 5L, 1L }, new long[] { 3L, 2L, 7L, 3L },
			new long[] { -1L, 8L, -4L, 8L } };
	private static long[][] matrix = getMatrix();

	private static final int n = 2000;

	public static void main(String[] args) {
		long result = getResult(matrix);
		System.out.println("RESULT = " + result);
	}

	private static long getResult(long[][] matrix) {
		long[][] verticalDP = getVerticalDPValues(matrix);
		long[][] horizontalDP = getHorizontalDPValues(matrix);

		long[][] diagonalDP = getDiagonalDPValues(matrix);
		long[][] antidiagonalDP = getAntidiagonalDPValues(matrix);

		long maxVertical = getMax(verticalDP);
		long maxHorizontal = getMax(horizontalDP);
		long maxDiagonal = getMax(diagonalDP);
		long maxAntidiagonal = getMax(antidiagonalDP);

		return getMax(maxVertical, maxHorizontal, maxDiagonal, maxAntidiagonal);
	}

	private static long getMax(long a, long b, long c, long d) {
		long ab = a > b ? a : b;
		long cd = c > d ? c : d;

		return ab > cd ? ab : cd;
	}

	private static long getMax(long[][] values) {
		long max = Long.MIN_VALUE;

		for (long[] v : values) {
			for (long vv : v) {
				if (vv > max) {
					max = vv;
				}
			}
		}

		return max;
	}

	private static long[][] getAntidiagonalDPValues(long[][] matrix) {
		long[][] values = new long[n][n];

		values[0][0] = matrix[0][0];
		values[n - 1][n - 1] = matrix[n - 1][n - 1];

		for (int column = 1; column < n; column++) {
			values[0][column] = matrix[0][column];

			for (int i = 1; i < n; i++) {
				if ((column - i) < 0) {
					break;
				}

				values[i][column - i] = values[i - 1][(column - i) + 1] <= 0 ? matrix[i][column
						- i]
						: matrix[i][column - i]
								+ values[i - 1][(column - i) + 1];
			}
		}

		for (int row = 1; row < n; row++) {
			values[row][n - 1] = matrix[row][n - 1];

			for (int i = 1; i < n; i++) {
				if ((row + i) >= n) {
					break;
				}

				values[row + i][(n - 1) - i] = values[(row + i) - 1][((n - 1) - i) + 1] <= 0 ? matrix[row
						+ i][(n - 1) - i]
						: matrix[row + i][(n - 1) - i]
								+ values[(row + i) - 1][((n - 1) - i) + 1];
			}
		}

		return values;
	}

	private static long[][] getDiagonalDPValues(long[][] matrix) {
		long[][] values = new long[n][n];

		values[0][n - 1] = matrix[0][n - 1];
		values[n - 1][0] = matrix[n - 1][0];

		for (int column = n - 2; column >= 0; column--) {
			values[0][column] = matrix[0][column];

			for (int i = 1; i < n; i++) {
				if ((column + i) >= n) {
					break;
				}

				values[i][column + i] = values[i - 1][(column + i) - 1] <= 0 ? matrix[i][column
						+ i]
						: matrix[i][column + i]
								+ values[i - 1][(column + i) - 1];
			}
		}

		for (int row = 1; row < n; row++) {
			values[row][0] = matrix[row][0];

			for (int i = 1; i < n; i++) {
				if ((row + i) >= n) {
					break;
				}

				values[row + i][i] = values[(row + i) - 1][i - 1] <= 0 ? matrix[row
						+ i][i]
						: matrix[row + i][i] + values[(row + i) - 1][i - 1];
			}
		}

		return values;
	}

	private static long[][] getHorizontalDPValues(long[][] matrix) {
		long[][] values = new long[n][n];

		for (int row = 0; row < n; row++) {
			values[row][n - 1] = matrix[row][n - 1];

			for (int column = n - 2; column >= 0; column--) {
				values[row][column] = values[row][column + 1] <= 0 ? matrix[row][column]
						: matrix[row][column] + values[row][column + 1];
			}
		}

		return values;
	}

	private static long[][] getVerticalDPValues(long[][] matrix) {
		long[][] values = new long[n][n];

		for (int column = 0; column < n; column++) {
			values[0][column] = matrix[0][column];

			for (int row = 1; row < n; row++) {
				values[row][column] = values[row - 1][column] <= 0 ? matrix[row][column]
						: matrix[row][column] + values[row - 1][column];
			}
		}

		return values;
	}

	private static long[][] getMatrix() {
		final long N = 4000000L;

		long[][] matrix = new long[n][n];

		for (long index = 1L; index <= 55L; index++) {
			long value = (((100003L - (200003L * index)) + (300007L * index
					* index * index)) % 1000000L) - 500000L;
			matrix[getRow(index - 1)][getColumn(index - 1)] = value;
		}

		for (long index = 56L; index <= N; index++) {
			long value = ((matrix[getRow(index - 25)][getColumn(index - 25)]
					+ matrix[getRow(index - 56)][getColumn(index - 56)] + 1000000L) % 1000000L) - 500000L;
			matrix[getRow(index - 1)][getColumn(index - 1)] = value;
		}

		return matrix;
	}

	private static int getColumn(long index) {
		return (int) (index % n);
	}

	private static int getRow(long index) {
		return (int) (index / n);
	}
}

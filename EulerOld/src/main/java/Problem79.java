import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.StringTokenizer;


public class Problem79 
{
	/*
	 * A common security method used for online banking is to ask the user for three random characters from a passcode. 
	 * For example, if the passcode was 531278, 
	 * they may ask for the 2nd, 3rd, and 5th characters; the expected reply would be: 317.
	 * 
	 * The text file, keylog.txt, contains fifty successful login attempts.
	 * 
	 * Given that the three characters are always asked for in order, 
	 * analyse the file so as to determine the shortest possible secret passcode of unknown length.
	 */
	
	//WORKS ONLY WHEN NO REPEATS OF DIGITS
	public static void main(String[] args) 
	{
		attempts = readAttempts("keylog.txt");
		passDigits = new HashSet<Character>(attempts.size()*4);
		
		for(int i=0; i<attempts.size(); i++)
		{
			String s = Integer.toString(attempts.get(i).intValue());
			for(int j=0; j<s.length(); j++)
				passDigits.add(new Character(s.charAt(j)));
		}
		
		//digitsFollowing.get(x) is the set of digits after the x in the password
		ArrayList<HashSet<Character>> digitsFollowing = new ArrayList<HashSet<Character>>(10);
		for(int i=0; i<10; i++)
			digitsFollowing.add(new HashSet<Character>(10));
		
		Iterator<Character> iter = passDigits.iterator();
		while(iter.hasNext())
		{
			char digit = iter.next().charValue();
			
			for(int i=0; i<attempts.size(); i++)
			{
				String attempt = Integer.toString(attempts.get(i).intValue());
				boolean startAdding = false;
				for(int j=0; j<attempt.length(); j++)
				{
					if(startAdding)
						digitsFollowing.get(digit-0x30).add(new Character(attempt.charAt(j)));
					else if(attempt.charAt(j)==digit)
						startAdding = true;
				}
			}
		}
		
		iter = passDigits.iterator();
		while(iter.hasNext())
		{
			char next = iter.next().charValue();
			System.out.print("DIGITS FOLLOWING \""+next+"\" = ");
			HashSet<Character> set = digitsFollowing.get(next-0x30);
			Iterator<Character> iter2 = set.iterator();
			while(iter2.hasNext())
				System.out.print(iter2.next().charValue()+", ");
			
			System.out.println();
		}
		
		//GET THE PASSWORD FROM THE OUTPUT
		//PASS IS 73162890
	}
	
	private static ArrayList<Integer> readAttempts(String filename)
	{
		BufferedReader f = null;		
		try	{	f = new BufferedReader(new FileReader(filename));	}
		catch(IOException e)	{	e.printStackTrace();		}
		
		String line = null;			
		ArrayList<Integer> attempts = new ArrayList<Integer>();
		do
		{
			try {	line = f.readLine();	} 
			catch(IOException ee) { ee.printStackTrace(); }
			
			if(line==null)
				break;
			
			StringTokenizer st = new StringTokenizer(line, ",");
			while(st.hasMoreTokens())
				attempts.add(new Integer(Integer.parseInt(st.nextToken())));
		}
		while(true);
		
		return attempts;
	}
	
	private static ArrayList<Integer> attempts;
	private static HashSet<Character> passDigits;
}

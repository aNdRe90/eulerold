

import MyStuff.CombinationGenerator;

public class Problem121 {
	/*
	 * A bag contains one red disc and one blue disc. In a game of chance 
	 * a player takes a disc at random and its colour is noted. 
	 * After each turn the disc is returned to the bag, an extra red disc is added, 
	 * and another disc is taken at random.
	 * 
	 * The player pays pound1 to play and wins if they have taken more blue discs than red discs 
	 * at the end of the game.
	 * 
	 * If the game is played for four turns, the probability of a player winning 
	 * is exactly 11/120, and so the maximum prize fund the banker should allocate 
	 * for winning in this game would be pound10 before they would expect to incur a loss. 
	 * Note that any payout will be a whole number of pounds and also includes 
	 * the original pound1 paid to play the game, so in the example given the player 
	 * actually wins pound9.
	 * 
	 * Find the maximum prize fund that should be allocated to a single game 
	 * in which fifteen turns are played.
	 */
	
	/*
	 * SOLUTION:
	 * Just analyze probability tree.
	 */
	public static void main(String[] args) {
		final int N = 15;
		long denominator = factorial((long)(N+1));
		long numerator = 0L;
		int lengthBound = getLengthBound(N);
		
		for(int i=0; i<=lengthBound; i++) {
			CombinationGenerator combGen = new CombinationGenerator(N, i);
			while(combGen.hasMore()) {
				int[] combination = combGen.getNext();
				long probabilityOfPath = getProbabilityOfPath(combination);
				numerator += probabilityOfPath;
			}
		}
		
		System.out.println(denominator/numerator);
		
	}

	private static long getProbabilityOfPath(int[] combination) {
		long probability = 1L;
		
		for(int i=0; i<combination.length; i++)
			probability *= (long)(combination[i]+1);
		
		return probability;
	}

	private static int getLengthBound(int n) {
		if(n%2==0) 
			return n/2 - 1;
		else
			return n/2;
	}

	private static long factorial(long n) {
		if(n<0)
			return -1L;
		if(n<2)
			return 1L;
		
		long factorial = 1L;
		for(int i=2; i<=n; i++)
			factorial *= i;
		
		return factorial;
	}

}


public class Problem17
{
	/*
	 * If the numbers 1 to 5 are written out in words: one, two, three, four, five, then 
	 * there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
	 * 
	 * 	If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, 
	 * how many letters would be used?
	 * 
	 * 		NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters. The use of "and" when writing out numbers is in compliance with British usage.
	 */
	
	public static void main(String[] args) 
	{
		int sum = 0;
		for(int i=1; i<=1000; i++)
		{
			sum += lettersInNumber(i);
		}
		System.out.println(sum);
	}
	
	public static int lettersInNumber(int v)  //v<=1000
	{
		if(v<0 || v>1000)
			return -1;
		
		if(v<21)
		{
			int len = words[v].length();
			return len;
		}
		
		if(v<100)
		{
			int vv = v;
			vv /= 10;
			
			int len = tens[vv].length() + lettersInNumber(v-10*vv);
			return len;
		}

		if(v<1000)
		{			
			int vv = v;
			
			
			if(vv%100==0)
			{
				int len = words[vv/100].length() + hundred.length()-3;
				return len;
			}
			else
			{
				vv /= 100;
				int len = words[vv].length() + hundred.length() + lettersInNumber(v - 100*vv);
				return len;
			}			
		}
		
		if(v==1000)
			return thousand.length();
		
		return -1;
	}
	
	private static String[] words = { "", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
									  "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen",
									  "eighteen", "nineteen", "twenty"};
	private static String[] tens = { "", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy",
								     "eighty", "ninety" };
	private static String thousand = "onethousand";
	private static String hundred = "hundredand";
}

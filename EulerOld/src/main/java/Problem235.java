

import java.math.BigDecimal;

import MyStuff.BigPolynomial;
import MyStuff.Polynomial;

public class Problem235 {
	/*
	 * Given is the arithmetic-geometric sequence 
	 * u(k) = (900-3k)r^(k-1).
	 * 
	 * Let s(n) = Sum(k=1...n) [u(k)].
	 * 
	 * 	Find the value of r for which s(5000) = -600,000,000,000.
	 * 
	 * 	Give your answer rounded to 12 places behind the decimal point.
	 */

	/*
	 * Chosed some value little bit over 1.00 by experiment
	 * as other results gives me NaN and were too big.
	 */
	public static void main(String[] args) {
		Polynomial polynomial = getPolynomial();
		double result = polynomial.getRootNear(1.11, 1000);
		double check = polynomial.getValue(result);

		System.out.println("RESULT = " + result);
		System.out.println("CHECK = " + check);
	}

	private static BigPolynomial getBigPolynomial() {
		final int n = 5000;
		BigDecimal[] coefficients = new BigDecimal[n];

		for (int k = 1; k <= n; k++) {
			coefficients[k - 1] = BigDecimal.valueOf(900.0 - (3.0 * k));
		}

		coefficients[0] = coefficients[0].add(BigDecimal.valueOf(6.0 * 1e11));
		return new BigPolynomial(coefficients);

		//		int k = 5000;
		//		double a = 6.0 * 1e11;
		//		BigDecimal[] coefficients = new BigDecimal[k + 3];
		//		for (int i = 0; i < coefficients.length; i++) {
		//			coefficients[i] = BigDecimal.ZERO;
		//		}
		//
		//		coefficients[k + 2] = BigDecimal.valueOf(900.0 - (3.0 * k));
		//		coefficients[k + 1] = BigDecimal.valueOf((6.0 * k) - 1800.0);
		//		coefficients[k] = BigDecimal.valueOf(903.0 - (3.0 * k));
		//
		//		coefficients[2] = BigDecimal.valueOf(a - 897.0);
		//		coefficients[1] = BigDecimal.valueOf(1791.0 - (2.0 * a));
		//		coefficients[0] = BigDecimal.valueOf(a - 897.0);
		//
		//		return new BigPolynomial(coefficients);
	}

	private static Polynomial getPolynomial() {
		final int n = 5000;
		double[] coefficients = new double[n];

		for (int k = 1; k <= n; k++) {
			coefficients[k - 1] = 900.0 - (3.0 * k);
		}

		coefficients[0] += 6.0 * 1e11;
		return new Polynomial(coefficients);

		//		int k = 5000;
		//		double a = 6.0 * 1e11;
		//		BigDecimal[] coefficients = new BigDecimal[k + 3];
		//		for (int i = 0; i < coefficients.length; i++) {
		//			coefficients[i] = BigDecimal.ZERO;
		//		}
		//
		//		coefficients[k + 2] = BigDecimal.valueOf(900.0 - (3.0 * k));
		//		coefficients[k + 1] = BigDecimal.valueOf((6.0 * k) - 1800.0);
		//		coefficients[k] = BigDecimal.valueOf(903.0 - (3.0 * k));
		//
		//		coefficients[2] = BigDecimal.valueOf(a - 897.0);
		//		coefficients[1] = BigDecimal.valueOf(1791.0 - (2.0 * a));
		//		coefficients[0] = BigDecimal.valueOf(a - 897.0);
		//
		//		return new BigPolynomial(coefficients);
	}
}

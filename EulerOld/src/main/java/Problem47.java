
public class Problem47 
{
	/*
	 * The first two consecutive numbers to have two distinct prime factors are:

	14 = 2 � 7
	15 = 3 � 5

	The first three consecutive numbers to have three distinct prime factors are:

	644 = 2^2 � 7 � 23
	645 = 3 � 5 � 43
	646 = 2 � 17 � 19.

	Find the first four consecutive integers to have four distinct primes factors. What is the first of these numbers?	 */
	
	public static void main(String[] args) 
	{
		long first = 0L;
		int counter = 0;
		
		for(long i = 647L; ; i++)
		{
			if(counter==4)
				break;
			
			long[] temp = MyMath.getPrimeDivisors(i);
			if(temp.length==4)
			{
				if(counter==0)
				{
					first = i;
					counter++;
				}
				else //if(counter>0)
				{
					if((i-(long)counter)==first)
						counter++;
					else
						counter = 0;
				}
			}
			else
				counter = 0;
		}
		
		System.out.println(first);
	}
}

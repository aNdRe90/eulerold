import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;


public class MyMath 
{
	public static void main(String[] args)
	{
		//long[] temp = reduceFraction(1424, 2144);		
		System.out.println(Arrays.toString(getPrimeDivisors(180)));
	}	
	
	/**
	 * Calculations are based on the theorem:
	 * http://en.wikipedia.org/wiki/Divisor_function
	 * 	saying that if X = p1^m1 * p2^m2 * ... pn^mn
	 * 	where pi is a prime, and mi integer
	 * 	then number of divisors = (m1+1)(m2+1)...(mn+1)
	 * So it is not calculating the divisors themselves and is more efficient that way.
	 * @param n number which number of divisors to calculate.
	 * @return number of divisors including that the 1 and number itself is its divisor.
	 * 
	 * For n<2 it returns 0.
	 */
	public static long getNumberOfDivisors(long n)
	{
		if(n<2)
			return 0L;
		
		long numberOfDivisors = 1L;
		long[] primeFactors = getPrimeFactorization(n);
		
		for(int i=0; i<primeFactors.length; i++)
		{
			long temp = primeFactors[i];
			int m = 1;
			
			while((++i)<primeFactors.length && primeFactors[i]==temp)
				++m;
			
			numberOfDivisors *= (long) (m+1);
			--i;
		}
		return numberOfDivisors;
	}
	/**
	 * Function computes all the divisors of the given number including 1 and the number itself.
	 * Divisors are not sorted in any way! But they do not repeat themselves.
	 * NOTE: function checks 2^n possibilities, where n = size of array returned by getPrimeFactorization() function.
	 * So it can take far too much time for big numbers.
	 * @param n number which divisors to return
	 * @return array of divisors. For n<2 it returns null.
	 */
	public static long[] getDivisors(long n)
	{
		if(n<2)
			return null;
		
		long[] primeDivs = getPrimeFactorization(n);		
		HashSet<Long> divisorsSet = new HashSet<Long>();		
		boolean[] bools = new boolean[primeDivs.length];
		
		int a = 1;
		a = a << primeDivs.length;
		for(int i=0; i<a; i++)
		{
			bools = new boolean[primeDivs.length];
			int index = 0;
			int val = i;
			while (val != 0) 
			{
			  if (val % 2 != 0)
			  {
			    bools[index]=true;
			  }
			  ++index;
			  val = val >>> 1;
			}
			
			long divisor = 1L;
			for(int j=0; j<primeDivs.length; j++)
				if(bools[j])
					divisor*=primeDivs[j];
			divisorsSet.add(divisor);
		}
		
		long[] divisors = new long[divisorsSet.size()];
		Iterator<Long> iter = divisorsSet.iterator();
		int i=0;
		while(iter.hasNext())
			divisors[i++] = iter.next().longValue();
		
		return divisors;
	}
	
	/**
	 * Function prime factorizes the number
	 * @param n number to factorize
	 * @return array of prime factors e.g. for given 180, array has elements { 2, 2, 3, 3, 5} 
	 * as the prime factorization of 180 is:   180 = 2^2*3^2*5
	 * Notice that the same values will always be adjacent.
	 * 
	 * For n<2 it returns null.
	 */
	public static long[] getPrimeFactorization(long n)
	{		
		if(n<2)
			return new long[0];
		
		ArrayList<Long> primeDivisors = new ArrayList<Long>(20);
		
		for(int i = 0; i<primes.length; i++)
		{
			while(true)
			{
				if(n==1)
				{
					i = primes.length;
					break;
				}
					
				if(n%primes[i]==0)
				{
					n /= primes[i];
					primeDivisors.add(new Long(primes[i]));
				}
				else
					break;
			}
		}	
		
		if(n>1)
		{
			
			for(long j=1001; n!=1; j+=2)
			{
				if(n%j==0 && isPrime(j))
				{
					n /= j;
					primeDivisors.add(new Long(j));
				}
			}
		}
		
		
		long[] primeDivs = new long[primeDivisors.size()];
		for(int i=0; i<primeDivs.length; i++)
			primeDivs[i] = primeDivisors.get(i).longValue();
		return primeDivs;
	}
	
	/**
	 * Function returns the prime divisors of the given number.
	 * @param v
	 * @return prime divisors 
	 * 
	 * e.g. for 180 which after prime factorization is equal to
	 * 2^2*3^2*5 we would get only {2,3,5}
	 * if you want to know the exponents as well, call 
	 * getPrimeFactorization() and you wille get {2,2,3,3,5} for 180.
	 * 
	 * For n<2 it returns null.
	 */
	public static long[] getPrimeDivisors(long n)
	{
		if(n<2)
			return null;
		
		HashSet<Long> primeDivisors = new HashSet<Long>(20);
		
		for(int i = 0; i<primes.length; i++)
		{
			while(true)
			{
				if(n==1)
				{
					i = primes.length;
					break;
				}
					
				if(n%primes[i]==0)
				{
					n /= primes[i];
					primeDivisors.add(new Long(primes[i]));
				}
				else
					break;
			}
		}	
		
		if(n>1)
		{
			
			for(long j=1001; n!=1; j+=2)
			{
				if(n%j==0 && isPrime(j))
				{
					n /= j;
					primeDivisors.add(new Long(j));
				}
			}
		}
		
		
		long[] primeDivs = new long[primeDivisors.size()];
		Iterator<Long> iter = primeDivisors.iterator();
		int i=0;
		while(iter.hasNext())
			primeDivs[i++] = iter.next().longValue();
		return primeDivs;
	}
	
	/**
	 * Only for positive numbers. It determines if the number is prime.
	 * @param v number to determine
	 * @return true if the number is prime, false if not.
	 */
	public static boolean isPrime(long v) 
	{
		if(v<=1L)
			return false;
		
		if(v<=primes[primes.length-1])
		{
			for(int i = 0; i<primes.length; i++)
			{
				if(v==primes[i])
					return true;
			}
			return false;
		}
		else
		{
			for(int i = 0; i<primes.length; i++)
			{
				if(v%primes[i]==0)
					return false;
			}
			long bound = (long) Math.sqrt((double)v) + 1L;
			//if(bound<primes[primes.length-1])  [not necessary]
			//	bound = v;
			
			for(long i = primes[primes.length-1]; i<=bound; i+=2)
			{
				if(v%i==0)
					return false;
			}
			
			return true;
		}
	}
	
	/**
	 * A perfect number is a number for which the sum of its proper divisors 
	 * is exactly equal to the number. For example, the sum of the proper divisors of 28 
	 * would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect number.
	 * NOTE: for big numbers calculating all the divisors can take far too much time.
	 * @param number number to determine whether is perfect
	 * @return true is t he number is perfect, false otherwise.
	 */
	public static boolean isPerfect(long number)
	{
		long[] divisors = getDivisors(number);
		long sum = 0L;
		for(int i=0; i<divisors.length; i++)
			sum += divisors[i];
		
		sum -= number;
		
		if(number==sum)
			return true;
		else
			return false;
	}
	
	/**
	 * A number n is called abundant if the sum of its proper divisors exceeds n.
	 * NOTE: for big numbers calculating all the divisors can take far too much time.
	 * @param number
	 * @return true if number is abundant, false otherwise.
	 */
	public static boolean isAbundant(long number)
	{
		long[] divisors = getDivisors(number);
		long sum = 0L;
		for(int i=0; i<divisors.length; i++)
			sum += divisors[i];
		
		sum -= number;
		
		if(number<sum)
			return true;
		else
			return false;
	}
	
	/**
	 * A number n is called deficient if the sum of its proper divisors is less than n.
	 * NOTE: for big numbers calculating all the divisors can take far too much time.
	 * @param number
	 * @return true if number is deficient, false otherwise.
	 */
	public static boolean isDeficient(long number)
	{
		long[] divisors = getDivisors(number);
		long sum = 0L;
		for(int i=0; i<divisors.length; i++)
			sum += divisors[i];
		
		sum -= number;
		
		if(number>sum)
			return true;
		else
			return false;
	}
	
	/**
	  * Function reduces the fraction with numerator and denominator given as 
	 * parameters. 
	 * @param num numerator of the fraction
	 * @param den
	 * @return array where [0] is the reduced numerator and [1] is reduced denominator.
	 */
	public static long[] reduceFraction(long num, long den)
	{
		long[] newFraction = new long[2];
		long[] numFactors = getPrimeFactorization(num);
		long[] denFactors = getPrimeFactorization(den);
		
		for(int i=0; i<numFactors.length; i++)
			for(int j=0; j<denFactors.length; j++)
			{
				if(numFactors[i]==denFactors[j])
				{
					num /= numFactors[i];
					den /= numFactors[i];
					
					numFactors[i] = denFactors[i] = 1L;
				}
			}
		
		newFraction[0] = num;
		newFraction[1] = den;
		
		return newFraction;
	}
	
	/**
	 * We shall say that an n-digit number is pandigital 
	 * if it makes use of all the digits 1 to n exactly once. 
	 * 
	 * Clearly   1<=n<=9  must be satisfied 
	 * for the n-digit number to be pandigital.
	 * 
	 * For example:
	 * 2143 is a 4-digit pandigital.
	 * 354127869 is a 9-digit pandigital.
	 * @param v
	 * @return
	 */
	public static boolean isPandigital(long v)
	{
		String num = Long.toString(v);
		int length = num.length();
		if(length<1 || length>9)
			return false;
		
		
		char[] chars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
		for(int i=1; i<=length; i++)
		{
			int counter = 0;
			char currentChar = chars[i];
			for(int j=0; j<length; j++)
				if(num.charAt(j)==currentChar)
					counter++;
			
			if(counter!=1)
				return false;
		}	
		return true;
	}
	
	/**
	 * The nth term of the sequence of triangle numbers is given by: t(n) = 0.5*n(n+1); 
	 * so the first ten triangle numbers are:
	 * 
	 * 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...
	 * 
	 * if [ t(n) = x ]
	 * 		then => 2x = n^2+n
	 * 			 => n^2 + n - 2x = 0
	 * 			 => delta = 1+8x
	 * 	
	 * So x is the triangle number if sqrt(1+8x) is natural number.
	 * Moreover, it is the [ 0.5*(sqrt(1+8x)-1) ]th  triangle number.
	 * @param v number to determine if is a triangle number
	 * @return true if the number is a triangle number, false otherwise
	 */
	public static boolean isTriangleNumber(long v)
	{
		if(v<1L)
			return false;
		
		double sqrtDouble = Math.sqrt(1.0+8.0*v);
		if(Math.floor(sqrtDouble)==sqrtDouble)
		{
			double resD = 0.5*(sqrtDouble-1.0);
			if(Math.floor(resD)==resD)
				return true;
			else
				return false;
		}
		return false;			
	}
	
	/**
	 * The nth term of the sequence of triangle numbers is given by: t(n) = 0.5*n(n+1); 
	 * so the first ten triangle numbers are:
	 * 
	 * 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...
	 * @param n which traingle number to return
	 * @return nth triangle number or -1L if given n is <1
	 */
	public static long getNthTriangleNumber(long n)
	{
		if(n<1L)
			return -1L;
		
		return (long) (n*(n+1)*0.5);			
	}
	
	/**
	 * The nth term of the sequence of triangle numbers is given by: t(n) = 0.5*n(n+1); 
	 * so the first ten triangle numbers are:
	 * 
	 * 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...
	 * 1 - 1st
	 * 6 - 3rd
	 * 28 - 7th   etc.
	 * 
	 * if [ t(n) = x ]
	 * 		then => 2x = n^2+n
	 * 			 => n^2 + n - 2x = 0
	 * 			 => delta = 1+8x
	 * 	
	 * So x is the triangle number if sqrt(1+8x) is natural number.
	 * Moreover, it is the [ 0.5*(sqrt(1+8x)-1) ]th  triangle number.
	 * @param v number to determine which triangle number
	 * @return integer>0 if v is a triangle number 
	 * (not necessary to check with isTriangleNumber(v) function first as it checks it the same way
	 * this function does, so you would eventually do the same thing twice.
	 * 
	 * value -1 if v is not a triangle number.
	 */
	public static int getWhichTriangleNumber(long v)
	{
		if(v<1L)
			return -1;
		
		double sqrtDouble = Math.sqrt(1.0+8.0*v);
		if(Math.floor(sqrtDouble)==sqrtDouble)
		{
			double resD = 0.5*(sqrtDouble-1.0);
			if(Math.floor(resD)==resD)
				return (int) resD;
			else
				return -1;
		}
		return -1;			
	}
	
	/**
	 * Pentagonal numbers are generated by the formula, P(n)=n(3n-1)/2. 
	 * The first ten pentagonal numbers are:
	 * 1, 5, 12, 22, 35, 51, 70, 92, 117, 145, ...
	 * 
	 * if [ P(n) = x ]
	 * 		then => 2x = 3n^2 - n
	 * 			 => 3n^2 - n - 2x = 0
	 * 			 => delta = 1+24x
	 * 	
	 * So x is the pentagonal number if sqrt(1+24x) is natural number.
	 * Moreover, it is the [ (1+sqrt(1+24x)/6) ]th  pentagonal number.
	 * @param v number to determine if is a pentagonal number
	 * @return true if the number is a pentagonal number, false otherwise
	 */
	public static boolean isPentagonalNumber(long v)
	{
		if(v<1L)
			return false;
		
		double sqrtDouble = Math.sqrt(1.0+24.0*v);
		if(Math.floor(sqrtDouble)==sqrtDouble)
		{
			double resD = (sqrtDouble+1.0)/6.0;
			if(Math.floor(resD)==resD)
				return true;
			else
				return false;
		}
		else
			return false;			
	}
	
	/**
	 * Pentagonal numbers are generated by the formula, P(n)=n(3n-1)/2. 
	 * The first ten pentagonal numbers are:
	 * 1, 5, 12, 22, 35, 51, 70, 92, 117, 145, ...
	 * 
	 * if [ P(n) = x ]
	 * 		then => 2x = 3n^2 - n
	 * 			 => 3n^2 - n - 2x = 0
	 * 			 => delta = 1+24x
	 * 	
	 * So x is the pentagonal number if sqrt(1+24x) is natural number.
	 * Moreover, it is the [ (1+sqrt(1+24x)/6) ]th  pentagonal number.
	 * @param v number to determine if is a pentagonal number
	 * @return integer>0 if v is a pentagonal number 
	 * (not necessary to check with isPentagonalNumber(v) function first as it checks it the same way
	 * this function does, so you would eventually do the same thing twice.
	 * 
	 * value -1 if v is not a pentagonal number.
	 */
	public static int getWhichPentagonalNumber(long v)
	{
		if(v<1L)
			return -1;
		
		double sqrtDouble = Math.sqrt(1.0+24.0*v);
		if(Math.floor(sqrtDouble)==sqrtDouble)
		{
			double resD = (sqrtDouble+1.0)/6.0;
			if(Math.floor(resD)==resD)
				return (int) resD;
			else
				return -1;
		}
		else
			return -1;			
	}
	
	/**
	 * Pentagonal numbers are generated by the formula, P(n)=n(3n-1)/2. 
	 * The first ten pentagonal numbers are:
	 * 1, 5, 12, 22, 35, 51, 70, 92, 117, 145, ...
	 * @param n which pentagonal number to return
	 * @return nth pentagonal number or -1L if given n is <1
	 */
	public static long getNthPentagonalNumber(long n)
	{
		if(n<1L)
			return -1L;
		
		return (long) (n*(3*n-1)*0.5);			
	}
	
	
	/**
	 * Hexagonal Formula:  	H(n)=n(2n-1) 	  	
	 * 1, 6, 15, 28, 45, ...
	 * 
	 * if [ H(n) = x ]
	 * 		then => x = 2n^2 - n
	 * 			 => 2n^2 - n - x = 0
	 * 			 => delta = 1+8x
	 * 	
	 * So x is the hexagonal number if sqrt(1+8x) is natural number.
	 * Moreover, it is the [ (1+sqrt(1+8x)/4) ]th  hexagonal number.
	 * @param v number to determine if is a hexagonal number
	 * @return true if the number is a hexagonal number, false otherwise
	 */
	public static boolean isHexagonalNumber(long v)
	{
		if(v<1L)
			return false;
		
		double sqrtDouble = Math.sqrt(1.0+8.0*v);
		if(Math.floor(sqrtDouble)==sqrtDouble)
		{
			double resD = (sqrtDouble+1.0)*0.25;
			if(Math.floor(resD)==resD)
				return true;
			else
				return false;
		}
		else
			return false;			
	}
	
	
	/**
	 * Hexagonal Formula:  	H(n)=n(2n-1) 	  	
	 * 1, 6, 15, 28, 45, ...
	 * 
	 * if [ H(n) = x ]
	 * 		then => x = 2n^2 - n
	 * 			 => 2n^2 - n - x = 0
	 * 			 => delta = 1+8x
	 * 	
	 * So x is the hexagonal number if sqrt(1+8x) is natural number.
	 * Moreover, it is the [ (1+sqrt(1+8x)/4) ]th  hexagonal number.
	 * @param v number to determine if is a hexagonal number
	 * @return integer>0 if v is a hexagonal number 
	 * (not necessary to check with isHexagonalNumber(v) function first as it checks it the same way
	 * this function does, so you would eventually do the same thing twice.
	 * 
	 * value -1 if v is not a hexagonal number.
	 */
	public static int getWhichHexagonalNumber(long v)
	{
		if(v<1L)
			return -1;
		
		double sqrtDouble = Math.sqrt(1.0+8.0*v);
		if(Math.floor(sqrtDouble)==sqrtDouble)
		{
			double resD = (sqrtDouble+1.0)*0.25;
			if(Math.floor(resD)==resD)
				return (int) resD;
			else
				return -1;
		}
		else
			return -1;			
	}
	
	/**
	 * Hexagonal Formula:  	H(n)=n(2n-1) 	  	
	 * 1, 6, 15, 28, 45, ...
	 * @param n which hexagonal number to return
	 * @return nth hexagonal number or -1L if given n is <1
	 */
	public static long getNthHexagonalNumber(long n)
	{
		if(n<1L)
			return -1L;
		
		return (long) (n*(2*n-1));			
	}
	
	/**
	 * Function returns the largest prime number than is less than
	 * given value
	 * @param value value that a returned prime must be lower than
	 * @return prime lower than given value. If value <=2 then -1L returned.
	 */
	public static long getLargestPrimeLowerThan(long value)
	{
		if(value<3)
			return -1L;
		
		if(value<=primes[primes.length-1])
		{
			for(int i=(primes.length-2); i>=0; i--)
			{
				if(primes[i]<value)
					return primes[i];
			}
		}
		else
		{
			long i=(value-1);
			if(i%2==0)
				i--;
			
			for( ; i>primes[primes.length-1]; i-=2)
			{
				if(isPrime(i))
					return i;
			}
		}
		return -1L;
	}
	
	/**
	 * Function determines whether 2 numbers consists of the same digits.
	 * For example numbers  12438 and 84231 satisfy the condition.
	 * @param first first number
	 * @param second second number
	 * @return true if(first==second) or first number consists of the same digits as second number
	 */
	public static boolean areOfSameDigits(long first, long second)
	{
		if(first==second)
			return true;
		
		String firstString = Long.toString(first);
		String secondString = Long.toString(second);
		
		if(firstString.length()!=secondString.length())
			return false;
		
		LinkedList<Character> digitsFirst = new LinkedList<Character>();
		LinkedList<Character> digitsSecond = new LinkedList<Character>();
		
		for(int j=0; j<firstString.length(); j++)
		{
			digitsFirst.add(new Character(firstString.charAt(j)));
			digitsSecond.add(new Character(secondString.charAt(j)));
		}
		
		Collections.sort(digitsFirst);
		Collections.sort(digitsSecond);
		
		Iterator<Character> iterFirst = digitsFirst.iterator();
		Iterator<Character> iterSecond = digitsSecond.iterator();
		
		while(iterFirst.hasNext()) //second is the same length so no need to check iterSecond
		{
			if(! (iterFirst.next().equals(iterSecond.next())) )
				return false;
		}
		
		return true;
	}
	
	/**
	 * newtonsSymbol(n,r) = n! / r!(n-r)!   = Newton`s symbol, combinatorics.
	 * @param r
	 * @param n
	 * @return correctValue or null if r>n or either of n,r is <0
	 */
	public static BigInteger binomialCoefficient(long n, long r)
	{
		if(n<0L || r<0L || r>n)
			return null;
		
		BigInteger cNumerator = factorial(n); 
		BigInteger cDenominator = factorial(r).multiply(factorial(n-r)); 
		
		return cNumerator.divide(cDenominator);
	}
	
	/**
	 * Return factorial of a given integer.
	 * @param n
	 * @return factorial or null if n<0
	 */
	public static BigInteger factorial(long n)
	{
		if(n<0L)
			return null;
		if(n==0L)
			return BigInteger.ONE;
		
		BigInteger b = BigInteger.ONE;
		for(long i=2L; i<=n; i++)
			b = b.multiply(BigInteger.valueOf(i));
		
		return b;
	}
	
	/**
	 * Determines whether given number is palindrom.
	 * Palindrom number can be read as the same value both from left to right
	 * and from rigth to left  e.g  12321, 8448 ...
	 * @param s 
	 * @return true if number is palindrome, false if not or if number <0; 
	 */
	public static boolean isPalindrome(long number)
	{
		String s = Long.toString(number);
		int halfLength = s.length()/2;
		int counter = 0;
		
		for(int i = 0; i<halfLength; i++)
		{
			if(s.charAt(i)==s.charAt(s.length()-1-i))
				counter++;
		}
		
		if(counter==halfLength)
			return true;
		else
			return false;
	}
	
	/**
	 * Create a reversed number. For example for 49112 it returns 21194.
	 * @param number number to reverse
	 * @return reversed number or the original number if its <0.
	 */
	public static long reverse(long number)
	{
		if(number<0)
			return number;
		
		StringBuffer buf = new StringBuffer(Long.toString(number));
		return Long.parseLong(buf.reverse().toString());
	}
	
	private static long[] primes = 
		{
		2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 
		31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 
		73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 
		127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 
		179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 
		233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 
		283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 
		353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 
		419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 
		467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 
		547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 
		607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 
		661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 
		739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 
		811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 
		877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 
		947, 953, 967, 971, 977, 983, 991, 997, 1009, 1013, 
		1019, 1021, 1031, 1033, 1039, 1049, 1051, 1061, 1063, 1069, 
		1087, 1091, 1093, 1097, 1103, 1109, 1117, 1123, 1129, 1151, 
		1153, 1163, 1171, 1181, 1187, 1193, 1201, 1213, 1217, 1223, 
		1229, 1231, 1237, 1249, 1259, 1277, 1279, 1283, 1289, 1291, 
		1297, 1301, 1303, 1307, 1319, 1321, 1327, 1361, 1367, 1373, 
		1381, 1399, 1409, 1423, 1427, 1429, 1433, 1439, 1447, 1451, 
		1453, 1459, 1471, 1481, 1483, 1487, 1489, 1493, 1499, 1511, 
		1523, 1531, 1543, 1549, 1553, 1559, 1567, 1571, 1579, 1583, 
		1597, 1601, 1607, 1609, 1613, 1619, 1621, 1627, 1637, 1657, 
		1663, 1667, 1669, 1693, 1697, 1699, 1709, 1721, 1723, 1733, 
		1741, 1747, 1753, 1759, 1777, 1783, 1787, 1789, 1801, 1811, 
		1823, 1831, 1847, 1861, 1867, 1871, 1873, 1877, 1879, 1889, 
		1901, 1907, 1913, 1931, 1933, 1949, 1951, 1973, 1979, 1987, 
		1993, 1997, 1999, 2003, 2011, 2017, 2027, 2029, 2039, 2053, 
		2063, 2069, 2081, 2083, 2087, 2089, 2099, 2111, 2113, 2129, 
		2131, 2137, 2141, 2143, 2153, 2161, 2179, 2203, 2207, 2213, 
		2221, 2237, 2239, 2243, 2251, 2267, 2269, 2273, 2281, 2287, 
		2293, 2297, 2309, 2311, 2333, 2339, 2341, 2347, 2351, 2357, 
		2371, 2377, 2381, 2383, 2389, 2393, 2399, 2411, 2417, 2423, 
		2437, 2441, 2447, 2459, 2467, 2473, 2477, 2503, 2521, 2531, 
		2539, 2543, 2549, 2551, 2557, 2579, 2591, 2593, 2609, 2617, 
		2621, 2633, 2647, 2657, 2659, 2663, 2671, 2677, 2683, 2687, 
		2689, 2693, 2699, 2707, 2711, 2713, 2719, 2729, 2731, 2741, 
		2749, 2753, 2767, 2777, 2789, 2791, 2797, 2801, 2803, 2819, 
		2833, 2837, 2843, 2851, 2857, 2861, 2879, 2887, 2897, 2903, 
		2909, 2917, 2927, 2939, 2953, 2957, 2963, 2969, 2971, 2999, 
		3001, 3011, 3019, 3023, 3037, 3041, 3049, 3061, 3067, 3079, 
		3083, 3089, 3109, 3119, 3121, 3137, 3163, 3167, 3169, 3181, 
		3187, 3191, 3203, 3209, 3217, 3221, 3229, 3251, 3253, 3257, 
		3259, 3271, 3299, 3301, 3307, 3313, 3319, 3323, 3329, 3331, 
		3343, 3347, 3359, 3361, 3371, 3373, 3389, 3391, 3407, 3413, 
		3433, 3449, 3457, 3461, 3463, 3467, 3469, 3491, 3499, 3511, 
		3517, 3527, 3529, 3533, 3539, 3541, 3547, 3557, 3559, 3571, 
		3581, 3583, 3593, 3607, 3613, 3617, 3623, 3631, 3637, 3643, 
		3659, 3671, 3673, 3677, 3691, 3697, 3701, 3709, 3719, 3727, 
		3733, 3739, 3761, 3767, 3769, 3779, 3793, 3797, 3803, 3821, 
		3823, 3833, 3847, 3851, 3853, 3863, 3877, 3881, 3889, 3907, 
		3911, 3917, 3919, 3923, 3929, 3931, 3943, 3947, 3967, 3989, 
		4001, 4003, 4007, 4013, 4019, 4021, 4027, 4049, 4051, 4057, 
		4073, 4079, 4091, 4093, 4099, 4111, 4127, 4129, 4133, 4139, 
		4153, 4157, 4159, 4177, 4201, 4211, 4217, 4219, 4229, 4231, 
		4241, 4243, 4253, 4259, 4261, 4271, 4273, 4283, 4289, 4297, 
		4327, 4337, 4339, 4349, 4357, 4363, 4373, 4391, 4397, 4409, 
		4421, 4423, 4441, 4447, 4451, 4457, 4463, 4481, 4483, 4493, 
		4507, 4513, 4517, 4519, 4523, 4547, 4549, 4561, 4567, 4583, 
		4591, 4597, 4603, 4621, 4637, 4639, 4643, 4649, 4651, 4657, 
		4663, 4673, 4679, 4691, 4703, 4721, 4723, 4729, 4733, 4751, 
		4759, 4783, 4787, 4789, 4793, 4799, 4801, 4813, 4817, 4831, 
		4861, 4871, 4877, 4889, 4903, 4909, 4919, 4931, 4933, 4937, 
		4943, 4951, 4957, 4967, 4969, 4973, 4987, 4993, 4999, 5003, 
		5009, 5011, 5021, 5023, 5039, 5051, 5059, 5077, 5081, 5087, 
		5099, 5101, 5107, 5113, 5119, 5147, 5153, 5167, 5171, 5179, 
		5189, 5197, 5209, 5227, 5231, 5233, 5237, 5261, 5273, 5279, 
		5281, 5297, 5303, 5309, 5323, 5333, 5347, 5351, 5381, 5387, 
		5393, 5399, 5407, 5413, 5417, 5419, 5431, 5437, 5441, 5443, 
		5449, 5471, 5477, 5479, 5483, 5501, 5503, 5507, 5519, 5521, 
		5527, 5531, 5557, 5563, 5569, 5573, 5581, 5591, 5623, 5639, 
		5641, 5647, 5651, 5653, 5657, 5659, 5669, 5683, 5689, 5693, 
		5701, 5711, 5717, 5737, 5741, 5743, 5749, 5779, 5783, 5791, 
		5801, 5807, 5813, 5821, 5827, 5839, 5843, 5849, 5851, 5857, 
		5861, 5867, 5869, 5879, 5881, 5897, 5903, 5923, 5927, 5939, 
		5953, 5981, 5987, 6007, 6011, 6029, 6037, 6043, 6047, 6053, 
		6067, 6073, 6079, 6089, 6091, 6101, 6113, 6121, 6131, 6133, 
		6143, 6151, 6163, 6173, 6197, 6199, 6203, 6211, 6217, 6221, 
		6229, 6247, 6257, 6263, 6269, 6271, 6277, 6287, 6299, 6301, 
		6311, 6317, 6323, 6329, 6337, 6343, 6353, 6359, 6361, 6367, 
		6373, 6379, 6389, 6397, 6421, 6427, 6449, 6451, 6469, 6473, 
		6481, 6491, 6521, 6529, 6547, 6551, 6553, 6563, 6569, 6571, 
		6577, 6581, 6599, 6607, 6619, 6637, 6653, 6659, 6661, 6673, 
		6679, 6689, 6691, 6701, 6703, 6709, 6719, 6733, 6737, 6761, 
		6763, 6779, 6781, 6791, 6793, 6803, 6823, 6827, 6829, 6833, 
		6841, 6857, 6863, 6869, 6871, 6883, 6899, 6907, 6911, 6917, 
		6947, 6949, 6959, 6961, 6967, 6971, 6977, 6983, 6991, 6997, 
		7001, 7013, 7019, 7027, 7039, 7043, 7057, 7069, 7079, 7103, 
		7109, 7121, 7127, 7129, 7151, 7159, 7177, 7187, 7193, 7207, 
		7211, 7213, 7219, 7229, 7237, 7243, 7247, 7253, 7283, 7297, 
		7307, 7309, 7321, 7331, 7333, 7349, 7351, 7369, 7393, 7411, 
		7417, 7433, 7451, 7457, 7459, 7477, 7481, 7487, 7489, 7499, 
		7507, 7517, 7523, 7529, 7537, 7541, 7547, 7549, 7559, 7561, 
		7573, 7577, 7583, 7589, 7591, 7603, 7607, 7621, 7639, 7643, 
		7649, 7669, 7673, 7681, 7687, 7691, 7699, 7703, 7717, 7723, 
		7727, 7741, 7753, 7757, 7759, 7789, 7793, 7817, 7823, 7829, 
		7841, 7853, 7867, 7873, 7877, 7879, 7883, 7901, 7907, 7919,
		7927, 7933, 7937, 7949, 7951, 7963, 7993, 8009, 8011, 8017,
		8039, 8053, 8059, 8069, 8081, 8087, 8089, 8093, 8101, 8111,
		8117, 8123, 8147, 8161, 8167, 8171, 8179, 8191, 8209, 8219,
		8221, 8231, 8233, 8237, 8243, 8263, 8269, 8273, 8287, 8291,
		8293, 8297, 8311, 8317, 8329, 8353, 8363, 8369, 8377, 8387,
		8389, 8419, 8423, 8429, 8431, 8443, 8447, 8461, 8467, 8501,
		8513, 8521, 8527, 8537, 8539, 8543, 8563, 8573, 8581, 8597,
		8599, 8609, 8623, 8627, 8629, 8641, 8647, 8663, 8669, 8677,
		8681, 8689, 8693, 8699, 8707, 8713, 8719, 8731, 8737, 8741,
		8747, 8753, 8761, 8779, 8783, 8803, 8807, 8819, 8821, 8831,
		8837, 8839, 8849, 8861, 8863, 8867, 8887, 8893, 8923, 8929,
		8933, 8941, 8951, 8963, 8969, 8971, 8999, 9001, 9007, 9011,
		9013, 9029, 9041, 9043, 9049, 9059, 9067, 9091, 9103, 9109,
		9127, 9133, 9137, 9151, 9157, 9161, 9173, 9181, 9187, 9199,
		9203, 9209, 9221, 9227, 9239, 9241, 9257, 9277, 9281, 9283,
		9293, 9311, 9319, 9323, 9337, 9341, 9343, 9349, 9371, 9377,
		9391, 9397, 9403, 9413, 9419, 9421, 9431, 9433, 9437, 9439,
		9461, 9463, 9467, 9473, 9479, 9491, 9497, 9511, 9521, 9533,
		9539, 9547, 9551, 9587, 9601, 9613, 9619, 9623, 9629, 9631,
		9643, 9649, 9661, 9677, 9679, 9689, 9697, 9719, 9721, 9733,
		9739, 9743, 9749, 9767, 9769, 9781, 9787, 9791, 9803, 9811,
		9817, 9829, 9833, 9839, 9851, 9857, 9859, 9871, 9883, 9887,
		9901, 9907, 9923, 9929, 9931, 9941, 9949, 9967, 9973,
		};
}

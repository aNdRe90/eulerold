
public class Problem5 
{
	/*
	 * 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
	 * What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
	 */
	public static void main(String[] args) 
	{
		//20 = 2, 4, 5, 10, 20
		//19 = 19
		//18 = 3, 6, 9, 18
		//17 = 17
		//16* = 2, 4, 8, 16
		//15* = 3, 5, 15
		//14* = 2, 7*, 14
		//13 = 13
		//12* = 2, 3, 4, 6, 12
		//11 = 11
		//10 = 2, 5, 10
		//9 = 3, 9
		//8* = 2, 4, 8
		//7 = 7
		//6 = 2, 3, 6
		//5 = 5
		//4 = 2, 4
		//3 = 3
		//2 = 2
		//1 = 1
		long number = (long) 20*19*18*17*13*11*7;   //116396280
		System.out.println(number);
		//1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 17, 18, 19, 20
		//jednak te� przez: 8, 14, 15
		//nie dzieli sie przez 16 tylko
		//*=2 i juz sie dzieli
		number*=2;
		System.out.println(number);
		System.out.println(isCorrect(number));
	}
	
	public static boolean isCorrect(long val)
	{
		int counter = 0;
		for(long i=1; i<=20; i++)
		{
			if(val%i==0)
				counter++;
			
			//2^2=4*1, 3^2=9*1, 4^2=16*1, 6^2=4*9=36, 12^2=2*6*3*4=144 (te sa dzielnikami liczby)
			if(i<5 || i==6 || i==12)
				continue;
			
			if(val%(i*i)==0)
				return false;
		}
		
		if(counter==20)
			return true;
		else
			return false;
	}
}

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;


public class Problem26 
{
	/*
	 * A unit fraction contains 1 in the numerator. 
	 * The decimal representation of the unit fractions with denominators 2 to 10 are given:

    	1/2	= 	0.5
    	1/3	= 	0.(3)
    	1/4	= 	0.25
    	1/5	= 	0.2
    	1/6	= 	0.1(6)
    	1/7	= 	0.(142857)
    	1/8	= 	0.125
    	1/9	= 	0.(1)
    	1/10	= 	0.1

		Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. 
		It can be seen that 1/7 has a 6-digit recurring cycle.

		Find the value of d < 1000 for which 1/d contains the longest recurring 
		cycle in its decimal fraction part.

	 */
	public static void main(String[] args) 
	{
		String s = getCycle(BigDecimal.ONE.divide(new BigDecimal(new BigInteger(Integer.toString(983))), new MathContext(initialScale)));
		
		
		int maxCycleLength = 0;
		int maxCycleD = 0;
		
		for(int d = 1; d<1000; d++)
		{			
			BigDecimal denominator = new BigDecimal(new BigInteger(Integer.toString(d)));
			BigDecimal dec = null;
			
			try
			{
				dec = BigDecimal.ONE.divide(denominator);
				// 1/d has finite decimal expansion - don`t bother then
			}
			catch(ArithmeticException e)   
			{
				// 1/d has non-terminating decimal expansion
				dec = BigDecimal.ONE.divide(denominator, new MathContext(initialScale));
				String cycle = getCycle(dec);
				
				if(maxCycleLength<cycle.length())
				{
					maxCycleLength = cycle.length();
					maxCycleD = d;
				}
			}
		}
		
		System.out.println(maxCycleD);
	}
	
	/**
	 * Returns recurring cycle in big decimal.
	 * @param d value
	 * @return String with cycle (which doesn`t need to be starting the decimal part) or null if no cycle found.
	 */	
	public static String getCycle(BigDecimal b)
	{
		String cycle = b.toString().substring(2);  //just decimal part
		
		String cycleCopy = cycle;
		do
		{
			scale = cycle.length();
			maxCycleLength = scale/2-1;
			if(maxCycleLength<0)
				maxCycleLength = 0;
			cycle = getEstimatedCycle(cycle);
			
			if(cycle!=null)
			{
				cycleCopy = cycle;
				if(cycle.length()%2==0)
					cycle += cycle.substring(0, 2);				
			}
				
		}
		while(cycle!=null);   //going out the loop only if cycle==null, 
							  //so we keep copy of the last "non-null" cycle
		
		char temp = cycleCopy.charAt(0);
		int counter = 1;
		for(int i=1; i<cycleCopy.length(); i++)
			if(cycleCopy.charAt(i)==temp)
				counter++;
		
		if(counter==cycleCopy.length())
			return new String(""+temp);
		else
			return cycleCopy;
	}
	
	private static String getEstimatedCycle(String decFractionPart)
	{
		//System.out.println(decFractionPart.charAt(0));
		String cycleToBe = new String(""); //?
		int cycleStartingPoint = -1;
		
		boolean isCycle = false;
		for(int i=maxCycleLength; i>0; i--)
		{			
			for(int cycleStart=0; ((scale-cycleStart)/2-1)>=i  ; cycleStart++)
			{
				cycleToBe = decFractionPart.substring(cycleStart, cycleStart+i);
				
				int previousStart = cycleStart+i;
				while(true)
				{
					int nextStart = previousStart+i;
					if(nextStart>=scale)
					{
						isCycle = true;
						cycleStartingPoint = cycleStart;
						i = 0;   //out the loop
						cycleStart = Integer.MAX_VALUE;  //out the loop
						break;
					}
					
					if(!cycleToBe.equals(decFractionPart.substring(previousStart, nextStart)))
						break;
					
					previousStart = nextStart;
				}
			}
		}
		
		if(isCycle)
			return cycleToBe;
		else
			return null;
	}
	
	private static final int initialScale = 2003; // must be odd
	private static int scale = initialScale;
	private static int maxCycleLength = -1;
}

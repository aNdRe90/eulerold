import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import MyStuff.CombinationGenerator;


public class Problem90 {
	/*
	 * Each of the six faces on a cube has a different digit (0 to 9) written on it; 
	 * the same is done to a second cube. 
	 * By placing the two cubes side-by-side in different positions 
	 * we can form a variety of 2-digit numbers.
	 * 
	 * In fact, by carefully choosing the digits on both cubes it is possible 
	 * to display all of the square numbers below one-hundred: 01, 04, 09, 16, 25, 36, 49, 64, and 81.
	 * 
	 * For example, one way this can be achieved is by placing {0, 5, 6, 7, 8, 9} 
	 * on one cube and {1, 2, 3, 4, 8, 9} on the other cube.
	 * 
	 * However, for this problem we shall allow the 6 or 9 to be turned upside-down 
	 * so that an arrangement like {0, 5, 6, 7, 8, 9} and {1, 2, 3, 4, 6, 7} 
	 * allows for all nine square numbers to be displayed; otherwise it would be impossible to obtain 09.
	 * 
	 * In determining a distinct arrangement we are interested in the digits on each cube, not the order.
	 * 
	 * {1, 2, 3, 4, 5, 6} is equivalent to {3, 6, 4, 1, 2, 5}
	 * {1, 2, 3, 4, 5, 6} is distinct from {1, 2, 3, 4, 5, 9}
	 * 
	 * But because we are allowing 6 and 9 to be reversed, the two distinct sets 
	 * in the last example both represent the extended set {1, 2, 3, 4, 5, 6, 9} 
	 * for the purpose of forming 2-digit numbers.
	 * 
	 * How many distinct arrangements of the two cubes allow 
	 * for all of the square numbers to be displayed?
	 */
	
	/*
	 * SOLUTION:
	 * There are  (10  6)*(10  6) = 44100 possibilities of two 6-number sets of numbers 0-9.
	 * Brute force approach with checking if sets are "valid".
	 */
	public static void main(String[] args) {
		CombinationGenerator combGen = new CombinationGenerator(10, 6);
		String[] numbers = { "01", "04", "06", "16", "25", "36", "46", "64", "81" };
		int result = 0;
		List<int[]> combinations1 = new LinkedList<int[]>();
		List<int[]> combinations2 = new LinkedList<int[]>();
		
		int[] t1 = {5,0,6,7,8,9};
		int[] t2 = {1,2,3,4,6,7};
		change9to6inComb(t1);
		change9to6inComb(t2);
		System.out.println(isPossibleToDisplayNumbersOnCubes(numbers, t1, t2));
		
		while(combGen.hasMore()) {
			int[] tempComb = combGen.getNext();
			int[] comb = change9to6inComb(tempComb);
			
			System.out.println(Arrays.toString(comb));
			
			combinations1.add(comb);
			combinations2.add(comb);
		}
		
		for(int[] comb1 : combinations1)
			for(int[] comb2 : combinations2)		
				if(isPossibleToDisplayNumbersOnCubes(numbers, comb1, comb2)) 
					result++;

		System.out.println(result/2);
	}

	private static int[] change9to6inComb(int[] comb) {
		int[] returned = new int[comb.length];
		
		for(int i=0; i<comb.length; i++)
			if(comb[i]==9)
				returned[i] = 6;
			else
				returned[i] = comb[i];
		
		return returned;
	}

	private static boolean isPossibleToDisplayNumbersOnCubes(String[] numbers,
			int[] comb1, int[] comb2) {
		for(String number : numbers) {
			int digit1 = number.charAt(0) - 0x30;
			int digit2 = number.charAt(1) - 0x30;
			
			if(digit1==9)
				digit1 = 6;
			if(digit2==9)
				digit2 = 6;
			
			if(! isPossibleToDisplayDigits(digit1, digit2, comb1, comb2))
				return false;
		}
		
		return true;
	}

	private static boolean isPossibleToDisplayDigits(int digit1, int digit2,
			int[] comb1, int[] comb2) {
		if(containsDigit(digit1, comb1) && containsDigit(digit2, comb2))
			return true;
		
		if(containsDigit(digit2, comb1) && containsDigit(digit1, comb2))
			return true;
		
		return false;
	}

	private static boolean containsDigit(int digit1, int[] comb1) {
		for(int i : comb1) 
			if(digit1==i)
				return true;
		
		return false;
	}
}



import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import MyStuff.Factorizator;

public class Problem211 {
	/*
	 * For a positive integer n, let S2(n) be the sum of the squares of its divisors. 
	 * For example,
	 * S2(10) = 1 + 4 + 25 + 100 = 130.
	 * 
	 * Find the sum of all n, 0 < n < 64,000,000 such that S2(n) is a perfect square.
	 */
	public static void main(String[] args) {
		final int N = 64;
		long sum = 0L;

		System.out.println(Factorizator.getDivisors(1013L));

		int s = 1000000;
		List<Set<Integer>> divisors = new ArrayList<Set<Integer>>(s);
		for (int i = 0; i < s; i++) {
			System.out.println(i);
			Set<Integer> set = new HashSet<Integer>(15);
			set.add(1);
			divisors.add(set);
		}

		for (int n = 0; n < N; n++) {
			System.out.println(n);

			int from = n * 1000000;
			int to = (n + 1) * 1000000;
			loadDivisors(from, to, divisors);
			int size = divisors.size();

			for (int i = 0; i < size; i++) {
				Set<Integer> divs = divisors.get(i);
				long sumOfSquares = getSumOfSquaresOf(divs);

				if (MyStuff.MyMath.isSquareNumber(sumOfSquares)) {
					sum += (from + i);
				}

				divs.clear();
			}
		}

		//		for (int n = 2; n < 1000000; n++) {
		//			if ((n % 1000) == 0) {
		//				System.out.println(n);
		//			}
		//
		//			Set<Long> divisors = Factorizator.getDivisors(n);
		//			long sumOfSquares = getSumOfSquaresOf(divisors);
		//
		//			if (MyStuff.MyMath.isSquareNumber(sumOfSquares)) {
		//				sum += n;
		//			}
		//		}

		System.out.println("Result = " + sum);
	}

	private static void loadDivisors(int from, int to,
			List<Set<Integer>> divisors) {
		int size = divisors.size();
		for (int i = 0; i < size; i++) {
			Set<Integer> set = divisors.get(i);
			set.clear();

			if ((from == 0) && (i == 0)) {
				continue;
			}
			set.add(1);
			set.add(from + i);
		}

		final int sqrtN = (int) Math.sqrt(to);

		for (int i = 2; i < sqrtN; i++) {
			//System.out.println("i = " + i);

			int cStart = from / i;
			for (int c = cStart;; c++) {
				int value = i * c;
				if (value >= to) {
					break;
				}

				if (value >= from) {
					divisors.get(value - from).add(i);
					divisors.get(value - from).add(c);
				}
			}
		}
	}

	private static long getSumOfSquaresOf(Set<Integer> values) {
		long sum = 0L;

		for (long v : values) {
			sum += (v * v);
		}

		return sum;
	}
}

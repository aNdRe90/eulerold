import java.math.BigInteger;

import MyStuff.Cycle;
import MyStuff.MyMath;

public class Problem64 
{
	/*
	 * All square roots are periodic when written as continued fractions and can be written in the form:
	
	sqrt(N) = a0 + 1/( a1 + 1/( ( a2 + 1/(( a3 + ...)
	
	
	For example, let us consider sqrt(23):
	
	sqrt(23) = 4 + sqrt(23) � 4 = 4 +  	1/ 1/( (sqrt(23) - 4))) = 4 + 1/ ( 1 + (sqrt(23)-3)/7 )

	If we continue we would get the following expansion:
	sqrt(23) = 4 + 	1 / 
  					1 + 1/
  						3 + 1/
  							1 + 1/
  								8 + ...

	The process can be summarised as follows:
	a0 = 4,  a1 = 1,  a2 = 3,  a3 = 1,  a4 = 8
			 a5 = 1,  a6 = 3,  a7 = 1,  a8 = 8

	It can be seen that the sequence is repeating. 
	For conciseness, we use the notation sqrt(23) = [4;(1,3,1,8)], 
	to indicate that the block (1,3,1,8) repeats indefinitely.
	
	The first ten continued fraction representations of (irrational) square roots are:

	sqrt(2)=[1;(2)], period=1
	sqrt(3)=[1;(1,2)], period=2
	sqrt(5)=[2;(4)], period=1
	sqrt(6)=[2;(2,4)], period=2
	sqrt(7)=[2;(1,1,1,4)], period=4
	sqrt(8)[2;(1,4)], period=2
	sqrt(10)=[3;(6)], period=1
	sqrt(11)=[3;(3,6)], period=2
	sqrt(12)= [3;(2,6)], period=2
	sqrt(13)=[3;(1,1,1,1,6)], period=5

	Exactly four continued fractions, for N <= 13, have an odd period.

	How many continued fractions for N <= 10000 have an odd period?
	 */

	public static void main(String[] args) 
	{
		//getPeriodLength(11);
		
		
		int sum = 0;
		for(int i=2; i<10000; i++)   //10000 is square
		{
			System.out.println(i);
			if(!MyMath.isSquareNumber(i) && getPeriodLength(i)%2==1)
				sum++;
		}
		
		System.out.println(sum);
	}
	
	
	//VEEERY TRICKY BUT WORKS:)
	public static int getPeriodLength(int n)
	{
		double sqrtN = Math.sqrt((double)n);

		int a0 = (int) Math.sqrt((double)n);
		int inNumerator = 1;		//in the numerator
		int inDenominator = -a0;  //in the denominator
		
		int[] buf = new int[howMuchToInvestigate];
		for(int i=0; i<howMuchToInvestigate; i++)
		{
			int tempNum = inNumerator;
			int tempDen = inDenominator;
			
			inDenominator = n - inDenominator*inDenominator;
			if(inDenominator%tempNum==0)
			{
				inDenominator /= tempNum;
				tempNum = 1;
			}	
			inNumerator = tempNum*tempDen*(-1);
						
			double allValue = (sqrtN+(double) inNumerator) / (double)inDenominator;  
			buf[i] = (int) allValue;
			
			inNumerator -= buf[i]*inDenominator;
			
			/*int dif = g-d;
			if(dif<0)
			{
				buf[i] = 1;
				g = dif;
			}
			else if(dif>=0)
			{
				int newA = 0;
				
				if(d==1)
				{
					newA = 2*g;
					g*=-1;					
				}
				else
				{
					int gg = g;
					while((g-d)>=-gg)
					{
						g -= d;
						newA++;
					}
				}				
				buf[i] = newA;
				//g*=-1;
			}
			//else
			//	System.out.println("BLAD");		*/
			
			int temp = inDenominator;
			inDenominator = inNumerator;
			inNumerator = temp;
		}
		
		int[] result = Cycle.getCycle(buf);
		return result.length;
	}	
	
	private static int howMuchToInvestigate = 1001;   //last a(n) = a(howMuchToInvestigate)
}



import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import MyStuff.CombinationGenerator;

public class Problem105 {
	/*
	 * Let S(A) represent the sum of elements in set A of size n. 
	 * We shall call it a special sum set if for any two non-empty disjoint subsets, 
	 * B and C, the following properties are true:
	 * 
	 *     S(B) != S(C); that is, sums of subsets cannot be equal.
	 *     If B contains more elements than C then S(B) > S(C).
	 *     
	 * For example, {81, 88, 75, 42, 87, 84, 86, 65} is not a special sum set because 
	 * 65 + 87 + 88 = 75 + 81 + 84, whereas {157, 150, 164, 119, 79, 159, 161, 139, 158} 
	 * satisfies both rules for all possible subset pair combinations and S(A) = 1286.
	 * 
	 * Using sets.txt (right click and "Save Link/Target As..."), a 4K text file 
	 * with one-hundred sets containing seven to twelve elements 
	 * (the two examples given above are the first two sets in the file), 
	 * identify all the special sum sets, A1, A2, ..., Ak, and find the value of 
	 * S(A1) + S(A2) + ... + S(Ak).
	 * 
	 * NOTE: This problem is related to problems 103 and 106.
	 */
	public static void main(String[] args) {
		List<ArrayList<Integer>> sets = readSetsFromFile("sets.txt");
		int sum = 0;
		
		int i = 0;
		for(ArrayList<Integer> set : sets) {
			System.out.println(i++);
			
			if(isSpecialSumSet(set)) {
				sum += getSumOfElements(set);
			}
		}
		
		System.out.println(sum);
	}

	private static int getSumOfElements(ArrayList<Integer> set) {
		int sum = 0;
		
		for(int element : set) 
			sum += element;
		
		return sum;
	}

	private static List<ArrayList<Integer>> readSetsFromFile(String filename) {
		List<ArrayList<Integer>> sets = new LinkedList<ArrayList<Integer>>();
		BufferedReader reader = getBufferedReaderFromFile(filename);
		String line = null;
		
		try {
			while( (line=reader.readLine())!=null  ) {
				sets.add(getSetFromLine(line));
			}
		} catch (IOException e) {e.printStackTrace();}
		
		return sets;
	}

	private static ArrayList<Integer> getSetFromLine(String line) {
		ArrayList<Integer> set = new ArrayList<Integer>(maxSetLength);
		StringTokenizer tokenizer = new StringTokenizer(line, ",");
		
		while(tokenizer.hasMoreTokens()) {
			set.add(Integer.parseInt(tokenizer.nextToken()));
		}
		
		return set;
	}

	private static BufferedReader getBufferedReaderFromFile(String filename) {
		BufferedReader reader = null;
		try	{
			reader = new BufferedReader(new FileReader(filename));
		}
		catch(IOException e) { e.printStackTrace(); }
		
		return reader;
	}
	
	private static boolean isSpecialSumSet(ArrayList<Integer> setArrayList) {	
		int[] set = getArrayFromArrayList(setArrayList);
		
		for(int i=1; i<=set.length/2; i++) {
			CombinationGenerator combGenA = new CombinationGenerator(set.length, i);
			
			while(combGenA.hasMore()) {
				List<Integer> positionsToCombinateFrom = getPositionsToCombinateFrom(set.length);
				int[] combinationA = getCombinationFrom(combGenA.getNext(), positionsToCombinateFrom);
				positionsToCombinateFrom = removeElementsFromPositions(positionsToCombinateFrom, combinationA);
				
				for(int j=1; (j+i)<=set.length; j++) {
					CombinationGenerator combGenB = new CombinationGenerator(set.length - i, j);
					while(combGenB.hasMore()) {
						int[] combinationB = getCombinationFrom(combGenB.getNext(), positionsToCombinateFrom);
						
						int[] subsetA = getSubsetOfElements(set, combinationA);
						int[] subsetB = getSubsetOfElements(set, combinationB);
						
						if(!specialSumSubsetsPropertiesTrue(subsetA, subsetB))
							return false;
					}
				}
			}
		}
		
		return true;		
	}
	
	private static int[] getArrayFromArrayList(ArrayList<Integer> setArrayList) {
		int[] set = new int[setArrayList.size()];
		
		for(int i=0; i<set.length; i++)
			set[i] = setArrayList.get(i);
		
		return set;
	}

	private static int[] getCombinationFrom(int[] combination, 
											List<Integer> positions) {
		int[] combs = combination.clone();
		
		for(int i=0; i<combs.length; i++)
			combs[i] = positions.get(combination[i]);
		
		return combs;
	}

	private static List<Integer> removeElementsFromPositions(
			List<Integer> positions, int[] combination) {
		
		for(int i=0; i<combination.length; i++) {
			if(positions.contains(combination[i])) {
				positions.remove(positions.indexOf(combination[i]));
			}
		}
			
		return positions;
	}

	private static List<Integer> getPositionsToCombinateFrom(int length) {
		List<Integer> positions = new ArrayList<Integer>(length);
		
		for(int i=0; i<length; i++) 
			positions.add(i);
		
		return positions;
	}
	
	private static boolean specialSumSubsetsPropertiesTrue(int[] A, int[] B) {		
		int sumA = getSumOfElements(A);
		int sumB = getSumOfElements(B);
		
		return (A.length==B.length && sumA!=sumB) || (A.length>B.length && sumA>sumB)
				|| (B.length>A.length && sumB>sumA);
	}

	private static int getSumOfElements(int[] a) {
		int sum = 0;
		for(int i : a)
			sum += i;
		
		return sum;
	}

	private static int[] getSubsetOfElements(int[] set, int[] combination) {
		int[] subset = new int[combination.length];
		
		for(int i=0; i<subset.length; i++)
			subset[i] = set[combination[i]];
			
		return subset;
	}
	
	private static int maxSetLength = 12;
}

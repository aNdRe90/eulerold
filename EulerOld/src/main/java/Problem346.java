

import java.math.BigInteger;

public class Problem346 {
	/*
	 * The number 7 is special, because 7 is 111 written in base 2, and 11 written in base 6
	 * (i.e. 7_10 = 11_6 = 111_2). In other words, 7 is a repunit in at least two bases b > 1.
	 * 
	 * We shall call a positive integer with this property a strong repunit. 
	 * It can be verified that there are 8 strong repunits below 50: {1,7,13,15,21,31,40,43}.
	 * 
	 * Furthermore, the sum of all strong repunits below 1000 equals 15864.
	 * Find the sum of all strong repunits below 10^12.
	 */

	/*SOLUTION
	 * y = R(n) = [ b^n - 1 ] / [ b - 1 ]
	 * 
	 * n=2:
	 * 		b + 1 = y
	 * So every value y is repunit in base b+1 of length 2
	 * 
	 * n=3:
	 * 		b^2 + b + 1 = y
	 * 		b^2 + b + (1-y) = 0
	 * 
	 * 		b = 0.5*( -1 + sqrt(4y-3) )
	 * 
	 * 4y-3 = x^2    =>  y = (x^2 + 3)/4
	 * 
	 * 
	 * Wikipedia "Repunit":
	 * The only known numbers that are repunits with at least 3 digits in more than one base 
	 * simultaneously are 31 (111 in base 5, 11111 in base 2) 
	 * and 8191 (111 in base 90, 1111111111111 in base 2). 
	 * The Goormaghtigh conjecture says there are only these two cases.
	 */
	public static void main(String[] args) {
		final long N = (long) 1e12;

		long sum = getSumOfValuesBeingRepunitOfLength3((long) Math
				.sqrt((4L * N) - 3L));
		sum += getSumOfValuesBeingRepunitOfLengthMoreThan3(N);

		sum = sum - 31L - 8191L;
		System.out.println("RESULT = " + sum);
	}

	private static long getSumOfValuesBeingRepunitOfLengthMoreThan3(long limit) {
		BigInteger bigLimit = BigInteger.valueOf(limit);
		long sum = 0;

		for (BigInteger base = BigInteger.valueOf(2L);; base = base
				.add(BigInteger.ONE)) {
			System.out.println("Base = " + base);

			BigInteger minValue = ((base.multiply(base).add(BigInteger.ONE))
					.multiply(base.add(BigInteger.ONE)));
			if (minValue.compareTo(bigLimit) >= 0) {
				break;
			}

			BigInteger denominator = base.subtract(BigInteger.ONE);
			for (int exp = 4;; exp++) {
				BigInteger numerator = base.pow(exp).subtract(BigInteger.ONE);
				BigInteger value = numerator.divide(denominator);

				if (value.compareTo(bigLimit) >= 0) {
					break;
				}

				sum += value.longValue();
			}
		}

		return sum;
	}

	private static long getSumOfValuesBeingRepunitOfLength3(long limit) {
		long sum = 1L;

		for (long x = 5L; x <= limit; x += 2L) {
			System.out.println("X = " + x);
			//x==1 we get base =0,  x==3 we get base = 1  WE DON`T WANT THEM
			//x needs to be odd
			//in order to 0.5*(-1 + x) be an integer

			long numerator = (x * x) + 3L;

			if ((numerator % 4L) == 0L) {
				long y = numerator / 4L;
				sum += y;
			}
		}

		return sum;
	}
}

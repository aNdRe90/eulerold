

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import MyStuff.BigFraction;
import MyStuff.VariationNumber;

public class Problem205 {
	/*
	 * Peter has nine four-sided (pyramidal) dice, each with faces numbered 1, 2, 3, 4.
	 * Colin has six six-sided (cubic) dice, each with faces numbered 1, 2, 3, 4, 5, 6.
	 * 
	 * Peter and Colin roll their dice and compare totals: the highest total wins. 
	 * The result is a draw if the totals are equal.
	 * 
	 * What is the probability that Pyramidal Pete beats Cubic Colin? 
	 * Give your answer rounded to seven decimal places in the form 0.abcdefg
	 */
	public static void main(final String[] args) {
		Player peter = new Player(9, 4);
		Player colin = new Player(6, 6);

		Map<Integer, BigFraction> probabilitiesPeter = peter
				.getValueProbabilities();
		Map<Integer, BigFraction> probabilitiesColin = colin
				.getValueProbabilities();

		BigFraction probabilityPeterWinning = getProbabilityOfPeterWinning(
				probabilitiesPeter, probabilitiesColin);
		System.out.println(probabilityPeterWinning.toBigDecimal(7));
	}

	private static BigFraction getProbabilityOfPeterWinning(
			final Map<Integer, BigFraction> peter,
			final Map<Integer, BigFraction> colin) {
		List<Integer> valuesPeter = getSortedValues(peter.keySet());
		List<Integer> valuesColin = getSortedValues(colin.keySet());

		BigFraction probability = new BigFraction(BigInteger.ZERO);

		for (int peterValue : valuesPeter) {
			BigFraction peterValueProbability = peter.get(peterValue);

			for (int colinValue : valuesColin) {
				if (colinValue >= peterValue) {
					break;
				}

				BigFraction colinValueProbability = colin.get(colinValue);
				BigFraction product = peterValueProbability
						.multiply(colinValueProbability);
				product.reduce();

				probability = probability.add(product);
				probability.reduce();
			}
		}

		return probability;
	}

	private static List<Integer> getSortedValues(final Set<Integer> set) {
		List<Integer> values = new ArrayList<Integer>(set.size());

		for (Integer element : set) {
			values.add(element);
		}

		Collections.sort(values);
		return values;
	}

	private static class Player {
		private final int dice;
		private final int[] values;

		public Player(final int dice, final int maxValue) {
			this.dice = dice;
			this.values = new int[maxValue];
			for (int i = 0; i < maxValue; i++) {
				this.values[i] = i + 1;
			}
		}

		public Map<Integer, BigFraction> getValueProbabilities() {
			Map<Integer, Integer> numerators = new HashMap<Integer, Integer>();
			int denominator = 0;

			VariationNumber var = new VariationNumber(getBases());
			while (!var.isOverflow()) {
				int[] values = var.getValues();
				int sum = getSum(values) + values.length;

				Integer numerator = numerators.get(sum);
				if (numerator == null) {
					numerators.put(sum, 1);
				} else {
					numerators.put(sum, numerator + 1);
				}

				denominator++;
				var.increment();
			}

			BigInteger bigDenominator = BigInteger.valueOf(denominator);
			Map<Integer, BigFraction> probabilities = new HashMap<Integer, BigFraction>();
			for (Integer key : numerators.keySet()) {
				probabilities.put(key,
						new BigFraction(
								BigInteger.valueOf(numerators.get(key)),
								bigDenominator));
			}

			return probabilities;
		}

		private int getSum(final int[] values) {
			int sum = 0;

			for (int value : values) {
				sum += value;
			}

			return sum;
		}

		private int[] getBases() {
			int[] bases = new int[this.dice];

			for (int i = 0; i < bases.length; i++) {
				bases[i] = this.values[this.values.length - 1];
			}

			return bases;
		}
	}
}


/**
 * Represents a fraction with numerator and denominator of LONG type.
 * Implements adding, subtracting, dividing, multiplying, raising to a given power,
 * reducing and reversing.
 * @author Andrzej *
 */
public class Fraction implements Comparable<Fraction>
{
	/**
	 * Creates a fraction with given numerator and denominator.
	 * If given denominator==0, ArithmeticException is thrown.
	 * *
	 * IT DOES NOT REDUCE IT.
	 * @param num numerator
	 * @param den denominator
	 */
	public Fraction(long num, long den)
	{
		if(den==0)
			throw new ArithmeticException("Zero in denominator!");
		
		numerator = num;
		denominator = den;
		
		if(numerator<0 && denominator<0)
		{
			numerator *=-1;
			denominator *=-1;
		}
	}
	
	public int hashCode() 
	{
		return (int) (denominator*17 + numerator*13);
	}
	
	/**
	 * Creates a fraction with given numerator (fraction from long value).
	 * @param val numerator (denominator==1)
	 */
	public Fraction(long val)
	{
		numerator = val;
		denominator = 1L;
	}
	
	/**
	 * Creates a fraction identical to the one given as parameter.
	 * * 
	 * IT DOES NOT REDUCE IT.
	 * @param f fraction to copy.
	 */
	public Fraction(Fraction f)
	{
		numerator = f.numerator;
		denominator = f.denominator;
	}
	
	/**
	 * Reverses the fraction exchanging the values of numerator and denominator.
	 * It does not change this object - only returns reversed fraction.
	 * *  
	 * IT DOES NOT REDUCE IT.
	 * @return fraction created by reversing this fraction.
	 */
	public Fraction reverse()
	{
		return new Fraction(denominator, numerator);
	}
	
	/**
	 * Adds a given fraction to this fraction.
	 * *  
	 * IT DOES NOT REDUCE IT.
	 * @param f fraction to add
	 * @return Fraction being a sum of this and given fraction.
	 */
	public Fraction add(Fraction f)
	{
		return new Fraction(numerator*f.denominator+f.numerator*denominator, denominator*f.denominator);
	}
	
	/**
	 * Subtracts a given fraction from this fraction.
	 * *  
	 * IT DOES NOT REDUCE IT.
	 * @param f fraction to subtract
	 * @return Fraction being a difference between this and given fraction.
	 */
	public Fraction subtract(Fraction f)
	{
		return new Fraction(numerator*f.denominator-f.numerator*denominator, denominator*f.denominator);
	}
	
	/**
	 * Multiplies a given fraction by this fraction.
	 * *  
	 * IT DOES NOT REDUCE IT.
	 * @param f fraction to multiply
	 * @return Fraction being a product of this and given fraction.
	 */
	public Fraction multiply(Fraction f)
	{	
		return new Fraction(numerator*f.numerator, denominator*f.denominator);
	}
	
	/**
	 * Divides this fraction by a given fraction.
	 * If given fraction has numerator==0, ArithmeticException is thrown.
	 * *  
	 * IT DOES NOT REDUCE IT.
	 * @param f fraction to divide by
	 * @return Fraction being a quotient of this and given fraction.
	 */
	public Fraction divide(Fraction f)
	{		
		if(f.numerator==0L)
			throw new ArithmeticException("Dividing by zero!");
		
		return new Fraction(numerator*f.denominator, denominator*f.numerator);
	}
	
	/**
	 * Raises the fraction to the power of given value.
	 * *  
	 * IT DOES NOT REDUCE IT.
	 * @param n
	 * @return Fraction being a nth power of this fraction.
	 */
	public Fraction pow(int n)
	{	
		if(n==0)
		{
			numerator = 1L;
			denominator = 1L;
		}
		else if(n<0)
		{
			reverse();
			n *= -1;
		}		
		
		long tempN = numerator;
		long tempD = denominator;
		for(int i=2; i<n; i++)
		{
			tempN *= numerator;
			tempD *= denominator;
		}
		
		return new Fraction(tempN, tempD);
	}
	
	/**
	 * Reduces the fraction. Reducing makes sure that numerator and denominator
	 * don`t have any common divisor.
	 * @return this after operation
	 */
	public Fraction reduce()
	{
		if(numerator==0)
			return this;
		
		long[] numFactors = MyMath.getPrimeFactorization(numerator);
		long[] denFactors = MyMath.getPrimeFactorization(denominator);
		
		for(int i=0; i<numFactors.length; i++)
			for(int j=0; j<denFactors.length; j++)
			{
				if(numFactors[i]==denFactors[j])
				{
					numerator /= numFactors[i];
					denominator /= numFactors[i];
					
					numFactors[i] = denFactors[j] = 1L;
					//numFactors[i] = denFactors[i] = 1L;  was before
				}
			}
		
		if(numerator<0 && denominator<0)
		{
			numerator *=-1;
			denominator *=-1;
		}
		return this;
	}
	
	/**
	 * Converts a fraction to String of form "numerator/denominator" e.g. 1/3.
	 */
	public String toString()
	{
		return numerator+"/"+denominator;
	}
	
	/**
	 * Compares this fraction with a given object and returns true if they are equal.
	 */
	public boolean equals(Object other)
	{
		Fraction otherFraction = (Fraction) other;
		if(numerator==otherFraction.numerator && numerator==0)
			return true;
		
		return numerator==otherFraction.numerator && denominator==otherFraction.denominator;
	}
	
	/**
	 * Compares this fraction with another fraction.
	 * Returns:
	 * 1 if this fraction is greater than given fraction.
	 * -1 if this fraction is lower than given fraction.
	 * 0 if both fraction represents the same value.
	 */
	public int compareTo(Fraction other)
	{
		if(denominator==other.denominator)
		{
			long difference = numerator-other.numerator;
			
			if(difference>0)
				return 1;
			else if(difference==0)
				return 0;
			else
				return -1;
		}
		
		//long commonDenominator = denominator*other.denominator;
		long newNumeratorThis = numerator*other.denominator;
		long newNumeratorOther = other.numerator*denominator;
		
		long difference = newNumeratorThis-newNumeratorOther;
		
		if(difference>0)
			return 1;
		else if(difference==0)
			return 0;
		else
			return -1;
	}
	
	/**
	 * Converts a fraction to double value;
	 * @return double of the same value as the fraction.
	 */
	public double toDouble()
	{
		return (double) numerator / (double) denominator;
	}
	public long getNumerator()
	{
		return numerator;
	}
	
	public long getDenominator()
	{
		return denominator;
	}
	
	private long numerator;
	private long denominator;
}



import java.util.Set;

import MyStuff.Factorizator;

public class Problem157 {
	/*
	 * Consider the diophantine equation 1/a+1/b= p/10^n 
	 * with a, b, p, n positive integers and a <= b.
	 * 
	 * For n=1 this equation has 20 solutions that are listed below:
	 * 1/1+1/1=20/10 	1/1+1/2=15/10 	1/1+1/5=12/10 	1/1+1/10=11/10 	1/2+1/2=10/10
	 * 1/2+1/5=7/10 	1/2+1/10=6/10 	1/3+1/6=5/10 	1/3+1/15=4/10 	1/4+1/4=5/10
	 * 1/4+1/20=3/10 	1/5+1/5=4/10 	1/5+1/10=3/10 	1/6+1/30=2/10 	1/10+1/10=2/10
	 * 1/11+1/110=1/10 	1/12+1/60=1/10 	1/14+1/35=1/10 	1/15+1/30=1/10 	1/20+1/20=1/10
	 * 
	 * How many solutions has this equation for 1 <= n <= 9?
	 */

	/*
	 * SOLUTION:
	 * 1/a + 1/b = p/10^n
	 * If b>=a then b = a+k  where k=0,1,2,...
	 * 
	 * (a+b)/ab = p/10^n
	 * (2a+k)/(a^2 + ak) = p/10^n
	 * p*a^2 + (pk - 2*10^n)*a + (-k*10^n) = 0
	 * 
	 * delta = p^2k^2 + 4*10^(2n)
	 * a = 1/2p * (2*10^n - pk + x)    where x = sqrt(delta)
	 * 
	 * Thus:
	 * x^2 = p^2k^2 + 4*10^(2n)
	 * (x-pk)(x+pk) = 4*10^(2n)
	 * 
	 * di = divisor of 4*10^(2n)
	 * x+pk = d1
	 * x-pk = d2			//d1>=d2
	 * pk = 0.5*(d1 - d2)   
	 * then: x = d1 - pk
	 * 
	 * if(d1==d2) then pk=0. it can only mean that k=0
	 * we therefore have
	 * delta = 4*10^(2n)
	 * a = 1/2p * 4*10^n  => ap = 2*10^n
	 * so a is divisor of 2*10^n
	 */
	public static void main(String[] args) {
		long solutions = 0L;

		for (long n = 1L; n <= 9L; n++) {
			long R = 4L * (long) (Math.pow(10.0, 2.0 * n));
			long sqrtR = 2L * (long) (Math.pow(10.0, n));

			Set<Long> divisors = Factorizator.getDivisorsBetter(R);

			for (long divisor : divisors) {
				if (divisor <= sqrtR) {
					long d1 = R / divisor;
					long d2 = divisor;

					if (((d1 - d2) % 2L) == 0L) {
						long pk = (d1 - d2) / 2L;

						if (pk == 0L) {
							long ap = 2L * (long) (Math.pow(10.0, n));
							Set<Long> divisorsAP = Factorizator
									.getDivisorsBetter(ap);
							for (long p : divisorsAP) {
								long a = ap / p;
								System.out.println("1/" + a + "+1/" + a + "="
										+ p + "/10^" + n);
								solutions++;
							}
						} else {
							long x = d1 - pk;
							long numerator = 2L * (long) (Math.pow(10.0, n));
							numerator -= pk;
							numerator += x;

							if ((numerator % 2L) == 0L) {
								numerator /= 2L;
								Set<Long> divisorsPK = Factorizator
										.getDivisorsBetter(pk);

								for (long p : divisorsPK) {
									if ((numerator % p) == 0L) {
										long k = pk / p;
										long a = numerator;
										System.out.println("1/" + a + "+1/"
												+ (a + k) + "=" + p + "/10^"
												+ n);
										solutions++;
									}
								}
							}
						}
					}
				}
			}
		}

		System.out.println("RESULT = " + solutions);
	}
}



import java.util.ArrayList;
import java.util.Arrays;

import numbermind.NumberMindHint;
import numbermind.NumberMindSolver;

public class Problem185 {
	/*
	 * The game Number Mind is a variant of the well known game Master Mind.
	 * 
	 * Instead of coloured pegs, you have to guess a secret sequence of digits. 
	 * After each guess you're only told in how many places you've guessed the correct digit. 
	 * So, if the sequence was 1234 and you guessed 2036, 
	 * you'd be told that you have one correct digit; 
	 * however, you would NOT be told that you also have another digit in the wrong place.
	 * 
	 * For instance, given the following guesses for a 5-digit secret sequence,
	 * 90342 ;2 correct
	 * 70794 ;0 correct
	 * 39458 ;2 correct
	 * 34109 ;1 correct
	 * 51545 ;2 correct
	 * 12531 ;1 correct
	 * 
	 * The correct sequence 39542 is unique.
	 * 
	 * Based on the following guesses,
	 * 5616185650518293 ;2 correct
	 * 3847439647293047 ;1 correct
	 * 5855462940810587 ;3 correct
	 * 9742855507068353 ;3 correct
	 * 4296849643607543 ;3 correct
	 * 3174248439465858 ;1 correct
	 * 4513559094146117 ;2 correct
	 * 7890971548908067 ;3 correct
	 * 8157356344118483 ;1 correct
	 * 2615250744386899 ;2 correct
	 * 8690095851526254 ;3 correct
	 * 6375711915077050 ;1 correct
	 * 6913859173121360 ;1 correct
	 * 6442889055042768 ;2 correct
	 * 2321386104303845 ;0 correct
	 * 2326509471271448 ;2 correct
	 * 5251583379644322 ;2 correct
	 * 1748270476758276 ;3 correct
	 * 4895722652190306 ;1 correct
	 * 3041631117224635 ;3 correct
	 * 1841236454324589 ;3 correct
	 * 2659862637316867 ;2 correct
	 * 
	 * Find the unique 16-digit secret sequence.
	 */
	public static void main(String[] args) {
		NumberMindHint[] instance = getTestInstance();

		StringBuffer result = NumberMindSolver
				.solveNEW(new ArrayList<NumberMindHint>(Arrays.asList(instance)));
		System.out.println("RESULT = " + result);
		System.out.println(NumberMindSolver.checkResult(instance, result));
	}

	private static NumberMindHint[] getInstance() {
		NumberMindHint[] instance = new NumberMindHint[] {
				new NumberMindHint(new StringBuffer("5616185650518293"), 2),
				new NumberMindHint(new StringBuffer("3847439647293047"), 1),
				new NumberMindHint(new StringBuffer("5855462940810587"), 3),
				new NumberMindHint(new StringBuffer("9742855507068353"), 3),
				new NumberMindHint(new StringBuffer("4296849643607543"), 3),
				new NumberMindHint(new StringBuffer("3174248439465858"), 1),
				new NumberMindHint(new StringBuffer("4513559094146117"), 2),
				new NumberMindHint(new StringBuffer("7890971548908067"), 3),
				new NumberMindHint(new StringBuffer("8157356344118483"), 1),
				new NumberMindHint(new StringBuffer("2615250744386899"), 2),
				new NumberMindHint(new StringBuffer("8690095851526254"), 3),
				new NumberMindHint(new StringBuffer("6375711915077050"), 1),
				new NumberMindHint(new StringBuffer("6913859173121360"), 1),
				new NumberMindHint(new StringBuffer("6442889055042768"), 2),
				new NumberMindHint(new StringBuffer("2321386104303845"), 0),
				new NumberMindHint(new StringBuffer("2326509471271448"), 2),
				new NumberMindHint(new StringBuffer("5251583379644322"), 2),
				new NumberMindHint(new StringBuffer("1748270476758276"), 3),
				new NumberMindHint(new StringBuffer("4895722652190306"), 1),
				new NumberMindHint(new StringBuffer("3041631117224635"), 3),
				new NumberMindHint(new StringBuffer("1841236454324589"), 3),
				new NumberMindHint(new StringBuffer("2659862637316867"), 2) };

		return instance;
	}

	private static NumberMindHint[] getTestInstance() {
		NumberMindHint[] instance = new NumberMindHint[] {
				new NumberMindHint(new StringBuffer("90342"), 2),
				new NumberMindHint(new StringBuffer("70794"), 0),
				new NumberMindHint(new StringBuffer("39458"), 2),
				new NumberMindHint(new StringBuffer("34109"), 1),
				new NumberMindHint(new StringBuffer("51545"), 2),
				new NumberMindHint(new StringBuffer("12531"), 1) };

		return instance;
	}
}



import java.math.BigInteger;

public class Problem218 {
	/*
	 * Consider the right angled triangle with sides a=7, b=24 and c=25. 
	 * The area of this triangle is 84, which is divisible 
	 * by the perfect numbers 6 and 28.
	 * 
	 * Moreover it is a primitive right angled triangle as 
	 * gcd(a,b)=1 and gcd(b,c)=1.
	 * Also c is a perfect square.
	 * 
	 * We will call a right angled triangle perfect if
	 * -it is a primitive right angled triangle
	 * -its hypotenuse is a perfect square
	 * 
	 * We will call a right angled triangle super-perfect if
	 * -it is a perfect right angled triangle and
	 * -its area is a multiple of the perfect numbers 6 and 28.
	 * 
	 * How many perfect right-angled triangles with c<=10^16 exist that are not super-perfect?
	 */

	/*
	 * SOLUTION:
	 * Primitive pythagorean triplet is:
	 * a = m^2 - n^2
	 * b = 2mn
	 * c = m^2 + n^2
	 * 
	 * In this case we want c=X^2  then m^2 + n^2 = X^2
	 * 
	 * So we need another triplet and thus iterating through
	 * p,q  where 
	 * m = q^2 - p^2
	 * n = 2pq
	 * X = p^2 + q^2
	 * 
	 * we want X<=sqrt(10e16)
	 * 
	 * Area of triangle is (m^2 - n^2)*mn
	 */
	private static BigInteger two = BigInteger.valueOf(2L);
	//if divided by 6 and 28 then divided by 84
	private static BigInteger modulo = BigInteger.valueOf(84L);

	public static void main(String[] args) {
		final long N = (long) 1e16;
		final long sqrtN = (long) Math.sqrt(N);

		long result = 0L;
		for (long p = 1L; ((p * p) + ((p + 1) * (p + 1))) <= sqrtN; p++) {
			System.out.println(p);

			for (long q = (p + 1L); ((p * p) + (q * q)) <= sqrtN; q += 2L) {
				if (MyStuff.MyMath.GCD(p, q) == 1L) {
					long m = (q * q) - (p * p);
					long n = 2 * q * p;
					long X = (q * q) + (p * p);

					long a = (m * m) - (n * n);
					if (a < 0L) {
						a = -a;
					}
					long b = 2L * m * n;
					long c = X * X;

					if (!MyStuff.MyMath.isSquareNumber(c)) {
						System.out.println("ADAD!!!!!!!");
					}

					BigInteger area = getArea(a, b);
					if (!area.mod(modulo).equals(BigInteger.ZERO)) {
						result++;
					}
				}
			}
		}

		System.out.println("RESULT = " + result);
	}

	private static BigInteger getArea(long a, long b) {
		BigInteger area = BigInteger.valueOf(a);
		area = area.multiply(BigInteger.valueOf(b));
		area = area.divide(two);

		return area;
	}
}

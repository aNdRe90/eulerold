

public class Problem174 {
	/*
	 * SIMILIAR TO 173!
	 * We shall define a square lamina to be a square outline with a square "hole" 
	 * so that the shape possesses vertical and horizontal symmetry.
	 * 
	 * Given eight tiles it is possible to form a lamina in only one way: 
	 * 3x3 square with a 1x1 hole in the middle. 
	 * However, using thirty-two tiles it is possible to form two distinct laminae.
	 * 
	 * If t represents the number of tiles used, we shall say that t = 8 is type L(1) 
	 * and t = 32 is type L(2).
	 * 
	 * Let N(n) be the number of t <= 1000000 such that t is type L(n); for example, N(15) = 832.
	 * 
	 * What is sum N(n) for 1 <= n <= 10?
	 */

	/*SOLUTION:
	 * [look at solution to problem 173]
	 */
	public static void main(final String[] args) {
		final long T = 1000000L;
		int Nfrom = 1;
		int Nto = 10;
		final int[] Ns = new int[Nto + 1]; //Ns[1] = N(1)

		for (long t = 8L; t <= T; t++) {
			System.out.println(t);

			long maxHoleSize = (t - 4L) / 4L;
			int type = 0;

			for (long holeSize = 1L; holeSize <= maxHoleSize; holeSize++) {
				long temp = (holeSize * holeSize) + t;
				if (MyStuff.MyMath.isSquareNumber(temp)) {
					long numerator = (long) Math.sqrt(temp) - holeSize;
					if ((numerator % 2L) == 0L) {
						type++;
					}
				}
			}

			if ((type >= Nfrom) && (type <= Nto)) {
				Ns[type]++;
			}
		}

		int sum = getSum(Ns);
		System.out.println(sum);
	}

	private static int getSum(final int[] values) {
		int sum = 0;

		for (int v : values) {
			sum += v;
		}

		return sum;
	}
}



import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Problem127 {
	/*
	 * The radical of n, rad(n), is the product of distinct 
	 * prime factors of n. For example, 504 = 2^3 � 3^2 � 7, 
	 * so rad(504) = 2 � 3 � 7 = 42.
	 * 
	 * We shall define the triplet of positive integers (a, b, c) 
	 * to be an abc-hit if:
	 * 
	 *     GCD(a, b) = GCD(a, c) = GCD(b, c) = 1
	 *     a < b
	 *     a + b = c
	 *     rad(abc) < c
	 *     
	 * For example, (5, 27, 32) is an abc-hit, because:
	 * 
	 * GCD(5, 27) = GCD(5, 32) = GCD(27, 32) = 1
	 * 5 < 27
	 * 5 + 27 = 32
	 * rad(4320) = 30 < 32
	 * 
	 * It turns out that abc-hits are quite rare and there are only 
	 * thirty-one abc-hits for c < 1000, with sum(c) = 12523.
	 * 
	 * Find sum(c) for c < 120000.
	 * 
	 * Note: This problem has been changed recently, 
	 * please check that you are using the right parameters.
	 */
	
	/*
	 * SOLUTION: Brute force, takes about 30 minutes to compute.
	 */
	public static void main(String[] args) {
		final int cBound = 120000;
		long sum = 0L;		
		primeFactors = getPrimeFactors(cBound);
		
		for(int a=1; a<=(cBound/2); a++) {
			System.out.println(a);
			for(int b=a+1; a+b<cBound; b++) {
				int c = a+b;
				
				if(areABCHit(a,b,c)) {
					sum += (long)c;
				}
			}
		}

		System.out.println(sum);
	}
	
	private static Map<Integer, long[]> primeFactors;

	private static boolean areABCHit(int a, int b, int c) {
		if(gcd(a,b)!=1L || gcd(a,c)!=1L || gcd(b,c)!=1L) {
			return false;
		}
		
		return rad(a,b,c) < (long)c;
	}

	private static long rad(int a, int b, int c) {
		//computes rad(a*b*c) using the fact that if:
		//X = a^p * b^q * c^r
		//Y = d^s * e^t * f^u
		//then rad(X) = a*b*c,  rad(Y) = d*e*f
		//rad(X*Y) = a*b*c*d*e*f
			
		//and after the gcd the check before we can do this:
		long rad = getProductOfElements(primeFactors.get(a));
		rad *= getProductOfElements(primeFactors.get(b));
		rad *= getProductOfElements(primeFactors.get(c));		
		
		return rad;
	}

	private static long getProductOfElements(long[] elements) {
		long product = 1L;
		
		for(long element : elements) {
			product *= element;
		}
		
		return product;
	}

	private static long gcd(int a, int b) {
		long[] primeFactorsA = primeFactors.get(a);
		long[] primeFactorsB = primeFactors.get(b);
		
		long gcd = 1L;

		for(long factorA : primeFactorsA) {
			//as the primeFactorsB is already sorted
			if(Arrays.binarySearch(primeFactorsB, factorA)>=0) {
				gcd = factorA;
			}
		}
		
		return gcd;
	}

	private static Map<Integer, long[]> getPrimeFactors(int bound) {
		Map<Integer, long[]> primeFactors = 
				new HashMap<Integer, long[]>(bound);		
		primeFactors.put(1, new long[0]);
		
		for(int i=2; i<=bound; i++) {
			primeFactors.put(i, MyStuff.MyMath.getPrimeDivisors(i));
		}
		
		return primeFactors;
	}
}

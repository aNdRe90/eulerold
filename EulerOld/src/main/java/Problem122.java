

import java.util.LinkedList;
import java.util.List;

import MyStuff.Node;
import MyStuff.SearchingTree;

public class Problem122 {
	/*
	 * The most naive way of computing n^15 requires fourteen multiplications:
	 * 
	 * n � n � ... � n = n^15
	 * 
	 * But using a "binary" method you can compute it in six multiplications:
	 * 
	 * n � n = n^2
	 * n^2 � n^2 = n^4
	 * n^4 � n^4 = n^8
	 * n^8 � n^4 = n^12
	 * n^12 � n^2 = n^14
	 * n^14 � n = n^15
	 * 
	 * However it is yet possible to compute it in only five multiplications:
	 * 
	 * n � n = n^2
	 * n^2 � n = n^3
	 * n^3 � n^3 = n^6
	 * n^6 � n^6 = n^12
	 * n^12 � n^3 = n^15
	 * 
	 * We shall define m(k) to be the minimum number of multiplications to 
	 * compute n^k; for example m(15) = 5.
	 * 
	 * For 1 <= k <= 200, find sum of m(k).
	 * 
	 */
	public static void main(String[] args) {
		final int N = 200;
		int sum = 0;
		
		for(int i=2; i<=N; i++) {
			System.out.println(i);
			MyNode root = new MyNode(new int[]{1,2}, i);
			SearchingTree tree = new SearchingTree(root);
			Node resultNode = tree.getBreadthSearchFirstResultNode();
			
			sum += resultNode.getTreeLevel();
		}
		
		System.out.println(sum);
	}
}

class MyNode extends Node {
	private int[] values;
	private int biggestParentValue;
	private int resultValue;
	
	public MyNode(int[] values, int resultValue) {
		if(values==null || values.length==0)
			throw new IllegalArgumentException("No value given. You have to give some.");
		
		this.values = values;
		this.resultValue = resultValue;
	}
	
	@Override
	public List<Node> getChildrenNodes() {
		List<Node> childrenNodes = new LinkedList<Node>();
		biggestParentValue = values[values.length-1];
		
		for(int i=0; i<values.length; i++) {
			int[] childrenValues = getChildrenValues(values[i]);
			Node children = new MyNode(childrenValues, resultValue);
			children.setTreeLevel(treeLevel+1);
			childrenNodes.add(children);
		}
		
		return childrenNodes;
	}

	private int[] getChildrenValues(int parentValue) {
		int[] childrenValues = new int[values.length+1];
		for(int i=0; i<values.length; i++)
			childrenValues[i] = values[i];
		
		childrenValues[childrenValues.length-1] = biggestParentValue + parentValue;
		
		return childrenValues;
	}

	@Override
	public Node clone() {
		Node cloned = new MyNode(this.values, resultValue);
		cloned.setTreeLevel(treeLevel);
		
		return cloned;
	}

	@Override
	public boolean isResultNode() {
		return values[values.length-1]==resultValue;
	}	
}

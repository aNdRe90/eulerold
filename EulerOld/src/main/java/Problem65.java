import java.math.BigInteger;


public class Problem65 
{
	/*
	 * The infinite continued fraction can be written, sqrt(2) = [1;(2)], 
	 * (2) indicates that 2 repeats ad infinitum. In a similar way, sqrt(23) = [4;(1,3,1,8)].
	 * 
	 * It turns out that the sequence of partial values of continued fractions 
	 * for square roots provide the best rational approximations.
	 * 
	 * Hence the sequence of the first ten convergents for sqrt(2) are:
		1, 3/2, 7/5, 17/12, 41/29, 99/70, 239/169, 577/408, 1393/985, 3363/2378, ...

		What is most surprising is that the important mathematical constant,
			e = [2; 1,2,1, 1,4,1, 1,6,1 , ... , 1,2k,1, ...].

		The first ten terms in the sequence of convergents for e are:
			2, 3, 8/3, 11/4, 19/7, 87/32, 106/39, 193/71, 1264/465, 1457/536, ...

		The sum of digits in the numerator of the 10th convergent is 1+4+5+7=17.

		Find the sum of digits in the numerator of the 100th convergent of the continued fraction for e.
	 */
	
	/*
	 * Lets assume that the result fraction won`t need to be reduced as my BigFraction class has no reducing implemented yet.
	 * Yeah, there was no need:)
	 */
	public static void main(String[] args) 
	{
		int whichConvergent = 100;
		
		long[] eContinuedFraction = new long[whichConvergent-1];
		long[] part = { 1, 2, 1};
		
		for(int i=0, j=0; i<eContinuedFraction.length; i++)
		{
			eContinuedFraction[i] = part[j++];
			if(j==part.length)
			{
				j = 0;
				part[1] += 2;
			}
		}
		
		BigFraction resultConvergent = new BigFraction(BigInteger.ZERO);
		for(int i=(eContinuedFraction.length-1); i>=0; i--)
			resultConvergent = resultConvergent.add(new BigFraction(new BigInteger(Long.toString(eContinuedFraction[i])))).reverse();
		
		resultConvergent = resultConvergent.add(new BigFraction(new BigInteger("2")));
		BigInteger numerator = resultConvergent.getNumerator();
		
		String num = numerator.toString();
		long sum = 0L;
		for(int i=0; i<num.length(); i++)
			sum += (num.charAt(i) - 0x30);
			
		System.out.println(sum);
		System.out.println(resultConvergent);
	}
}

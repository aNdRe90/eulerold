import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;


public class Problem82
{
	/*
	 * NOTE: This problem is a more challenging version of Problem 81.

		The minimal path sum in the 5 by 5 matrix below, 
		by starting in any cell in the left column and finishing in any cell in the right column, 
		and only moving up, down, and right, is indicated in red and bold; the sum is equal to 994.
		
		131	673	234	103	18
		201	96	342	965	150
		630	803	746	422	111
		537	699	497	121	956
		805	732	524	37	331

		Find the minimal path sum, in matrix.txt (right click and 'Save Link/Target As...'), 
		a 31K text file containing a 80 by 80 matrix, from the left column to the right column.
	 */
	
	public static void main(String[] args) 
	{
		matrix = readMatrix("matrix.txt");
		
		for(int col=(columns-2); col>=0; col--)
		{
			int[] newFilledColumn = new int[rows];
			
			for(int rowLeft=0; rowLeft<rows; rowLeft++)
			{
				int minimalSumForThisRow = Integer.MAX_VALUE;
				for(int rowRight=0; rowRight<rows; rowRight++)
				{
					int currentSum = getMinimalSumBetween(col, rowLeft, rowRight);
					if(currentSum<minimalSumForThisRow)
						minimalSumForThisRow = currentSum;
				}
				newFilledColumn[rowLeft] = minimalSumForThisRow;
			}	
			
			for(int i=0; i<rows; i++)
				matrix[i][col] = newFilledColumn[i];
		}
		
		int minInFirstCol = Integer.MAX_VALUE;
		for(int i=0; i<rows; i++)
			if(matrix[i][0]<minInFirstCol)
				minInFirstCol = matrix[i][0];
		
		System.out.println(minInFirstCol);
	}
	
	private static int getMinimalSumBetween(int columnLeft, int rowLeft, int rowRight) //columnRight == columnLeft+1
	{
		int currentValue = matrix[rowLeft][columnLeft];
		
		if(rowLeft==rowRight)
			return matrix[rowRight][columnLeft] + matrix[rowRight][columnLeft+1];
		
		int difference = rowRight - rowLeft;
		boolean goDown = true;
		if(difference<0) 
		{
			goDown = false; //goUp
			difference = -difference;
		}
		difference++;
		
		int minSum = Integer.MAX_VALUE;
		for(int positionOfGoingRight=0; positionOfGoingRight<difference; positionOfGoingRight++)
		{
			int currentSum = 0;
			int currentCol = columnLeft;
			int currentRow = rowLeft;
			
			for(int i=0; i<difference; i++)
			{
				if(i==positionOfGoingRight)
				{
					currentSum += matrix[currentRow][currentCol+1];
					currentCol++;
				}
				else if(goDown)
					currentSum += matrix[++currentRow][currentCol];	
				else //goUp
					currentSum += matrix[--currentRow][currentCol];	
			}
			
			if(currentSum<minSum)
				minSum = currentSum;
		}	
		
		return minSum + currentValue;
	}
	
	private static int[][] readMatrix(String filename)
	{
		BufferedReader f = null;		
		try	{	f = new BufferedReader(new FileReader(filename));	}
		catch(IOException e)	{	e.printStackTrace();		}
		
		String line = null;			
		int row = 0;
		int[][] matrix = new int[rows][columns];
		do
		{
			try {	line = f.readLine();	} 
			catch(IOException ee) { ee.printStackTrace(); }
			
			if(line==null)
				break;
			
			StringTokenizer st = new StringTokenizer(line, ",");
			int col = 0;
			while(st.hasMoreTokens())
				matrix[row][col++] = Integer.parseInt(st.nextToken());
			
			row++;
		}
		while(true);
		
		return matrix;
	}
	
	private static final int rows = 80;
	private static final int columns = 80;
	private static int[][] matrix;
}

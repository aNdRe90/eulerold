import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;


public class Problem81 
{
	/*
	 * In the 5 by 5 matrix below, the minimal path sum from the top left to the bottom right, 
	 * by only moving to the right and down, is indicated in bold red and is equal to 2427.
	
			131	673	234	103	18
			201	96	342	965	150
			630	803	746	422	111
			537	699	497	121	956
			805	732	524	37	331	

		Find the minimal path sum, in matrix.txt (right click and 'Save Link/Target As...'), 
		a 31K text file containing a 80 by 80 matrix, 
		from the top left to the bottom right by only moving right and down.
	 */
	public static void main(String[] args) 
	{
		int[][] matrix = readMatrix("matrix.txt");
		
		for(int row=(rows-2); row>=0; row--)
			matrix[row][columns-1] += matrix[row+1][columns-1];
		
		for(int column=(columns-2); column>=0; column--)
			matrix[rows-1][column] += matrix[rows-1][column+1];
		
		
		int currentRow = rows - 2;
		int currentColumn = columns - 2;
		
		while(currentRow>0 && currentColumn>0)
		{			
			int currentRight = matrix[currentRow][currentColumn+1];
			int currentDown = matrix[currentRow+1][currentColumn];
			int currentLower = currentDown<currentRight ? currentDown : currentRight;
			
			matrix[currentRow][currentColumn] += currentLower;
			
			for(int row=(currentRow-1); row>=0; row--)
			{
				int right = matrix[row][currentColumn+1];
				int down = matrix[row+1][currentColumn];
				int lower = down<right ? down : right;
				
				matrix[row][currentColumn] += lower;
			}
			
			for(int column=(currentColumn-1); column>=0; column--)
			{
				int right = matrix[currentRow][column+1];
				int down = matrix[currentRow+1][column];
				int lower = down<right ? down : right;
				
				matrix[currentRow][column] += lower;
			}
			
			currentRow--;
			currentColumn--;
		}
		
		int minSum = matrix[0][1] < matrix[1][0] ? matrix[0][1] : matrix[1][0];
		minSum += matrix[0][0];
		System.out.println(minSum);
	}
	
	private static int[][] readMatrix(String filename)
	{
		BufferedReader f = null;		
		try	{	f = new BufferedReader(new FileReader(filename));	}
		catch(IOException e)	{	e.printStackTrace();		}
		
		String line = null;			
		int row = 0;
		int[][] matrix = new int[rows][columns];
		do
		{
			try {	line = f.readLine();	} 
			catch(IOException ee) { ee.printStackTrace(); }
			
			if(line==null)
				break;
			
			StringTokenizer st = new StringTokenizer(line, ",");
			int col = 0;
			while(st.hasMoreTokens())
				matrix[row][col++] = Integer.parseInt(st.nextToken());
			
			row++;
		}
		while(true);
		
		return matrix;
	}
	
	private static final int rows = 80;
	private static final int columns = 80;
}



import java.util.ArrayList;
import java.util.List;

import MyStuff.Prime;

public class Problem347 {
	/*
	 * The largest integer <= 100 that is only divisible by both the primes 2 and 3 is 96, 
	 * as 96=32*3=2^5*3. For two distinct primes p and q let M(p,q,N) be the largest positive 
	 * integer <=N only divisible by both p and q and M(p,q,N)=0
	 * if such a positive integer does not exist.
	 * 
	 * E.g. M(2,3,100)=96.
	 * M(3,5,100)=75 and not 90 because 90 is divisible by 2 ,3 and 5.
	 * Also M(2,73,100)=0 because there does not exist a positive integer <= 100 
	 * that is divisible by both 2 and 73.
	 * 
	 * Let S(N) be the sum of all distinct M(p,q,N). S(100)=2262.
	 * Find S(10 000 000).
	 */
	public static void main(String[] args) {
		final long N = (long) 1e7;
		long sum = 0L;

		List<Long> primes = new ArrayList<Long>(665000);
		Prime.loadPrimesUnder(N / 2L, primes);
		int size = primes.size();
		List<Double> logPrimes = getLog(primes);

		for (int i = 0; i < size; i++) {
			long p = primes.get(i);
			System.out.println("p = " + p);

			for (int j = i + 1; j < size; j++) {
				long q = primes.get(j);
				double logQ = logPrimes.get(j);

				long largest = 0L;
				for (int pExp = 1;; pExp++) {
					long pFactor = (long) Math.pow(p, pExp);

					if ((pFactor * q) > N) {
						break;
					}

					double temp = (double) N / (double) pFactor;
					int maxQexp = (int) (Math.log(temp) / logQ);

					long qFactor = (long) Math.pow(q, maxQexp);
					long current = pFactor * qFactor;

					if ((current <= N) && (current > largest)) {
						largest = current;
					}
				}

				sum += largest;
			}
		}

		System.out.println("RESULT = " + sum);
	}

	private static List<Double> getLog(List<Long> primes) {
		List<Double> logPrimes = new ArrayList<Double>(primes.size());

		for (long prime : primes) {
			logPrimes.add(Math.log(prime));
		}

		return logPrimes;
	}
}



import java.util.ArrayList;
import java.util.List;

import MyStuff.Prime;

public class Problem193 {
	/*
	 * A positive integer n is called squarefree, if no square of a prime divides n, 
	 * thus 1, 2, 3, 5, 6, 7, 10, 11 are squarefree, but not 4, 8, 9, 12.
	 * 
	 * How many squarefree numbers are there below 2^50?
	 */

	/*SOLUTION
	 * I computes NOTsquarefree numbers and subtract them fron N.
	 * 
	 * If we divide N / 4 then we get all the numbers which divide by 4.
	 * If we divide N / 9 then we get all the numbers which divide by 9.
	 * 
	 * If we add them we get all the numbers which divide by 4 or 9 
	 * BUT WE COUNTED TWICE NUMBERS THAT DIVIDE BY 36.
	 * 
	 * So what we do is to:
	 * div = N / 4   				add to sum
	 * 		div2 = div / 9			subtract from sum
	 * 			div3 = div2 / 25	add to sum
	 * 			...
	 * 		...
	 * ...
	 * 
	 * N = 36
	 * Example for squares: 4,9,25   (primes 2,3,5 because 5<sqrt(N) and 7>sqrt(N))
	 * 
	 * 36 / 4 = 9		add
	 * 		9 / 9 = 1 	subtract
	 * 36 / 9 = 4		add
	 * 		4 / 25 = 0  BREAK
	 * 36 / 25 = 1		add
	 * BREAK
	 * 
	 * RESULT = N - (9-1+4+1) = 36 - 13 = 23
	 */

	private static final long N = (long) Math.pow(2.0, 50);
	private static final long sqrtN = (long) Math.sqrt(N);
	//private static final List<Long> primes = getPrimes(sqrtN);
	private static final List<Long> primeSquares = getPrimeSquares(sqrtN);
	private static final int size = primeSquares.size();

	private static long result = 0L;

	public static void main(String[] args) {
		//bruteForce();

		long div = N;
		boolean add = true;

		getResult(div, 0, add);

		System.out.println("RESULT = " + (N - result));
	}

	private static void getResult(long div, int index, boolean add) {
		for (int i = index; i < size; i++) {
			long primeSquare = primeSquares.get(i);

			if (primeSquare > div) {
				break;
			}

			long innerDiv = div / primeSquare;
			if (add) {
				result += innerDiv;
			} else {
				result -= innerDiv;
			}

			getResult(innerDiv, i + 1, !add);
		}
	}

	private static List<Long> getPrimeSquares(long n) {
		List<Long> primeSquares = new ArrayList<Long>();
		primeSquares.add(4L);

		for (long i = 3L; i <= n; i++) {
			if (Prime.isPrime(i)) {
				primeSquares.add(i * i);
			}
		}

		return primeSquares;
	}

	//	private static List<Long> getPrimes(long n) {
	//		List<Long> primes = new ArrayList<Long>();
	//		primes.add(2L);
	//
	//		for (long i = 3L; i <= n; i++) {
	//			if (Prime.isPrime(i)) {
	//				primes.add(i);
	//			}
	//		}
	//
	//		return primes;
	//	}
	//
	//	private static void bruteForce() {
	//		long result = 0L;
	//
	//		for (long i = 1; i <= N; i++) {
	//			if (isSquareFree(i)) {
	//				System.out.println("SquareFree = " + i);
	//				result++;
	//			}
	//		}
	//
	//		System.out.println("BRUTE FORCE RESULT = " + result);
	//	}
	//
	//	private static boolean isSquareFree(long n) {
	//		for (long prime : primes) {
	//			long primeSquare = prime * prime;
	//			if (primeSquare > n) {
	//				break;
	//			}
	//
	//			if ((n % primeSquare) == 0L) {
	//				System.out.println(n + "%" + primeSquare + "==0");
	//				return false;
	//			}
	//		}
	//
	//		return true;
	//	}
}

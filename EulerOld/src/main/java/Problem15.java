import java.math.BigInteger;


public class Problem15 
{
	/*
	 * Starting in the top left corner of a 2�2 grid, 
	 * there are 6 routes (without backtracking) to the bottom right corner.
	 * 
	 * How many routes are there through a 20�20 grid?
	 */
	
	
	//SOLUTION (my knowledge from discrete mathematics) for n x n grid is newton`s symbol  (2n  n)
	public static void main(String[] args) 
	{
		//lets just calculate (40  20) 
		BigInteger numerator = new BigInteger("21");
		for(int i=1; i<20; i++)
			numerator = numerator.multiply(new BigInteger(Integer.toString(21+i)));
		
		BigInteger denominator = BigInteger.ONE;
		for(int i=1; i<20; i++)
			denominator = denominator.multiply(new BigInteger(Integer.toString(1+i)));
		
		BigInteger result = numerator.divide(denominator);		
		System.out.println(result);
	}
}

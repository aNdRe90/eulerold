

import java.math.BigDecimal;
import java.math.BigInteger;

public class Problem190 {
	/*
	 * Let Sm = (x1, x2, ... , xm) be the m-tuple of positive real numbers 
	 * with x1 + x2 + ... + xm = m for which Pm = x1 * x2^2 * ... * xm^m is maximised.
	 * 
	 * For example, it can be verified that [P10] = 4112
	 * ([ ] is the integer part function).
	 * 
	 * Find sum [Pm] for 2 <= m <= 15.
	 */

	/*SOLUTION:
	 * Lagrange Multiplier!! :)
	 * http://mathworld.wolfram.com/LagrangeMultiplier.html
	 * 
	 * If   f(a1, a2, a3, ..., am) = a1*a2^2*a3^3*...*am^m
	 * 		g(a1, a2, a3, ..., am) = a1 + a2 + a3 + ... am
	 * 		and g(a1, a2, a3, ..., am) = m
	 * 
	 * then gradientF = lamda*gradientG
	 * 
	 * gradientF = (a2^2*a3^3*...*am^m, 2a1*a2*a3^3*...*am^m, 
	 * 			..., m*a1*a2^2*a3^3*...am-1 ^ (m-1) * am^(m-1))
	 * gradientG = (1, 1, 1, ..., 1)
	 * 
	 * If we have f(a,b,c) = ab^2c^3d^4
	 * 				g(a,b,c) = a+b+c
	 * 				g(a,b,c) = 3
	 * 
	 *  gradientF = (b^2c^3, 2abc^3, 3ab^2c^2)
	 *  gradientG = (1, 1, 1)
	 *  
	 *  gradientF = lamda*gradientG  if
	 *  b^2c^3 = lambda
	 *  2abc^3 = lambda
	 *  3ab^2c^2 = lambda which gives us:
	 *  b=2a, c=3a
	 *  
	 *  In fact with more variables we always getthe result that:
	 *  (a1, a2, a3, ..., am) is an arithmetic sequence with a1=a, r=a
	 *  (a, 2a, 3a, 4a, ...)
	 *  
	 *  So  a1 + a2 + a3 + ... am = m   if
	 *  a + 2a + 3a + ... + ma = m
	 *  a*(1+2+3+...+m) = m
	 *  a = 2/(m+1)
	 *  
	 *	Pm = 1^1*a * 2^2*a^2 * 3^3*a^3 * m^m*a^m =
	 *  	= 1^1*2^2*3^3*...*m^m * a^( (m^2+m)/2 )
	 *  
	 *  a is a fraction  p/q  so I don`t use double values 
	 *  up until the final operation
	 */
	public static void main(String[] args) {
		BigInteger result = BigInteger.ZERO;

		for (long m = 2L; m <= 15; m++) {
			int exp = (int) ((m * m) + m) / 2;
			BigInteger p = BigInteger.valueOf(2L);
			BigInteger q = BigInteger.valueOf(m + 1L);

			BigInteger PmNumerator = BigInteger.ONE;
			BigInteger PmDenominator = q.pow(exp);

			for (long i = 2L; i <= m; i++) {
				PmNumerator = PmNumerator.multiply(BigInteger.valueOf(i).pow(
						(int) i));
			}
			PmNumerator = PmNumerator.multiply(p.pow(exp));

			BigDecimal dec = new BigDecimal(PmNumerator);
			dec = dec.divideToIntegralValue(new BigDecimal(PmDenominator));
			result = result.add(dec.toBigInteger());
		}

		System.out.println("RESULT = " + result);
	}
}

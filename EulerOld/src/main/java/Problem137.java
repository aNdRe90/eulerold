

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Problem137 {
	/*
	 * Consider the infinite polynomial series 
	 * AF(x) = x*F(1) + x^2*F(2) + x^3*F(3) + ..., where F(k) is the kth term 
	 * in the Fibonacci sequence: 1, 1, 2, 3, 5, 8, ... ; 
	 * that is, F(k) = F(k-1) + F(k-2), F1 = 1 and F2 = 1.
	 * 
	 * For this problem we shall be interested in values of x 
	 * for which AF(x) is a positive integer.
	 * 
	 * Surprisingly AF(1/2)  =  (1/2).1 + (1/2)2.1 + (1/2)3.2 
	 * + (1/2)4.3 + (1/2)5.5 + ...	 =  
	 * 1/2 + 1/4 + 2/8 + 3/16 + 5/32 + ... = 2
	 * 
	 * The corresponding values of x for the first five natural 
	 * numbers are shown below.
	 * 
	 * AF(x) = 1    => x = sqrt(2) - 1
	 * AF(x) = 2    => x = 1/2
	 * AF(x) = 3    => x = (sqrt(13)-2)/3
	 * AF(x) = 4    => x = (sqrt(89)-5)/8
	 * AF(x) = 5    => x = (sqrt(34)-3)/5
	 * 
	 * We shall call AF(x) a golden nugget if x is rational, 
	 * because they become increasingly rarer; 
	 * for example, the 10th golden nugget is 74049690.
	 * 
	 * Find the 15th golden nugget.
	 */
	
	/*
	 * SOLUTION:
	 * F(k) = 1/sqrt(5) * [ ( (1+sqrt(5))/2 )^k - ( (1-sqrt(5))/2 )^k ]
	 * 
	 * sum means summing from i=1 to i=infinity
	 * So AF(x) = 1/sqrt(5) * 
	 * 		[ sum( ((x+x*sqrt(5))/2)^i - sum( ((x-x*sqrt(5))/2)^i  ] =
	 * 
	 * sums are geometric sums where  
	 * (a1,q) = (x+x*sqrt(5))/2 , (x+x*sqrt(5))/2     - first sum
	 * (a1,q) = (x-x*sqrt(5))/2 , (x-x*sqrt(5))/2     - second sum
	 * 
	 * Thus their values are a1/(1-q) where |q|<1   (it forces x<0.618)
	 * 
	 * And finally AF(x) = x/(-x^2 -x +1)
	 * 
	 * AF(x) = k   =>   -x = kx^2 + kx - k    <=>  kx^2 + (k+1)x - k = 0
	 * 
	 * delta = 5k^2 + 2k + 1
	 * 
	 * x = (-k-1 + sqrt(delta)) / 2k
	 * 
	 * delta = n^2  when 5k^2 + 2k + (1-n^2) = 0
	 * 
	 * delta2 = 4(5n^2 - 4)
	 * 
	 * delta2 = m^2 when   5n^2 - 4 = m^2
	 * 
	 * Generalised Pells Equation:
	 * 
	 * m^2 - 5n^2 = -4
	 * 
	 * Solving it i got the answer.
	 */
	private static final int N = 15;
	private static List<BigInteger> goldenNuggets 
				= new ArrayList<BigInteger>(N);
	private static BigInteger five = BigInteger.valueOf(5L);
	private static BigInteger two = BigInteger.valueOf(2L);
	
	public static void main(String[] agrs) {		
		List<SolutionPair> rootPairs = getRootPairs();
		int rootSize = rootPairs.size();

		for(SolutionPair root : rootPairs) {
			analyzeSolutionPair(root);
		}
		
		for(int i=0; ;i++) {
			if(i==rootSize) {
				i = 0;
			}
			
			SolutionPair root = rootPairs.get(i);
			SolutionPair current = root.getNextSolutionPair();
			rootPairs.set(i, current);			
			
			analyzeSolutionPair(current);
			
			if(goldenNuggets.size()==N) {
				break;
			}
		}
		
		System.out.println("RESULT = " + goldenNuggets.get(N-1));
	}

	private static void analyzeSolutionPair(SolutionPair current) {
		BigInteger numerator = current.x.subtract(BigInteger.ONE);
		BigInteger[] kAndRemainderK = numerator.divideAndRemainder(five);
		
		if(kAndRemainderK[1].compareTo(BigInteger.ZERO)==0) {
			BigInteger k = kAndRemainderK[0];
			BigInteger temp = k.add(BigInteger.ONE);
			temp = temp.negate();
			numerator = current.y.add(temp);
			BigInteger denominator = two.multiply(k);
			
			if(denominator.compareTo(BigInteger.ZERO)!=0 &&
					numerator.mod(denominator).compareTo(BigInteger.ZERO)!=0) {
				goldenNuggets.add(k);
			}
		}
	}
	
	private static List<SolutionPair> getRootPairs() {
		List<SolutionPair> rootPairs = new ArrayList<SolutionPair>(3);
		
		rootPairs.add(new SolutionPair(BigInteger.valueOf(1L), 
				   BigInteger.valueOf(1L)));
		
		rootPairs.add(new SolutionPair(BigInteger.valueOf(4L), 
				   BigInteger.valueOf(2L)));
		
		rootPairs.add(new SolutionPair(BigInteger.valueOf(11L), 
				   BigInteger.valueOf(5L)));
		
		return rootPairs;
	}
	
	private static class SolutionPair 
							implements Comparable <SolutionPair> {
		private BigInteger x;
		private BigInteger y;
		private static BigInteger nine = BigInteger.valueOf(9L);
		private static BigInteger twenty = BigInteger.valueOf(20L);
		private static BigInteger four = BigInteger.valueOf(4L);
		
		public SolutionPair(BigInteger x, BigInteger y) {
			this.x = x;
			this.y = y;
		}

		@Override
		public int compareTo(SolutionPair other) {
			return x.compareTo(other.x);
		}
		
		public SolutionPair getNextSolutionPair() {
			return new SolutionPair(
					nine.multiply(x).add(twenty.multiply(y)),
					four.multiply(x).add(nine.multiply(y)));
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((x == null) ? 0 : x.hashCode());
			result = prime * result + ((y == null) ? 0 : y.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			SolutionPair other = (SolutionPair) obj;

			return x.compareTo(other.x)==0;
		}

		@Override
		public String toString() {
			return "[x=" + x + ", y=" + y + "]";
		}		
	}
}

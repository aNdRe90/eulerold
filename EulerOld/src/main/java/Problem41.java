
public class Problem41 
{
	/*
	 * We shall say that an n-digit number is pandigital if it makes use 
	 * of all the digits 1 to n exactly once. 
	 * For example, 2143 is a 4-digit pandigital and is also prime.
	 * 
	 * What is the largest n-digit pandigital prime that exists?
	 */
	
	/*
	 * SOLUTION:
	 * 3 <= n <= 9   as there is no 1-digit or 2-digit pandigital prime.
	 */
	public static void main(String[] args) 
	{
		int largest = 0;
		
		for(int i=9; i>2; i--)
		{			
			PermutationGenerator p = new PermutationGenerator(i, false);
			while(p.hasMore())
			{
				int[] nextPerm = p.getNext();
				StringBuffer number = new StringBuffer(nextPerm.length);
				
				for(int j=0; j<nextPerm.length; j++)
					number.append(nextPerm[j]);
				
				int currentValue = Integer.parseInt(number.toString());
				if(currentValue>largest && MyMath.isPrime(currentValue))
					largest = currentValue;
			}
		}
		
		System.out.println(largest);
	}
	
	public static boolean isPandigital(String num)
	{
		int length = num.length();
		if(length<1 || length>9)
			return false;
		
		
		char[] chars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
		for(int i=1; i<=length; i++)
		{
			int counter = 0;
			char currentChar = chars[i];
			for(int j=0; j<length; j++)
				if(num.charAt(j)==currentChar)
					counter++;
			
			if(counter!=1)
				return false;
		}	
		return true;
	}
}

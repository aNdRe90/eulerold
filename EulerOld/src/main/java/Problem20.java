import java.math.BigInteger;


public class Problem20 
{
	/*
	 * n! means n(n-1)(n-2)...3*2*1

	For example, 10! = 10 x 9 x ... x 3 x 2 x 1 = 3628800,
	and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

	Find the sum of the digits in the number 100!

	 */
	public static void main(String[] args) 
	{
		BigInteger b = new BigInteger("100");
		for(int i=99; i>1; i--)
			b = b.multiply(new BigInteger(Integer.toString(i)));
		
		String result = b.toString();
		int len = result.length();
		long sum = 0L;
		
		for(int i=0; i<len; i++)
		{
			sum += (result.charAt(i) - 0x30);
		}
		System.out.println(sum);
	}

}

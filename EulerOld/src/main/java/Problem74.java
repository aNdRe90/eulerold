import java.math.BigInteger;
import java.util.HashSet;


public class Problem74 
{
	/*
	 * The number 145 is well known for the property that the sum of the factorial of its digits is equal to 145:

		1! + 4! + 5! = 1 + 24 + 120 = 145

		Perhaps less well known is 169, in that it produces the longest chain of numbers that link back to 169; 
		it turns out that there are only three such loops that exist:

		169 -> 363601 -> 1454 -> 169
		871 -> 45361 -> 871
		872 -> 45362 -> 872

		It is not difficult to prove that EVERY starting number will eventually get stuck in a loop. For example,

		69 -> 363600 -> 1454 -> 169 -> 363601 (-> 1454)
		78 -> 45360 -> 871 -> 45361 (-> 871)
		540 -> 145 (-> 145)

		Starting with 69 produces a chain of five non-repeating terms, but the longest non-repeating chain 
		with a starting number below one million is sixty terms.

		How many chains, with a starting number below one million, contain exactly sixty non-repeating terms?

	 */
	public static void main(String[] args) 
	{
		int howManyChains = 0;
		
		for(int i=69; i<1000000; i++)
		{
			int chain = 1;
			
			HashSet<BigInteger> set = new HashSet<BigInteger>(61);
			BigInteger previous = BigInteger.valueOf(i);
			set.add(previous);
			
			while(true)
			{
				BigInteger next = getNextChainNumber(previous);
				int sizeBefore = set.size();
				set.add(next);
				int sizeAfter = set.size();
				
				if(sizeAfter==sizeBefore) //loop started
				{
					chain = sizeAfter;
					break;
				}
				previous = next;
			}
			
			if(chain==60)
			{
				++howManyChains;
			}
		}
		
		System.out.println(howManyChains);
	}
	
	public static BigInteger getNextChainNumber(BigInteger b)
	{
		BigInteger factorial = BigInteger.ZERO;
		
		String value = b.toString();
		int length = value.length();
		
		for(int i=0; i<length; i++)
		{
			factorial = factorial.add(BigInteger.valueOf(factorials[value.charAt(i) - 0x30]));
		}
		
		return factorial;
	}
	
	private static long[] factorials = { 1L, 1L, 2L, 6L, 24L, 120L, 720L, 5040L, 40320L, 362880L };
}


public class Problem9 
{
	/*A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
	 * a^2 + b^2 = c^2
	 * For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
	 * There exists exactly one Pythagorean triplet for which a + b + c = 1000.
	 * Find the product abc.*/
	public static void main(String[] args) 
	{
		for(int b=500; b>0; b--)
		{
			for(int a=b-1; a>0; a--)
			{
				double cDouble = Math.sqrt(1.0*a*a + 1.0*b*b);
				int cInt = (int) cDouble;
				double difference = cDouble - cInt;
				if(difference==0.0 && (a+b+cInt)==1000)
				{
					System.out.println(a*b*cInt);
				}
			}
		}
		
	}

}

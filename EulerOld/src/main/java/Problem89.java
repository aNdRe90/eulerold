import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import MyStuff.RomanNumbers;


public class Problem89 {
	/*
	 * The rules for writing Roman numerals allow for many ways of writing each number 
	 * (see About Roman Numerals...). However, there is always a "best" way of writing a particular number.

		For example, the following represent all of the legitimate ways of writing the number sixteen:

		IIIIIIIIIIIIIIII
		VIIIIIIIIIII
		VVIIIIII
		XIIIIII
		VVVI
		XVI

	The last example being considered the most efficient, as it uses the least number of numerals.
	The 11K text file, roman.txt (right click and 'Save Link/Target As...'), 
	contains one thousand numbers written in valid, but not necessarily minimal, 
	Roman numerals; that is, they are arranged in descending units and obey the subtractive pair rule 
	(see About Roman Numerals... for the definitive rules for this problem).
	
	Find the number of characters saved by writing each of these in their minimal form.
	
	Note: You can assume that all the Roman numerals in the file contain no more than four 
	consecutive identical units. */
	
	public static void main(String[] args) {
		BufferedReader reader = getBufferedReader("roman.txt");
		List<String> romanNumbers = getNumbersFromReader(reader);
		
		int numberOfSavedCharacters = 0;
		for(String roman : romanNumbers) {
			int currentSize = roman.length();
			int arabicValue = RomanNumbers.romanToArabic(roman);
			String minimalRoman = RomanNumbers.arabicToRoman(arabicValue);
			
			numberOfSavedCharacters += (currentSize - minimalRoman.length());
		}
		
		System.out.println(numberOfSavedCharacters);
	}
	
	private static List<String> getNumbersFromReader(BufferedReader reader) {
		List<String> numbers = new LinkedList<String>();
		String line = null;
		
		try {
			while( (line=reader.readLine())!=null )
				numbers.add(line);
		} 
		catch (IOException e) { e.printStackTrace(); }
		
		return numbers;
	}

	private static BufferedReader getBufferedReader(String filename) {
		BufferedReader f = null;
		try
		{
			f = new BufferedReader(new FileReader(filename));
		}
		catch(IOException e) { e.printStackTrace(); }
		
		return f;
	}
}

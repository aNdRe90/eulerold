

import MyStuff.Fraction;

public class Problem183 {
	/*
	 * Let N be a positive integer and let N be split into k equal parts, 
	 * r = N/k, so that N = r + r + ... + r.
	 * 
	 * Let P be the product of these parts, P = r � r � ... � r = r^k.
	 * 
	 * For example, if 11 is split into five equal parts, 
	 * 11 = 2.2 + 2.2 + 2.2 + 2.2 + 2.2, then P = 2.2^5 = 51.53632.
	 * 
	 * Let M(N) = Pmax for a given value of N.
	 * 
	 * It turns out that the maximum for N = 11 is found by splitting eleven 
	 * into four equal parts which leads to Pmax = (11/4)4; that is, M(11) = 14641/256 = 57.19140625, 
	 * which is a terminating decimal.
	 * 
	 * However, for N = 8 the maximum is achieved by splitting it into three equal parts, 
	 * so M(8) = 512/27, which is a non-terminating decimal.
	 * 
	 * Let D(N) = N if M(N) is a non-terminating decimal and D(N) = -N if M(N) is a terminating decimal.
	 * 
	 * For example, sum D(N) for 5 <= N <= 100 is 2438.
	 * 
	 * Find sum D(N) for 5 <= N <= 10000.
	 */
	public static void main(final String[] args) {
		final long minN = 5L;
		final long maxN = 10000L;
		long sum = 0L;

		for (long N = minN; N <= maxN; N++) {
			System.out.println(N);
			sum += D(N);
		}

		System.out.println("RESULT = " + sum);
	}

	private static long D(final long N) {
		Fraction M = M(N);

		if (isTerminatingDecimal(M)) {
			return -N;
		} else {
			return N;
		}
	}

	private static boolean isTerminatingDecimal(final Fraction m) {
		//fraction is terminating when its denominator is of form:
		//2^x * 5^y   where x>=0 and y>=0
		long denominator = m.getDenominator();
		if (denominator == 1L) {
			return true;
		}

		long[] divisors = MyStuff.MyMath.getSinglePrimeDivisors(denominator);

		if (divisors.length == 1) {
			return (divisors[0] == 2L) || (divisors[0] == 5L);
		} else if (divisors.length == 2) {
			return (divisors[0] * divisors[1]) == 10L;
		} else {
			return false;
		}

	}

	private static Fraction M(final long N) {
		Fraction max = new Fraction(0L);
		double maxValue = 1.0;
		int maxPow = 1;

		for (long i = 1L; i <= N; i++) {
			Fraction current = new Fraction(N, i);
			double currentValue = current.toDouble();

			if ((i * Math.log(currentValue)) > (maxPow * Math.log(maxValue))) {
				max = current.reduce();
				maxValue = currentValue;
				maxPow = (int) i;
			} else {
				break;
			}
		}

		//i dont return power of fraction as if it is terminating then
		//without any powering the fraction is too. Same with NOT.
		return max;
	}
}



import java.math.BigInteger;
import java.util.Collection;
import java.util.Set;

import MyStuff.Factorizator;

public class Problem401 {
	/*
	 * The divisors of 6 are 1,2,3 and 6.
	 * The sum of the squares of these numbers is 1+4+9+36=50.
	 * 
	 * Let sigma2(n) represent the sum of the squares of the divisors of n. 
	 * Thus sigma2(6)=50.
	 * 
	 * Let SIGMA2 represent the summatory function of sigma2, 
	 * that is SIGMA2(n)= sum of sigma2(i) for i=1 to n.
	 * 
	 * The first 6 values of SIGMA2 are: 1,6,16,37,63 and 113.
	 * Find SIGMA2(10^15) modulo 10^9.
	 */
	private static final long N = 8;
	private static final long modulo = (long) 1e9;
	private static final BigInteger bigModulo = BigInteger.valueOf(modulo);

	public static void main(String[] args) {
		BigInteger result = SIGMA2(N);
		long bruteForceResult = bruteForce(N);

		result = result.mod(bigModulo);
		System.out.println("RESULT = " + result);
		System.out.println("BRUTE RESULT = " + bruteForceResult);
	}

	private static long bruteForce(long n) {
		long result = 1L;

		for (long i = 2L; i <= n; i++) {
			Set<Long> divisors = Factorizator.getDivisors(i);
			long sumOfSquares = getSumOfSquares(divisors);
			result += (sumOfSquares % modulo);
			result = result % modulo;
		}

		return result;
	}

	private static long getSumOfSquares(Collection<Long> values) {
		long sumOfSquares = 0L;

		for (long v : values) {
			sumOfSquares += v * v;
		}

		return sumOfSquares;
	}

	private static BigInteger SIGMA2(long n) {
		BigInteger result = getSumOfSquaresUpTo(n);
		long sqrtN = (long) Math.sqrt(n);

		for (long i = 1L; i <= sqrtN; i++) {
			long j = n / i;
			BigInteger i2 = BigInteger.valueOf(i * i);
			BigInteger j2 = BigInteger.valueOf(j * j);

			BigInteger addend = BigInteger.valueOf((i - 1L)).multiply(j2);
			result = result.add(addend);

			if (i != j) {
				addend = BigInteger.valueOf((j - 1L)).multiply(i2);
				result = result.add(addend);
			}
		}

		return result;
	}

	private static BigInteger getSumOfSquaresUpTo(long n) {
		BigInteger two = BigInteger.valueOf(2L);
		BigInteger three = BigInteger.valueOf(3L);
		BigInteger six = BigInteger.valueOf(6L);
		BigInteger bigN = BigInteger.valueOf(n);

		BigInteger result = two.multiply(bigN.pow(3));
		result = result.add(three.multiply(bigN.pow(2)));
		result = result.add(bigN);
		result = result.divide(six);

		return result;
	}
}

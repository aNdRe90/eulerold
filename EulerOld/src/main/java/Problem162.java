

import java.math.BigInteger;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Problem162 {
	/*
	 * In the hexadecimal number system numbers are represented using 
	 * 16 different digits:
	 * 0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F
	 * 
	 * The hexadecimal number AF when written in the decimal number system 
	 * equals 10x16+15=175.
	 * 
	 * In the 3-digit hexadecimal numbers 10A, 1A0, A10, and A01 the digits 
	 * 0,1 and A are all present.
	 * 
	 * Like numbers written in base ten we write hexadecimal numbers without leading zeroes.
	 * 
	 * How many hexadecimal numbers containing at most sixteen hexadecimal digits exist 
	 * with all of the digits 0,1, and A present at least once?
	 * 
	 * Give your answer as a hexadecimal number.
	 * 
	 * (A,B,C,D,E and F in upper case, without any leading or trailing code that marks 
	 * the number as hexadecimal and without leading zeroes , e.g. 1A3F and not: 
	 * 1a3f and not 0x1a3f and not $1A3F and not #1A3F and not 0000001A3F)
	 */
	private static BigInteger factorialThree = BigInteger.valueOf(6L);
	private static BigInteger factorialTwo = BigInteger.valueOf(2L);
	private static BigInteger two = BigInteger.valueOf(2L);
	private static BigInteger sixteen = BigInteger.valueOf(16L);
	private static BigInteger thirteen = BigInteger.valueOf(13L);
	private static BigInteger fifteen = BigInteger.valueOf(15L);
	private static BigInteger fourteen = BigInteger.valueOf(14L);
	private static char[] hexChars = new char[] { '0', '1', '2', '3', '4', '5',
			'6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	public static void main(final String[] args) {
		//		List<String> list = getValuesForLength(6);
		//		for (String hex : list) {
		//			System.out.println(hex);
		//		}
		//		System.out.println("SIZE = " + list.size());

		BigInteger sum = BigInteger.valueOf(4L);

		for (int n = 4; n <= 16; n++) {
			BigInteger sumInRow = getSumInRowOfLength(n);
			sum = sum.add(sumInRow);
		}

		System.out.println(sum.toString(16).toUpperCase() + " = "
				+ sum.toString());
	}

	private static BigInteger getSumInRowOfLength(final int n) {
		BigInteger a = BigInteger.ZERO;
		int area = n - 1;
		for (int ones = 1; ones < area; ones++) {
			for (int zeros = 1; (zeros + ones) < area; zeros++) {
				for (int As = 1; (zeros + ones + As) <= area; As++) {
					BigInteger val = MyStuff.MyMath.binomialCoefficient(area,
							ones);
					val = val.multiply(MyStuff.MyMath.binomialCoefficient(area
							- ones, zeros));
					val = val.multiply(MyStuff.MyMath.binomialCoefficient(area
							- ones - zeros, As));

					val = val.multiply(thirteen.pow(area - ones - zeros - As));
					a = a.add(val);
				}
			}
		}

		a = a.multiply(thirteen);

		BigInteger b = BigInteger.ZERO;
		for (int ones = 1; ones < area; ones++) {
			for (int zeros = 1; (zeros + ones) <= area; zeros++) {
				BigInteger val = MyStuff.MyMath.binomialCoefficient(area, ones);
				val = val.multiply(MyStuff.MyMath.binomialCoefficient(area
						- ones, zeros));
				val = val.multiply(fourteen.pow(area - ones - zeros));
				b = b.add(val);
			}
		}

		b = b.multiply(two);

		return a.add(b);
	}

	private static List<String> getValuesForLength(final int n) {
		List<String> list = new LinkedList<String>();

		int min = (int) Math.pow(16.0, n - 1);
		int max = min * 16;

		for (int i = min; i <= max; i++) {
			String hex = Integer.toHexString(i);
			if (hex.contains("0") && hex.contains("1") && hex.contains("a")) {
				list.add(hex);
			}
		}

		Collections.sort(list);
		return list;
	}
}

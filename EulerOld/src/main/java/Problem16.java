import java.math.BigInteger;


public class Problem16 
{
	/*
	 * 2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.

	   What is the sum of the digits of the number 2^1000?

	 */
	
	public static void main(String[] args) 
	{
		//2^1000 = 1024^100 = (1024^10)^10
		BigInteger num = new BigInteger("1024");
		num = num.pow(10);
		num = num.pow(10);
		
		String result = num.toString();
		int length = result.length();
		
		long sum = 0L;
		for(int i=0; i<length; i++)
		{
			sum = sum + result.charAt(i) - 0x30;
		}
		
		System.out.println(sum);
	}
}

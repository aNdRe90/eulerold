

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

public class Problem135 {
	/*
	 * Given the positive integers, x, y, and z, 
	 * are consecutive terms of an arithmetic progression, 
	 * the least value of the positive integer, n, 
	 * for which the equation, x^2 - y^2 - z^2 = n, 
	 * has exactly two solutions is n = 27:
	 * 
	 * 34^2 - 27^2 - 20^2 = 12^2 - 9^2 - 6^2 = 27
	 * 
	 * It turns out that n = 1155 is the least value 
	 * which has exactly ten solutions.
	 * 
	 * How many values of n less than one million 
	 * have exactly ten distinct solutions?
	 */
	
	/*
	 * SOLUTION:
	 * (a0+2r)^2 - (a0+r)^2 - a0^2 = n
	 * 3r^2 + 2a0r - a0^2 = n
	 * (3r-a0)(r+a0) = n
	 * 
	 * 1) 3r^2 + 2a0r - a0^2 = n		a0=const.
	 * delta = 4a0^2 - 12(-a0^2 - n) = 4(4a0^2 + 3n)
	 * r = 1/3 * ( sqrt(4a0^2 + 3n) - a0 )
	 * 
	 * 2) 3r^2 + 2a0r - a0^2 = n		r=const.
	 * a0^2 - 2a0r - 3r^2 + n = 0
	 * delta = 16r^2 - 4n = 4(4r^2 - n)
	 * a0 = r +- sqrt(4r^2 - n)
	 * 
	 *  4r^2 - n >=0   <=>   r >= sqrt(n)/2
	 *    
	 *  So we need to have divisors (d`s) of n such that a0+r = d.
	 *  if r >= sqrt(n)/2 and a0>0 then  d >= sqrt(n)/2 + 1
	 *  
	 *  a0 + r = d
	 *  r + r +- sqrt(4r^2 - n) = d
	 *  d - 2r = +- sqrt(4r^2 - n)
	 *  d^2 + 4r^2 - 4rd = 4r^2 - n
	 *  r = (d^2 + n) / 4d
	 */
	public static void main(String[] args) {
		final int N = 1000000;
		final int distinctSolutions = 10;
		int result = 0;		
		Map<Long, LinkedList<Long>> divisors = getDivisors(N);
		

		for(long n=1; n<N; n++) {
			System.out.println("Solving... " + n);
			
			Set<Solution> solutions = new HashSet<Solution>();			
			LinkedList<Long> divisorsN = divisors.get(n);		
			if(divisorsN.size()<distinctSolutions) {
				continue;
			}
			
			for(long divisor : divisorsN) {
				long numerator = (divisor*divisor + n);
				long denominator = 4L*divisor;
				if(numerator%denominator==0L) {
					long r = numerator/denominator;
					if(divisor>r) {
						solutions.add(new Solution(divisor - r, r));
					}					
				}
			}
			
			if(solutions.size()==distinctSolutions) {
				result++;
			}
		}
		
		System.out.println(result);
	}
	
	private static Map<Long, LinkedList<Long>> getDivisors(
											long N) {
		Map<Long, LinkedList<Long>> divisors 
				= new HashMap<Long, LinkedList<Long>>((int)N);
		
		for(long n=1; n<N; n++) {
			divisors.put(n, new LinkedList<Long>());
		}
		
		for(long n=1; n<N; n++) {
			if(n%1000==0) {
				System.out.println("Computing divisor... " + n);
			}
			
			for(long c=1; ;c++) {
				long value = c*n;
				long minDivisor 
					= (long) Math.ceil((Math.sqrt(value) / 2.0)) + 1;
				
				if(value >= N) {
					break;
				}
				
				if(n>=minDivisor) {
					divisors.get(value).add(n);
				}				
			}
		}
		System.out.println(divisors.size());
		return divisors;
	}
	
	private static class Solution {
		private long a0;
		private long r;
		
		public Solution(long a0, long r) {
			this.a0 = a0;
			this.r = r;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (int)a0;
			result = prime * result + (int)r;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Solution other = (Solution) obj;
			if (a0 != other.a0)
				return false;
			if (r != other.r)
				return false;
			return true;
		}
		
		@Override
		public String toString() {
			return "[a0 = " + a0 + ", r = " + r + "]";
		}
	}
}



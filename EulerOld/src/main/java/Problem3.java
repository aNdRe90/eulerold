import java.math.BigInteger;


public class Problem3
{
	/*
	 * The prime factors of 13195 are 5, 7, 13 and 29.
	 * What is the largest prime factor of the number 600851475143 ?
	 */
	public static void main(String[] args) 
	{
		long number = 600851475143L;
		long halfNumber = number/2L;
		
		long maxPrimeFactor = 1L;
		
		for(long i = 3; i<=number; i++)
		{
			if(number%i==0)
			{
				number = number/i;
				
				if(isPrime(i))
				{
					maxPrimeFactor = i;
				}
			}
		}
		
		System.out.println(maxPrimeFactor);
	}
	
	public static boolean isPrime(long v)
	{
		if(v%2==0 || v%3==0 || v%5==0 || v%7==0 || v%11==0 || v%13==0 || v%17==0 || v%19==0)
			return false;
		
		long sqrt = (long) Math.sqrt((double)v) + 1L;
		
		for(long i = 23; i<=sqrt; i+=2)
		{
			if(v%i==0)
				return false;
		}
		
		return true;
	}
}



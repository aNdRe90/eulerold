

import java.math.BigInteger;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Problem303 {
	private static final long maxDigit = 2;
	private static final char maxChar = '0' + (int) maxDigit;
	private static final BigInteger minusOne = BigInteger.valueOf(-1L);

	/*
	 * For a positive integer n, define f(n) as the least positive multiple of n that, 
	 * written in base 10, uses only digits <= 2.
	 * 
	 * Thus f(2)=2, f(3)=12, f(7)=21, f(42)=210, f(89)=1121222.
	 * 
	 * Also sum n=1:100 [f(n)/n] = 11363107
	 * 
	 * Find sum n=1:10000 [f(n)/n]
	 */
	public static void main(String[] args) {
		final long N = 10000L;
		BigInteger sum = BigInteger.ZERO;

		for (long n = 1L; n <= N; n++) {
			System.out.println(n);

			if (consistsOnlyOf9(n)) {
				BigInteger result = getMultipleForOnly9(n);
				sum = sum.add(result);
			} else {
				BigInteger result = getMultiplierFor(n);
				sum = sum.add(result);
			}
		}

		System.out.println("RESULT = " + sum);
	}

	private static long getBruteForce(long n) {
		long counter = 1L;
		long current = n;

		while (!isResult(current)) {
			current += n;
			counter++;
		}

		return counter;
	}

	private static void bruteForce(long N) {
		BigInteger sum = BigInteger.ZERO;

		for (long n = 1L; n <= N; n++) {
			System.out.println("n=" + n);
			if (consistsOnlyOf9(n)) {
				BigInteger result = getMultipleForOnly9(n);
				sum = sum.add(result);
			} else {

				long counter = 1L;
				long current = n;

				while (!isResult(current)) {
					current += n;
					counter++;
				}

				sum = sum.add(BigInteger.valueOf(counter));
			}
		}

		System.out.println("BRUTE FORCE = " + sum);
	}

	private static BigInteger getMultipleForOnly9(long n) {
		String s = Long.toString(n);
		int length = s.length();

		int twos = length * 4;
		int ones = length;

		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < twos; i++) {
			buf.append('2');
		}
		for (int i = 0; i < ones; i++) {
			buf.append('1');
		}

		BigInteger f = new BigInteger(buf.reverse().toString());
		return f.divide(BigInteger.valueOf(n));
	}

	private static boolean consistsOnlyOf9(long n) {
		String s = Long.toString(n);
		int length = s.length();

		for (int i = 0; i < length; i++) {
			if (s.charAt(i) != '9') {
				return false;
			}
		}

		return true;
	}

	private static BigInteger getMultiplierFor(long n) {
		if (n <= maxDigit) {
			return BigInteger.ONE;
		}

		BigInteger bigN = BigInteger.valueOf(n);
		List<BigInteger> possibilities = getInitialPossibilities(n);
		BigInteger modulo = BigInteger.valueOf(100L);

		BigInteger result = minusOne;
		while (result.compareTo(minusOne) == 0) {
			result = updatePossibilities(bigN, possibilities, modulo);
			modulo = modulo.multiply(BigInteger.TEN);
		}

		return result;
	}

	private static BigInteger updatePossibilities(BigInteger n,
			List<BigInteger> possibilities, BigInteger modulo) {
		List<BigInteger> toAdd = new LinkedList<BigInteger>();

		for (BigInteger possibility : possibilities) {
			if (isResult(n.multiply(possibility))) {
				return possibility;
			}

			BigInteger difference = (modulo.divide(BigInteger.TEN));
			for (BigInteger addend = BigInteger.ZERO; addend.compareTo(modulo) < 0; addend = addend
					.add(difference)) {
				BigInteger newPossibility = addend.add(possibility);
				BigInteger value = (n.multiply(newPossibility));

				if (isResult(value.mod(modulo))) {
					toAdd.add(newPossibility);
				}
			}
		}

		Collections.sort(toAdd);
		possibilities.clear();
		possibilities.addAll(toAdd);

		return BigInteger.valueOf(-1L);
	}

	private static boolean isResult(BigInteger v) {
		String s = v.toString();
		int length = s.length();

		for (int i = 0; i < length; i++) {
			if (s.charAt(i) > maxChar) {
				return false;
			}
		}

		return true;
	}

	private static boolean isResult(long v) {
		String s = Long.toString(v);
		int length = s.length();

		for (int i = 0; i < length; i++) {
			if (s.charAt(i) > maxChar) {
				return false;
			}
		}

		return true;
	}

	private static List<BigInteger> getInitialPossibilities(long n) {
		List<BigInteger> possibilities = new LinkedList<BigInteger>();

		for (long i = 1L; i < 10L; i++) {
			if (((n * i) % 10L) <= maxDigit) {
				possibilities.add(BigInteger.valueOf(i));
			}
		}

		return possibilities;
	}
}


public class Problem6 
{
	/*
	 * The sum of the squares of the first ten natural numbers is,
	 *                        1^2 + 2^2 + ... + 10^2 = 385
	 * The square of the sum of the first ten natural numbers is,
	 *                        (1 + 2 + ... + 10)^2 = 55^2 = 3025
	 *   Hence the difference between the sum of the squares of the first ten
	 *   natural numbers and the square of the sum is 3025 385 = 2640.
	 *   
	 *   Find the difference between the sum of the squares of the first one
	 *   hundred natural numbers and the square of the sum.	 */
	
	public static void main(String[] args) 
	{
		long sumOfSquares = 0;
		long squareOfTheSum = 0;
		
		for(long i = 1; i<101; i++)
			sumOfSquares += i*i;
		
		for(int i = 1; i<101; i++)
			squareOfTheSum +=i;
		
		squareOfTheSum = squareOfTheSum*squareOfTheSum;
		
		long difference = squareOfTheSum - sumOfSquares;
		if(difference<0)
			difference*=-1;
		
		System.out.println(difference);
		
	}
}

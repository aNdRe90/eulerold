

import java.math.BigInteger;

public class Problem132 {
	/*
	 * A number consisting entirely of ones is called a repunit. 
	 * We shall define R(k) to be a repunit of length k.
	 * 
	 * For example, R(10) = 1111111111 = 11�41�271�9091, 
	 * and the sum of these prime factors is 9414.
	 * 
	 * Find the sum of the first forty prime factors of R(10^9).
	 */
	
	/*
	 * SOLUTION:
	 * R(n) = (10^n - 1)/9
	 * 
	 * So R(n) mod p = 0    if   10^n mod 9p = 1
	 * 
	 * We have n=10^9  so I thought the best way to check the modulo
	 * 10^(10^9) mod 9p   is to do:
	 * 
	 * [ 10^(10^9) = (10^10)^9 ]
	 * 10^10 mod 9p = m1		10^10 mod 9p
	 * m1^10 mod 9p = m2		10^100 mod 9p
	 * m2^10 mod 9p = m3		10^1000 mod 9p
	 * m3^10 mod 9p = m4		10^10000 mod 9p
	 * m4^10 mod 9p = m5		10^100000 mod 9p
	 * m5^10 mod 9p = m6		10^1000000 mod 9p
	 * m6^10 mod 9p = m7		10^10000000 mod 9p
	 * m7^10 mod 9p = m8		10^100000000 mod 9p
	 * m8^10 mod 9p = m9		10^1000000000 mod 9p
	 * 
	 * For each p i compute m9 and check if it`s 1. 
	 * If so, it divides R(10^9).
	 */
	public static void main(String[] args) {
		long sum = 0L;
		int factorsFound = 0;
		int factorsNeeded = 40;
		
		BigInteger x = new BigInteger("10000000000");
		final int Xexp = 10;
		final int Nexp = 9;   //N = 10^9
		
		//p=2 and p=5 divides no R(n) and 3 divides only R(3*k) k=1,2,...
		//so neither of them divides R(10^9) and we start with p=7
		for(long p=7L; ;p+=2) {
			if(MyStuff.MyMath.isPrime(p)) {
				System.out.println(p);
				
				BigInteger modular = BigInteger.valueOf(9L*p);
				BigInteger modulo = x.mod(modular);
				
				for(int i=2; i<=Nexp; i++) {
					modulo = modulo.pow(Xexp).mod(modular);
				}
				
				if(modulo.equals(BigInteger.ONE)) {
					factorsFound++;
					sum += p;
					
					if(factorsFound==factorsNeeded) {
						System.out.println("SUM = " + sum);
						break;
					}
				}
			}
		}
	}
}

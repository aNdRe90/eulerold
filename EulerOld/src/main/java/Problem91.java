import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import MyStuff.CombinationGenerator;


public class Problem91 {
	/*
	 * The points P (x1, y1) and Q (x2, y2) 
	 * are plotted at integer co-ordinates and are joined to the origin, O(0,0), to form OPQ.
	 * 
	 * There are exactly fourteen triangles containing a right angle that can be formed 
	 * when each co-ordinate lies between 0 and 2 inclusive; that is,
	 * 0 <= x1, y1, x2, y2 <= 2.
	 * 
	 * Given that 0 <= x1, y1, x2, y2 <= 50, how many right triangles can be formed?
	 */	
	public static void main(String[] args) {
		int N = 50;		
		List<int[]> points = getPoints(N);
		int rightTriangles = 0;
		
		int[] point0 = {0,0};
		int[] point1 = {-1, -1};
		int[] point2 = {-1, -1};
		//51*51 - 1 [(0,0)] = 2600
		CombinationGenerator combGen = new CombinationGenerator( (N+1)*(N+1)-1, 2);
		
		while(combGen.hasMore()) {
			int[] combination = combGen.getNext();
			point1 = points.get(combination[0]);
			point2 = points.get(combination[1]);
			//String x = Arrays.toString(point0)+"  "+Arrays.toString(point1)+"  "+Arrays.toString(point2);
			//System.out.println(x);
			if(pointsCreateRightTriangle(point0, point1, point2)) {
				//String x = Arrays.toString(point0)+"  "+Arrays.toString(point1)+"  "+Arrays.toString(point2);
				//System.out.println(x);
				rightTriangles++;
			}				
		}
		
		System.out.println(rightTriangles);
	}

	private static boolean pointsCreateRightTriangle(int[] point0, int[] point1, int[] point2) {
		if(pointsAreInOneLine(point0, point1, point2))
			return false;
		
		//y = Ax + B     - line		
		if(areLinesOrthogonal(point0, point1, point1, point2) || 
		   areLinesOrthogonal(point0, point2, point2, point1) || 	
		   areLinesOrthogonal(point1, point0, point0, point2) ) 	
			return true;
		
		return false;
	}

	private static boolean areLinesOrthogonal(double a1, double a2) {
		if( (a1==0.0 && a2==Double.MAX_VALUE) || (a2==0.0 && a1==Double.MAX_VALUE) )
			return true;
		
		return a1==-1.0/a2;
	}
	
	private static boolean areLinesOrthogonal(int[] point1, int[] point2, int[] point3, int[] point4) {
		//a = (y2-y1)/(x2-x1)
		//orthogonal when  a1 = -1/a2:
		//
		//that means when:
		//(y2-y1)/(x2-x1) = (x3-x4)/(y4-y3)
		int x1 = point1[0];
		int y1 = point1[1];
		
		int x2 = point2[0];
		int y2 = point2[1];
		
		int x3 = point3[0];
		int y3 = point3[1];
		
		int x4 = point4[0];
		int y4 = point4[1];
		
		if(x2==x1)
			return y3==y4;
		if(y3==y4)
			return x1==x2;
		
		Fraction a1 = new Fraction(y2-y1, x2-x1);
		a1.reduce();
		Fraction a2 = new Fraction(x3-x4, y4-y3);
		a2.reduce();
		
		return a1.equals(a2);
	}

	private static boolean pointsAreInOneLine(int[] point0, int[] point1, int[] point2) {
		return (point0[0]==point1[0] && point1[0]==point2[0]) || 
			   (point0[1]==point1[1] && point1[1]==point2[1]);
	}

	private static double getAlineCoefficient(int[] point1, int[] point2) {
		double x1 = (double) point1[0];
		double x2 = (double) point2[0];
		double y1 = (double) point1[1];
		double y2 = (double) point2[1];
		
		if(x2==x1)
			return Double.MAX_VALUE;
		
		return (y2-y1)/(x2-x1);
	}

	private static List<int[]> getPoints(int n) {
		List<int[]> points = new ArrayList<int[]>( (n+1)*(n+1) );
		
		for(int i=0; i<=n; i++)
			for(int j=0; j<=n; j++) {
				if(i==0 && j==0)
					continue;
				
				int[] point = new int[2];
				point[0] = i;
				point[1] = j;
				points.add(point);
			}
		
		return points;
	}
}

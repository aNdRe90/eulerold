

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import MyStuff.CombinationGenerator;
import MyStuff.VariationNumber;

public class Problem111 {
	/*
	 * Considering 4-digit primes containing repeated digits it is clear that they cannot 
	 * all be the same: 1111 is divisible by 11, 2222 is divisible by 22, and so on. 
	 * But there are nine 4-digit primes containing three ones:
	 * 
	 * 1117, 1151, 1171, 1181, 1511, 1811, 2111, 4111, 8111
	 * 
	 * We shall say that M(n, d) represents the maximum number of repeated digits 
	 * for an n-digit prime where d is the repeated digit, N(n, d) represents the 
	 * number of such primes, and S(n, d) represents the sum of these primes.
	 * 
	 * So M(4, 1) = 3 is the maximum number of repeated digits for a 4-digit prime 
	 * where one is the repeated digit, there are N(4, 1) = 9 such primes, and the sum 
	 * of these primes is S(4, 1) = 22275. It turns out that for d = 0, 
	 * it is only possible to have M(4, 0) = 2 repeated digits, but there are 
	 * N(4, 0) = 13 such cases.
	 * 
	 * In the same way we obtain the following results for 4-digit primes.
	 * 
	 * 	Digit, d 		M(4, d) 	N(4, d) 		S(4, d)
		0 				2 			13 				67061
		1 				3 			9 				22275
		2 				3 			1 				2221
		3 				3 			12 				46214
		4 				3 			2 				8888
		5 				3 			1 				5557
		6 				3 			1 				6661
		7 				3 			9 				57863
		8 				3 			1 				8887
		9 				3 			7 				48073
		
		For d = 0 to 9, the sum of all S(4, d) is 273700.

		Find the sum of all S(10, d).
	 */
	public static void main(String[] args) {
		long sum = 0L;
		
		for(int i=0; i<10; i++)
			sum += S(10, i);
		
		System.out.println(sum);
	}

	private static long S(int n, int d) {
		List<Long> primesWithMaxNumberOfDigitD = getPrimesOfLengthXWithMaxNumberOfDigitY(n, d);
		
		long sum = 0L;
		for(long prime : primesWithMaxNumberOfDigitD)
			sum += prime;
		
		return sum;
	}

	private static List<Long> getPrimesOfLengthXWithMaxNumberOfDigitY(int n, int d) {
		if(d<0 || d>9 || n<1 || n>18)
			return null;
		
		int[] digitsToUse = getDigitsWithout(d);
		List<Long> primes = new LinkedList<Long>();
		
		boolean maxFound = false;
		for(int maxDigitsD=n-1; maxDigitsD>0 && !maxFound; maxDigitsD--) {
			CombinationGenerator combGen = new CombinationGenerator(n, maxDigitsD);
			while(combGen.hasMore()) {
				int[] numberPart = getNumberFilledWithDigitsDonNextCombinationPositions
										(n, combGen.getNext(), d);
				//System.out.println(Arrays.toString(numberPart)+"------"+maxDigitsD);
				
				VariationNumber variation = new VariationNumber(getBases(9, n-maxDigitsD));
				while(!variation.isOverflow()) {
					int[] numberAll = getAllNumberFillingBlankPlacesWithGivenDigits
										(numberPart, variation.getValues(), digitsToUse);
					if(numberAll[0]==0) {
						variation.increment();
						continue;
					}
					
					long number = getNumberFromDigits(numberAll);
					if(MyStuff.MyMath.isPrime(number)) {
						maxFound = true;
						primes.add(number);
					}
					
					variation.increment();
				}
			}
		}		
		
		return primes;
	}
	
	private static int[] getAllNumberFillingBlankPlacesWithGivenDigits
						(int[] numberPart, int[] digitsToUsePositions, int[] digitsToUse) {
		int[] number = new int[numberPart.length];
		
		for(int i=0, k=0; i<number.length; i++) {
			if(numberPart[i]!=-1)
				number[i] = numberPart[i];
			else
				number[i] = digitsToUse[digitsToUsePositions[k++]];
		}
		
		return number;
	}

	private static long getNumberFromDigits(int[] numberAll) {
		StringBuffer buffer = new StringBuffer("");
		
		for(int i=0; i<numberAll.length; i++)
			buffer.append(numberAll[i]);
			
		return Long.parseLong(buffer.toString());
	}

	private static int[] getBases(int base, int length) {
		int[] bases = new int[length];
		
		for(int i=0; i<length; i++)
			bases[i] = base;
		
		return bases;
	}

	private static int[] getNumberFilledWithDigitsDonNextCombinationPositions
						(int n, int[] combination, int d) {
		int[] number = new int[n];
		for(int i=0; i<number.length; i++)
			number[i] = -1;
		
		for(int i=0; i<combination.length; i++)
			number[combination[i]] = d;
		
		return number;
	}

	private static int[] getDigitsWithout(int d) {		
		int[] result = new int[9];
		
		for(int i=0, k=0; i<10; i++) {
			if(i==d)
				continue;
			
			result[k++] = i;
		}
		
		return result;
	}
}

import java.math.BigInteger;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;


public class Problem49 
{
	/*
	 * The arithmetic sequence, 1487, 4817, 8147, in which each of the terms 
	 * increases by 3330, is unusual in two ways: (i) each of the three terms are prime, 
	 * and, (ii) each of the 4-digit numbers are permutations of one another.
	 * 
	 * There are no arithmetic sequences made up of three 1-, 2-, or 3-digit primes, 
	 * exhibiting this property, but there is one other 4-digit increasing sequence.
	 * 
	 * What 12-digit number do you form by concatenating the three terms in this sequence?
	 */
	
	public static void main(String[] args) 
	{
		int first = 0;
		int second = 0;
		int third = 0;
		
		LinkedList<Integer> primes = new LinkedList<Integer>();
		
		for(int i=1000; i<10000; i++)
		{
			if(MyMath.isPrime((long)i))
				primes.add(new Integer(i));
		}
		
		Iterator<Integer> iter = primes.iterator();
		int[] permPrimes = new int[primes.size()];

		for(int i=0; i<permPrimes.length; i++)
			permPrimes[i] = iter.next().intValue();
		
		primes = null;		
		
		for(int i=0; i<permPrimes.length; i++)
		{
			//first = permPrimes[i];
			//if(permPrimes[i]!=1487)
			//{
				for(int j=(i+1); j<permPrimes.length; j++)
				{
					//second = permPrimes[j];
					
					if(areOfSameDigits(permPrimes[i], permPrimes[j]))
					{
						int difference = permPrimes[j] - permPrimes[i];
						int possibleThird = permPrimes[j] + difference;
						if(permPrimes[i]==1487 && permPrimes[j]==4817 && possibleThird==8147)
							continue;
						
						if(areOfSameDigits(permPrimes[j], possibleThird))
						{
							for(int k=(j+1); k<permPrimes.length; k++)
							{
								if(permPrimes[k]>possibleThird)
									break;
								else if(permPrimes[k]==possibleThird)
								{
									first = permPrimes[i];
									second = permPrimes[j];
									third = permPrimes[k];
									i = j = permPrimes.length;
									
									break;
								}
							}
						}
					}				
				//}
			}
		}
		
		StringBuffer concatenatedTerms = new StringBuffer(12);
		concatenatedTerms.append(first);
		concatenatedTerms.append(second);
		concatenatedTerms.append(third);
		System.out.println(concatenatedTerms.toString());	
	}
	
	public static boolean areOfSameDigits(int first, int second)
	{
		String firstString = Integer.toString(first);
		String secondString = Integer.toString(second);
		
		if(firstString.length()!=secondString.length())
			return false;
		
		LinkedList<Character> digitsFirst = new LinkedList<Character>();
		LinkedList<Character> digitsSecond = new LinkedList<Character>();
		
		for(int j=0; j<firstString.length(); j++)
		{
			digitsFirst.add(new Character(firstString.charAt(j)));
			digitsSecond.add(new Character(secondString.charAt(j)));
		}
		
		Collections.sort(digitsFirst);
		Collections.sort(digitsSecond);
		
		Iterator<Character> iterFirst = digitsFirst.iterator();
		Iterator<Character> iterSecond = digitsSecond.iterator();
		
		while(iterFirst.hasNext()) //second is the same length so no need to check iterSecond
		{
			if(! (iterFirst.next().equals(iterSecond.next())) )
				return false;
		}
		
		return true;
	}
}


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Problem126 {
	/*
	 * The minimum number of cubes to cover every visible face 
	 * on a cuboid measuring 3 x 2 x 1 is twenty-two.
	 * 
	 * If we then add a second layer to this solid it would require 
	 * forty-six cubes to cover every visible face, 
	 * the third layer would require seventy-eight cubes, 
	 * and the fourth layer would require one-hundred and 
	 * eighteen cubes to cover every visible face.
	 * 
	 * However, the first layer on a cuboid measuring 5 x 1 x 1
	 * also requires twenty-two cubes; 
	 * similarly the first layer on cuboids measuring 5 x 3 x 1, 
	 * 7 x 2 x 1, and 11 x 1 x 1 all contain forty-six cubes.
	 * 
	 * We shall define C(n) to represent the number of cuboids 
	 * that contain n cubes in one of its layers. So C(22) = 2, 
	 * C(46) = 4, C(78) = 5, and C(118) = 8.
	 * 
	 * It turns out that 154 is the least value of n for 
	 * which C(n) = 10.
	 * 
	 * Find the least value of n for which C(n) = 1000.
	 */
	
	/*SOLUTION
	 * Creating equation for number of cubes in i-th layer.
	 * 
	 * First layer of cuboid a x b x c  where a>=b>=c has:
	 * 2ab + 2ac + 2bc cubes.
	 * 
	 * Next layer also add 2ab cubes (on top and bottom),
	 * however on sides it "expands" c by 2 and eventually
	 * adds 2a(c+2) + 2b(c+2).
	 * 
	 * We must also take care of those cubes on corner.
	 * Using program to draw 3D solids from cubes: 
	 * http://connectedmath.msu.edu/CD/Grade6/Ruins/index.html
	 * 
	 * it looks that each corner needs to add c cubes.
	 * 
	 * So finally, second layer has:
	 * 2ab + 2a(c+2) + 2b(c+2) + 4c   cubes.
	 * 
	 * Using the program further we analyze that each corner
	 * adds on second layer c, third layer 2c + 2*1,
	 * fourth layer 3c + 2*2 + 2*1.
	 * 
	 * Assuming that first layer is layer number 0,
	 * next is 1 etc. and the previous conslusions we present an
	 * equation for cubes in i-th layer:
	 * 
	 * S(i) = 2ab + 2a(c+2i) + 2b(c+2i) + 4(ic + 2*( (1+i-1)(i-1)/2 )) =
	 * =  2ab + 2ac + 2bc + 4ai + 4bi + 4ci + 4i(i-1) =
	 * = x + 4i(a+b+c-1 + i) = x + 4i(y+i)
	 * where x=2ab + 2ac + 2bc;   y=a+b+c-1
	 * and x is also the first layer cubes.
	 * 
	 * Then we create map of number of cubes in layer and its counters.
	 * We guess the max expected result to border the layer to compute
	 * for each cube.
	 * 
	 * Then we lopp over a,b,c such that a>=b>=c and
	 * cosnidering that first layer of each cube must be <expectedMax.
	 * First layer is x=2ab+2ac+2bc  so the borders for b takes c=1.
	 */
	public static void main(String[] args) {	
		final long maxExpectedResult = 20000L;	
		final int maxCuboidSidelength = 5000;
		final int N = 1000;
		Map<Long, Integer> values = new LinkedHashMap<Long, Integer>();
		
		for(int a=1; a<=maxCuboidSidelength; a++) {
			System.out.println(a);
			//for(int b=a; b<=maxCuboidSidelength; b++) {
			//	for(int c=b; c<=maxCuboidSidelength; c++) {
			
			for(int b=a; (2*(a*b+b+a))<maxExpectedResult; b++) {
				for(int c=b; (2*(a*b+b*c+a*c))<maxExpectedResult; c++) {
					Cuboid cube = new Cuboid(c,b,a);
					for(int i=0; ; i++) {
						Long cubesInLayer = 
								Long.valueOf(cube.getLayersCubes(i));
						if(cubesInLayer>maxExpectedResult) {
							break;
						}
						
						Integer currentCounter = values.get(cubesInLayer);
						int counter = 0;
						if(currentCounter!=null) {
							counter = currentCounter.intValue();
						}
						
						values.put(cubesInLayer, counter+1);
					}
				}
			}
		}
		
		List<Long> keys = new ArrayList<Long>(values.keySet());
		Collections.sort(keys);
		
		//List<Integer> vals = new ArrayList<Integer>(values.values());
		//Collections.sort(vals);
		//System.out.println(vals.contains(Integer.valueOf(N)));
		
		for(Long key : keys) {
			int value = values.get(key).intValue();
			if(value==N) {
				System.out.println(key);
				break;
			}
		}
	}
	
	private static class Cuboid implements Comparable<Cuboid>{
		private long a;
		private long b;
		private long c;		
		private long helper;
		private long firstLayerCubes;
		
		public Cuboid(long a, long b, long c) {
			this.a = a;
			this.b = b;
			this.c = c;
			this.helper = a + b + c - 1L;
			firstLayerCubes = 2L*(a*b + a*c + b*c);
		}
		
		public long getLayersCubes(long layer) {
			if(layer<0L) {
				throw new IllegalArgumentException(
							"Layer must be >=0.");
			}
			
			return firstLayerCubes + 4L*layer*(helper + layer);
		}

		@Override
		public int compareTo(Cuboid other) {
			long difference = this.firstLayerCubes - other.firstLayerCubes;
			if(difference>0L) {
				return 1;
			}
			else if(difference<0L) {
				return -1;
			}
			else {
				return 0;
			}
		}
		
		@Override
		public String toString() {
			return "[" + a + ", " + b + ", " + c + "]";
		}
	}
}



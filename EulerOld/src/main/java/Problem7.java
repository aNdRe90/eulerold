import java.util.ArrayList;


public class Problem7 
{
	/*
	 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see
	 * that the 6^th prime is 13.
	 * 
	 * What is the 10001^st prime number?
	 */
	public static void main(String[] args) 
	{
		ArrayList<Long> primes = new ArrayList<Long>(10001);
		primes.add(new Long(2));
		
		for(long i = 3; ; i+=2)
		{
			int size = primes.size();
			if(size==10001)
				break;
			
			int counter = 0;
			
			for(int j = 0; j<size; j++)
			{
				long prime = primes.get(j).longValue();
				if(i%prime!=0)
					counter++;					
			}
			
			if(counter==size)
				primes.add(new Long(i));
		}	
		
		System.out.println(primes.get(primes.size()-1).longValue());
	}
}


public class Problem31 
{
	/*
	 * In England the currency is made up of pound, P, and pence, p, and there are eight coins in general circulation:

    	1p, 2p, 5p, 10p, 20p, 50p, P1 (100p) and P2 (200p).

		It is possible to make P2 in the following way:

    	1�P1 + 1�50p + 2�20p + 1�5p + 1�2p + 3�1p

	How many different ways can P2 be made using any number of coins?	 */
	public static void main(String[] args) 
	{
		int sumToGet = 200;	
		for(int i=0; i<coinValues.length; i++)
			maxCoinNumber[i] = sumToGet/coinValues[i];
		
		long combinations = combinationsWithCoinMax(0, sumToGet);
		System.out.println(combinations);
		
	}
	
	public static long combinationsWithCoinMax(int maxCoinIndex, int valueLeft)
	{
		long combinations = 0L;
		for(int i=0; i<=maxCoinNumber[maxCoinIndex]; i++)
		{
			int newValue = valueLeft - coinValues[maxCoinIndex]*i;
			
			if(newValue==0)
			{
				++combinations;
				if( (maxCoinIndex+1)==coinValues.length)
					break;
			}			
			
			if(newValue>0 && (maxCoinIndex+1)<coinValues.length)
				combinations += combinationsWithCoinMax(maxCoinIndex+1, newValue);			
		}
		
		return combinations;
	}
	
	private static int[] coinValues = { 200, 100, 50, 20, 10, 5, 2, 1 };
	private static int[] maxCoinNumber = new int[coinValues.length];
}

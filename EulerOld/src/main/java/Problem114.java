

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

public class Problem114 {
	/*
	 * A row measuring seven units in length has red blocks with a minimum length 
	 * of three units placed on it, such that any two red blocks
	 *  (which are allowed to be different lengths) are separated by 
	 *  at least one black square. There are exactly seventeen ways of doing this.
	 *  
	 *  How many ways can a row measuring fifty units in length be filled?
	 *  
	 *  NOTE: Although the example above does not lend itself to the possibility, 
	 *  in general it is permitted to mix block sizes. For example, on a row measuring 
	 *  eight units in length you could use red (3), black (1), and red (4).
	 *  
	 */
	public static void main(String[] args) {
		System.out.println(getSolutionForRow(N));
	}

	private static BigInteger getSolutionForRow(int rowLength) {
		BigInteger preResult = getPreResult(rowLength, -1);
		if(preResult!=null)
			return preResult;
		
		BigInteger result = BigInteger.ONE;		
		if(rowLength<minBlockLength)
			return result;		
		
		for(int blockLength=minBlockLength; blockLength<=rowLength; blockLength++) {
			result = result.add(getSolutionForRowAndBlockLength(rowLength, blockLength));
		}
		
		solutions.get(rowLength).put(-1, result);
		return result;
	}

	private static BigInteger getSolutionForRowAndBlockLength(int rowLength, int blockLength) {
		BigInteger preResult = getPreResult(rowLength, blockLength);
		if(preResult!=null)
			return preResult;
		
		BigInteger result = BigInteger.ZERO;
		int maxStartIndex = rowLength - blockLength;
		
		for(int startIndex=0; startIndex<=maxStartIndex; startIndex++) {
			result = result.add(getSolutionForRow(maxStartIndex-startIndex-1));
		}
		
		solutions.get(rowLength).put(blockLength, result);
		return result;
	}
	
	
	private static BigInteger getPreResult(int rowLength, int blockLength) {
		BigInteger preResult = null;
		
		Map<Integer, BigInteger> temp = solutions.get(rowLength);
		if(temp!=null)
			preResult = temp.get(blockLength);
			
		return preResult;
	}

	private static final int N = 50;
	private static final int minBlockLength = 3;
	private static Map<Integer, Map<Integer, BigInteger>> solutions 
				= new HashMap<Integer, Map<Integer, BigInteger>>();
	
	static {
		for(int i=0; i<=N; i++)
			solutions.put(i, new HashMap<Integer, BigInteger>());
	}
}

import java.math.BigInteger;


public class Problem57 
{
	/*
	 * It is possible to show that the square root of two can be expressed as an infinite continued fraction.

	sqrt(2) = 1 + 1/(2 + 1/(2 + 1/(2 + ... ))) = 1.414213...

	By expanding this for the first four iterations, we get:

	1 + 1/2 = 3/2 = 1.5
	1 + 1/(2 + 1/2) = 7/5 = 1.4
	1 + 1/(2 + 1/(2 + 1/2)) = 17/12 = 1.41666...
	1 + 1/(2 + 1/(2 + 1/(2 + 1/2))) = 41/29 = 1.41379...

	The next three expansions are 99/70, 239/169, and 577/408, but the eighth expansion, 
	1393/985, is the first example where the number of digits in the numerator exceeds 
	the number of digits in the denominator.

	In the first one-thousand expansions, how many fractions contain a numerator 
	with more digits than denominator?
	 */
	public static void main(String[] args) 
	{
		int numberOfExpansions = 1000;
		BigFraction[] expansions = new BigFraction[numberOfExpansions];
		BigFraction golden = new BigFraction(new BigInteger("2"));    //magic value:)
		BigFraction one = new BigFraction(BigInteger.ONE);
		BigFraction two = new BigFraction(new BigInteger("2"));
		
		expansions[0] = new BigFraction(new BigInteger("2"));   //expansions without this first "1" and reversed.		
		for(int i=1; i<numberOfExpansions; i++)     //I reverse it at the end and add 1.
		{
			expansions[i] = new BigFraction(golden.reverse().add(two));
			golden = new BigFraction(expansions[i]);
			//reduce???			
		}
		
		int counter = 0;
		for(int i=0; i<numberOfExpansions; i++)
		{
			expansions[i] = expansions[i].reverse().add(one);
			
			String num = expansions[i].getNumerator().toString();
			String den = expansions[i].getDenominator().toString();
			
			if(num.length()>den.length())
				counter++;
		}
		
		System.out.println(counter);
	}
}

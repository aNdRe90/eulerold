import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;


public class Problem86 {
	/*
	 * A spider, S, sits in one corner of a cuboid room, measuring 6 by 5 by 3, and a fly, F, 
	 * sits in the opposite corner. By travelling on the surfaces of the room the shortest "straight line" 
	 * distance from S to F is 10 and the path is shown on the diagram.
	 * 
	 * However, there are up to three "shortest" path candidates for any given cuboid 
	 * and the shortest route is not always integer.
	 * 
	 * By considering all cuboid rooms with integer dimensions, up to a maximum size of M by M by M, 
	 * there are exactly 2060 cuboids for which the shortest distance is integer when M=100, 
	 * and this is the least value of M for which the number of solutions first exceeds two thousand; 
	 * the number of solutions is 1975 when M=99.
	 * 
	 * Find the least value of M such that the number of solutions first exceeds one million.
	 */
	
	/*SOLUTION: a>=b>=c
	 * Wyrysuj siatk� bryly i okaze sie, ze sa 3 sciezki najkrotsze z czego 2 tej samej dlugosci.
	 */
	public static void main(String[] args) {			
		int resultM = -1;
		for(int M=1; ;M++) {
			getCuboids(M);
			System.out.println(cuboids);
			if(cuboids>1000000) {
				resultM = M;
				break;
			}
		}		
		
		System.out.println(resultM );
	}
	
	private static double getFirstDistance(double a, double b, double c) {
		return Math.sqrt((a+c)*(a+c) + b*b);
	}
	
	private static double getSecondDistance(double a, double b, double c) {
		return Math.sqrt((b+c)*(b+c) + a*a);
	}
	//third is the same as second
	
	private static int getCuboids(int M) {		
		//int cuboids = 0;
		//for(int a=1; a<=M; a++) {
		int a = M;
			for(int b=1; b<=a; b++) {
				for(int c=1; c<=b; c++) {
					double firstDistance = getFirstDistance((double)a, (double)b, (double)c);
					double secondDistance = getSecondDistance((double)a, (double)b, (double)c);
					
					double minDistance = firstDistance>secondDistance ? secondDistance : firstDistance;
					if(Math.floor(minDistance)==minDistance) {
						cuboids++;
					}					
				}
			}
		//}		
		return cuboids;
	}
	
	private static int cuboids = 0;
}
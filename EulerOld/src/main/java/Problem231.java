

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import MyStuff.Prime;

public class Problem231 {
	/*
	 * The binomial coefficient C(10,3) = 120.
	 * 120 = 2^3 x 3 x 5 = 2 x 2 x 2 x 3 x 5, and 2 + 2 + 2 + 3 + 5 = 14.
	 * 
	 * So the sum of the terms in the prime factorisation of C(10,3) is 14.
	 * 
	 * Find the sum of the terms in the prime factorisation of C(20000000, 15000000). 
	 */

	/*
	 * SOLUTION:
	 * C(10, 3) = 10*9*8 / 3*2 = 2*5*3*3*2*2*2 / 3*2
	 * Sum = 2+5+3+3+2+2+2 -3-2 = 14
	 * 
	 * C(20.000.000, 15.000.000) = C(20.000.000, 5.000.000)
	 */
	private static long n = (long) (2.0 * 1e7);
	private static long k = (long) (5.0 * 1e6);
	private static List<Long> primes = getPrimesUpTo((long) Math.sqrt(n));

	public static void main(final String[] args) {
		long sum = 0L;

		for (long i = 0L; i < k; i++) {
			if ((i % 1000L) == 0L) {
				System.out.println(i);
			}

			long numerator = n - i;
			long denominator = k - i;
			sum += getSum(getPrimeFactorization(numerator));
			sum -= getSum(getPrimeFactorization(denominator));
		}

		System.out.println("RESULT = " + sum);
	}

	private static List<Long> getPrimesUpTo(final long v) {
		List<Long> primes = Prime.getPrimesUnder(v);

		if (Prime.isPrime(v)) {
			primes.add(v);
		}

		return primes;
	}

	private static List<Long> getPrimeFactorization(long v) {
		if (Prime.isPrime(v)) {
			return Arrays.asList(v);
		}

		boolean end = false;
		List<Long> primeDivisors = new LinkedList<Long>();

		for (long prime : primes) {
			if (prime > (long) Math.sqrt(v)) {
				break;
			}

			while (true) {
				if (v == 1) {
					end = true;
					break;
				}

				if ((v % prime) == 0L) {
					v /= prime;

					primeDivisors.add(prime);
				} else if (Prime.isPrime(v)) {
					primeDivisors.add(v);
					end = true;
					break;
				} else {
					break;
				}
			}

			if (end) {
				break;
			}
		}

		return primeDivisors;
	}

	private static long getSum(final List<Long> values) {
		long sum = 0L;

		for (long v : values) {
			sum += v;
		}

		return sum;
	}

}

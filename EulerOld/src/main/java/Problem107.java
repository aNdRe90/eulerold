

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.StringTokenizer;

import MyStuff.GraphMatrix;

public class Problem107 {
	/*
	 * The following undirected network consists of 
	 * seven vertices and twelve edges with a total weight of 243.
	 * 
	 * The same network can be represented by the matrix below.
	 * 
	 *     	A	B	C	D	E	F	G
		A	-	16	12	21	-	-	-
		B	16	-	-	17	20	-	-
		C	12	-	-	28	-	31	-
		D	21	17	28	-	18	19	23
		E	-	20	-	18	-	-	11
		F	-	-	31	19	-	-	27
		G	-	-	-	23	11	27	-

		However, it is possible to optimise the network by removing some edges 
		and still ensure that all points on the network remain connected. 
		The network which achieves the maximum saving is shown below. 
		It has a weight of 93, representing a saving of 243 - 93 = 150 from the original network.
		
		Using network.txt (right click and 'Save Link/Target As...'), 
		a 6K text file containing a network with forty vertices, and given in matrix form, 
		find the maximum saving which can be achieved by removing redundant edges 
		whilst ensuring that the network remains connected.
	 */
	public static void main(String[] args) {
		ArrayList<ArrayList<Integer>> matrix = readMatrixFromFile("network.txt");
		GraphMatrix graph = new GraphMatrix(matrix, false);
		long sumOfWeightsBefore = graph.getSumOfWeights();
		
		GraphMatrix mst = graph.getMinimumSpanningTree();
		long sumOfWeightsAfter = mst.getSumOfWeights();
		
		System.out.println(sumOfWeightsBefore - sumOfWeightsAfter);
	}

	private static ArrayList<ArrayList<Integer>> readMatrixFromFile(String filename) {
		ArrayList<ArrayList<Integer>> matrix = new ArrayList<ArrayList<Integer>>(vertices);		
		BufferedReader reader = getReaderFromFile(filename);
		String line = null;
		
		int k=0;
		try {
			while ( (line=reader.readLine())!=null ) {
				matrix.add(new ArrayList<Integer>(vertices));
				matrix.get(k++).addAll(getRowFromLine(line));
			}
		} catch (IOException e) { e.printStackTrace(); }
		
		return matrix;
	}

	private static ArrayList<Integer> getRowFromLine(String line) {
		ArrayList<Integer> row = new ArrayList<Integer>(vertices);
		StringTokenizer tokenizer = new StringTokenizer(line, ",");
		
		while(tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			if(token.equals("-"))
				row.add(Integer.valueOf(0));
			else
				row.add(Integer.parseInt(token));
		}
			
		return row;
	}

	private static BufferedReader getReaderFromFile(String filename) {
		BufferedReader reader = null;
		
		try {
			reader = new BufferedReader(new FileReader(filename));
		}
		catch(IOException e) { e.printStackTrace(); };
		
		
		return reader;
	}
	
	private static final int vertices = 40;
}

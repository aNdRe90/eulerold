import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

import MyStuff.GraphList;


public class Problem83
{
	/*
	 * NOTE: This problem is a significantly more challenging version of Problem 81.

		In the 5 by 5 matrix below, the minimal path sum from the top left to the bottom right, 
		by moving left, right, up, and down, is indicated in bold red and is equal to 2297.
		
		131	673	234	103	18
		201	96	342	965	150
		630	803	746	422	111
		537	699	497	121	956
		805	732	524	37	331

		Find the minimal path sum, in matrix.txt (right click and 'Save Link/Target As...'), 
		a 31K text file containing a 80 by 80 matrix, from the top left to the bottom right by moving left, right, up, and down.
	 */
	
	/*
	 * SOLUTION:
	 * Dijkstra algorithm for finding shortest path in graph between two vertice.
	 */
	public static void main(String[] args) 
	{
		matrix = readMatrix("matrix.txt");
		GraphList graph = new GraphList(rows*columns);
		
		for(int row=0; row<rows; row++)
			for(int col=0; col<columns; col++)
			{
				int upRow = row-1;
				int upCol = col;
				if(upRow>=0)
					graph.addEdge(row*rows+col, upRow*rows+upCol, matrix[upRow][upCol]);

				
				int downRow = row+1;
				int downCol = col;
				if(downRow<rows)
					graph.addEdge(row*rows+col, downRow*rows+downCol, matrix[downRow][downCol]);

				
				int leftRow = row;
				int leftCol = col-1;
				if(leftCol>=0)
					graph.addEdge(row*rows+col, leftRow*rows+leftCol, matrix[leftRow][leftCol]);

				
				int rightRow = row;
				int rightCol = col+1;
				if(rightCol<columns)
					graph.addEdge(row*rows+col, rightRow*rows+rightCol, matrix[rightRow][rightCol]);

			}
		
		int minPath = graph.getShortestPath(0, rows*columns-1);
		System.out.println(minPath+matrix[0][0]);
	}
	
	
	private static int[][] readMatrix(String filename)
	{
		BufferedReader f = null;		
		try	{	f = new BufferedReader(new FileReader(filename));	}
		catch(IOException e)	{	e.printStackTrace();		}
		
		String line = null;			
		int row = 0;
		int[][] matrix = new int[rows][columns];
		do
		{
			try {	line = f.readLine();	} 
			catch(IOException ee) { ee.printStackTrace(); }
			
			if(line==null)
				break;
			
			StringTokenizer st = new StringTokenizer(line, ",");
			int col = 0;
			while(st.hasMoreTokens())
				matrix[row][col++] = Integer.parseInt(st.nextToken());
			
			row++;
		}
		while(true);
		
		return matrix;
	}
	
	private static final int rows = 80;
	private static final int columns = 80;
	private static int[][] matrix;
}

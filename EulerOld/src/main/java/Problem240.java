

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import MyStuff.VariationNumber;

public class Problem240 {
	/*
	 * There are 1111 ways in which five 6-sided dice (sides numbered 1 to 6) 
	 * can be rolled so that the top three sum to 15. Some examples are:
	 * 
	 * D1,D2,D3,D4,D5 = 4,3,6,3,5
	 * D1,D2,D3,D4,D5 = 4,3,3,5,6
	 * D1,D2,D3,D4,D5 = 3,3,3,6,6
	 * D1,D2,D3,D4,D5 = 6,6,3,3,3
	 * 
	 * In how many ways can twenty 12-sided dice (sides numbered 1 to 12)
	 * be rolled so that the top ten sum to 70?
	 */

	//	private static final int sides = 6;
	//	private static final int topSum = 15;
	//	private static final int topAmount = 3;
	private static final int sides = 12;
	private static final int topSum = 70;
	private static final int topAmount = 10;

	private static final Map<Integer, Map<Top, Long>> memory = new HashMap<Integer, Map<Top, Long>>();

	public static void main(String[] args) {
		final int dice = 20;
		//bruteForce(dice);

		Top top = new Top();
		long result = getResult(dice, top);

		System.out.println("RESULT = " + result);
		//System.out.println(memory);
	}

	private static void bruteForce(int dice) {
		int[] bases = new int[dice];
		for (int i = 0; i < bases.length; i++) {
			bases[i] = sides;
		}

		int counter = 0;
		VariationNumber var = new VariationNumber(bases);
		while (!var.isOverflow()) {
			int[] values = var.getValues();
			for (int i = 0; i < values.length; i++) {
				values[i]++;
			}

			int[] copy = Arrays.copyOf(values, values.length);
			Arrays.sort(copy);
			int sum = 0;
			for (int i = 1; i <= topAmount; i++) {
				sum += copy[copy.length - i];
			}

			if (sum == topSum) {
				counter++;
				System.out.println(Arrays.toString(values));
			}

			var.increment();
		}

		System.out.println("RESULT = " + counter);
	}

	private static long getResult(int dice, Top top) {
		//System.out.println("DICE = " + dice + ", top = " + top);

		Long resultFromMemory = getFromMemory(dice, top);
		if (resultFromMemory != null) {
			return resultFromMemory;
		}

		//consider that top has to be of length topAmount!!!

		long result = 0L;
		int sum = top.getSum();
		int min = top.getMinimum();
		int topSize = top.size();

		if (dice == 1) {
			if ((sum < topSum) && (topSize == topAmount)) {
				int left = (topSum - sum) + min;
				if ((left >= 1) && (left <= sides)) {
					long res = 1L;
					putToMemory(dice, top, res);
					return res;
				} else {
					long res = 0L;
					putToMemory(dice, top, res);
					return res;
				}
			} else if ((sum < topSum) && (topSize == (topAmount - 1))) {
				int left = topSum - sum;
				if ((left >= 1) && (left <= sides)) {
					long res = 1L;
					putToMemory(dice, top, res);
					return res;
				} else {
					long res = 0L;
					putToMemory(dice, top, res);
					return res;
				}
			}

			if (sum == topSum) {
				long res = min;
				putToMemory(dice, top, res);
				return res;
			}
		}
		if (sum > topSum) {
			long res = 0L;
			putToMemory(dice, top, res);
			return res;
		}

		if (topSize < topAmount) {
			Top copy = new Top(top);
			for (int i = 1; i <= sides; i++) {
				copy.add(i);
				result += getResult(dice - 1, copy);
				copy.remove(new Integer(i));
			}
		} else { //if topSize==topAmount
			if (sum == topSum) {
				long res = (long) Math.pow(min, dice);
				putToMemory(dice, top, res);
				return res;
			} else { //if sum < topSum
				Top copy = new Top(top);
				for (int i = 1; i <= min; i++) {
					result += getResult(dice - 1, top);
				}

				copy.remove(new Integer(min));
				for (int i = min + 1; i <= sides; i++) {
					copy.add(i);
					result += getResult(dice - 1, copy);
					copy.remove(new Integer(i));
				}
			}
		}

		putToMemory(dice, top, result);
		return result;
	}

	private static void putToMemory(int dice, Top top, long result) {
		Map<Top, Long> map1 = memory.get(dice);
		if (map1 == null) {
			System.out.println("NEW HASHMAP FOR DICE = " + dice);
			map1 = new HashMap<Top, Long>();
			memory.put(dice, map1);
		}

		top.sort();
		map1.put(new Top(top), result);
	}

	private static Long getFromMemory(int dice, Top top) {
		Map<Top, Long> map1 = memory.get(dice);
		if (map1 == null) {
			return null;
		}

		return map1.get(top);
	}

	private static class Top {
		private List<Integer> top = new ArrayList<Integer>(topAmount);

		public Top() {

		}

		public void sort() {
			Collections.sort(this.top);
		}

		public Top(Top t) {
			this.top = new ArrayList<Integer>(t.top);
		}

		public void add(Integer i) {
			this.top.add(i);
		}

		@Override
		public int hashCode() {
			final int prime = 31;

			return getProduct();
		}

		private int getProduct() {
			int product = 1;

			for (int v : this.top) {
				product *= v;
			}

			return product;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			Top other = (Top) obj;
			if (this.top == null) {
				if (other.top != null) {
					return false;
				}
			} else {
				Collections.sort(this.top);
				Collections.sort(other.top);

				int thisSize = this.top.size();
				int otherSize = other.top.size();

				if (thisSize != otherSize) {
					return false;
				}

				for (int i = 0; i < thisSize; i++) {
					if (this.top.get(i) != other.top.get(i)) {
						return false;
					}
				}
			}
			return true;
		}

		public void remove(Integer i) {
			this.top.remove(i);
		}

		public int getMinimum() {
			int min = Integer.MAX_VALUE;

			for (int v : this.top) {
				if (v < min) {
					min = v;
				}
			}

			return min;
		}

		public int getSum() {
			int sum = 0;

			for (int v : this.top) {
				sum += v;
			}

			return sum;
		}

		public int size() {
			return this.top.size();
		}

		@Override
		public String toString() {
			return this.top.toString();
		}
	}
}

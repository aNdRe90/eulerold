

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

public class Problem113 {
	/*
	 * Working from left-to-right if no digit is exceeded by the digit to its left 
	 * it is called an increasing number; for example, 134468.
	 * 
	 * Similarly if no digit is exceeded by the digit to its right 
	 * it is called a decreasing number; for example, 66420.
	 * 
	 * We shall call a positive integer that is neither increasing nor decreasing 
	 * a "bouncy" number; for example, 155349.
	 * 
	 * As n increases, the proportion of bouncy numbers below n increases 
	 * such that there are only 12951 numbers below one-million that are not bouncy 
	 * and only 277032 non-bouncy numbers below 10^10.
	 * 
	 * How many numbers below a googol (10^100) are not bouncy?
	 */
	
	/*
	 * MAGIC SOLUTION:)
	 */
	public static void main(String[] args) {
		BigInteger result = BigInteger.ZERO;
		int N = 100;
		
		for(int i=1; i<=N; i++) {
			for(int d=1; d<10; d++) {
				getIncreasingNumbersOfLengthXUsingDigitsFromY(i, d);
				getDecreasingNumbersOfLengthXUsingDigitsFromY(i, d);
			}
			result = result.add(increasingNumbersOfLentghXStartingFromDigitY.get(i).get(1));
			result = result.add(decreasingNumbersOfLentghXStartingFromDigitY.get(i).get(9));
			result = result.subtract(BigInteger.valueOf(9));
		}
		
		//because in decreasingNumbers... are included "numbers" 0,00,000, etc.
		result = result.subtract(BigInteger.valueOf(N));
		
		System.out.println(result.toString());
	}

	private static BigInteger getIncreasingNumbersOfLengthXUsingDigitsFromY(
			int length, int digit) {
		if (length < 0 || digit < 0 || digit > 9)
			return BigInteger.ZERO;

		if (length == 1)
			return BigInteger.valueOf(10L - digit);

		Map<Integer, BigInteger> increasingOfDecrementedLength = increasingNumbersOfLentghXStartingFromDigitY
				.get(length-1);

		if (increasingOfDecrementedLength != null) {
			BigInteger result = BigInteger.ZERO;
			
			for (int d = digit; d <10; d++) {
				BigInteger increasingNumbersOfThisLengthStartingFromThisDigit = increasingOfDecrementedLength
						.get(d);
				if (increasingNumbersOfThisLengthStartingFromThisDigit != null)
					result = result
							.add(increasingNumbersOfThisLengthStartingFromThisDigit);
				else
					result = result.add(increasingOfDecrementedLength.get(d));
			}
			increasingNumbersOfLentghXStartingFromDigitY.get(length).put(digit,
					result);
			return result;
		} 
		else {
			
			BigInteger result = BigInteger.ZERO;
			for (int d = digit; d <10; d++)
				result = result
						.add(getIncreasingNumbersOfLengthXUsingDigitsFromY(
								length - 1, d));

			increasingNumbersOfLentghXStartingFromDigitY.get(length).put(digit,
					result);
			return result;
		}
	}
	
	private static BigInteger getDecreasingNumbersOfLengthXUsingDigitsFromY(int length, int digit) {
		if(length<0 || digit<0 || digit>9)
			return BigInteger.ZERO;
		
		if(length==1 || digit==0)
			return BigInteger.valueOf(digit+1);
		
		Map<Integer, BigInteger> decreasingOfDecrementedLength
				= decreasingNumbersOfLentghXStartingFromDigitY.get(length-1);
		
		if(decreasingOfDecrementedLength!=null) {
			BigInteger result = BigInteger.ZERO;
			for(int d=digit; d>=0; d--) {
				BigInteger decreasingNumbersOfThisLengthStartingFromThisDigit 
										= decreasingOfDecrementedLength.get(d);
				if(decreasingNumbersOfThisLengthStartingFromThisDigit!=null)
					result = result.add(decreasingNumbersOfThisLengthStartingFromThisDigit);
				else
					result = result.add(BigInteger.ONE);
			}
			result.add(BigInteger.ONE);
			decreasingNumbersOfLentghXStartingFromDigitY.get(length).put(digit, result);
			return result;
		}
		else {
			BigInteger result = BigInteger.ZERO;
			for(int d=digit; d>=0; d--)
				result = result.add(getDecreasingNumbersOfLengthXUsingDigitsFromY(length-1, d));
			
			decreasingNumbersOfLentghXStartingFromDigitY.get(length).put(digit, result);
			return result;
		}		
		
	}
	
	private static Map<Integer, Map<Integer, BigInteger>> increasingNumbersOfLentghXStartingFromDigitY
					= new HashMap<Integer, Map<Integer, BigInteger>>();
	private static Map<Integer, Map<Integer, BigInteger>> decreasingNumbersOfLentghXStartingFromDigitY
	= new HashMap<Integer, Map<Integer, BigInteger>>();
	
	static {
		for(int i=0; i<=100; i++) {
			increasingNumbersOfLentghXStartingFromDigitY.put(
					i, new HashMap<Integer, BigInteger>());
			decreasingNumbersOfLentghXStartingFromDigitY.put(
					i, new HashMap<Integer, BigInteger>());		
		}	
		
		for(int d=0; d<10; d++) {
			increasingNumbersOfLentghXStartingFromDigitY.get(1).put(d, BigInteger.valueOf(10L - d));
			decreasingNumbersOfLentghXStartingFromDigitY.get(1).put(d, BigInteger.valueOf(d+1L));
		}	
	}
}

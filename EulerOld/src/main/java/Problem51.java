import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;


public class Problem51 
{
	/*
	 * By replacing the 1st digit of *3, it turns out that six of the nine 
	 * possible values: 13, 23, 43, 53, 73, and 83, are all prime.
	 * 
	 * By replacing the 3rd and 4th digits of 56**3 with the same digit, 
	 * this 5-digit number is the first example having seven primes among 
	 * the ten generated numbers, yielding the family: 
	 * 56003, 56113, 56333, 56443, 56663, 56773, and 56993. 
	 * Consequently 56003, being the first member of this family, 
	 * is the smallest prime with this property.
	 * 
	 * Find the smallest prime which, by replacing part of the number 
	 * (not necessarily adjacent digits) with the same digit, 
	 * is part of an eight prime value family.
	 */
	public static void main(String[] args) 
	{
		long smallest = 0L;
		long bound = 1000000L;
		ArrayList<Integer> primes = new ArrayList<Integer>(1000);
		for(int i=56003; i<bound ; i+=2) 
		{
			if(MyMath.isPrime(i))			
				primes.add(new Integer(i));			
		}	
		int length = primes.size();
		
		
		permutations = new ArrayList<ArrayList<ArrayList<Integer>>>(6);
		for(int i=1; i<=6; i++) //for number of length i
		{
			permutations.add(new ArrayList<ArrayList<Integer>>());
			for(int j=1; j<=9; j++)   //for digits to replace
				permutations.get(i-1).add(new ArrayList<Integer>());
		}
		//PermutationGenerator p = new PermutationGenerator()		
		for(int i=1; i<=6; i++) //for number of length i
		{
			for(int j=1; j<=i; j++)   //for digits to replace
			{
				ArrayList<Integer> temp = permutations.get(i-1).get(j-1);
				int UB = pow(10, j);			
				for(int k=pow(10, j-1); k<UB; k++) 
				{
					char valueLengthChar = (char) (i+0x30);
					String s = Long.toString(k);
					int len = s.length();
					HashSet<Character> chars = new HashSet<Character>(len);
					for(int l=0; l<len; l++)
					{
						char c = s.charAt(l);
						if( c>=valueLengthChar )
							break;  //then if(chars.size()==length) is false
							
						chars.add(new Character(s.charAt(l)));
					}
					if(chars.size()==len)
						temp.add(new Integer(k));
				}
			}
		}
		
		
		
		
		
		//----------------------------------------------------
		for(int i=0; i<length ; i++)
		{
			int current = primes.get(i).intValue();
			System.out.println(current);
			int len = Integer.toString(current).length();
			for(int j=1; j<=len; j++)
				if(hasFamilyOfSizeEight(current,j))
				{
					smallest = current;
					i =  length;
					break;
				}
		}		
		
		System.out.println(smallestMemberOfFamily);
	}
	
	public static boolean hasFamilyOfSizeEight(long value, int howManyDigits)
	{
		HashSet<Integer> family = new HashSet<Integer>(8);
		
		String valueString = Long.toString(value);
		int valueLength = valueString.length();
		StringBuffer buffer = new StringBuffer(valueString);
		
		//PERMUTATIONS TO CHECK FOR VALUE OF LENGTH x 
		//WHILE CHANGING y DIGITS IN IT, ARE IN: permutations.get(x-1).get(y-1) 
		ArrayList<Integer> temp = permutations.get(valueLength-1).get(howManyDigits-1);
		int size = temp.size();
		for(int j=0; j<size; j++)  //if(howManyDigits-1)==0    pow is 0  (!!!)
		{		
			family.clear();
			String s = Long.toString(temp.get(j));
			int len = s.length();
			for(char digit='0'; digit<='9'; digit++)
			{
				for(int k=0; k<len; k++)
				{
					int whichToReplace = s.charAt(k) - 0x30;
					buffer.setCharAt(whichToReplace, digit);
				}	
				
				int num = Integer.parseInt(buffer.toString());
				if(Integer.toString(num).length()==valueLength && MyMath.isPrime(num))
					family.add(new Integer(num));
				else
					buffer = new StringBuffer(valueString);
			}	
			
			if(family.size()==8)
			{
				Iterator<Integer> iter = family.iterator();
				int minMember = Integer.MAX_VALUE;
				while(iter.hasNext())
				{
					int current = iter.next().intValue();
					if(current<minMember)
						minMember = current;
				}
				smallestMemberOfFamily = minMember;
				return true;
			}
		}			
		return false;
	}
	
	public static boolean isPermutation(long v)
	{
		String s = Long.toString(v);
		int length = s.length();
		HashSet<Character> chars = new HashSet<Character>(length);
		for(int i=0; i<length; i++)
			chars.add(new Character(s.charAt(i)));
		
		return chars.size()==length;
	}
	
	public static int pow(int a, int b)
	{
		if(b==0)
			return 0;   //has to be like that:)
		
		int pow = a;
		for(int i=1; i<b; i++)
			pow *= a;
		
		return pow;
	}
	
	private static ArrayList<ArrayList<ArrayList<Integer>>> permutations = null;
	private static int smallestMemberOfFamily = 0;
}

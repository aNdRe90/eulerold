

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Problem215 {
	/*
	 * Consider the problem of building a wall out of 2x1 and 3x1 bricks 
	 * (horizontal×vertical dimensions) such that, for extra strength, 
	 * the gaps between horizontally-adjacent bricks never line up in consecutive layers, 
	 * i.e. never form a "running crack".
	 * 
	 * For example, the following 9x3 wall is not acceptable due to the running crack shown in red:
	 * PICTURE:)
	 * 
	 * There are eight ways of forming a crack-free 9x3 wall, written W(9,3) = 8.
	 * 
	 * Calculate W(32,10).
	 */

	private static final int rowLength = 32;
	private static final int rows = 10;
	private static final int[] bricks = new int[] { 2, 3 };
	private static final List<Cracks> allCracks = new ArrayList<Cracks>();
	private static final Map<Integer, Map<List<Cracks>, Long>> memory = new HashMap<Integer, Map<List<Cracks>, Long>>();

	public static void main(String[] args) {
		fillMemory();
		fillNeighbours();

		long result = 0L;
		int counter = 0;
		int size = allCracks.size();
		for (Cracks cracks : allCracks) {
			System.out.println((++counter) + "/" + size);
			result += getResult(cracks.neighbours, 1);
		}

		System.out.println("RESULT = " + result);
	}

	private static long getResult(List<Cracks> neighbours, int row) {
		Long resultFromMemory = getFromMemory(neighbours, row);
		if (resultFromMemory != null) {
			return resultFromMemory;
		}

		long result = 0L;
		if (row == (rows - 1)) {
			return neighbours.size();
		}

		for (Cracks cracks : neighbours) {
			result += getResult(cracks.neighbours, row + 1);
		}

		putToMemory(neighbours, row, result);
		return result;
	}

	private static void putToMemory(List<Cracks> neighbours, int row,
			long result) {
		Map<List<Cracks>, Long> map = memory.get(row);
		if (map == null) {
			map = new HashMap<List<Cracks>, Long>();
			memory.put(row, map);
		}

		map.put(neighbours, result);
	}

	private static Long getFromMemory(List<Cracks> neighbours, int row) {
		Map<List<Cracks>, Long> map = memory.get(row);
		if (map == null) {
			return null;
		}

		return map.get(neighbours);
	}

	private static void fillNeighbours() {
		int size = allCracks.size();
		for (int i = 0; i < size; i++) {
			for (int j = i + 1; j < size; j++) {
				Cracks cracks1 = allCracks.get(i);
				Cracks cracks2 = allCracks.get(j);

				if (Collections.disjoint(cracks1.positions, cracks2.positions)) {
					cracks1.addNeighbour(cracks2);
					cracks2.addNeighbour(cracks1);
				}
			}
		}
	}

	private static void fillMemory() {
		for (int brick : bricks) {
			LinkedList<Integer> list = new LinkedList<Integer>();
			list.add(brick);

			computeCracks(list);
		}
	}

	private static void computeCracks(LinkedList<Integer> list) {
		for (int brick : bricks) {
			int newCrack = list.getLast() + brick;

			if (newCrack < rowLength) {
				list.add(newCrack);
				computeCracks(list);
				list.remove(new Integer(newCrack));
			} else if (newCrack == rowLength) {
				allCracks.add(new Cracks(list));
			}
		}
	}

	private static class Cracks {
		private final List<Integer> positions;
		private final List<Cracks> neighbours;

		public Cracks(LinkedList<Integer> list) {
			this.positions = new ArrayList<Integer>(list);
			this.neighbours = new LinkedList<Cracks>();
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = (prime * result)
					+ ((this.positions == null) ? 0 : this.positions.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			Cracks other = (Cracks) obj;
			if (this.positions == null) {
				if (other.positions != null) {
					return false;
				}
			} else if (!this.positions.equals(other.positions)) {
				return false;
			}
			return true;
		}

		public void addNeighbour(Cracks cracks) {
			this.neighbours.add(cracks);
		}

		@Override
		public String toString() {
			return this.positions.toString();
		}
	}
	//	private static class BrickRow {
	//		private final List<Integer> crackPositions;
	//		private final int length;
	//		private boolean full;
	//
	//		public BrickRow(int length) {
	//			this.crackPositions = new ArrayList<Integer>((length / 2) + 1);
	//			this.length = length;
	//			this.full = false;
	//		}
	//
	//		public int addBrick(int brickLength) {
	//			int lastCrackPosition = this.crackPositions.get(this.crackPositions
	//					.size() - 1);
	//			int newLastCrackPosition = lastCrackPosition + brickLength;
	//
	//			if (newLastCrackPosition < this.length) {
	//				this.crackPositions.add(newLastCrackPosition);
	//			}
	//
	//			if (newLastCrackPosition == this.length) {
	//				this.full = true;
	//			}
	//
	//			return newLastCrackPosition;
	//		}
	//
	//		public boolean isFull() {
	//			return this.full;
	//		}
	//	}
}

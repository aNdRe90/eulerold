import algorithm.ExtendedEuclideanAlgorithm;
import generator.EratosthenesSievePrimeGenerator;

/*
 * For a prime p let S(p) = (sum(p-k)!) mod(p) for 1 <= k <= 5.
 * For example, if p=7,
 * (7-1)! + (7-2)! + (7-3)! + (7-4)! + (7-5)! = 6! + 5! + 4! + 3! + 2! = 720+120+24+6+2 = 872.
 * As 872 mod(7) = 4, S(7) = 4.
 * 
 * It can be verified that sum S(p) = 480 for 5 <= p < 100.
 * 
 * Find sum S(p) for 5 <= p < 10^8.
 */

/*
 * SOLUTION:
 * (p-5)! + (p-4)! + (p-3)! + (p-2)! + (p-1)! = (p-5)! * (1 + p-4 + (p-4)(p-3) + (p-4)(p-3)(p-2) + (p-4)(p-3)(p-2)(p-1) )
 * = (p-5)! * (p^4 - 9p^3 + 27p^2 -30p + 9)
 * 
 * S(p) = (p-5)! * (p^4 - 9p^3 + 27p^2 -30p + 9) mod p = (9*(p-5)!) mod p
 * 
 * Wilson's theorem
 * Extended Euclidean algorithm
 */
public class Problem381 {
	private static ExtendedEuclideanAlgorithm algorithm = new ExtendedEuclideanAlgorithm();
	
	public static void main(String[] args) {
		final long N = (long) 1e8;
		final long pMin = 5L;
		long result = 0L;
		
		for(int prime : new EratosthenesSievePrimeGenerator((int)N)) {
			System.out.println(prime);
			if(prime < pMin) {
				continue;
			}
			result += S(prime);
		}
		
		System.out.println("RESULT = " + result);
	}

	private static long S(long p) {
		long a = 24L % p;
		long b = -p;
		
		long[] xy = algorithm.getBezoutsCoefficientsWithGCD(a, b);
		long x = xy[0];
		if(x<0L) {
			x = p + x;
		}
		
		return ( (x % p)*( (p-1L)%p )*9L % p);
	}
}

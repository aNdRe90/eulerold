

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import MyStuff.CombinationGenerator;
import MyStuff.PermutationGenerator;

public class Problem158 {
	/*
	 * Taking three different letters from the 26 letters of the alphabet, 
	 * character strings of length three can be formed.
	 * 
	 * Examples are 'abc', 'hat' and 'zyx'.
	 * When we study these three examples we see that for 'abc' two characters 
	 * come lexicographically after its neighbour to the left.
	 * 
	 * For 'hat' there is exactly one character that comes lexicographically 
	 * after its neighbour to the left. For 'zyx' there are zero characters 
	 * that come lexicographically after its neighbour to the left.
	 * 
	 * In all there are 10400 strings of length 3 for which exactly one 
	 * character comes lexicographically after its neighbour to the left.
	 * 
	 * We now consider strings of n <= 26 different characters from the alphabet.
	 * 
	 * For every n, p(n) is the number of strings of length n for which exactly 
	 * one character comes lexicographically after its neighbour to the left.
	 * 
	 * What is the maximum value of p(n)?
	 */
	private static final List<Character> allCharacters = getCharacters();
	private static char maxChar = 'a' - 1;

	public static void main(String[] args) {
		final int N = 26;
		long maxP = Long.MIN_VALUE;
		int maxN = -1;

		for (int n = 2; n <= N; n++) {
			maxChar = (char) ('a' + (n - 1));
			//memory.clear();
			System.out.println(n);
			//long bruteP = bruteForce(n);
			long currentP = MyStuff.MyMath.binomialCoefficient(26, n)
					.longValue() * p(n);

			if (currentP > maxP) {
				maxP = currentP;
				maxN = n;
			}
		}

		System.out.println("Max is p(" + maxN + ") = " + maxP);
	}

	private static long bruteForce(int n) {
		long result = 0L;

		CombinationGenerator combGen = new CombinationGenerator(26, n);
		while (combGen.hasMore()) {
			int[] comb = combGen.getNext();
			char[] chars = getChars(comb);

			PermutationGenerator permGen = new PermutationGenerator(n, true);
			while (permGen.hasMore()) {
				int[] perm = permGen.getNext();
				String s = getString(chars, perm);

				if (isResult(s)) {
					result++;
				}
			}
		}

		return result;
	}

	private static boolean isResult(String s) {
		int limit = s.length() - 1;

		int lexNexts = 0;
		for (int i = 0; i < limit; i++) {
			if (s.charAt(i) < s.charAt(i + 1)) {
				lexNexts++;
				if (lexNexts > 1) {
					return false;
				}
			}
		}

		return lexNexts == 1;
	}

	private static String getString(char[] chars, int[] perm) {
		StringBuffer buf = new StringBuffer();

		for (int element : perm) {
			buf.append(chars[element]);
		}

		return buf.toString();
	}

	private static char[] getChars(int[] comb) {
		char[] chars = new char[comb.length];

		for (int i = 0; i < comb.length; i++) {
			chars[i] = allCharacters.get(comb[i]);
		}

		return chars;
	}

	private static long p(int n) {
		long p = 0L;

		for (char c = 'a'; c <= maxChar; c++) {
			System.out.println("n=" + n + ", char=" + c);

			List<Character> set = new ArrayList<Character>(26);
			set.add(c);
			p += getResult(set, c, 0, n - 1);
		}

		return p;
	}

	private static long getResult(List<Character> characters, char current,
			int lexNexts, int length) {
		if (length == 0) {
			return lexNexts;
		}

		if (lexNexts == 1) {
			char max = getMaxNotIncludedIn(characters);
			return max < current ? 1L : 0L;
		}

		long result = 0L;
		for (char c = 'a'; c <= maxChar; c++) {
			if (!characters.contains(c)) {
				if (c < current) {
					characters.add(c);
					result += getResult(characters, c, lexNexts, length - 1);
					characters.remove(new Character(c));
				} else if (lexNexts == 0) {
					characters.add(c);
					result += getResult(characters, c, 1, length - 1);
					characters.remove(new Character(c));
				} else {
					break;
				}
			}
		}

		return result;
	}

	private static char getMaxNotIncludedIn(List<Character> characters) {
		for (char c = maxChar; c >= 'a'; c--) {
			if (!characters.contains(c)) {
				return c;
			}
		}

		return 0;
	}

	private static Set<Character> getCharactersWithout(char c) {
		Set<Character> characters = new HashSet<Character>(26);

		for (char v = 'a'; v <= 'z'; v++) {
			if (v != c) {
				characters.add(v);
			}
		}

		return characters;
	}

	private static List<Character> getCharacters() {
		List<Character> characters = new ArrayList<Character>(26);

		for (char c = 'a'; c <= 'z'; c++) {
			characters.add(c);
		}

		return characters;
	}
}

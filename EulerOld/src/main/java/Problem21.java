import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;


public class Problem21
{
	/*
	 * Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
		If d(a) = b and d(b) = a, where a != b, 
		then a and b are an amicable pair and each of a and b are called amicable numbers.

		For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; //220 doesnt count
		therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.

		Evaluate the sum of all the amicable numbers under 10000.	 */
	
	public static void main(String[] args) 
	{		
		long[] sumOfDivisors = new long[10001];
		long sum = 0L;
		
		for(long i=1; i<10000; i++)
		{
			sumOfDivisors[(int)i] = getDivisorsSum(i) - i;
		}
		
		boolean[] bools = new boolean[10001];
		for(long i=1L; i<10000L; i++)
			for(long j=1L; j<10000L; j++)
				if(i!=j && sumOfDivisors[(int)i]==j && sumOfDivisors[(int)j]==i)
				{
					if(!bools[(int)i])
						sum += i;
					if(!bools[(int)j])
						sum += j;
					bools[(int)i] = true;
					bools[(int)j] = true;
				}
		
		System.out.println(sum);
	}
	
	public static long getDivisorsSum(long n)
	{
		ArrayList<Long> primeDivisors = getPrimeDivisors(n);		
		HashSet<Long> divisorsSet = new HashSet<Long>();
		
		long[] divisors = new long[primeDivisors.size()];
		boolean[] bools = new boolean[divisors.length];
		for(int i=0; i<divisors.length; i++)
			divisors[i] = primeDivisors.get(i).longValue();
		
		int a = 1;
		a = a << divisors.length;
		for(int i=0; i<a; i++)
		{
			bools = new boolean[divisors.length];
			int index = 0;
			int val = i;
			while (val != 0) 
			{
			  if (val % 2 != 0)
			  {
			    bools[index]=true;
			  }
			  ++index;
			  val = val >>> 1;
			}
			
			long divisor = 1L;
			for(int j=0; j<divisors.length; j++)
				if(bools[j])
					divisor*=divisors[j];
			divisorsSet.add(divisor);
		}
		
		long sum = 0L;
		Iterator<Long> iter = divisorsSet.iterator();
		while(iter.hasNext())
			sum += iter.next().longValue();
		
		return sum;
	}
	
	private static ArrayList<Long> getPrimeDivisors(long n)
	{
		int[] primes = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 
				71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 
				179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 
				283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 
				419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 
				547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 
				661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 
				811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 
				947, 953, 967, 971, 977, 983, 991, 997 };
		
		ArrayList<Long> primeDivisors = new ArrayList<Long>(10);
		
		for(int i = 0; i<primes.length; i++)
		{
			while(true)
			{
				if(n==1)
				{
					i = primes.length;
					break;
				}
					
				if(n%primes[i]==0)
				{
					n /= primes[i];
					primeDivisors.add(new Long(primes[i]));
				}
				else
					break;
			}
		}	
		
		if(n>1)
		{
			
			for(long j=1001; n!=1; j+=2)
			{
				if(n%j==0 && isPrime(j))
				{
					n /= j;
					primeDivisors.add(new Long(j));
				}
			}
		}
		
		return primeDivisors;
	}
	
	public static boolean isPrime(long v)
	{
		if(v==1)
			return false;
		
		if(v%2==0 || v%3==0 || v%5==0 || v%7==0 || v%11==0 || v%13==0 || v%17==0 || v%19==0)
			return false;
		
		long sqrt = (long) Math.sqrt((double)v) + 1L;
		
		for(long i = 23; i<=sqrt; i+=2)
		{
			if(v%i==0)
				return false;
		}
		
		return true;
	}
}
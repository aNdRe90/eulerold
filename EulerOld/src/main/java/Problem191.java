

public class Problem191 {
	/*
	 * A particular school offers cash rewards to children with 
	 * good attendance and punctuality. 
	 * If they are absent for three consecutive days or late on 
	 * more than one occasion then they forfeit their prize.
	 * 
	 * During an n-day period a trinary string is formed for each 
	 * child consisting of L's (late), O's (on time), and A's (absent).
	 * 
	 * Although there are eighty-one trinary strings for a 4-day period 
	 * that can be formed, exactly forty-three strings would lead to a prize:
	 * 
	 * OOOO OOOA OOOL OOAO OOAA OOAL OOLO OOLA OAOO OAOA
	 * OAOL OAAO OAAL OALO OALA OLOO OLOA OLAO OLAA AOOO
	 * AOOA AOOL AOAO AOAA AOAL AOLO AOLA AAOO AAOA AAOL
	 * AALO AALA ALOO ALOA ALAO ALAA LOOO LOOA LOAO LOAA
	 * LAOO LAOA LAAO
	 * 
	 * How many "prize" strings exist over a 30-day period?
	 */
	private static final int N = 30;
	private static final Long[][][] memory = new Long[N + 1][3][2];

	public static void main(final String[] args) {
		System.out.println(getResultFor(N, 0, 0));
	}

	private static long getResultFor(final int length, final int aConsecutive,
			final int ls) {
		if (memory[length][aConsecutive][ls] != null) {
			return memory[length][aConsecutive][ls];
		}

		boolean canBeAnext = aConsecutive < 2;
		boolean canBeLnext = ls < 1;

		if (length == 1) {
			long possibilities = 1L;
			if (canBeAnext) {
				possibilities++;
			}
			if (canBeLnext) {
				possibilities++;
			}

			return possibilities;
		}

		long result = getResultFor(length - 1, 0, ls); //placed "O"
		if (canBeAnext) {
			result += getResultFor(length - 1, aConsecutive + 1, ls); //placed "A"
		}
		if (canBeLnext) {
			result += getResultFor(length - 1, 0, ls + 1); //placed "L"
		}

		memory[length][aConsecutive][ls] = result;
		return result;
	}
}



import java.awt.Point;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import javax.swing.text.TabableView;

public class Problem102 {
	/*
	 * Three distinct points are plotted at random on a Cartesian plane, 
	 * for which -1000 <= x, y ,<= 1000, such that a triangle is formed.
	 * 
	 * Consider the following two triangles:
	 * 
	 * A(-340,495), B(-153,-910), C(835,-947) 
	 * X(-175,41), Y(-421,-714), Z(574,-645)
	 * 
	 * It can be verified that triangle ABC contains the origin, 
	 * whereas triangle XYZ does not.
	 * 
	 * Using triangles.txt (right click and 'Save Link/Target As...'), 
	 * a 27K text file containing the co-ordinates of one thousand "random" triangles, 
	 * find the number of triangles for which the interior contains the origin.
	 * 
	 * NOTE: The first two examples in the file represent the triangles 
	 * in the example given above.
	 */
	
	/*
	 * SOLUTION:
	 * http://www.blackpawn.com/texts/pointinpoly/default.html
	 */
	
	public static void main(String[] args) {
		List<Triangle2D> triangles = null;
		try {
			triangles = readTrianglesFromFile("triangles.txt");
		} catch (IOException e) {e.printStackTrace();}
		
		int counter = 0;
		for(Triangle2D triangle : triangles) {
			if(triangle.containsOrigin())
				counter++;
		}
		
		System.out.println(counter);
	}

	private static List<Triangle2D> readTrianglesFromFile(String filename) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		List<Triangle2D> triangles = new LinkedList<Triangle2D>();
		String line = null;
		
		while ( (line=reader.readLine() )!=null) {
			triangles.add(getTriangleFromLine(line));
		}
		
		return triangles;
	}

	private static Triangle2D getTriangleFromLine(String line) {
		StringTokenizer tokenizer = new StringTokenizer(line, ",");
		Point a = new Point(Integer.parseInt(tokenizer.nextToken()), 
							Integer.parseInt(tokenizer.nextToken()));
		Point b = new Point(Integer.parseInt(tokenizer.nextToken()), 
							Integer.parseInt(tokenizer.nextToken()));
		Point c = new Point(Integer.parseInt(tokenizer.nextToken()), 
							Integer.parseInt(tokenizer.nextToken()));
		
		return new Triangle2D(a,b,c);
	}
}

class Triangle2D {
	private Point A;
	private Point B;
	private Point C;
	
	private double crossABC;
	private double crossACB;
	private double crossBCA;
	
	public Triangle2D(Point p1, Point p2, Point p3) {
		A = new Point(p1);
		B = new Point(p2);
		C = new Point(p3);
		
		crossABC = getCrossProductValue(A,B,C);
		crossACB = getCrossProductValue(A,C,B);
		crossBCA = getCrossProductValue(B,C,A);
	}
	
	private double getCrossProductValue(Point a, Point b, Point c) {
		double x1 = (double) a.x;
		double x2 = (double) b.x;
		double x3 = (double) c.x;
		double y1 = (double) a.y;
		double y2 = (double) b.y;
		double y3 = (double) c.y;
		
		return (x2-x1)*(y3-y1) - (y2-y1)*(x3-x1);
	}

	public boolean containsOrigin() {
		return containsPoint(new Point(0,0));
	}

	private boolean containsPoint(Point point) {
		double crossABP = getCrossProductValue(A,B,point);
		double crossBCP = getCrossProductValue(B,C,point);
		double crossACP = getCrossProductValue(A,C,point);
		
		return crossABP*crossABC>0.0 && crossBCP*crossBCA>0.0 && crossACP*crossACB>0.0;
	}

	@Override
	public String toString() {
		return "("+A.x+", "+A.y+")"+" "+
				"("+B.x+", "+B.y+")"+" "+
				"("+C.x+", "+C.y+")";
	}
}



import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import MyStuff.Fraction;

public class Problem155 {
	/*
	 * An electric circuit uses exclusively identical capacitors 
	 * of the same value C.
	 * 
	 * The capacitors can be connected in series or in parallel to form sub-units, 
	 * which can then be connected in series or in parallel with other capacitors 
	 * or other sub-units to form larger sub-units, and so on up to a final circuit.
	 * 
	 * Using this simple procedure and up to n identical capacitors, 
	 * we can make circuits having a range of different total capacitances. 
	 * For example, using up to n=3 capacitors of 60 F each, 
	 * we can obtain the following 7 distinct total capacitance values: 
	 * 
	 * [picture] (180, 120, 90, 60, 40, 30, 20)
	 * 
	 * If we denote by D(n) the number of distinct total capacitance values we can obtain 
	 * when using up to n equal-valued capacitors and the simple procedure described above, 
	 * we have: D(1)=1, D(2)=3, D(3)=7 ...
	 * 
	 * Find D(18).
	 * 
	 * Reminder : When connecting capacitors C1, C2 etc in parallel, 
	 * the total capacitance is CT = C1 + C2 +...,
	 * 	whereas when connecting them in series, the overall capacitance is given by: 
	 * 1/CT = 1/C1 + 1/C2 + ...
	 */
	private static final Set<Fraction> resultSet = new HashSet<Fraction>();
	private static final Map<Integer, Set<Fraction>> resultMap = new HashMap<Integer, Set<Fraction>>();
	private static final int N = 18;
	static {
		Fraction f = new Fraction(1L);
		Set<Fraction> list = new HashSet<Fraction>();
		list.add(f);
		resultMap.put(1, list);
		resultSet.add(f);

		for (int i = 2; i <= N; i++) {
			resultMap.put(i, new HashSet<Fraction>());
		}
	}

	public static void main(String[] args) {
		for (int n = 2; n <= N; n++) {
			System.out.println("n = " + n);
			for (int i = n - 1; i >= (n / 2); i--) {
				computeResultsMultiplying(i, n - i);
			}
		}

		System.out.println("RESULT = " + resultSet.size());
	}

	private static void computeResultsMultiplying(int a, int b) {
		Set<Fraction> listA = resultMap.get(a);
		Set<Fraction> listB = resultMap.get(b);

		for (Fraction fractionA : listA) {
			for (Fraction fractionB : listB) {
				Fraction parallel = fractionA.add(fractionB);
				Fraction series = (fractionA.reverse())
						.add(fractionB.reverse()).reverse();

				parallel = parallel.reduce();
				series = series.reduce();

				resultSet.add(parallel);
				resultSet.add(series);
				resultMap.get(a + b).add(parallel);
				resultMap.get(a + b).add(series);
			}
		}
	}
}

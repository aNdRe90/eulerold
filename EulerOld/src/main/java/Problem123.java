

import java.util.ArrayList;
import java.util.List;

public class Problem123 {
	/*
	 * Let pn be the nth prime: 2, 3, 5, 7, 11, ..., and let r be the remainder 
	 * when (pn-1)n + (pn+1)n is divided by pn^2.
	 * 
	 * For example, when n = 3, p3 = 5, and 43 + 63 = 280 = 5 mod 25.
	 * 
	 * The least value of n for which the remainder first exceeds 10^9 is 7037.
	 * 
	 * Find the least value of n for which the remainder first exceeds 10^10.
	 */
	public static void main(String[] args) {
		final int primesToGenerate = 100000;
		List<Long> primes = getFirstNPrimes(primesToGenerate);
		
		final double N = 1e10;
		final long longN = (long) N;
		long sqrt = (long) Math.sqrt(N); 
		int startingPrimeNumber = getIndexOfPrimeGreaterThanN(primes, sqrt);
		
		for(int index=startingPrimeNumber; index<=primesToGenerate; index++) {
			if(index%2==0)
				continue;
			
			long remainder = 2L*primes.get(index)*(long)index;
			if(remainder>longN) {
				System.out.println(index);
				break;
			}
		}
	}
	
	private static int getIndexOfPrimeGreaterThanN(List<Long> primes, long n) {
		int size = primes.size();
		
		for(int i=1; i<size; i++)
			if(primes.get(i)>n)
				return i;
		
		return -1;
	}

	private static List<Long> getFirstNPrimes(int primesToGenerate) {
		List<Long> primes = new ArrayList<Long>(primesToGenerate);
		primes.add(null);
		
		int primesGenerated = 0;
		for(long i=2; primesGenerated<primesToGenerate; i++) {
			if(MyStuff.MyMath.isPrime(i)) {
				primesGenerated++;
				primes.add(i);
			}
		}
		
		return primes;
	}

}

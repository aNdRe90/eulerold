
public class Problem97 
{
	/*
	 * The first known prime found to exceed one million digits was discovered in 1999, 
	 * and is a Mersenne prime of the form 2^6972593-1; it contains exactly 2,098,960 digits. 
	 * Subsequently other Mersenne primes, of the form 2p-1, have been found which contain more digits.
	 * 
	 * However, in 2004 there was found a massive non-Mersenne prime which contains 2,357,207 digits: 
	 * 28433�2^7830457+1.
	 * 
	 * Find the last ten digits of this prime number.
	 */
	public static void main(String[] args) 
	{
		long result = 2L;
		for(long i=1; i<7830457; i++)
		{
			result *= 2L;
			result = truncateToLastTenDigits(result);
		}
		
		result *= 28433L;
		result = truncateToLastTenDigits(result);
		result += 1L;
		result = truncateToLastTenDigits(result);
		
		System.out.println(result);
	}
	
	public static long truncateToLastTenDigits(long v)
	{
		String val = Long.toString(v);
		if(val.length()<11)
			return v;
		else
			return Long.parseLong(val.substring(val.length()-10));
	}
}

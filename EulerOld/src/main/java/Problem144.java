

public class Problem144 {
	/*
	 * In laser physics, a "white cell" is a mirror system that acts 
	 * as a delay line for the laser beam. The beam enters the cell, 
	 * bounces around on the mirrors, and eventually works its way back out.
	 * 
	 * The specific white cell we will be considering 
	 * is an ellipse with the equation 4x^2 + y^2 = 100
	 * 
	 * The section corresponding to -0.01 <= x <= +0.01 at the top is missing,
	 * allowing the light to enter and exit through the hole.
	 * 
	 * The light beam in this problem starts at the point (0.0,10.1) 
	 * just outside the white cell, and the beam first impacts 
	 * the mirror at (1.4,-9.6).
	 * 
	 * Each time the laser beam hits the surface of the ellipse, 
	 * it follows the usual law of reflection "angle of incidence 
	 * equals angle of reflection." That is, both the incident and 
	 * reflected beams make the same angle with the normal line at 
	 * the point of incidence.
	 * 
	 * In the figure on the left, the red line shows the first 
	 * two points of contact between the laser beam and the wall 
	 * of the white cell; the blue line shows the line tangent to 
	 * the ellipse at the point of incidence of the first bounce.
	 * 
	 * The slope m of the tangent line at any point (x,y) 
	 * of the given ellipse is: m = -4x/y
	 * 
	 * The normal line is perpendicular to this tangent line 
	 * at the point of incidence.
	 * 
	 * The animation on the right shows the first 10 reflections of the beam.
	 * 
	 * How many times does the beam hit the internal surface 
	 * of the white cell before exiting?
	 */
	
	/*SOLUTION
	 * Having point (xn, yn) from which laser hits (x0, y0), the normal line for
	 * that hit is given by:  y=(y0/4x0)x + (3x0)/4
	 * 
	 * Then we need an orthogonal line to the normal line
	 * which includes point (xn, yn). This line and a normal line cross in point
	 * (xp, yp) which I called crossingPoint.
	 * 
	 * Having that point I can get a point that is symetrical to (xn, yn)
	 * along the normal line symetry (xs, ys).  (vector operations using crossingPoint)
	 * 
	 * Having (xs, ys) I can easily get the line that is reflected path for laser
	 * by calculating the line going throug (xs, ys) and (x0, y0).
	 * Repeat the process and get the result in a moment!
	 *  
	 * P.S. I used double so had to use epsilon to compare numbers which 
	 * lost their accuracy a bit after all the calculations along the way.
	 */
	private static double epsilon = 0.0000001;
	public static void main(String[] args) {
		Point previousPoint = new Point(0.0, 10.1);
		Point nextPoint = new Point(1.4, -9.6);
		Line normal = null;
		Line reflectedLine = null;
		long reflections = 1L;
		
		while(true) {
			normal = getNormalAtPoint(nextPoint);
			
			if(normal.a==0.0) {
				previousPoint = nextPoint;
				nextPoint = new Point(nextPoint.x, -nextPoint.y);
			}
			else {
				Point symetrical = getSymetricalPointToGivenPointAlongTheLine(
										previousPoint, normal);
				reflectedLine = new Line(symetrical, nextPoint);
				
				previousPoint = nextPoint;
				nextPoint = getPointWhereEllipseCrossLine(reflectedLine, nextPoint);
			}			
			
			if(wentOut(nextPoint)) {
				break;
			}
			else {
				reflections++;
				//if(reflections%10000==0) {
					System.out.println(reflections);
				//}
			}
		}
		
		System.out.println("Result = " + reflections);
	}
	
	private static Point getSymetricalPointToGivenPointAlongTheLine(
			Point point, Line line) {
		Line orthLine = line.getOrthogonalLineThroughPoint(point);
		Point crossingPoint = getCommonPointOfLines(line, orthLine);
		
		double dx = point.x - crossingPoint.x;
		double dy = point.y - crossingPoint.y;

		return new Point(crossingPoint.x - dx, crossingPoint.y - dy);
	}

	private static boolean wentOut(Point p) {
		return p.x>-0.01 && p.x<0.01 && p.y>0.0;
	}

	private static Line getNormalAtPoint(Point p) {
		return new Line(p.y/(4.0*p.x), 3.0*p.y/4.0);
	}
	
	private static Point getPointWhereEllipseCrossLine(Line l, 
							Point previousPoint) {
		double a = l.a;
		double b = l.b;
		double a2 = a*a;		
		double delta = 400.0*a2*a2 + 1600.0*a2 - 16.0*a2*b*b;
		
		double x, y;
		y = (8.0*b + Math.sqrt(delta)) / (2.0*a2 + 8.0);
		if(areSameValue(previousPoint.y,y)) {
			y = (8.0*b - Math.sqrt(delta)) / (2.0*a2 + 8.0);
		}
		
		x = (100.0 - y*y)/4.0;
		x = Math.sqrt(x);
		
		boolean xPositive = ( (y-b)/a )>0.0;
		if(!xPositive) {
			x = -x;
		}
		
		return new Point(x,y);
	}
	
	private static boolean areSameValue(double a, double b) {
		double bigger = a>b ? a : b;
		double smaller = a<b ? a : b;
		
		return bigger - smaller < epsilon;
	}

	private static Point getCommonPointOfLines(Line l1, Line l2) {
		if(l1.a==l2.a) {
			return null;
		}
		
		double x = (l2.b - l1.b) / (l1.a - l2.a);
		double y = l1.a*x + l1.b;
		
		return new Point(x,y);
	}
	
	private static class Line {
		private double a;
		private double b;
		
		private Line(double a, double b) {
			this.a = a;
			this.b = b;
		}
		
		private Line(Point p1, Point p2) {
			if(p1.x==p2.x) {
				throw new IllegalArgumentException(
						"Not y=ax +b  but x=" + p1.x +" line!");
			}
			
			this.a = (p2.y - p1.y)/(p2.x - p1.x);
			this.b = p1.y - this.a*p1.x;
		}
		
		private Line(double a, Point p) {
			this.a = a;
			this.b = p.y - a*p.x;
		}
		
		private Line getOrthogonalLineThroughPoint(Point p) {
			if(a==0.0) {
				throw new IllegalArgumentException(
				"Orthogonal is not a line y=ax+b but x=" + p.x + " line!");
			}
			
			return new Line(-1.0/a, p);
		}
		
		public String toString() {
			return String.format("y = %.2fx + %.2f", a, b);
		}
	}
	
	private static class Point {
		private double x;
		private double y;
		
		private Point(double x, double y) {
			this.x = x;
			this.y = y;
		}
		
		public String toString() {
			return String.format("[%.2f, %.2f]", x, y);
		}
	}
}


import MyStuff.MyMath;

public class Problem85 
{
	/*
	 * By counting carefully it can be seen that a rectangular grid measuring 3 by 2 contains eighteen rectangles:
	 * 
	 *  __ .......
	 * |__|...:..: 
	 * :..:...:..:         etc.
	 * 
	 * six squares 1x1
	 * 
	 * IN GRID 2x3: 
	 * 6 * 1x1
	 * 4 * 2x1
	 * 2 * 3x1
	 * 3 * 1x2
	 * 2 * 2x2
	 * 1 * 3x2
	 * 
	 * 18 total.
	 * 
	 * Although there exists no rectangular grid that contains exactly two million rectangles, 
	 * find the area of the grid with the nearest solution.
	 */
	
	/*
	 * SOLUTION:   (x y) = newton`s symbol
	 * Number of squares in grid m x n is equal to:
	 * 
	 * (n 1)*(m 1) + (n-1 1)*(m 1) + ... + (1 1)*(m 1) + (n 1)*(m-1 1) + (n-1 1)*(m-1 1) + ... + (1 1)*(m-1 1) + ... + (1 1)*(1 1)
	 * 
	 * = nm +(n-1)m + ... + m + n(m-1) + (n-1)(m-1) + ... (m-1) + n + n-1 + ... + 1 =
	 * = m(n+n-1+n-2+...1) + (m-1)(n+n-1+n-2+...+1) + ... (n+n-1+n-2+...+1) = (0.5*(n^2+n))*(m+m-1+m-2+...+1)=
	 * = 0.25(n^2+n)(m^2+m)
	 */
	public static void main(String[] args) 
	{
		final int N = 2000000;
		
		int maxBelow = -1;
		int maxBelowM = -1;
		int maxBelowN = -1;
		int minAbove = -1;
		int minAboveM = -1;
		int minAboveN = -1;
		
		int resultN = 0;
		int resultM = 0;
		int currentMinDifference = N;
		
		for(int n=1; n!=0 ;n++)
		{
			for(int m=n; ;m++)
			{
				int squares = getNumberOfSquaresInGrid(n, m);
				if(m==n && squares>N)
				{
					n = -1;
					break;
				}
				
				System.out.println(squares);
				
				if(maxBelow<squares && squares<N)
				{
					maxBelow = squares;
					maxBelowN = n;
					maxBelowM = m;
				}
				else if(squares>=N)
				{
					minAbove = squares;
					minAboveN = n;
					minAboveM = m;
					//n = -1;
					break;
				}
			}	
			
			if( (minAbove-N) < currentMinDifference )
			{
				resultN = minAboveN;
				resultM = minAboveM;
				currentMinDifference = minAbove-N;
			}
			else if( (N-maxBelow) < currentMinDifference)
			{
				resultN = maxBelowN;
				resultM = maxBelowM;
				currentMinDifference = N-maxBelow;
			}
		}
		
		System.out.println((long)resultN*(long)resultM);
	}
	
	private static int getNumberOfSquaresInGrid(int n, int m)
	{
		return (m*m+m)*(n*n+n)/4;
	}
}

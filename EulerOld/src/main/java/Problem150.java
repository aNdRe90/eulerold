

public class Problem150 {
	/*
	 * In a triangular array of positive and negative integers, 
	 * we wish to find a sub-triangle such that the sum of the numbers 
	 * it contains is the smallest possible.
	 * 
	 * In the example below, it can be easily verified that the marked 
	 * triangle satisfies this condition having a sum of -42.
	 * 
	 * We wish to make such a triangular array with one thousand rows, 
	 * so we generate 500500 pseudo-random numbers sk in the range +-2^19, 
	 * 	 * using a type of random number generator (known as a Linear Congruential Generator) 
	 * as follows:
	 * 
	 * t := 0
	 * for k = 1 up to k = 500500:
	 *     t := (615949*t + 797807) modulo 2^20
	 *     sk := t-2^19
	 *     
	 * Thus: s1 = 273519, s2 = -153582, s3 = 450905 etc
	 * 
	 * Our triangular array is then formed using the pseudo-random numbers thus:
	 * s1
	 * s2  s3
	 * s4  s5  s6 
	 * s7  s8  s9  s10
	 * ...
	 * 
	 * Sub-triangles can start at any element of the array and extend down as far as we like 
	 * (taking-in the two elements directly below it from the next row, 
	 * the three elements directly below from the row after that, and so on).
	 * 
	 * The "sum of a sub-triangle" is defined as the sum of all the elements it contains.
	 * Find the smallest possible sub-triangle sum.
	 */
	private static final int levels = 1000;
	private static final int elements = (int) (((1.0 + levels) * levels) / 2.0);
	private static long[][] triangle = getTriangle();

	public static void main(String[] args) {
		long minSubTriangleSum = Long.MAX_VALUE;

		for (int level = 0; level < levels; level++) {
			for (int topIndex = 0; topIndex <= level; topIndex++) {
				System.out.println("Level = " + level + ", element = "
						+ topIndex);

				long subTriangleSum = Long.MAX_VALUE;
				for (int levelsInSum = 0;; levelsInSum++) {
					if ((level + levelsInSum) == levels) {
						break;
					}

					long levelSum = getSum(level + levelsInSum, topIndex,
							levelsInSum + 1);
					if (subTriangleSum == Long.MAX_VALUE) {
						subTriangleSum = levelSum;
					} else {
						subTriangleSum += levelSum;
					}

					if (subTriangleSum < minSubTriangleSum) {
						minSubTriangleSum = subTriangleSum;
					}
				}
			}
		}

		System.out.println("RESULT = " + minSubTriangleSum);
	}

	private static long getSum(int level, int topIndex, int n) {
		long sum = 0L;

		for (int i = 0; i < n; i++) {
			sum += triangle[level][topIndex + i];
		}

		return sum;
	}

	private static long[][] getTestTriangle() {
		long[][] triangle = new long[6][];

		triangle[0] = new long[1];
		triangle[0][0] = 15;

		triangle[1] = new long[2];
		triangle[1][0] = -14;
		triangle[1][1] = -7;

		triangle[2] = new long[3];
		triangle[2][0] = 20;
		triangle[2][1] = -13;
		triangle[2][2] = -5;

		triangle[3] = new long[4];
		triangle[3][0] = -3;
		triangle[3][1] = 8;
		triangle[3][2] = 23;
		triangle[3][3] = -26;

		triangle[4] = new long[5];
		triangle[4][0] = 1;
		triangle[4][1] = -4;
		triangle[4][2] = -5;
		triangle[4][3] = -18;
		triangle[4][4] = 5;

		triangle[5] = new long[6];
		triangle[5][0] = -16;
		triangle[5][1] = 31;
		triangle[5][2] = 2;
		triangle[5][3] = 9;
		triangle[5][4] = 28;
		triangle[5][5] = 3;

		return triangle;
	}

	private static long[][] getTriangle() {
		long[][] triangle = new long[levels][];
		long modulo = (long) Math.pow(2.0, 20.0);
		long difference = (long) Math.pow(2.0, 19.0);

		int level = 1;
		int putInLevel = 0;
		triangle[level - 1] = new long[level];

		long t = 0L;
		for (int k = 1; k <= elements; k++) {
			if (putInLevel == level) {
				putInLevel = 0;
				level++;
				triangle[level - 1] = new long[level];
			}

			t = ((615949L * t) + 797807L) % modulo;
			triangle[level - 1][putInLevel] = t - difference;
			putInLevel++;
		}

		return triangle;
	}
}

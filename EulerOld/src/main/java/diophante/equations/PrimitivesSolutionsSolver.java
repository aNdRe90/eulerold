package diophante.equations;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class PrimitivesSolutionsSolver {
	private static BigSquareRoot bigSquareRoot = new BigSquareRoot();
	static {
		bigSquareRoot.setScale(200);
	}
	
	public static List<SolutionPair> 
				getPrimitiveSolutions(long D, long k) {
		if(k*k < D) {
			return getPrimitiveSolutionsK2LessD(D,k);
		}
		else if(k*k > D) {
			return getPrimitiveSolutionsK2MoreThanD(D,k);
		}
		else {
			return null;
		}
	}
	
	private static List<SolutionPair> 
				getPrimitiveSolutionsK2MoreThanD(long D, long k) {
		//The Lagrange-Matthews-Mollin algorithm
		List<SolutionPair> primitives = new LinkedList<SolutionPair>();
		List<Long> zs = new ArrayList<Long>();
		List<Long> ms = new ArrayList<Long>();
		
		long tempK = k>=0 ? k : -k;
		long[] kDivisors = MyStuff.MyMath.getDivisors(tempK);
		for(long divisor : kDivisors) {
			if(MyStuff.MyMath.isSquareNumber(divisor)) {
				long f = (long) Math.sqrt(divisor);
				BigInteger bigF = BigInteger.valueOf(f);
				long m = k/divisor;
				long tempM = m>=0 ? m : -m;
				
				for(long z=(-tempM/2L), max=(tempM/2L); z<=max; z++) {
					if( D%m == (z*z)%m) {
						BigInteger currentU = BigInteger.valueOf(z);
						BigInteger currentV = BigInteger.valueOf(tempM);
						BigInteger firstA = BigInteger.valueOf(
													(long)Math.floor(Math.sqrt(D)));
						BigDecimal tempStartA = new BigDecimal(firstA.add(currentU)).
								divide(new BigDecimal(currentV), 0, BigDecimal.ROUND_FLOOR);
						//temp = temp.setScale(0, RoundingMode.FLOOR);				
						BigInteger currentA = tempStartA.toBigInteger();
						BigInteger previousP = currentU.negate();
						BigInteger previousQ = BigInteger.ONE;
						BigInteger currentP = new BigInteger(currentV.toString());
						BigInteger currentQ = BigInteger.ZERO;
						BigInteger bigK = BigInteger.valueOf(k);
						
						int index=0;
						BigInteger bigD = BigInteger.valueOf(D);
						BigInteger nextU;
						BigInteger nextV;
						BigInteger nextA;
						BigInteger nextP;
						BigInteger nextQ;
						
						while(true) {
							nextU = currentA.multiply(currentV).subtract(currentU);
							nextV = (bigD.subtract(
											nextU.multiply(nextU) )).divide(currentV);
							BigDecimal tempA = new BigDecimal(firstA.add(nextU)).
											divide(new BigDecimal(nextV), 0, BigDecimal.ROUND_FLOOR);
							//temp = temp.setScale(0, RoundingMode.FLOOR);
							nextA = tempA.toBigInteger();
							nextP = currentA.multiply(currentP).add(previousP);
							nextQ = currentA.multiply(currentQ).add(previousQ);
							
							if(nextU.compareTo(currentU)==0 && nextV.compareTo(currentV)==0) {
								break;
							}
							
							currentU = nextU;
							currentV = nextV;
							currentA = nextA;
							previousP = currentP;
							previousQ = currentQ;
							currentP = nextP;
							currentQ = nextQ;
							
							index++;
							if(currentV.compareTo(BigInteger.ONE)==0 || 
									currentV.compareTo(BigInteger.ONE.negate())==0) {
								if(currentP.multiply(currentP).subtract( 
										currentQ.multiply(currentQ).multiply(bigD)  )
										.compareTo(BigInteger.valueOf(m))==0) {
									primitives.add(new SolutionPair(
											currentP.multiply(bigF), currentQ.multiply(bigF)));
									break;
								}		
								
								if(currentP.multiply(currentP).subtract( 
										currentQ.multiply(currentQ).multiply(bigD)  )
										.compareTo(BigInteger.valueOf(-m))==0) {
									ProperPellsEquationSolver solver = new 
											ProperPellsEquationSolver(D, -1L);
									
									if(solver.hasSolution()) {
										BigInteger[] rs = solver.getNextSolution();
										
										BigInteger solutionX = (currentP.multiply(rs[0]).add(
												currentQ.multiply(rs[1]).multiply(bigD)))
														    	.multiply(bigF);
										BigInteger solutionY = (currentP.multiply(rs[1]).add(
												currentQ.multiply(rs[0])))
																.multiply(bigF);
										
										primitives.add(new SolutionPair(solutionX, solutionY));
									}
									
									break;
								}
							}
						}
					}
				}
			}
		}
		
		return trimEquivalentPrimitiveSolutions(primitives, D, k);
	}

	private static List<SolutionPair> 
					getPrimitiveSolutionsK2LessD(long D, long k) {
		List<SolutionPair> primitives = new LinkedList<SolutionPair>();
		
		BigInteger currentU = BigInteger.ZERO;
		BigInteger currentV = BigInteger.ONE;
		BigInteger currentA = BigInteger.valueOf(
									(long)Math.floor(Math.sqrt(D)));
		BigInteger firstA = new BigInteger(currentA.toString());
		BigInteger previousP = BigInteger.ONE;
		BigInteger previousQ = BigInteger.ZERO;
		BigInteger currentP = new BigInteger(currentA.toString());
		BigInteger currentQ = BigInteger.ONE;
		BigInteger bigK = BigInteger.valueOf(k);
		
		int index=0;
		BigInteger bigD = BigInteger.valueOf(D);
		BigInteger nextU;
		BigInteger nextV;
		BigInteger nextA;
		BigInteger nextP;
		BigInteger nextQ;
		
		while(true) {
			nextU = currentA.multiply(currentV).subtract(currentU);
			nextV = (bigD.subtract(
							nextU.multiply(nextU) )).divide(currentV);
			BigDecimal temp = new BigDecimal(firstA.add(nextU)).
							divide(new BigDecimal(nextV), 10, BigDecimal.ROUND_HALF_UP);
			nextA = temp.toBigInteger();
			nextP = nextA.multiply(currentP).add(previousP);
			nextQ = nextA.multiply(currentQ).add(previousQ);
			
			currentU = nextU;
			currentV = nextV;
			currentA = nextA;
			previousP = currentP;
			previousQ = currentQ;
			currentP = nextP;
			currentQ = nextQ;
			
			index++;
			if(currentV.compareTo(BigInteger.ONE)==0 || 
			   currentA.compareTo(firstA)>1) {
				break;
			}
			
			BigInteger tempV = new BigInteger(currentV.toString());
			if(index%2==1) {
				tempV = tempV.negate();
			}
			
			if((long)tempV.signum()*k>0L) {
				BigInteger[] m2AndRemainder = bigK.divideAndRemainder(tempV);
				if(m2AndRemainder[1].compareTo(BigInteger.ZERO)==0) {
					BigInteger m2 = m2AndRemainder[0];
					BigDecimal m = bigSquareRoot.get(new BigDecimal(m2));
					m.setScale(200, BigDecimal.ROUND_HALF_UP);
					
					BigInteger mm = m.toBigInteger();
					if(new BigDecimal(mm).compareTo(m)==0) {
						BigInteger x = mm.multiply(previousP);
						BigInteger y = mm.multiply(previousQ);
						primitives.add(new SolutionPair(x, y));
					}
				}
			}
		}
		
		return trimEquivalentPrimitiveSolutions(primitives, D, k);
	}

	private static List<SolutionPair> trimEquivalentPrimitiveSolutions(
			List<SolutionPair> primitives, long D, long k) {
		for(int i=0; i<primitives.size(); i++) {
			for(int j=i+1; j<primitives.size(); j++) {
				if(areEquivalent(primitives.get(i), primitives.get(j), D, k)) {
					primitives.remove(j);
					j--;
				}
			}
		}
		
		return primitives;
	}

	private static boolean areEquivalent(SolutionPair pair1, SolutionPair pair2, 
																	long D, long k) {
		BigInteger bigK = k>=0L ? BigInteger.valueOf(k) : BigInteger.valueOf(-k);		
		
		BigInteger a = pair1.getX().multiply(pair2.getX()).subtract(   
				BigInteger.valueOf(D).multiply(pair1.getY().multiply(pair2.getY()))
				);
		
		if(a.signum()<0) {
			a = a.negate();
		}
		
		if(a.mod(bigK).compareTo(BigInteger.ZERO)==0) {
			return true;
		}
		
		BigInteger b = pair1.getX().multiply(pair2.getY()).subtract(   
							pair1.getY().multiply(pair2.getX())
																   );		

		if(b.signum()<0) {
			b = b.negate();
		}
		
		return b.mod(bigK).compareTo(BigInteger.ZERO)==0;
	}
}

package diophante.equations;

import java.math.BigInteger;

//Look Problem66.pdf in ROZNE
public class StandardPellsEquationSolver extends PellsEquationSolver {
	private long[] continuedFraction;
	private int continuedFractionPeriodLength;
	
	public static void main(String[] args) {
		StandardPellsEquationSolver stdSolver = 
				new StandardPellsEquationSolver(61L, 1L);
		for(int i=0; i<50; i++) {
			BigInteger[] xy = stdSolver.getNextSolution();
			System.out.println("[x = " + xy[0] + ", y = " + xy[1] + "]");
		}
	}

	public StandardPellsEquationSolver(long D, long k) {
		super(D, k);
		
		if(k!=1L && k!=-1L) {
			throw new IllegalArgumentException(
					"Value of k must be either 1 or -1.");
		}
		
		this.D = D;
		this.k = k;
		continuedFraction = getContinuedFraction(D);
		continuedFractionPeriodLength 
				= Cycle.getCycle(continuedFraction).length;
		
		isAnySolution = !(continuedFractionPeriodLength%2==0 && k==-1L);
	}
	
	public BigInteger[] getSolution(int n) {
		if(!isAnySolution) {
			System.out.println("NO SOLUTION TO EQUATION: " + toString());
			return null;
		}
		
		BigInteger x = null;
		BigInteger y = null;
		
		int index = getIndex(n);
		valuesA = new BigInteger[index+1];
		valuesB = new BigInteger[index+1];
		x = A(index, continuedFraction);
		y = B(index, continuedFraction);
		
		return new BigInteger[] {x,y};
	}

	@Override
	public BigInteger[] getNextSolution() {
		return getSolution(++solutionsMade);
	}
	
	private int getIndex(int n) {
		int index = -1;
		
		if(k==1L && continuedFractionPeriodLength%2==0) {
			index = n*continuedFractionPeriodLength - 1;
		}		
		else if(k==1L && continuedFractionPeriodLength%2==1) {
			index = 2*n*continuedFractionPeriodLength - 1;
		}
		else if(k==-1L && continuedFractionPeriodLength%2==1) {
			index = (2*n-1)*continuedFractionPeriodLength - 1;
		}
		
		return index;
	}
	
	private BigInteger A(int k, long[] q) {		
		if(k>q.length)
			return null;
		
		if(k==-2)
			return BigInteger.ZERO;
		if(k==-1)
			return BigInteger.ONE;
		
		BigInteger qk = BigInteger.valueOf(q[k]);
		BigInteger a1 = null;
		BigInteger a2 = null;		
		
		if((k-1)>=0 && valuesA[k-1]==null)
			valuesA[k-1] = A(k-1, q);
		if((k-2)>=0 && valuesA[k-2]==null)
			valuesA[k-2] = A(k-2, q);
		
		if(k==0) {
			a1 = BigInteger.ONE;
			a2 = BigInteger.ZERO;
			if(valuesA[k]==null)
				valuesA[k] = a1.multiply(qk).add(a2);
			return valuesA[k];
		}		
		
		if(k==1) {
			a1 = valuesA[0];
			a2 = BigInteger.ONE;
			if(valuesA[k]==null)
				valuesA[k] = a1.multiply(qk).add(a2);
			return valuesA[k];
		}

		
		a1 = valuesA[k-1];
		a2 = valuesA[k-2];

		BigInteger result = qk.multiply(a1).add(a2);		
		return result;
	}
	
	private BigInteger B(int k, long[] q) {		
		if(k>q.length)
			return null;
		
		if(k==-2)
			return BigInteger.ONE;
		if(k==-1)
			return BigInteger.ZERO;
		
		BigInteger qk = BigInteger.valueOf(q[k]);
		BigInteger b1 = null;
		BigInteger b2 = null;		
		
		if((k-1)>=0 && valuesB[k-1]==null)
			valuesB[k-1] = B(k-1, q);
		if((k-2)>=0 && valuesB[k-2]==null)
			valuesB[k-2] = B(k-2, q);
		
		if(k==0) {
			b1 = BigInteger.ZERO;
			b2 = BigInteger.ONE;
			if(valuesB[k]==null)
				valuesB[k] = b1.multiply(qk).add(b2);
			return valuesB[k];
		}		
		
		if(k==1) {
			b1 = valuesB[0];
			b2 = BigInteger.ZERO;
			if(valuesB[k]==null)
				valuesB[k] = b1.multiply(qk).add(b2);
			return valuesB[k];
		}
		
		b1 = valuesB[k-1];
		b2 = valuesB[k-2];

		BigInteger result = qk.multiply(b1).add(b2);		
		return result;
	}
	
	//VEEERY TRICKY BUT WORKS:)   //from problem 64
	//gets the of period of continued fraction of sqrt(n)
	private long[] getContinuedFraction(long n) {
		double sqrtN = Math.sqrt((double)n);
		long a0 = (long) Math.sqrt((double)n);
		long inNumerator = 1L;		//in the numerator
		long inDenominator = -a0;  //in the denominator
		
		long[] buf = new long[continuedFractionPeriodGeneratedLength+1];
		buf[0] = a0;
		for(int i=0; i<continuedFractionPeriodGeneratedLength; i++) {
			long tempNum = inNumerator;
			long tempDen = inDenominator;
			
			inDenominator = n - inDenominator*inDenominator;
			if(inDenominator%tempNum==0) {
				inDenominator /= tempNum;
				tempNum = 1;
			}	
			inNumerator = tempNum*tempDen*(-1);
						
			double allValue = (sqrtN+(double) inNumerator) / 
								(double)inDenominator;  
			buf[i+1] = (int) allValue;
			
			inNumerator -= buf[i+1]*inDenominator;
			
			long temp = inDenominator;
			inDenominator = inNumerator;
			inNumerator = temp;
		}
		
		return buf;
	}		
		
	private int continuedFractionPeriodGeneratedLength = 1001;
	private BigInteger[] valuesA = null;
	private BigInteger[] valuesB = null;
}

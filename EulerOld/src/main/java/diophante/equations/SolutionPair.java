package diophante.equations;

import java.math.BigInteger;

class SolutionPair implements Comparable<SolutionPair> {
	private BigInteger x;
	private BigInteger y;


	public SolutionPair(BigInteger x, BigInteger y) {
		this.x = x;
		this.y = y;
	}
	
	public SolutionPair(SolutionPair other) {
		this.x = new BigInteger(other.x.toString());
		this.y = new BigInteger(other.y.toString());
	}

	@Override
	public int compareTo(SolutionPair other) {
		return x.compareTo(other.x);
	}

	public SolutionPair getNextSolutionPair(BigInteger a, BigInteger b,
											BigInteger c, BigInteger d) {		
		return new SolutionPair(a.multiply(x).add(b.multiply(y)),
				c.multiply(x).add(d.multiply(y)));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((x == null) ? 0 : x.hashCode());
		result = prime * result + ((y == null) ? 0 : y.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SolutionPair other = (SolutionPair) obj;

		return x.compareTo(other.x) == 0;
	}

	@Override
	public String toString() {
		return "[x=" + x + ", y=" + y + "]";
	}

	public BigInteger getX() {
		return new BigInteger(x.toString());
	}
	
	public BigInteger getY() {
		return new BigInteger(y.toString());
	}
}

package diophante.equations;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;

public class ProperPellsEquationSolver extends PellsEquationSolver {
	public static void main(String[] agrs) {
		ProperPellsEquationSolver pell 
					= new ProperPellsEquationSolver(2L, -1L);
		
		for(int i=0; i<50; i++) {
			BigInteger[] xy = pell.getNextSolution();
			System.out.println("[x = " + xy[0] + ", y = " + xy[1] + "]");
		}
	}
	
	public ProperPellsEquationSolver(long D, long k) {
		super(D, k);
		
		if(k!=1L && k!=-1L) {
			throw new IllegalArgumentException(
					"Value of k must be either 1 or -1.");
		}
		
		this.D = D;
		this.k = k;
		
		this.fundamentalSolution = getFundamentalSolution();
		this.currentSolution = fundamentalSolution==null ? null 
								: new SolutionPair(fundamentalSolution);
		if(isAnySolution) {
			setABCD();
		}
	}

	private SolutionPair getFundamentalSolution() {
		//PQa algorithm
		BigInteger currentU = BigInteger.ZERO;
		BigInteger currentV = BigInteger.ONE;
		BigInteger currentA = BigInteger.valueOf(
									(long)Math.floor(Math.sqrt(D)));
		BigInteger firstA = new BigInteger(currentA.toString());
		BigInteger previousP = BigInteger.ONE;
		BigInteger previousQ = BigInteger.ZERO;
		BigInteger currentP = new BigInteger(currentA.toString());
		BigInteger currentQ = BigInteger.ONE;
		
		int index=0;
		BigInteger bigD = BigInteger.valueOf(D);
		BigInteger nextU;
		BigInteger nextV;
		BigInteger nextA;
		BigInteger nextP;
		BigInteger nextQ;
		
		while(true) {
			nextU = currentA.multiply(currentV).subtract(currentU);
			nextV = (bigD.subtract(
							nextU.multiply(nextU) )).divide(currentV);
			BigDecimal temp = new BigDecimal(firstA.add(nextU)).
							divide(new BigDecimal(nextV), 10, BigDecimal.ROUND_HALF_UP);
			nextA = temp.toBigInteger();
			nextP = nextA.multiply(currentP).add(previousP);
			nextQ = nextA.multiply(currentQ).add(previousQ);
			
			currentU = nextU;
			currentV = nextV;
			currentA = nextA;
			previousP = currentP;
			previousQ = currentQ;
			currentP = nextP;
			currentQ = nextQ;
			
			index++;
			if(currentV.compareTo(BigInteger.ONE)==0 || 
			   currentA.compareTo(firstA)>1) {
				break;
			}
		}
		
		if(index%2==0) {
			if(k==1L) {
				return new SolutionPair(previousP, previousQ);
			}
			else {// if(k==-1L) {
				isAnySolution = false;
				return null;
			}
		}
		else {
			if(k==1L) {
				BigInteger solutionX = previousP.multiply(previousP).add(
								bigD.multiply(previousQ).multiply(previousQ));
				BigInteger solutionY = BigInteger.valueOf(2L).
										multiply(previousP).multiply(previousQ);
				return new SolutionPair(solutionX, solutionY);
			}
			else {// if(k==-1L) {
				return new SolutionPair(previousP, previousQ);
			}
		}
	}

	@Override
	public BigInteger[] getNextSolution() {
		if(!isAnySolution) {
			System.out.println("NO SOLUTION TO EQUATION: " + toString());
			return null;
		}
		
		BigInteger x = currentSolution.getX();
		BigInteger y = currentSolution.getY();		
		currentSolution = currentSolution.getNextSolutionPair(a, b, c, d);
		
		return new BigInteger[] {x,y};
	}
}

package diophante.equations;

import java.math.BigInteger;

public abstract class PellsEquationSolver {
	//x(n+1) = x0*x(n) + y0*D*y(n)
	//y(n+1) = y0*x(n) + x0*y(n)

	//x(n+1) = a*x(n) + b*y(n)
	//y(n+1) = c*x(n) + d*y(n)	
	protected BigInteger a;
	protected BigInteger b;
	protected BigInteger c;
	protected BigInteger d;

	protected boolean isAnySolution = true;
	protected long D;
	protected long k;
	protected int solutionsMade = 0;
	protected SolutionPair fundamentalSolution;
	protected SolutionPair currentSolution;

	public PellsEquationSolver(long D, long k) {
		if (isSquareNumber(D)) {
			throw new IllegalArgumentException("D cannot be a square number.");
		}

		if (D <= 0L) {
			throw new IllegalArgumentException("D must be positive.");
		}

		if (k == 0L) {
			throw new IllegalArgumentException("Value of k must not be 0.");
		}
	}

	@Override
	public String toString() {
		return "x^2 - " + this.D + "y^2 = " + this.k;
	}

	public boolean hasSolution() {
		return this.isAnySolution;
	}

	abstract public BigInteger[] getNextSolution();

	protected boolean isSquareNumber(long v) {
		if (v < 1L) {
			return false;
		}

		double sqrtDouble = Math.sqrt(v);
		return Math.floor(sqrtDouble) == sqrtDouble;
	}

	protected void setABCD() {
		if (this.k == -1L) {
			BigInteger x0 = this.fundamentalSolution.getX();
			BigInteger y0 = this.fundamentalSolution.getY();
			BigInteger bigD = BigInteger.valueOf(this.D);
			this.a = x0.multiply(x0).add(bigD.multiply(y0).multiply(y0));
			this.c = BigInteger.valueOf(2L).multiply(x0).multiply(y0);
			this.b = this.c.multiply(bigD);
			this.d = new BigInteger(this.a.toString());
		} else {
			this.a = this.fundamentalSolution.getX();
			this.c = this.fundamentalSolution.getY();
			this.d = new BigInteger(this.a.toString());
			this.b = this.c.multiply(BigInteger.valueOf(this.D));
		}
	}
}

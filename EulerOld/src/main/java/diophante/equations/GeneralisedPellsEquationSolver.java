package diophante.equations;

import java.math.BigInteger;
import java.util.Collections;
import java.util.List;

/**
 * Class solves equation x^2 - Dy^2 = k where D is not square number.
 * 
 * @author Andrzej
 */
public class GeneralisedPellsEquationSolver extends PellsEquationSolver {
	private final int maxInDepthIndependancyPrimitiveSolutionsSearch = 3;
	private ProperPellsEquationSolver stdSolver;
	private List<SolutionPair> primitiveSolutions;

	public static void main(String[] args) {
		//		BigInteger m2 = new BigInteger("15246956988844808483287109004266125");
		//		BigSquareRoot b = new BigSquareRoot();
		//		b.setScale(200);
		//		BigDecimal m = b.get(new BigDecimal(m2));
		//		m.setScale(200, BigDecimal.ROUND_HALF_UP);
		//		BigInteger mm = m.toBigInteger();
		//		System.out.println(new BigDecimal(mm).compareTo(m)==0);

		GeneralisedPellsEquationSolver gpeSolver = new GeneralisedPellsEquationSolver(
				1L, 400L);
		for (int i = 0; i < 40; i++) {
			BigInteger[] xy = gpeSolver.getNextSolution();
			System.out.println("[x = " + xy[0] + ", y = " + xy[1] + "]");
		}
	}

	public GeneralisedPellsEquationSolver(long D, long k) {
		super(D, k);

		this.D = D;
		this.k = k;

		if (!isProperPellsEquation()) {
			if (isSolveable()) {
				this.fundamentalSolution = getFundamentalSolution();
				if (this.isAnySolution) {
					setABCD();
					this.primitiveSolutions = PrimitivesSolutionsSolver
							.getPrimitiveSolutions(D, k);
					Collections.sort(this.primitiveSolutions);
				}
			} else {
				this.isAnySolution = false;
			}

		} else {
			this.stdSolver = new ProperPellsEquationSolver(D, k);
		}
	}

	private boolean isSolveable() {
		//		We would like to find a simple criterion for x^2 - Dy^2 = K 
		//		to have solutions by writing x^2 == K mod (D), that is K 
		//		must be a quadratic residue of D.
		//		The only thing we can say then is that when K is not 
		//		a quadratic residue of D, then the Pell equation has no solutions. 
		//		For instance equation x^2 - 97y^2 = 51 has no solutions 
		//		Legendre symbol (51/97) = -1 hence 51 is not 
		//		a quadratic residue of 97.
		//		But this necessary condition is not sufficient... 
		//TODO for future maybe
		return true;
	}

	@Override
	public BigInteger[] getNextSolution() {
		if (!this.isAnySolution) {
			System.out.println("NO SOLUTION TO EQUATION: " + toString());
			return null;
		}
		if (isProperPellsEquation()) {
			return this.stdSolver.getNextSolution();
		}

		SolutionPair solution;

		BigInteger xSolution = null;
		BigInteger ySolution = null;
		if (this.solutionsMade < this.primitiveSolutions.size()) {
			do {
				if (this.solutionsMade == this.primitiveSolutions.size()) {
					break;
				}

				solution = this.primitiveSolutions.get(this.solutionsMade++);
				xSolution = solution.getX();
				ySolution = solution.getY();

			} while ((xSolution.signum() < 0) || (ySolution.signum() < 0));

			//solutionsMade--;
		}

		else { //if(solutionsMade>=primitiveSolutions.size()) {
			int whichPrimitive = this.solutionsMade
					% this.primitiveSolutions.size();
			solution = this.primitiveSolutions.get(whichPrimitive)
					.getNextSolutionPair(this.a, this.b, this.c, this.d);
			xSolution = solution.getX();
			ySolution = solution.getY();

			if (xSolution.signum() < 0) {
				xSolution = xSolution.negate();
			}

			if (ySolution.signum() < 0) {
				ySolution = ySolution.negate();
			}

			this.primitiveSolutions.set(whichPrimitive, solution);
			this.solutionsMade++;
		}

		return new BigInteger[] { xSolution, ySolution };
	}

	private List<SolutionPair> getIndependantPrimitiveSolutions() {
		for (int i = 0; i < this.primitiveSolutions.size(); i++) {
			SolutionPair primitive = this.primitiveSolutions.get(i);
			for (int j = 0; j < this.maxInDepthIndependancyPrimitiveSolutionsSearch; j++) {
				SolutionPair equivalent = primitive.getNextSolutionPair(this.a,
						this.b, this.c, this.d);

				int index = this.primitiveSolutions.indexOf(equivalent);

				if (index >= 0) {
					this.primitiveSolutions.remove(index);
				}
			}
		}

		return this.primitiveSolutions;
	}

	//	private List<SolutionPair> getPrimitiveSolutions() {
	//		if(k*k < D) {
	//			return getPrimitiveSolutionsK2LessD();
	//		}
	//		
	//		
	//		
	//		
	//		long yBound;
	//		long x0 = fundamentalSolution.getX().longValue();
	//		long y0 = fundamentalSolution.getY().longValue();
	//		
	//		if(k>0L) {
	//			yBound = y0*(long)Math.floor(Math.sqrt( ((double)k/2.0)*
	//												((double)x0 + 1.0)));
	//		}
	//		else {
	//			yBound = y0*(long)Math.floor(Math.sqrt( ((double)(-k)/2.0)*
	//					((double)x0 - 1.0)));
	//		}
	//		
	//		List<SolutionPair> primitiveSolutions 
	//					= new LinkedList<SolutionPair>();
	//
	//		for(long y=1; y<=yBound; y++) {
	//			long x2 = D*y*y + k;
	//			if(isSquareNumber(x2)) {
	//				long x = (long) Math.sqrt(x2);
	//				primitiveSolutions.add(new SolutionPair(
	//										BigInteger.valueOf(x),
	//										BigInteger.valueOf(y)));
	//			}
	//		}
	//		
	//		isAnySolution = primitiveSolutions.size()>0;
	//		
	//		return primitiveSolutions;
	//	}

	private SolutionPair getFundamentalSolution() {
		ProperPellsEquationSolver stdSolver = new ProperPellsEquationSolver(
				this.D, 1L);

		if (stdSolver.hasSolution()) {
			BigInteger[] xy = stdSolver.getNextSolution();
			return new SolutionPair(xy[0], xy[1]);
		} else {
			this.isAnySolution = false;
			return null;
		}
	}

	private boolean isProperPellsEquation() {
		return (this.k == 1L) || (this.k == -1L);
	}
}

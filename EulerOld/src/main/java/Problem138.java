

import java.math.BigInteger;

import diophante.equations.ProperPellsEquationSolver;
import diophante.equations.StandardPellsEquationSolver;

public class Problem138 {
	/*
	 * Consider the isosceles triangle with base length, 
	 * b = 16, and legs, L = 17.
	 * 
	 * By using the Pythagorean theorem it can be seen 
	 * that the height of the triangle, h = sqrt(172 - 82) = 15, 
	 * which is one less than the base length.
	 *
	 * With b = 272 and L = 305, we get h = 273, 
	 * which is one more than the base length, 
	 * and this is the second smallest isosceles triangle 
	 * with the property that h = b � 1.
	 * 
	 * Find sum of L for the twelve smallest isosceles triangles 
	 * for which h = b � 1 and b, L are positive integers.
	 */
	
	/*
	 * SOLUTION:
	 * (b/2)^2 + (b+-1)^2 = L^2
	 * 5/4b^2 + 1 +-2b = L^2
	 * 5b^2 +-8b + (4-4L^2) = 0
	 * 
	 * delta = 64 - 20(4 - 4L^2) = 80L^2 - 16 = 16(5L^2 - 1)
	 * 
	 * b = 1/5 * (+-4 +-2sqrt(delta) )
	 * 
	 * 5L^2 - 1 must be a square number.
	 * 
	 * Therefore we get a Pell`s equation:
	 * x^2 - 5L^2 = -1			(x^2 - Dy^2 = k)
	 *  
	 */
	public static void main(String[] args) {
		ProperPellsEquationSolver stdSolver = 
				new ProperPellsEquationSolver(5L, -1L);
		
		long sum = 0L;
		final int N = 12;
		int trianglesFound = 0;
		
		while(true) {
			BigInteger[] xy = stdSolver.getNextSolution();
			long L = xy[1].longValue();
			long underSqrt = xy[0].longValue();
			
			if(canBbeIntegerForUnderSqrt(underSqrt)) {
				sum += L;
				trianglesFound++;
			}
			
			if(trianglesFound==N) {
				break;
			}
		}
		
		System.out.println(sum);
	}
	
	private static boolean canBbeIntegerForUnderSqrt(long underSqrt) {
		long numerator;
		
		numerator = 4L + 2*underSqrt;
		if(numerator>0L && numerator%5L==0L) {
			return true;
		}
		
		numerator = -4L + 2*underSqrt;
		if(numerator>0L && numerator%5L==0L) {
			return true;
		}
		
		numerator = 4L - 2*underSqrt;
		if(numerator>0L && numerator%5L==0L) {
			return true;
		}
		
		numerator = -4L - 2*underSqrt;
		if(numerator>0L && numerator%5L==0L) {
			return true;
		}
		
		return false;
	}
}



public class Problem130 {
	/*
	 * A number consisting entirely of ones is called a repunit.
	 * We shall define R(k) to be a repunit of length k; for example, 
	 * R(6) = 111111.
	 * 
	 * Given that n is a positive integer and GCD(n, 10) = 1, 
	 * it can be shown that there always exists a value, k, 
	 * for which R(k) is divisible by n, and let A(n) be 
	 * the least such value of k; for example, A(7) = 6 and A(41) = 5.
	 * 
	 * You are given that for all primes, p > 5, that p - 1 
	 * is divisible by A(p). For example, when p = 41, A(41) = 5, 
	 * and 40 is divisible by 5.
	 * 
	 * However, there are rare composite values 
	 * for which this is also true; the first five examples being 
	 * 91, 259, 451, 481, and 703.
	 * 
	 * Find the sum of the first twenty-five composite values of n for which
	 * GCD(n, 10) = 1 and n - 1 is divisible by A(n).
	 */
	public static void main(String[] args) {
		final int N = 25;
		int compositesFound = 0;
		long[] composites = new long[N];
		
		for(long n=3; ;n+=2L) {
			if(n%5L!=0 && !MyStuff.MyMath.isPrime(n)) {
				long an = A(n);
				if( (n-1L)%an == 0L ) {
					composites[compositesFound++] = n;
				}
				System.out.println(n + ",\t" + compositesFound);
				
				if(compositesFound==N) {
					break;
				}
			}
		}
		
		System.out.println(getSumOfElements(composites));
	}

	private static long getSumOfElements(long[] composites) {
		long sum = 0L;
		
		for(long c : composites) {
			sum += c;
		}
		
		return sum;
	}

	private static int A(long n) {
		//I`m using the fact that when A mod B = p
		//then (x*A+Y) mod B = (x*p+Y) mod B
		
		//Also if i have repunit R(3) = 111 next will be 10*111 + 1
		//and for the rest of repunits R(k) as well.
		
		//so for example if I want to find such repunit that 
		//it would be divisible by n = 41 i would go:
		//111 mod 41 = 29		//k=3
		//10*29+1 mod 41 = 4	//k=4
		//10*4+1 mod 41 = 0		//k=5

		int nSize = Long.toString(n).length();
		int k = nSize;
		long repunit = (long) (Math.pow(10.0, (double)nSize)/9.0);
		
		if(repunit<n) {
			repunit = 10L*repunit + 1L;
			k++;
		}
		
		long modulo = repunit % n;
		
		while(true) {
			if(modulo==0L) {
				break;
			}
			
			k++;
			modulo = (10L*modulo + 1L) % n;
		}
		
		return k;
	}
}


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import MyStuff.Fraction;

public class Problem141 {
	/*
	 * A positive integer, n, is divided by d and the quotient and remainder 
	 * are q and r respectively. 
	 * In addition d, q, and r are consecutive positive integer terms 
	 * in a geometric sequence, but not necessarily in that order.
	 * 
	 * For example, 58 divided by 6 has quotient 9 and remainder 4. 
	 * It can also be seen that 4, 6, 9 are consecutive terms in a geometric sequence 
	 * (common ratio 3/2).
	 * 
	 * We will call such numbers, n, progressive.
	 * 
	 * Some progressive numbers, such as 9 and 10404 = 102^2, 
	 * happen to also be perfect squares.
	 * The sum of all progressive perfect squares below one hundred thousand is 124657.
	 * 
	 * Find the sum of all progressive perfect squares below one trillion (10^12).
	 */
	
	/*
	 * SOLUTION:
	 * It was easy to deduce that when n^2 = q*d + r  then r is the smallest.
	 * So our order is (r, d, q).
	 * 
	 * d<n  && q>n     - obvious:]
	 * 
	 * d = (s/t * r) = (s*r/t)  q = (s/t * d) = (s*d/t) = (r*s^2/t^2)
	 * 
	 * n^2 = (s^3 * r^2)/t^3 + r
	 * 
	 * gcd(s,t)==1  so r^2 = x*t^3   where x is integer.
	 * Furthermore r must be also integer so if r is integer and r^2 is divisible by t^3
	 * then r =  k*t^2  where k is integer   (r^2 = k^2 * t^4   is divisible by t^3)
	 * 
	 * n^2 = (s^3 * r^2)/t^3 + r    and r=k*t^2   then:
	 * n^2 = k*t^2 + k^2*t*s^3    (d=k*t*s,  q = k*s^3,  r = k*t^2)
	 * 
	 * 
	 * 
	 * min t = 1  so min s = 2
	 * 
	 * then 8t + t^2 = n^2 <10e12   (bound for t)
	 * solving for t  we get maxT = 0.5*(-8.0 + Math.sqrt(64.0 + 4.0*maxN2))
	 * if(d<n)  then k*t*s < n    if min k = 1  then (s*t)<10e6   (bound for s)
	 * 
	 * then we increase k until n^2 = k*t^2 + k^2*t*s^3 > 10e12
	 * 
	 * for each set (s,t,k) we check if k*t^2 + k^2*t*s^3  is square searching
	 * for the value in generated squares of value < 10e12
	 */
	private static Set<Long> squares;
	
	public static void main(String[] agrs) {		
		final long maxN2 = (long)1e12;
		final long maxN = (long) Math.sqrt(maxN2);
		
		squares = getSqauresBelow(maxN2);
		Set<Solution> solutions = new HashSet<Solution>();
		long tMax = (long) (  0.5*(-8.0 + Math.sqrt(64.0 + 4.0*maxN2))  );
		
		for(long t=1L; t<=tMax; t++) {			
			System.out.println(t + "/" + tMax);
			
			for(long s=t+1L; (s*t)<maxN; s++) {
				if(MyStuff.MyMath.GCD(s, t)==1L) {
					long s3 = s*s*s;
					for(long k=1; ; k++) {
						long squareToBe = k*t*(k*s3 + t);
						
						if(squareToBe>=maxN2) {
							break;
						}
						
						if(isSquare(squareToBe)) {
							solutions.add(new Solution(k*t*t, k*t*s, k*s*s, s, t));
						}
					}
				}					
			}
		}
		
		List<Solution> solutionsList = getListFromSet(solutions);
		BigInteger sum = BigInteger.ZERO;
		Collections.sort(solutionsList);
		
		System.out.println("SOLUTIONS:");
		for(Solution s : solutionsList) {
			System.out.println(s);
			sum = sum.add(BigInteger.valueOf(s.value));
		}
		
		System.out.println("\nSum of n2 = " + sum.toString());
	}
	
	private static List<Solution> getListFromSet(Set<Solution> set) {
		List<Solution> list = new ArrayList<Solution>(set.size());
		
		for(Solution s : set) {
			list.add(s);
		}
		
		return list;
	}

	private static boolean isSquare(long v) {
		return squares.contains(Long.valueOf(v));
	}

	private static Set<Long> getSqauresBelow(long n) {		
		long bound = (long)Math.sqrt(n);
		Set<Long> squares = new HashSet<Long>();
		
		for(long i=1L; i<bound; i++) {
			squares.add(i*i);
		}
		
		
		if(bound*bound < n) {
			squares.add(bound*bound);
		}
		return squares;
	}
	
	private static class Solution implements Comparable<Solution> {
		private long d;
		private long q;
		private long r;
		private long s;
		private long t;
		private long value;
		
		public Solution(long r, long d, long q, long s, long t) {
			super();
			this.d = d;
			this.q = q;
			this.r = r;
			this.s = s;
			this.t = t;
			this.value = r+d*q;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (int) (d ^ (d >>> 32));
			result = prime * result + (int) (q ^ (q >>> 32));
			result = prime * result + (int) (r ^ (r >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Solution other = (Solution) obj;
			if (d != other.d)
				return false;
			if (q != other.q)
				return false;
			if (r != other.r)
				return false;
			return true;
		}

		@Override
		public String toString() {
			return value + " = " + r + " + " + d + "*" + q + " (ratio = " + s + "/" + t + ")";
		}

		@Override
		public int compareTo(Solution o) {
			long difference = this.value - o.value;
			
			if(difference==0L) {
				return 0;
			}
			
			return difference>0L ? 1 : -1;
		}
	}
}

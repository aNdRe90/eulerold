//package MyStuff;

import java.math.BigDecimal;
import java.math.BigInteger;



/**
 * Represents a big fraction with numerator and denominator of BigInteger type.
 * Implements adding, subtracting, dividing, multiplying, raising to a given power,
 * reducing (NOT IMPLEMENTED YET - TO DO!) and reversing.
 * @author Andrzej *
 */
public class BigFraction implements Comparable<BigFraction>
{
	/**
	 * Creates a big fraction with given numerator and denominator.
	 * If given denominator==0, ArithmeticException is thrown.
	 * *  
	 * IT DOES NOT REDUCE IT.
	 * @param num numerator
	 * @param den denominator
	 */
	public BigFraction(BigInteger num, BigInteger den)
	{
		if(den.compareTo(BigInteger.ZERO)==0)
			throw new ArithmeticException("Zero in denominator!");
		
		numerator = new BigInteger(num.toString());
		denominator = new BigInteger(den.toString());
		
		if(numerator.compareTo(BigInteger.ZERO)<0 && denominator.compareTo(BigInteger.ZERO)<0)
		{
			numerator = numerator.multiply(minusOne);
			denominator = denominator.multiply(minusOne);
		}
	}
	
	/**
	 * Creates a big fraction with given numerator (fraction from BigInteger value).
	 * @param val numerator (denominator==1)
	 */
	public BigFraction(BigInteger val)
	{
		numerator = new BigInteger(val.toString());
		denominator = BigInteger.ONE;
	}
	
	/**
	 * Creates a big fraction identical to the one given as parameter.
	 * *  
	 * IT DOES NOT REDUCE IT.
	 * @param f big fraction to copy.
	 */
	public BigFraction(BigFraction f)
	{
		numerator = new BigInteger(f.numerator.toString());
		denominator = new BigInteger(f.denominator.toString());
	}
	
	/**
	 * Reverses the big fraction exchanging the values of numerator and denominator.
	 * It does not change this object - only returns reversed big fraction.
	 * *  
	 * IT DOES NOT REDUCE IT.
	 * @return big fraction created by reversing this big fraction.
	 */
	public BigFraction reverse()
	{
		return new BigFraction(new BigInteger(denominator.toString()), new BigInteger(numerator.toString()));
	}
	
	/**
	 * Adds a given big fraction to this big fraction.
	 * *  
	 * IT DOES NOT REDUCE IT.
	 * @param f big fraction to add
	 * @return Big fraction being a sum of this and given big fraction.
	 */
	public BigFraction add(BigFraction f)
	{
		return new BigFraction(numerator.multiply(f.denominator).add(f.numerator.multiply(denominator)), 
							   denominator.multiply(f.denominator));
	}
	
	/**
	 * Subtracts a given big fraction from this big fraction.
	 * *  
	 * IT DOES NOT REDUCE IT.
	 * @param f big fraction to subtract
	 * @return Big fraction being a difference between this and given big fraction.
	 */
	public BigFraction subtract(BigFraction f)
	{
		return new BigFraction(numerator.multiply(f.denominator).subtract(f.numerator.multiply(denominator)), 
				   denominator.multiply(f.denominator));
	}
	
	/**
	 * Multiplies a given big fraction by this big fraction.
	 * *  
	 * IT DOES NOT REDUCE IT.
	 * @param f big fraction to multiply
	 * @return Big fraction being a product of this and given big fraction.
	 */
	public BigFraction multiply(BigFraction f)
	{	
		return new BigFraction(numerator.multiply(f.numerator), denominator.multiply(f.denominator));
	}
	
	/**
	 * Divides this big fraction by a given big fraction.
	 * If given big fraction has numerator==0, ArithmeticException is thrown.
	 * *  
	 * IT DOES NOT REDUCE IT.
	 * @param f big fraction to divide by
	 * @return Big fraction being a quotient of this and given fbig raction.
	 */
	public BigFraction divide(BigFraction f)
	{		
		if(f.numerator.compareTo(BigInteger.ZERO)==0)
			throw new ArithmeticException("Dividing by zero!");
		
		return new BigFraction(numerator.multiply(f.denominator), denominator.multiply(f.numerator));
	}
	
	/**
	 * Raises the big fraction to the power of given value.
	 * *  
	 * IT DOES NOT REDUCE IT.
	 * @param n
	 * @return Big fraction being a nth power of this big fraction.
	 */
	public BigFraction pow(int n)
	{	
		if(n==0)
		{
			numerator = BigInteger.ONE;
			denominator = BigInteger.ONE;
		}
		else if(n<0)
		{
			reverse();
			n *= -1;
		}		
		
		BigInteger tempN = new BigInteger(numerator.toString());
		BigInteger tempD = new BigInteger(denominator.toString());
		for(int i=2; i<n; i++)
		{
			tempN = tempN.multiply(numerator);
			tempD = tempD.multiply(denominator);
		}
		
		return new BigFraction(tempN, tempD);
	}
	
	/*
	/**
	 * Reduces the big fraction. Reducing makes sure that numerator and denominator
	 * don`t have any common divisor.
	 * @return this after operation
	 /
	public BigFraction reduce()
	{
		long[] numFactors = MyMath.getPrimeFactorization(numerator);
		long[] denFactors = MyMath.getPrimeFactorization(denominator);
		
		for(int i=0; i<numFactors.length; i++)
			for(int j=0; j<denFactors.length; j++)
			{
				if(numFactors[i]==denFactors[j])
				{
					numerator /= numFactors[i];
					denominator /= numFactors[i];
					
					numFactors[i] = denFactors[i] = 1L;
				}
			}
		
		if(numerator<0 && denominator<0)
		{
			numerator *=-1;
			denominator *=-1;
		}
		return this;
	} */
	
	/**
	 * Converts a fraction to String of form "numerator/denominator" e.g. 1/3.
	 */
	public String toString()
	{
		return numerator.toString()+"/"+denominator.toString();
	}
	
	/**
	 * Compares this fraction with a given object and returns true if they are equal.
	 */
	public boolean equals(Object other)
	{
		BigFraction otherFraction = (BigFraction) other;
		return numerator.compareTo(otherFraction.numerator)==0 
				&& denominator.compareTo(otherFraction.denominator)==0 ;
	}
	
	/**
	 * Compares this big fraction with another big fraction.
	 * Returns:
	 * 1 if this big fraction is greater than given big fraction.
	 * -1 if this big fraction is lower than given big fraction.
	 * 0 if both big fraction represents the same value.
	 */
	public int compareTo(BigFraction other)
	{
		if(denominator.compareTo(other.denominator)==0)
			return numerator.compareTo(other.numerator);
		
		//BigInteger commonDenominator = denominator.multiply(other.denominator);
		BigInteger newNumeratorThis = numerator.multiply(other.denominator);
		BigInteger newNumeratorOther = other.numerator.multiply(denominator);
		
		return newNumeratorThis.subtract(newNumeratorOther).compareTo(BigInteger.ZERO);
	}
	
	/**
	 * Converts a big fraction to BigDecimal value;
	 * @return BigDecimal of the same value as the big fraction.
	 */
	public BigDecimal toBigDecimal()
	{
		BigDecimal quotient = new BigDecimal(numerator);
		quotient = quotient.divide(new BigDecimal(denominator));
		return quotient; 
	}
	public BigInteger getNumerator()
	{
		return new BigInteger(numerator.toString());
	}
	
	public BigInteger getDenominator()
	{
		return new BigInteger(denominator.toString());
	}
	
	private BigInteger numerator;
	private BigInteger denominator;
	private BigInteger minusOne = new BigInteger("-1");
}


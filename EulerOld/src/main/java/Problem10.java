
public class Problem10 
{
	/*
	 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
	 * 
	 * Find the sum of all the primes below two million.
	 */
	public static void main(String[] args) 
	{
		long sum = 77L;
		for(long i = 23; i<2000000; i+=2)
		{
			if(isPrime(i))
			{
				sum += i;
			}
		}	
		
		System.out.println(sum);
	}
	
	public static boolean isPrime(long v)
	{
		if(v%2==0 || v%3==0 || v%5==0 || v%7==0 || v%11==0 || v%13==0 || v%17==0 || v%19==0)
			return false;
		
		long sqrt = (long) Math.sqrt((double)v) + 1L;
		
		for(long i = 23; i<=sqrt; i+=2)
		{
			if(v%i==0)
				return false;
		}
		
		return true;
	}
}

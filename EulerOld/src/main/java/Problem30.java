
public class Problem30 
{
	/*
	 * Surprisingly there are only three numbers that can be written as the sum of fourth powers of their digits:

    	1634 = 1^4 + 6^4 + 3^4 + 4^4
    	8208 = 8^4 + 2^4 + 0^4 + 8^4
    	9474 = 9^4 + 4^4 + 7^4 + 4^4

		As 1 = 1^4 is not a sum it is not included.

		The sum of these numbers is 1634 + 8208 + 9474 = 19316.

		Find the sum of all the numbers that can be written as the sum of fifth powers of their digits.	 */
	
	
	/*SOLUTION:
	 * n-digit number can have maximum sum (di = i-th digit) d1^5 + d2^5 + ... dn^5 equal to n*9^5 = n*59049.
	 * n-digit number X also satisfies the condition:  10^(n-1) < X < 10^n
	 * 
	 * Then maximum sum of digits (maxSum) and maximum value (max) for n-digit numbers are:
	 * 
	 * n=1:		maxSum = 59049		max = 10
	 * n=2:		maxSum = 118098		max = 100	
	 * n=3:		maxSum = 177147		max = 1000	
	 * n=4:		maxSum = 236196		max = 10 000	
	 * n=5:		maxSum = 295245		max = 100 000	
	 * n=6:		maxSum = 354294		max = 1000 000	
	 * n=7:		maxSum = 413343		max = 10 000 000
	 * 
	 * THUS:
	 * All the numbers that are "able" to be equal 
	 * to their sum of fifth powers of their digits	are from range [10; 354294]
	 */
	public static void main(String[] args) 
	{
		long sum = 0L;
		for(long i=10; i<354295L; i++)
		{
			if(sumOfFifthPowersOfDigits(i)==i)
				sum += i;
		}
		
		System.out.println(sum);
	}
	
	public static long sumOfFifthPowersOfDigits(long n)
	{
		long sum = 0L;
		String s = Long.toString(n);
		int length = s.length();
		
		for(int i=0; i<length; i++)
		{
			long temp = s.charAt(i) - 0x30L;
			sum += (temp*temp*temp*temp*temp);
		}
		return sum;
	}
}

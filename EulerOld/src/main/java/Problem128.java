

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Problem128 {
	/*
	 * A hexagonal tile with number 1 is surrounded 
	 * by a ring of six hexagonal tiles, starting at "12 o'clock" 
	 * and numbering the tiles 2 to 7 in an anti-clockwise direction.
	 * 
	 * New rings are added in the same fashion, with the next rings 
	 * being numbered 8 to 19, 20 to 37, 38 to 61, and so on. 
	 * The diagram below shows the first three rings.
	 * 
	 * By finding the difference between tile n and each its 
	 * six neighbours we shall define PD(n) to be the number of those 
	 * differences which are prime.
	 * 
	 * For example, working clockwise around tile 8 the differences 
	 * are 12, 29, 11, 6, 1, and 13. So PD(8) = 3.
	 * 
	 * In the same way, the differences around tile 17 are 
	 * 1, 17, 16, 1, 11, and 10, hence PD(17) = 2.
	 * 
	 * It can be shown that the maximum value of PD(n) is 3.
	 * 
	 * If all of the tiles for which PD(n) = 3 are listed in 
	 * ascending order to form a sequence, the 10th tile would be 271.
	 * 
	 * Find the 2000th tile in this sequence.
	 */		
	
	/*
	 * SOLUTION:
	 * I divided the whole space into 6 parts + "star".
	 * "Star" are the 6 lines spreading with values:
	 * 1,2,8,20,38,...   (TOP LINE)
	 * 1,3,10,23,42,...
	 * 1,4,12,26,46,...
	 * 1,5,14,29,50,...
	 * 1,6,16,32,54,...
	 * 1,7,18,35,58,...
	 * 
	 * I also assumed "layers" which are circles with values:
	 * 0th layer = 1
	 * 1st layer = 2,3,4,5,6,7
	 * 2nd layer = 8,9,10,11,12,13,14,15,16,17,18,19,
	 * etc.
	 * 
	 * Nth layer where N>0 contains 6N values.
	 * 
	 * Inside layers there are "positions" starting with 0 on top on going
	 * anti-clockwise. E.g. in 1st layer  2 has position 0 
	 * 5 has position 3, and 7 has position 5.
	 * 
	 * Following TOP LINE values:
	 * 2,8,20,38,62,...
	 * I created the equation representing them: a(n) = 2 + 3n(n-1)
	 * a(1) = 2,  a(2) = 8,  a(3) = 20,  a(4) = 38,  a(5) = 62,  ...
	 * 
	 * So given value X we would have them o such n-th layer that:
	 * 3n^2 - 3n + 2 = X
	 * 
	 * Solving it we get:
	 * n = (3 + sqrt(12x-15))/6
	 * 
	 * So given value X we can determine its layer and position by:
	 * layer = floor( (3 + sqrt(12X-15))/6 )
	 * position = X - a(layer)
	 * 
	 * 
	 * AFTER LONG ATTEMPTS I FINALLY MANAGE TO CONCLUDE THAT:
	 * "Value v can have 3 neighbours with prime difference to them 
	 *  (PD(v)=3) if and only if it is on the "star" or it is the last
	 *  element in the layer (max position number in layer)."
	 *  
	 *  So I only LOOP through: TOP LINE, other star lines, 
	 *  						last position in layer.
	 *  
	 *  Values on TOP LINE can have desired neighbours (difference prime)
	 *  only on places right-top, right-bottom, left-bottom such as
	 *  considering 8 its places are values 37,19,21.
	 *  
	 *  Values on star lines can have desired neighbours
	 *  only on places top, left-top, left-bottom, right-bottom such as
	 *  considering 23 its places are values 41,42,43,10.
	 *  
	 *  Values in last position of layer have desired neighbours
	 *  only on places top, right-top, left-top, left-bottom, bottom 
	 *  such as considering 19 its places are values 37,36,8,2,7.
	 *  
	 *  Using the fact that Nth layer where N>0 contains 6N values
	 *  I compute mentioned values in each of the loop.
	 */
	public static void main(String[] args) {
		final int N = 2000;
		int pd3Found = 2;  //as "1" has pd=3, and "2" too
		List<Long> sequence = new ArrayList<Long>(N);
		sequence.add(1L);
		sequence.add(2L);
		
		for(long n=8L, step=6L; ; step+=6L, n+=step) {
			long layer = (long) ((3.0 + Math.sqrt(12.0*n - 15.0))/6.0);
			long position = n - 3L*layer*(layer-1L) - 2L;
			//--------------------------------------------------
			int pd = 0;
			
			long neighbour = 7L + 3L*layer*(layer+3L);
			if(isDifferencePrime(neighbour, n)) {
				pd++;
			}
			
			neighbour = n + 1L + layer*6L;
			if(isDifferencePrime(neighbour, n)) {
				pd++;
			}
			
			neighbour = n - 1L + layer*6L;
			if(isDifferencePrime(neighbour, n)) {
				pd++;
			}
			
			if(pd==3) {
				pd3Found++;
				sequence.add(Long.valueOf(n));
			}			
			if(pd3Found==N) {
				break;
			}
			//--------------------------------------------------
			
			long m = 0;
			for(long i=1; i<6L; i++) {
				m = n+i*layer;
				
				long innerLayer = (long) ((3.0 + Math.sqrt(12.0*m - 15.0))/6.0);
				long innerPosition = m - 3L*innerLayer*(innerLayer-1L) - 2L;
				pd = 0;
				
				long multiplier = innerPosition/innerLayer;
				
				if(innerLayer>1) {
					neighbour = m - multiplier - (innerLayer-1L)*6L;
					if(isDifferencePrime(neighbour, m)) {
						pd++;
					}
				}				
				
				neighbour = m + multiplier + innerLayer*6L;
				if(isDifferencePrime(neighbour, m)) {
					pd++;
				}
				
				neighbour = neighbour + 1L;
				if(isDifferencePrime(neighbour, m)) {
					pd++;
				}
				
				neighbour = neighbour -2L;
				if(isDifferencePrime(neighbour, m)) {
					pd++;
				}
				
				if(pd==3) {
					pd3Found++;
					sequence.add(Long.valueOf(m));
				}			
				if(pd3Found==N) {
					break;
				}
			}			
			
			
			//--------------------------------------------------
			m = m + layer - 1L;			
			pd = 0;
			layer = (long) ((3.0 + Math.sqrt(12.0*m - 15.0))/6.0);
			position = m - 3L*layer*(layer-1L) - 2L;
			
			neighbour = m + 1L - layer*6L;
			if(isDifferencePrime(neighbour, m)) {
				pd++;
			}
			
			neighbour = neighbour - (layer-1L)*6L;
			if(isDifferencePrime(neighbour, m)) {
				pd++;
			}
			
			int area = (int) (position/layer);
			
			neighbour = m - layer*6L;
			if(isDifferencePrime(neighbour, m)) {
				pd++;
			}
			
			neighbour = 3L*(layer+1L)*layer + 2L + position + area;
			if(isDifferencePrime(neighbour, m)) {
				pd++;
			}
			
			neighbour = neighbour + 1L;
			if(isDifferencePrime(neighbour, m)) {
				pd++;
			}
			
			if(pd==3) {
				pd3Found++;
				sequence.add(Long.valueOf(m));
			}
			
			if(pd3Found==N) {
				break;
			}	
			//--------------------------------------------------
		}
		
		Collections.sort(sequence);
		System.out.println("RESULT = " + sequence.get(N-1));
	}

	private static boolean isDifferencePrime(long a, long b) {
		long difference = a - b;
		if(difference<0L) {
			difference = -difference;
		}
		
		return MyStuff.MyMath.isPrime(difference);
	}	
}

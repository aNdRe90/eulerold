import static MyStuff.MyMath.isSquareNumber;
/*
 * Let ABC be a triangle with all interior angles being less than 120 degrees. 
 * Let X be any point inside the triangle and let XA = p, XC = q, and XB = r.
 * 
 * Fermat challenged Torricelli to find the position of X such that p + q + r was minimised.
 * 
 * Torricelli was able to prove that if equilateral triangles AOB, BNC and AMC are constructed on each side of triangle ABC, 
 * the circumscribed circles of AOB, BNC, and AMC will intersect at a single point, T, inside the triangle. 
 * Moreover he proved that T, called the Torricelli/Fermat point, minimises p + q + r. 
 * 
 * Even more remarkable, it can be shown that when the sum is minimised, AN = BM = CO = p + q + r 
 * and that AN, BM and CO also intersect at T.
 * 
 * If the sum is minimised and a, b, c, p, q and r are all positive integers we shall call triangle ABC a Torricelli triangle. 
 * For example, a = 399, b = 455, c = 511 is an example of a Torricelli triangle, with p + q + r = 784.
 * 
 * Find the sum of all distinct values of p + q + r <= 120000 for Torricelli triangles.
 */

public class Problem143 {
	public static void main(String[] args) {
		final long maxSum = 785L;
		long result = 0L;
		
		long maxA = 5000; //(long) ( maxSum / Math.sqrt(3.0) );
		long maxB = 5000;
		
		for(long a=1; a<=maxA; a++) {
			System.out.println("a = " + a);
			for(long b=a; b<=maxB; b++) {				
				for(long c=b; c<=(long)Math.sqrt(a*a + a*b + b*b); c++) {
					//1)-----------------------------------------------
					long temp = (a+b+c)*(a+b-c)*(a-b+c)*(-a+b+c);
					if(temp%3 != 0) {
						continue;
					}
					//1)-----------------------------------------------
					
					//2)-----------------------------------------------
					long X2 = temp / 3;
					double sqrt = Math.sqrt(X2);		
					if(Math.floor(sqrt) != sqrt) {
						continue;
					}
					long X = (long) sqrt;
					//2)-----------------------------------------------
					
					//3)-----------------------------------------------
					temp = 4*b*b*c*c - temp;
					sqrt = Math.sqrt(temp);
					if(Math.floor(sqrt) != sqrt) {
						continue;
					}
					//3)-----------------------------------------------
					
					//4)-----------------------------------------------
					temp = 3*X - (long)sqrt;
					if(temp%2 != 0) {
						continue;
					}			
					long A = temp / 2;
					//4)-----------------------------------------------
					
					//5)-----------------------------------------------
					temp = c*c + b*b + A;
					sqrt = Math.sqrt(temp);
					if(Math.floor(sqrt) != sqrt) {
						continue;
					}					
					long pqrSum = (long) sqrt;
					//5)-----------------------------------------------
					
					//System.out.println("Torricelli triangle: (" + a + ", " + b + ", " + c +") -> p+q+r = " + pqrSum);
					//6)-----------------------------------------------
					if( pqrSum <= maxSum ) {
						result += pqrSum;
					}
					//6)-----------------------------------------------
				}
			}
		}
		//Torricelli triangle: (936, 945, 1629)
		System.out.println("RESULT = " + result);
		//TODO distinct + 120deg max
	}
}

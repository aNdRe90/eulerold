

import java.util.LinkedList;
import java.util.List;

import MyStuff.Factorizator;
import MyStuff.Prime;

public class Problem243 {
	/*
	 * A positive fraction whose numerator is less than its denominator 
	 * is called a proper fraction.
	 * 
	 * For any denominator, d, there will be d-1 proper fractions; 
	 * for example, with d = 12:
	 * 1/12 , 2/12 , 3/12 , 4/12 , 5/12 , 6/12 , 7/12 , 8/12 , 9/12 , 10/12 , 11/12 .
	 * 
	 * We shall call a fraction that cannot be cancelled down a resilient fraction.
	 * Furthermore we shall define the resilience of a denominator, R(d), 
	 * to be the ratio of its proper fractions that are resilient;
	 * for example, R(12) = 4/11 .
	 * 
	 * In fact, d = 12 is the smallest denominator having a resilience R(d) < 4/10 .
	 * 
	 * Find the smallest denominator d, having a resilience R(d) < 15499/94744 .
	 */

	/*SOLUTION:
	 * phi(n) = n*PRODUCT[ (p(i) - 1)/p(i)) ]   where p(i) is the prime dividing n
	 * So phi(n)/n = PRODUCT[ (p(i) - 1)/p(i)) ]
	 * 
	 * We know that  phi(n)/n < phi(n)/(n-1)
	 * So finding minimal primes to satisfy:
	 * PRODUCT[ (p(i) - 1)/p(i)) ] < 15499/94744
	 * 
	 * We can define the result as minimal x*PRODUCT[p(i)]  where x is 1,2,3...
	 * satisfying the phi(n)/(n-1) < 15499/94744 
	 */
	public static void main(final String[] args) {
		double limit = 15499.0 / 94744.0;
		List<Long> primesUsed = getPrimesUsed(limit);
		long max = getProduct(primesUsed);

		for (long d = max;; d += max) {
			double totient = Factorizator.getEulersTotient(d);

			if ((totient / (d - 1.0)) < limit) {
				System.out.println("RESULT = " + d);
				break;
			}
		}
	}

	private static List<Long> getPrimesUsed(final double limit) {
		List<Long> primesUsed = new LinkedList<Long>();
		long currentPrime = 2L;

		double currentTotientDOverD = 1L;
		while (true) {
			if (currentTotientDOverD < limit) {
				System.out.println(primesUsed);
				break;
			}
			primesUsed.add(currentPrime);
			currentTotientDOverD *= ((currentPrime - 1.0) / currentPrime);
			currentPrime = Prime.getNextPrime(currentPrime);
		}

		return primesUsed;
	}

	private static long getProduct(final List<Long> primesUsed) {
		long product = 1L;

		for (long prime : primesUsed) {
			product *= prime;
		}

		return product;
	}
}

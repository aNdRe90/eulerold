

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import MyStuff.VariationNumber;

public class Problem169 {
	/*
	 * Define f(0)=1 and f(n) to be the number of different ways n 
	 * can be expressed as a sum of integer powers of 2 using 
	 * each power no more than twice.
	 * 
	 * For example, f(10)=5 since there are five different ways to express 10:
	 * 
	 * 1 + 1 + 8
	 * 1 + 1 + 4 + 4
	 * 1 + 1 + 2 + 2 + 4
	 * 2 + 4 + 4
	 * 2 + 8
	 * 
	 * What is f(10^25)?
	 */
	private static final BigInteger N = BigInteger.TEN.pow(25);
	private static BigInteger two = BigInteger.valueOf(2L);
	private static List<BigInteger> powers = getPowersOf2UpTo(N);
	private static final int powersSize = powers.size();
	private static final Map<Integer, Map<BigInteger, BigInteger>> memory = new HashMap<Integer, Map<BigInteger, BigInteger>>();

	public static void main(String[] args) {
		//long resultBrute = bruteForce(N.longValue());
		BigInteger result = getResult(N, 0);
		System.out.println("RESULT = " + result);
		//System.out.println("RESULT_BRUTE = " + resultBrute);
	}

	private static long bruteForce(long n) {
		int[] bases = getBases();
		long counter = 0L;

		VariationNumber var = new VariationNumber(bases);
		while (!var.isOverflow()) {
			int[] variation = var.getValues();
			long number = getNumber(variation);

			if (number == n) {
				counter++;
			}

			var.increment();
		}

		return counter;
	}

	private static long getNumber(int[] variation) {
		long number = 0L;

		for (int i = 0; i < variation.length; i++) {
			number += variation[i] * powers.get(i).longValue();
		}

		return number;
	}

	private static int[] getBases() {
		int[] bases = new int[powersSize];
		for (int i = 0; i < bases.length; i++) {
			bases[i] = 3;
		}

		return bases;
	}

	private static BigInteger getResult(BigInteger n, int indexStart) {
		BigInteger resultFromMemory = getFromMemory(n, indexStart);
		if (resultFromMemory != null) {
			System.out.println("FOUND IN MEMORY ----------------");
			return resultFromMemory;
		}

		if (indexStart < powersSize) {
			BigInteger mod = n.mod(powers.get(indexStart));
			if (mod.compareTo(BigInteger.ZERO) != 0) {
				putToMemory(n, indexStart, BigInteger.ZERO);
				return BigInteger.ZERO;
			}
		}

		//		if (indexStart == powersSize) {
		//			putToMemory(n, indexStart, BigInteger.ZERO);
		//			return BigInteger.ZERO;
		//		}

		if ((n.mod(two).compareTo(BigInteger.ZERO) != 0) && (indexStart > 0)) {
			putToMemory(n, indexStart, BigInteger.ZERO);
			return BigInteger.ZERO;
		}

		int compareZero = n.compareTo(BigInteger.ZERO);
		if (compareZero == 0) {
			putToMemory(n, indexStart, BigInteger.ONE);
			return BigInteger.ONE;
		}
		if (compareZero < 0) {
			putToMemory(n, indexStart, BigInteger.ZERO);
			return BigInteger.ZERO;
		}

		BigInteger result = BigInteger.ZERO;

		for (int i = indexStart; i < powersSize; i++) {
			BigInteger power = powers.get(i);
			if (power.compareTo(n) > 0) {
				break;
			}

			BigInteger newN = n.subtract(power);
			result = result.add(getResult(newN, i + 1));
			newN = newN.subtract(power);
			result = result.add(getResult(newN, i + 1));
		}

		putToMemory(n, indexStart, result);
		return result;
	}

	private static void putToMemory(BigInteger n, int indexStart,
			BigInteger result) {
		System.out.println("n=" + n + ", index=" + indexStart + ", result="
				+ result);

		Map<BigInteger, BigInteger> map = memory.get(indexStart);
		if (map == null) {
			map = new HashMap<BigInteger, BigInteger>();
			memory.put(indexStart, map);
		}

		map.put(n, result);
	}

	private static BigInteger getFromMemory(BigInteger n, int indexStart) {
		Map<BigInteger, BigInteger> map = memory.get(indexStart);
		if (map == null) {
			return null;
		}

		return map.get(n);
	}

	private static List<BigInteger> getPowersOf2UpTo(BigInteger limit) {
		int size = (int) (Math.log10(limit.doubleValue()) / Math.log10(2.0));
		List<BigInteger> powers = new ArrayList<BigInteger>(size);

		for (BigInteger power2 = BigInteger.ONE; power2.compareTo(limit) <= 0; power2 = power2
				.multiply(two)) {
			powers.add(power2);
		}

		return powers;
	}
}

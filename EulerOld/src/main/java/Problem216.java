

import MyStuff.Prime;

public class Problem216 {
	/*
	* Consider numbers t(n) of the form t(n) = 2n^2-1 with n > 1.
	* The first such numbers are 7, 17, 31, 49, 71, 97, 127 and 161.
	* It turns out that only 49 = 7*7 and 161 = 7*23 are not prime.
	* For n <= 10000 there are 2202 numbers t(n) that are prime.
	* 
	* How many numbers t(n) are prime for n <= 50,000,000 ?
	*/
	private static final long N = (long) (5.0 * 1e7);
	private static final long limitPrime = (long) Math.sqrt((2L * N * N) - 1L);

	public static void main(String[] args) {
		long counter = 0L;

		for (long n = 2L; n <= N; n++) {
			if ((n % 10000L) == 0L) {
				System.out.println(n);
			}

			if (Prime.isPrimeMillerRabin((2L * n * n) - 1L, 10)) {
				counter++;
			}
		}

		System.out.println("RESULT = " + counter);
	}
}

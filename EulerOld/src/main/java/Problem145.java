

import java.util.HashSet;
import java.util.Set;

public class Problem145 {
	/*
	 * Some positive integers n have the property that the sum 
	 * [ n + reverse(n) ] consists entirely of odd (decimal) digits. 
	 * For instance, 36 + 63 = 99 and 409 + 904 = 1313. 
	 * We will call such numbers reversible; so 36, 63, 409, and 904 are reversible. 
	 * Leading zeroes are not allowed in either n or reverse(n).
	 * 
	 * There are 120 reversible numbers below one-thousand.
	 * 
	 * How many reversible numbers are there below one-billion (10^9)?
	 */
	public static void main(String[] args) {
		final int N = (int) 1e9;
		Set<Integer> reversibles = new HashSet<Integer>();
		
		for(int i=11; i<N; i++) {
			if(i%10000==0) {
				System.out.println(i);
			}
			
			if(i%10!=0 && !reversibles.contains(i)) {
				int reverse = getReverse(i);
				int sum = reverse + i;
				
				if(consistsOfOddDigitsOnly(sum)) {
					reversibles.add(i);
					reversibles.add(reverse);
				}
			}			
		}
		
		System.out.println("Result = " + reversibles.size());
	}

	private static boolean consistsOfOddDigitsOnly(int n) {
		String nString = Integer.toString(n);
		int length = nString.length();
		
		for(int i=0; i<length; i++) {
			if( !isOddDigit(nString.charAt(i)) ) {
				return false;
			}
		}
		
		return true;
	}

	private static boolean isOddDigit(char c) {
		return c=='1' || c=='3' || c=='5' || c=='7' || c=='9';
	}

	private static int getReverse(int n) {
		String nString = Integer.toString(n);
		String reverseString = new StringBuffer(nString).reverse().toString();
		return Integer.parseInt(reverseString);
	}

}

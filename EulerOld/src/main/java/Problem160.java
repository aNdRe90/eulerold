

import java.util.ArrayList;
import java.util.List;

public class Problem160 {
	/*
	 * For any N, let f(N) be the last five digits 
	 * before the trailing zeroes in N!.
	 * 
	 * For example,
	 * 
	 * 9! = 362880 so f(9)=36288
	 * 10! = 3628800 so f(10)=36288
	 * 20! = 2432902008176640000 so f(20)=17664
	 * 
	 * Find f(1,000,000,000,000)
	 */

	//NOT SOLVED!!!

	private static final long five5 = (long) Math.pow(5.0, 5.0);

	public static void main(String[] args) {
		final long N = (long) 1e12;
		long result = 1L;
		long modulo = (long) 1e10;//100000;
		List<Long> results = new ArrayList<Long>(100000);

		//		double sum = 0.0;
		//		for (long n = 1L; n <= N; n++) {
		//			sum += Math.log(n);
		//		}
		//
		//		System.out.println(Math.pow(Math.E, sum));
		long last5 = -1L;
		long nextLast5 = -1L;
		for (long n = 1L; n <= N; n++) {
			if ((n % 50000000L) == 0L) {
				System.out.println(n);
			}

			if (n == 9375L) {
				System.out.println();
			}

			result *= (n % modulo);
			while ((result % 10L) == 0L) {
				result /= 10L;
			}

			result = result % modulo;
			last5 = result % 100000L;

			if (nextLast5 == last5) {
				System.out.println("JUHU!");
			}

			int index = results.indexOf(last5);
			if (index == -1) {
				//System.out.println("f(" + n + ") = " + result);
				//} else {
				results.add(last5);
				nextLast5 = -1L;

				System.out.print("\nf(" + n + ") = " + last5);
				if (((n % 5L) != 0L) || ((n % five5) == 0L)) {
					System.out.print("   true");
				} else {
					System.out.print(" ----------> FALSE!!!");
				}
			} else {
				index++;
				if (index < results.size()) {
					nextLast5 = results.get(index);
				}
			}

			if ((last5 % 2) == 1) {
				int sdf = 32;
			}
		}

		System.out.println("RESULT = " + result);
	}
}

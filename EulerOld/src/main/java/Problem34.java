
public class Problem34 
{
	/*
	 * 145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.

		Find the sum of all numbers which are equal to the sum of the factorial of their digits.

		Note: as 1! = 1 and 2! = 2 are not sums they are not included.	 */
	
	
	/*
	 * SOLUTION:
	 * Let X be the sum of factorials in n-digit number.
	 * Then:
	 *     n <= X <= n*9! (=n*362880)
	 *     
	 *  n=1:	1 <= X <= 362 880    (=6-digit number = OK) 
	 *  n=2:	2 <= X <= 725 760    (=6-digit number = OK) 
	 *  n=3:	3 <= X <= 1 088 640  (=7-digit number = OK) 
	 *  n=4:	4 <= X <= 1 451 520  (=7-digit number = OK) 
	 *  n=5:	5 <= X <= 1 814 400  (=7-digit number = OK) 
	 *  n=6:	6 <= X <= 2 177 280  (=7-digit number = OK) 
	 *  n=7:	7 <= X <= 2 540 160  (=7-digit number = OK)
	 *  n=8:    8 <= X <= 2 903 040  (=7-digit number = too short) 
	 */
	public static void main(String[] args) 
	{
		long sum = 0L;
		
		for(int i=10; i<2540161; i++)
		{
			if(i==934992)
			{
				int a = 0;
			}
			long temp = sumOfFactorialsOfDigitFrom(i);
			if(temp==(long)i)
				sum += temp;
		}
				
		System.out.println(sum);
	}
	
	public static long sumOfFactorialsOfDigitFrom(int number)
	{
		String s = Integer.toString(number);
		int length = s.length();
		long sum = 0L;
		
		for(int i=0; i<length; i++)
		{
			sum += facts[s.charAt(i)-0x30];
		}
		
		return sum;
	}
	
	private static long[] facts = { 1L, 1L, 2L, 6L, 24L, 120L, 720L, 5040L, 40320L, 362880L };
}

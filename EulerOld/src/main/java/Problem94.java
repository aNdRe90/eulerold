import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import MyStuff.BigSquareRoot;


public class Problem94 {
	/*
	 * It is easily proved that no equilateral triangle exists with integral length sides 
	 * and integral area. However, the almost equilateral triangle 5-5-6 
	 * has an area of 12 square units.
	 * 
	 * We shall define an almost equilateral triangle to be a triangle 
	 * for which two sides are equal and the third differs by no more than one unit.
	 * 
	 * Find the sum of the perimeters of all almost equilateral triangles 
	 * with integral side lengths and area and whose perimeters do not 
	 * exceed one billion (1,000,000,000).
	 */
	
	/*
	 * SOLUTION:
	 * 
	 	//Heron:
		//S = sqrt( (a+b+c)(a+b-c)(a-b+c)(b+c-a) ) /4
		
		//we have 2 possibilities of triangles (a,b,c):
		//1) a,a,a+1
		//2) a,a,a-1
		 * 
		 *  1) gives heron P=0.25*(a+1)*sqrt(3a^2 - 2a - 1)
		 *  2) gives heron P=0.25*(a-1)*sqrt(3a^2 + 2a - 1)
		 *  
		 *  I use the procedure of generating 20 next squares and comparing with X = 3a^2 [+-] 2a - 1
		 *  is in my generated array. If so, i check its root value.
		 *  If X > my greatest generated square then I generate 20 next squares.
		 *  
		 *  I generate 20 because the greater this number is the longer it takes 
		 *  to search the array in each iteration.
		 *  
		 *  Having the root and the A=(a [+-] 1) factor I can determine whether root*A  is divisible by 4.
		 *  To avoid overflow I multiply only last 2 digits of root and A and check if the result % 4 == 0.
	 */
	public static void main(String[] args) {
		long sum = 0L;	
		long startRoot = 1L;
		int howManySquares = 20;
		List<Long> squares = getSquares(startRoot, howManySquares);
		
		for(long a=2; a<=333333333; a++) {
			if(a%100000==0)
				System.out.println(a);
			
			long squareToBe = 3L*a*a-1L - 2L*a;
			
			if(squareToBe>squares.get(squares.size()-1)) {
				startRoot = squares.size() + startRoot;
				squares = getSquares(startRoot, howManySquares);
			}
			int index = squares.indexOf(squareToBe);
			if(index!=-1) {
				long root = (long)index + startRoot;
				if(isProductDivisibleBy4(root, a+1))
					sum = sum + 3*a + 1;
			}
		}
		
		startRoot = 1L;
		squares = null;
		squares = getSquares(startRoot, howManySquares);
		
		for(long a=2; a<=333333333; a++) {
			if(a%100000==0)
				System.out.println(a);
			
			long squareToBe = 3L*a*a-1L + 2L*a;
			
			if(squareToBe>squares.get(squares.size()-1)) {
				startRoot = squares.size() + startRoot;
				squares = getSquares(startRoot, howManySquares);
			}
			else {
				int index = squares.indexOf(squareToBe);
				if(index!=-1) {
					long root = (long)index + startRoot;
					if(isProductDivisibleBy4(root, a-1))
						sum = sum + 3*a - 1;
				}
			}
		}
		
		System.out.println(sum);
	}

	private static boolean isProductDivisibleBy4(long A, long B) {
		int a = getLastTwoDigits(A);
		int b = getLastTwoDigits(B);
		
		return (a*b)%4==0;
	}

	private static int getLastTwoDigits(long a) {
		if(a<100)
			return (int)a;
		
		String s = Long.toString(a);
		s = s.substring(s.length()-2);
		return Integer.parseInt(s);
	}

	private static List<Long> getSquares(long startRoot, int howManySquares) {
		List<Long> squares = new ArrayList<Long>(howManySquares);
		
		for(long i=0L; i<howManySquares; i++)
			squares.add((i+startRoot)*(i+startRoot));
		
		return squares;
	}
}
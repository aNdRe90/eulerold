

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Problem124 {
	/*
	 * The radical of n, rad(n), is the product of distinct prime factors of n. 
	 * For example, 504 = 23 � 32 � 7, so rad(504) = 2 � 3 � 7 = 42.
	 * 
	 * If we calculate rad(n) for 1 <= n <= 10, then sort them on rad(n), 
	 * and sorting on n if the radical values are equal, we get:
	 * 
	 * 	Unsorted						Sorted
	 * 
	 * 	n 		rad(n)					n		rad(n)		k
	 * 	1		1						1		1			1
	 * 	2  		2 						2		2			2
	 * 	3		3						4		2			3
	 * 	4		2						8		2			4
	 * 	5		5						3		3			5
	 * 	6		6						9		3			6
	 * 	7		7						5		5			7
	 * 	8		2						6		6			8
	 * 	9		3						7		7			9
	 * 	10		10						10		10			10
	 * 
	 * Let E(k) be the kth element in the sorted n column; for example, 
	 * E(4) = 8 and E(6) = 9.
	 * 
	 * If rad(n) is sorted for 1 <= n <= 100000, find E(10000).
	 */
	
	/*
	 * RUNS IN OVER 40minutes!
	 */
	public static void main(String[] args) {
		final int N = 100000;
		final int resultN = 10000;
		List<Element> elements = new ArrayList<Element>(N);
		
		for(int i=1; i<=N; i++) {
			if(i%100==0)
				System.out.println(i);
			
			elements.add(new Element(i, rad(i)));
		}	
		
		Collections.sort(elements);
		System.out.println(elements.get(resultN-1).getN());
	}
	
	public static long rad(int n) {
		if(n==1)
			return 1L;
		
		long[] distinctFactors = MyStuff.MyMath.getSinglePrimeDivisors(n);
		long result = 1L;
		
		for(int i=0; i<distinctFactors.length; i++)
			result *= distinctFactors[i];
		
		return result;
		
	}
}

class Element implements Comparable<Element> {
	private int n;
	private long rad;
	
	public Element(int n, long rad) {
		this.n = n;
		this.rad = rad;
	}
	
	public int getN() {
		return n;
	}
	
	public void setN(int n) {
		this.n = n;
	}
	
	public long getRad() {
		return rad;
	}
	
	public void setRad(long rad) {
		this.rad = rad;
	}

	@Override
	public int compareTo(Element other) {
		long radDifference = other.rad - this.rad;
		
		if(radDifference>0L)
			return -1;
		if(radDifference<0L)
			return 1;
		
		return this.n - other.n;
	}
	
	@Override
	public String toString() {
		return "n="+n+", rad(n)="+rad;
	}
}

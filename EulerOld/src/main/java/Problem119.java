

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Problem119 {
	/*
	 * The number 512 is interesting because it is equal to the sum of its digits 
	 * raised to some power: 5 + 1 + 2 = 8, and 8^3 = 512. 
	 * Another example of a number with this property is 614656 = 28^4.
	 * 
	 * We shall define an to be the nth term of this sequence and insist 
	 * that a number must contain at least two digits to have a sum.
	 * 
	 * You are given that a2 = 512 and a10 = 614656.
	 */
	public static void main(String[] args) {
		final int minBase=2;
		final int maxBase=100;
		final int maxExponent=8;
		final int N = 30;
		
		List<Long> powers = new ArrayList<Long>( (maxBase-minBase)*maxExponent );
		
		for(int base=minBase; base<=maxBase; base++)
			for(int exp=1; exp<=maxExponent; exp++) {
				long power = (long) Math.pow(base, exp);
				String powerString = Long.toString(power);
				
				if(powerString.length()==1)
					continue;
				
				int sum = getSumOfDigits(powerString);
				if(sum==base) {
					powers.add(power);
				}
			}
		
		Collections.sort(powers);
		System.out.println(powers.get(N-1));
	}

	private static int getSumOfDigits(String numberString) {
		int sum = 0;		
		int length = numberString.length();
		
		for(int i=0; i<length; i++)
			sum += (numberString.charAt(i) - 0x30);
		
		return sum;
	}
}

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.StringTokenizer;


public class Problem22 
{
	/*
	 * Using names.txt, a 46K text file containing over five-thousand first names, begin by sorting it into alphabetical order. Then working out the alphabetical value for each name, multiply this value by its alphabetical position in the list to obtain a name score.

	For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN would obtain a score of 938 � 53 = 49714.

	What is the total of all the name scores in the file?

	 */
	
	public static void main(String[] args) 
	{	
		BufferedReader f = null;
	
		try
		{
			f = new BufferedReader(new FileReader("names.txt"));
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
		String line = null;
		int i = 0;
		do
		{
			try 
			{
				line = f.readLine();
			} 
			catch(IOException ee) { ee.printStackTrace(); }
			
			if(line==null)
				break;
			
			StringTokenizer st = new StringTokenizer(line, ",");
			while(st.hasMoreTokens())
			{
				String temp = st.nextToken();
				names.add(temp.substring(1, temp.length()-1));
			}				
		}
		while(true);
		Collections.sort(names);
		
		long sum = 0L;
		Iterator<String> iter = names.iterator();
		
		int counter = 1;
		while(iter.hasNext())
		{
			String temp = iter.next();
			int size = temp.length();
			int nameProduct = 0;
			for(int j=0; j<size; j++)
			{
				nameProduct += (temp.charAt(j) - 0x40);
			}				
			sum += (long)nameProduct*(counter++);
		}


		System.out.println(sum);
	}
	private static LinkedList<String> names = new LinkedList<String>();
}



import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import MyStuff.BigFraction;

public class Problem151 {
	/*
	 * A printing shop runs 16 batches (jobs) every week and each batch 
	 * requires a sheet of special colour-proofing paper of size A5.
	 * 
	 * Every Monday morning, the foreman opens a new envelope, 
	 * containing a large sheet of the special paper with size A1.
	 * 
	 * He proceeds to cut it in half, thus getting two sheets of size A2. 
	 * Then he cuts one of them in half to get two sheets of size A3 and 
	 * so on until he obtains the A5-size sheet needed for the first batch of the week.
	 * 
	 * All the unused sheets are placed back in the envelope.
	 * At the beginning of each subsequent batch, he takes from the envelope one 
	 * sheet of paper at random. If it is of size A5, he uses it. If it is larger, 
	 * he repeats the 'cut-in-half' procedure until he has what he needs and any 
	 * remaining sheets are always placed back in the envelope.
	 * 
	 * Excluding the first and last batch of the week, find the expected number 
	 * of times (during each week) that the foreman finds a single sheet of paper 
	 * in the envelope.
	 * 
	 * Give your answer rounded to six decimal places using the format x.xxxxxx .
	 */
	private static Map<List<Integer>, BigFraction> memory = new HashMap<List<Integer>, BigFraction>();

	public static void main(String[] args) {
		List<Integer> values = getInitialValues();
		BigFraction result = getResult(values);

		System.out.println("RESULT = " + result.toDouble() + " (" + result
				+ ")");
	}

	private static BigFraction getResult(List<Integer> values) {
		BigFraction resultFromMemory = memory.get(values);
		if (resultFromMemory != null) {
			return resultFromMemory;
		}

		int size = values.size();

		if ((size == 1) && (values.get(0) == 1)) {
			return new BigFraction(BigInteger.ZERO);
		}
		//		if ((size == 2) && ((values.get(0) == 1) && (values.get(1) == 1))) {
		//			return new BigFraction(BigInteger.ONE, BigInteger.valueOf(2));
		//		}

		BigFraction multiplier = new BigFraction(BigInteger.ONE,
				BigInteger.valueOf(size));
		BigFraction result = (size == 1) ? new BigFraction(BigInteger.ONE)
				: new BigFraction(BigInteger.ZERO);

		for (int i = 0; i < size; i++) {
			//			if (values.get(i) == 1) {
			//				result = result.add(multiplier);
			//				result = result.reduce();
			//			}

			System.out.println(values);
			List<Integer> newValues = getNewValues(values, i);
			BigFraction innerResult = getResult(newValues);
			result = result.add(multiplier.multiply(innerResult));
			result = result.reduce();
		}

		System.out.println("result=" + result);
		memory.put(new ArrayList<Integer>(values), result);
		return result;
	}

	private static List<Integer> getNewValues(List<Integer> values, int i) {
		List<Integer> copy = new LinkedList<Integer>(values);
		copy.remove(Integer.valueOf(values.get(i)));

		List<Integer> cutParts = getCutParts(values.get(i));
		cutParts.addAll(copy);

		return cutParts;
	}

	private static List<Integer> getCutParts(int v) {
		List<Integer> values = new ArrayList<Integer>();
		//if v==1, empty list

		if (v == 2) {
			values.add(1);
		}

		if (v == 4) {
			values.add(1);
			values.add(2);
		}

		if (v == 8) {
			values.add(1);
			values.add(2);
			values.add(4);
		}

		return values;
	}

	private static List<Integer> getInitialValues() {
		List<Integer> values = new ArrayList<Integer>();

		values.add(8);
		values.add(4);
		values.add(2);
		values.add(1);

		return values;
	}
}

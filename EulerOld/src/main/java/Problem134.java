

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Problem134 {
	/*
	 * Consider the consecutive primes p1 = 19 and p2 = 23. 
	 * It can be verified that 1219 is the smallest number 
	 * such that the last digits are formed by p1 whilst also 
	 * being divisible by p2.
	 * 
	 * In fact, with the exception of p1 = 3 and p2 = 5, 
	 * for every pair of consecutive primes, p2 > p1, 
	 * there exist values of n for which the last digits 
	 * are formed by p1 and n is divisible by p2. 
	 * 
	 * Let S be the smallest of these values of n.
	 * Find sum(S) for every pair of consecutive primes 
	 * with 5 <= p1 <= 1000000.
	 */
	public static void main(String[] args) {
		List<Integer> primes = getPrimes();
		long sum = 0L;		
		int p1 = -1;
		int p2 = -1;
		
		Iterator<Integer> iter = primes.iterator();
		if(iter.hasNext()) {
			p1 = iter.next();
		}
		
		while(iter.hasNext()) {
			p2 = iter.next();
			
			sum += getS(p1, p2);			
			System.out.println("p2=" + p2);		
			p1 = p2;
		}
		
		System.out.println(sum);
	}

	private static List<Integer> getPrimes() {
		List<Integer> primes = getPrimesBetween(5, 1000000);
		
		for(int i=1000001; ;i++) {
			if(MyStuff.MyMath.isPrime(i)) {
				primes.add(Integer.valueOf(i));
				break;
			}
		}
		
		return primes;
	}

	private static long getS(int p1, int p2) {
		int p1Length = Integer.toString(p1).length();
		int toAdd = (int) Math.pow(10.0, p1Length);
		
		int modulo = p1;
		long head = 0L;
				
		while(modulo!=0) {				
			modulo = (modulo+toAdd) % p2;
			head++;
		}
		
		return getConcatenatedLong(head, p1);
	}

	private static long getConcatenatedLong(long head, long tail) {
		return Long.parseLong(Long.toString(head) + Long.toString(tail));
	}

	private static List<Integer> getPrimesBetween(int from, int to) {
		List<Integer> primes = new LinkedList<Integer>();
		
		for(int i=from; i<=to; i++) {
			if(MyStuff.MyMath.isPrime(i)) {
				primes.add(Integer.valueOf(i));
			}
		}
		
		return primes;
	}
}

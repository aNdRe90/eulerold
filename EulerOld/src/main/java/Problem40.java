
public class Problem40
{
	/*
	 * An irrational decimal fraction is created by concatenating the positive integers:

		0.123456789101112131415161718192021...

		It can be seen that the 12th digit of the fractional part is 1.

		If d(n) represents the nth digit of the fractional part, find the value of the following expression.

		d(1) x d(10) x d(100) x d(1000) x d(10000) x d(100000) x d(1000000)

	 */
	public static void main(String[] args) 
	{
		StringBuffer fraction = new StringBuffer(1000000);
		for(int i=1; ; i++)
		{
			fraction.append(Integer.toString(i));
			if(fraction.length()>1000000)
				break;
		}
		
		StringBuffer result = new StringBuffer(new String(""+fraction.charAt(0)));
		result.append(fraction.charAt(9));
		result.append(fraction.charAt(99));
		result.append(fraction.charAt(999));
		result.append(fraction.charAt(9999));
		result.append(fraction.charAt(99999));
		result.append(fraction.charAt(999999));
		

		System.out.println(result.toString());
	}
}

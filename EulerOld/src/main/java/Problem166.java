

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import MyStuff.VariationNumber;

public class Problem166 {
	/*
	 * A 4x4 grid is filled with digits d, 0 <= d <= 9.
	 * 
	 * It can be seen that in the grid
	 * 
	 * 6 3 3 0
	 * 5 0 4 3
	 * 0 7 1 4
	 * 1 2 4 5
	 * 
	 * the sum of each row and each column has the value 12. 
	 * Moreover the sum of each diagonal is also 12.
	 * 
	 * In how many ways can you fill a 4x4 grid with the digits d, 
	 * 0 <= d <= 9 so that each row, each column, and both diagonals have the same sum?
	 */
	private static final int maxDigit = 9;
	private static final int N = 4;
	private static final int N2 = N * N;

	public static void main(String[] args) {
		//long bruteForceResult = bruteForce(N);

		int[][] grid = new int[N][N];

		int maxSum = maxDigit * N;
		long result = 0L;
		for (int sum = 0; sum <= maxSum; sum++) {
			result += getResult(grid, 0, sum);
			System.out.println(sum + " = " + result);
			clear(grid);
		}

		//		Map<Integer, List<int[]>> possibilities = getPossibilities(maxDigit);
		//
		//		for (int i = 0; i <= maxSum; i++) {
		//			for (int[] values : possibilities.get(i)) {
		//				int[] grid = getGrid(values);
		//
		//				result += getResult(grid, N, i);
		//			}
		//		}

		//
		//		for (int i = 0; i <= 36; i++) {
		//			System.out.println("SUM = " + i + ":");
		//			List<int[]> list = possibilities.get(i);
		//			System.out.println(list.size());
		//			//for (int[] v : list) {
		//			//	if (Arrays.binarySearch(v, 4) > 0) {
		//			//		System.out.println(Arrays.toString(v));
		//			//	}
		//			//}
		//		}

		System.out.println("RESULT = " + result);
		//System.out.println("RESULT BRUTE = " + bruteForceResult);
	}

	private static void clear(int[][] grid) {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				grid[i][j] = 0;
			}
		}
	}

	private static long getResult(int[][] grid, int index, int exactSum) {
		//printGrid(grid);

		if (index == N2) {
			if (isGridCorrectWithSum(grid, exactSum)) {
				//printGrid(grid);
				//System.out.println("SUM = " + exactSum);
				return 1L;
			}

			return 0L;
		}

		int sumDiagonal1 = getSumDiagonal1(grid);
		int sumDiagonal2 = getSumDiagonal2(grid);
		if ((sumDiagonal1 > exactSum) || (sumDiagonal2 > exactSum)) {
			return 0L;
		}

		int row = index / N;
		int column = index % N;

		int sumColumn = getSumInColumn(grid, column);
		int sumRow = getSumInRow(grid, row);

		if (sumColumn > exactSum) {
			return 0L;
		}

		if (sumRow > exactSum) {
			return 0L;
		}

		if ((column == 0) && (row > 0)) {
			int sumRowPrevious = getSumInRow(grid, row - 1);
			if (sumRowPrevious != exactSum) {
				return 0L;
			}
		}
		if ((row == (N - 1)) && (column > 0)) {
			int sumColumnPrevious = getSumInColumn(grid, column - 1);
			if (sumColumnPrevious != exactSum) {
				return 0L;
			}
		}
		if ((row == (N - 1)) && (column == 1)) {
			int sumDiagonal2All = getSumDiagonal2(grid);
			if (sumDiagonal2All != exactSum) {
				return 0L;
			}
		}

		int maxSum = sumRow > sumColumn ? sumRow : sumColumn;//getMax(sumColumn, sumRow, sumDiagonal1, sumDiagonal2);
		int maxSumLimit = exactSum - maxSum;
		//
		if (maxSumLimit < 0) {
			return 0L;
		}

		int limit = maxDigit < maxSumLimit ? maxDigit : maxSumLimit;

		long result = 0L;
		int temp = grid[row][column];
		for (int i = 0; i <= limit; i++) {
			grid[row][column] = i;
			result += getResult(grid, index + 1, exactSum);
		}

		grid[row][column] = temp;
		return result;
	}

	private static long bruteForce(int N) {
		int[][] grid = new int[N][N];
		int maxSum = maxDigit * N;

		int[] bases = new int[N * N];
		for (int i = 0; i < bases.length; i++) {
			bases[i] = maxDigit + 1;
		}

		long result = 0L;
		VariationNumber var = new VariationNumber(bases);
		while (!var.isOverflow()) {
			int[] values = var.getValues();

			int index = 0;
			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++) {
					grid[i][j] = values[index++];
				}
			}

			for (int sum = 0; sum <= maxSum; sum++) {
				if (isGridCorrectWithSum(grid, sum)) {
					result++;
					break;
				}
			}

			var.increment();
		}

		return result;
	}

	private static boolean isGridCorrectWithSum(int[][] grid, int exactSum) {
		boolean correct = true;

		for (int i = 0; i < N; i++) {
			int sumColumn = getSumInColumn(grid, i);
			int sumRow = getSumInRow(grid, i);

			if ((sumColumn != exactSum) || (sumRow != exactSum)) {
				correct = false;
				break;
			}
		}

		if (correct) {
			int sumDiagonal1 = getSumDiagonal1(grid);
			int sumDiagonal2 = getSumDiagonal2(grid);

			if ((sumDiagonal1 != exactSum) || (sumDiagonal2 != exactSum)) {
				correct = false;
			}
		}

		return correct;
	}

	private static int getSumDiagonal1(int[][] grid) {
		int sum = 0;

		for (int i = 0; i < N; i++) {
			sum += grid[i][i];
		}

		return sum;
	}

	private static int getSumDiagonal2(int[][] grid) {
		int sum = 0;

		for (int i = 0; i < N; i++) {
			sum += grid[i][N - i - 1];
		}

		return sum;
	}

	private static int getSumInRow(int[][] grid, int row) {
		int sum = 0;

		for (int i = 0; i < N; i++) {
			sum += grid[row][i];
		}

		return sum;
	}

	private static int getSumInColumn(int[][] grid, int column) {
		int sum = 0;

		for (int i = 0; i < N; i++) {
			sum += grid[i][column];
		}

		return sum;
	}

	private static Map<Integer, List<int[]>> getPossibilities(int maxDigit) {
		Map<Integer, List<int[]>> possibilities = new HashMap<Integer, List<int[]>>();

		for (int a = 0; a <= maxDigit; a++) {
			for (int b = a; b <= maxDigit; b++) {
				for (int c = b; c <= maxDigit; c++) {
					for (int d = c; d <= maxDigit; d++) {
						int sum = a + b + c + d;
						int[] values = new int[] { a, b, c, d };
						Arrays.sort(values);
						putToPossibilities(values, sum, possibilities);
					}
				}
			}
		}

		return possibilities;
	}

	private static void putToPossibilities(int[] values, int sum,
			Map<Integer, List<int[]>> possibilities) {
		List<int[]> list = possibilities.get(sum);
		if (list == null) {
			list = new LinkedList<int[]>();
			possibilities.put(sum, list);
		}

		list.add(values);
	}
}



public class Problem159 {
	/*
	 * A composite number can be factored many different ways. 
	 * For instance, not including multiplication by one, 24 can be factored in 7 distinct ways:
	 * 
	 * 24 = 2x2x2x3
	 * 24 = 2x3x4
	 * 24 = 2x2x6
	 * 24 = 4x6
	 * 24 = 3x8
	 * 24 = 2x12
	 * 24 = 24
	 * 
	 * Recall that the digital root of a number, in base 10, 
	 * is found by adding together the digits of that number, and repeating that process 
	 * until a number is arrived at that is less than 10. Thus the digital root of 467 is 8.
	 * 
	 * We shall call a Digital Root Sum (DRS) the sum of the digital roots 
	 * of the individual factors of our number.
	 * 
	 * The chart below demonstrates all of the DRS values for 24.
	 * Factorisation	Digital Root Sum
	 * 2x2x2x3			9
	 * 2x3x4			9
	 * 2x2x6			10
	 * 4x6				10
	 * 3x8				11
	 * 2x12				5
	 * 24				6
	 * 
	 * The maximum Digital Root Sum of 24 is 11.
	 * The function mdrs(n) gives the maximum Digital Root Sum of n. So mdrs(24)=11.
	 * Find sum of mdrs(n) for 1 < n < 1,000,000.
	 */
	private static final int N = 1000000;
	private static int[] digitalRoots = getDigitalRoots(N - 1);
	private static int[] maximalDigitalRootSums = new int[N];

	public static void main(String[] args) {
		long sum = 0L;

		for (int n = 2; n < N; n++) {
			System.out.println(n);

			sum += getMaximumDigitalRootSum(n);
		}

		System.out.println("RESULT = " + sum);
	}

	private static int getMaximumDigitalRootSum(int n) {
		if (maximalDigitalRootSums[n] != 0) {
			return maximalDigitalRootSums[n];
		}

		int sqrt = (int) Math.sqrt(n);
		int max = digitalRoots[n];

		for (int i = 2; i <= sqrt; i++) {
			if ((n % i) == 0) {
				int current = digitalRoots[i] + getMaximumDigitalRootSum(n / i);
				if (current > max) {
					max = current;
				}
			}
		}

		maximalDigitalRootSums[n] = max;
		return max;
	}

	private static int[] getDigitalRoots(int N) {
		int[] digitalRoots = new int[N + 1];

		for (int n = 1; n <= N; n++) {
			String s = Integer.toString(n);
			int sumOfDigits = getSum(s);

			if (sumOfDigits < 10) {
				digitalRoots[n] = sumOfDigits;
			} else {
				digitalRoots[n] = digitalRoots[sumOfDigits];
			}
		}

		return digitalRoots;
	}

	private static int getSum(String s) {
		int sum = 0;
		int length = s.length();

		for (int i = 0; i < length; i++) {
			sum += (s.charAt(i) - '0');
		}

		return sum;
	}
}

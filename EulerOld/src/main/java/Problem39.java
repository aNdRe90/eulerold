
public class Problem39 
{
	/*
	 * f p is the perimeter of a right angle triangle with integral length sides, 
	 * {a,b,c}, there are exactly three solutions for p = 120.
	 * 
	 * {20,48,52}, {24,45,51}, {30,40,50}
	 * 
	 * For which value of p <= 1000, is the number of solutions maximised?
	 */
	
	public static void main(String[] args) 
	{
		int maxP = 1000;
		//int minP = 12; //3,4,5
		int[] perimeterAppearances = new int[1001];

		for(int a = 3; a!=0; a++)
		{
			if(3*a>1000)
				break;
			
			for(int b = a; ; b++)
			{
				int max = a>b? a : b;
				if((a+b+max)>1000)
					break;

				else
				{
					double c = Math.sqrt((double) a*a + b*b);
					int cc = (int) c;
					int perimeter = a+b+cc;
					if((double)cc==c && perimeter<1001)
					{
						perimeterAppearances[perimeter]++;
					}
				}
			}
		}
		
		int maximum = perimeterAppearances[0];
		int maximumIndex = 0;
		for(int i=12; i<perimeterAppearances.length; i++)
			if(perimeterAppearances[i]>maximum)
			{
				maximum = perimeterAppearances[i];
				maximumIndex = i;
			}
		
		System.out.println(maximumIndex);
	}
}

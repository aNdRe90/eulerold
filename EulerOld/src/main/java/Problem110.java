

import java.math.BigInteger;
import java.util.Arrays;

import MyStuff.VariationNumber;

public class Problem110 {
	/*
	 * In the following equation x, y, and n are positive integers.
	 * 
	 * 1/x + 1/y = 1/n
	 * 
	 * It can be verified that when n = 1260 there are 113 distinct solutions 
	 * and this is the least value of n for which the total number of 
	 * distinct solutions exceeds one hundred.
	 * 
	 * What is the least value of n for which the number 
	 * of distinct solutions exceeds four million?
	 * 
	 * NOTE: This problem is a much more difficult version of problem 108 
	 * and as it is well beyond the limitations of a brute force approach 
	 * it requires a clever implementation.
	 */
	
	/*
	 * SOLUTION:
	 * 1/x + 1/y = 1/n
	 * yn + xn = xy
	 * (n+l)n + (n+k)n = (n+l)(n+k)     //k=1,2,...,n    l=1,2,...,n
	 * n^2 + nl + n^2 + nk = n^2 + nk + nl + kl
	 * n^2 = kl
	 * 
	 * k,l  are divisors of n^2 and k*l = n^2
	 * 
	 * n^2 is a square number, so its have odd number of divisors e.g
	 * 16  = 1,2,4,8,16
	 * (k,l) = (1,16), (2,8), (4,4)   - 3 solutions   to problem  1/x + 1/y = 1/4
	 * 									(x,y) = { (4+1,4+16), (4+2, 4+8), (4+4, 4+4) }
	 * 
	 * So for n we have  numberOfDivisors(n^2)/2 + 1 solutions for problem 1/x + 1/y = 1/n.
	 * 
	 * numberOfDivisors(n^2)/2 + 1 >1000
	 * numberOfDivisors(n^2) >= 2000
	 * 
	 * n^2 = p1^exp1 * p2^exp2 * ... * pi^expi
	 * 
	 * (exp1+1)*(exp2+1)*...*(expi+1) = numberOfDivisors(n^2) >= 8.000.000
	 * furthermore, expi%2==0 as it must be a square number	 * 
	 * 
	 * expiMIN = 2, so   (2+1)^i >= 8.000.000    =>  iMAX = 15
	 * 
	 * I needed to find minimum n^2 that numberOfDivisors(n^2) >= 8.000.000
	 * 
	 * I workaround after many attemps of taking expi value ranges to brute force by
	 * assuming expi = {2, 4, 6} (taking into consideration result from problem 108 
	 * and placing them on each position (variation with repetitions)
	 * finding the least value of exponents and therefore least value of n^2.
	 * 
	 * I then calculated n by dividing exponents by 2   
	 * [  n = p1^(exp1/2) * p2^(exp2/2) * ... * pi^(expi/2)  ]
	 * 
	 * It was enough! 
	 */
	public static void main(String[] args) {		
		BigInteger minResultToBe = null;
		int[] minExponents = null;
		int iMAX = (int) Math.ceil(Math.log(range)/Math.log(3.0));
		int[] primes = getPrimesArray(iMAX);
		
		for(int i=iMAX; i>1; i--) {
			//System.out.println(i);
			
			int[] expsPossible = getExpsPossible(i);
			VariationNumber number = new VariationNumber(getBases(expsPossible, i));
			
			while(!number.isOverflow()) {
				int[] exponents = getExponents(number.getValues(), expsPossible);

				if(everyExponentEven(exponents)) {
					long product = getProductOfValues(incrementByOne(exponents));
					BigInteger resultToBe = getValueFromPrimesAndExponents(
										primes, getExponentsDividedBy2(exponents));
					
					if(product>=range) {
						if( minResultToBe==null || resultToBe.compareTo(minResultToBe)<0 )  {
							minResultToBe = resultToBe;
							minExponents = exponents;
						}
						//break;
					}
				}						
				
				number.increment();
			}
		}
		
		System.out.println(minResultToBe);
		
	}
	
	private static int[] getExponentsDividedBy2(int[] exponents) {
		int[] exp = new int[exponents.length];
		
		for(int i=0; i<exp.length; i++)
			exp[i] = exponents[i]/2;
		
		return exp;
	}

	private static int[] getExponents(int[] values, int[] expsPossible) {
		int[] exponents = new int[values.length];
		
		if(expsPossible.length==0) {
			for(int i=0; i<exponents.length; i++)
				exponents[i] = expsPossible[values[i]];
		}
		
		for(int i=0; i<exponents.length; i++)
			exponents[i] = expsPossible[values[i]];
		
		return exponents;
	}

	private static int[] getBases(int[] expsPossible, int v) {
		int[] bases = new int[v];
		
		for(int i=0; i<bases.length; i++)
			bases[i] = expsPossible.length;
		
		return bases;
	}

	private static boolean everyExponentEven(int[] exponents) {
		int counter = 0;
		
		for(int e : exponents)
			if(e%2==0)
				counter++;
		
		return counter==exponents.length;
	}

	private static BigInteger getValueFromPrimesAndExponents(int[] primes, int[] exponents) {
		BigInteger value = BigInteger.ONE;
		
		for(int i=0; i<exponents.length; i++)
			value = value.multiply(BigInteger.valueOf((long) Math.pow(primes[i], exponents[i])));
		
		return value;
	}

	private static long getProductOfValues(int[] values) {
		long product = 1;
		for(int v : values)
			product *= (long)v;
		
		return product;
	}

	private static int[] incrementByOne(int[] values) {
		int[] newValues = new int[values.length];
		
		for(int i=0; i<values.length; i++)
			newValues[i] = values[i]+1;
		
		return newValues;
	}

	private static int[] getPrimesArray(int howMany) {
		int[] primes = new int[howMany];
		int i=0;
		
		for(int n=2; i<howMany; n++) {
			if(MyStuff.MyMath.isPrime(n))
				primes[i++] = n;
		}
		
		return primes;
	}

	private static int[] getExpsPossible(int v) {
		return new int[] { 2, 4, 6 };
		
		/*int expMAX = (int) Math.ceil( Math.pow((double)range, 1.0/(double)v));
		expMAX = (int) Math.floor(expMAX);
		int[] expsPossible = new int[expMAX/2];
		
		int i=0;
		for(int j=2; j<=expMAX; j+=2)
			expsPossible[i++] = j;
		
		return expsPossible;*/
	}
	
	private static int range = 8000000;
}

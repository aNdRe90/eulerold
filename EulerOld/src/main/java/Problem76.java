import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;


public class Problem76 
{
	/*
	 * It is possible to write five as a sum in exactly six different ways:

		4 + 1
		3 + 2
		3 + 1 + 1
		2 + 2 + 1
		2 + 1 + 1 + 1
		1 + 1 + 1 + 1 + 1

		How many different ways can one hundred be written as a sum of at least two positive integers?
	 */
	
	/*
	 * SOLUTION: http://en.wikipedia.org/wiki/Partition_(number_theory)
	 * 
	 * In number theory, the partition function p(n) represents the number of possible partitions of a natural number n, 
	 * which is to say the number of distinct (and order independent) ways of representing n as a sum of natural numbers. 
	 * By convention p(0) = 1, p(n) = 0 for n negative.
	 * 
	 * The first few values of the partition function are (starting with p(0)=1):
	 * 
	 * 1, 1, 2, 3, 5, 7, 11, 15, 22, 30, 42, � (sequence A000041 in OEIS). 
	 * 
	 *     	p(k, n) = 0 if k > n

    		p(k, n) = 1 if k = n

    		p(k, n) = p(k+1, n) + p(k, n - k) otherwise.
	
	 */
	public static void main(String[] args) 
	{
		System.out.println(p(1,100)-1);
	}
	
	public static long p(int k, int n)
	{
		if(k>n)
			return 0L;
		if(k==n)
			return 1L;
		
		return p(k+1,n) + p(k, n-k);
	}
	
}
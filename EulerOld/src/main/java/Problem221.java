

import java.io.IOException;
import java.math.BigInteger;
import java.util.TreeSet;

public class Problem221 {
	/*
	 * We shall call a positive integer A an "Alexandrian integer", 
	 * if there exist integers p, q, r such that:
	 * 
	 * A = p � q � r    and   	
	 * 1/A = 1/p + 1/q + 1/r
	 * 
	 * For example, 630 is an Alexandrian integer (p = 5, q = -7, r = -18). 
	 * In fact, 630 is the 6th Alexandrian integer, 
	 * the first 6 Alexandrian integers being: 6, 42, 120, 156, 420 and 630.
	 * 
	 * Find the 150000th Alexandrian integer.
	 */

	/*
	 * SOLUTION:
	 * First, we can easily say that if A>0 and A=pqr then
	 * two of p,q,r must mus negative.
	 * 
	 * Then, lets write:
	 * 1/A = 1/p - 1/a - 1/b    where p>0, a>0, b>0
	 * 1/A = (ab - pb - pa)/pab
	 * 
	 * A = pab
	 * ab - pb - pa = 1
	 * 
	 * We know that p>a and p>b for this to work. Then we can write:
	 * a = p+k
	 * b = p+m
	 * 
	 * where k,m is integer
	 * 
	 * we have now:
	 * (p+k)(p+m) - p(p+m) - p(p+k) = 1
	 * km = p^2 + 1
	 * m = (p^2 + 1)/k
	 * 
	 * A = p(p+k)(p+m) = p(p+k)(p+ (p^2+1)/k)
	 * dA/dk = (p^2k^2 - p^4 - p^2)/k^2
	 * So the minimum value for A when p is constant is when kMIN=sqrt(p^2+1)
	 * Then A = p(p+kMIN)(p+ (p^2+1)/kMIN) is the minimal value of A which
	 * can be found for p`>=p. If we found 150000 Alexandrian integers and
	 * the biggest of them is lower than minimal A, we can stop our procedure. 
	 */
	public static void main(String[] args) throws IOException {
		final long N = 150000;
		TreeSet<BigInteger> alexandrians = new TreeSet<BigInteger>();

		for (long p = 1L;; p++) {
			long P21 = (p * p) + 1L;
			long sqrt = (long) Math.sqrt(P21);

			BigInteger minA = BigInteger.valueOf(p);
			minA = minA.multiply(BigInteger.valueOf(p + sqrt));
			minA = minA.multiply(BigInteger.valueOf(p + (P21 / sqrt)));
			if ((alexandrians.size() == (int) N)
					&& (minA.compareTo(alexandrians.descendingIterator().next()) > 0)) {
				break;
			}

			if ((p % 1024L) == 0L) {
				System.out.println("p=" + p + ", minA=" + minA);
			}

			for (long d = sqrt; d >= 1L; d--) {
				int size = alexandrians.size();

				if ((P21 % d) == 0L) {
					BigInteger A = BigInteger.valueOf(p);
					A = A.multiply(BigInteger.valueOf(p + d));
					A = A.multiply(BigInteger.valueOf(p + (P21 / d)));

					if ((size == (int) N)
							&& (alexandrians.descendingIterator().next()
									.compareTo(A) > 0)) {
						alexandrians.remove(alexandrians.descendingIterator()
								.next());
						alexandrians.add(A);
					} else if (size < (int) N) {
						alexandrians.add(A);
					}
				}
			}
		}

		System.out.println("RESULT = "
				+ alexandrians.descendingIterator().next());
	}
}

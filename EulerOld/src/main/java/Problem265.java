

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Problem265 {
	/*
	 * 2^N binary digits can be placed in a circle so that all 
	 * the N-digit clockwise subsequences are distinct.
	 * 
	 * For N=3, two such circular arrangements are possible, ignoring rotations:
	 * 
	 * [picture]
	 * 
	 * For the first arrangement, the 3-digit subsequences, in clockwise order, are:
	 * 000, 001, 010, 101, 011, 111, 110 and 100.
	 * 
	 * Each circular arrangement can be encoded as a number by concatenating 
	 * the binary digits starting with the subsequence of all zeros as the most 
	 * significant bits and proceeding clockwise. The two arrangements for N=3 
	 * are thus represented as 23 and 29:
	 * 
	 * 00010111(2) = 23
	 * 00011101(2) = 29
	 * 
	 * Calling S(N) the sum of the unique numeric representations, 
	 * we can see that S(3) = 23 + 29 = 52.
	 * 
	 * Find S(5).
	 */

	private static final int N = 5;
	private static final int maxLength = (int) Math.pow(2.0, N);
	private static final int minCircledIndex = (maxLength - N) + 1;
	private static final Set<Long> results = new HashSet<Long>();
	private static final Set<List<Integer>> subsequences = new HashSet<List<Integer>>();

	public static void main(String[] args) {
		List<Integer> arrangement = getInitialArrangement();
		long result = getResult(arrangement, 0);

		System.out.println("RESULT = " + result);
	}

	private static List<Integer> getInitialArrangement() {
		List<Integer> arrangement = new ArrayList<Integer>(maxLength);

		for (int i = 0; i < maxLength; i++) {
			arrangement.add(0);
		}

		return arrangement;
	}

	private static long getResult(List<Integer> arrangement, int length) {
		if (length == maxLength) {
			long value = 0L;

			if (isCorrectCircledArrangement(arrangement)) {
				List<Integer> reversed = new ArrayList<Integer>(arrangement);
				Collections.reverse(reversed);

				value = getMinRotationValue(reversed);
				if (!results.contains(value)) {
					results.add(value);
					return value;
				}

				value = 0L;
			}

			return value;
		}

		long result = 0L;

		if (length >= (N - 1)) {
			for (int i = 1; i >= 0; i--) {
				arrangement.set(length, i);
				List<Integer> subsequence = getSubsequence(arrangement,
						(length - N) + 1);
				if (subsequences.contains(subsequence)) {
					continue;
				}

				subsequences.add(subsequence);
				result += getResult(arrangement, length + 1);
				subsequences.remove(subsequence);
			}
		} else {
			arrangement.set(length, 1);
			result += getResult(arrangement, length + 1);

			arrangement.set(length, 0);
			result += getResult(arrangement, length + 1);
		}

		return result;
	}

	private static long getMinRotationValue(List<Integer> reversed) {
		long min = Long.MAX_VALUE;

		for (int i = 0; i < maxLength; i++) {
			int temp = reversed.remove(0);
			reversed.add(temp);

			long value = getValue(reversed);
			if (value < min) {
				min = value;
			}
		}

		return min;
	}

	private static boolean isCorrectCircledArrangement(List<Integer> arrangement) {
		Set<List<Integer>> subsequencesCopy = new HashSet<List<Integer>>(
				subsequences);

		for (int index = minCircledIndex; index < maxLength; index++) {
			List<Integer> subsequence = getSubsequence(arrangement, index);

			if (subsequencesCopy.contains(subsequence)) {
				return false;
			}

			subsequencesCopy.add(subsequence);
		}

		return true;
	}

	private static List<Integer> getSubsequence(List<Integer> arrangement,
			int indexFrom) {
		List<Integer> subsequence = new ArrayList<Integer>(N);

		for (int i = 0; i < N; i++) {
			int index = (indexFrom + i) % maxLength;
			subsequence.add(arrangement.get(index));
		}

		return subsequence;
	}

	private static long getValue(List<Integer> arrangement) {
		long multiplier = 1L;
		long value = 0L;

		for (int i = 0; i < maxLength; i++) {
			int positionValue = arrangement.get(i);

			if (positionValue == 1) {
				value += multiplier;
			}

			multiplier *= 2L;
		}

		return value;
	}
}

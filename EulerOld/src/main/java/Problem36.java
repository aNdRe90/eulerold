
public class Problem36 
{
	/*
	 * The decimal number, 585 = 1001001001 (binary), is palindromic in both bases.

	Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.

	(Please note that the palindromic number, in either base, may not include leading zeros.)

	 */
	
	public static void main(String[] args) 
	{
		long sum = 0L;
		for(int i=1; i<1000000; i++)
			if(isPalindromic(i, 10) && isPalindromic(i, 2))
				sum += (long) i;
		
		System.out.println(sum);
	}
	
	public static boolean isPalindromic(int number, int base)
	{
		return isPalindrome(Integer.toString(number, base));
	}
	
	private static boolean isPalindrome(String s)
	{
		int halfLength = s.length()/2;
		int counter = 0;
		
		for(int i = 0; i<halfLength; i++)
		{
			if(s.charAt(i)==s.charAt(s.length()-1-i))
				counter++;
		}
		
		if(counter==halfLength)
			return true;
		else
			return false;
	}
}

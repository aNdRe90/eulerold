

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Problem172 {
	/*
	 * How many 18-digit numbers n (without leading zeros) 
	 * are there such that no digit occurs more than three times in n?
	 */

	private static Map<List<Integer>, Long> memory = new HashMap<List<Integer>, Long>();
	private static final int max = 3;
	private static final int N = 18;

	public static void main(String[] args) {
		List<Integer> valueCounters = getValueCounters();
		long result = 0L;

		for (int i = 1; i < 10; i++) {
			System.out.println("i = " + i);

			int previous = valueCounters.get(i);
			valueCounters.set(i, previous + 1);

			result += getResult(valueCounters);

			valueCounters.set(i, previous);
		}

		System.out.println("RESULT = " + result);
	}

	private static long getResult(List<Integer> valueCounters) {
		Long resultFromMemory = memory.get(valueCounters);
		if (resultFromMemory != null) {
			return resultFromMemory;
		}

		if (containsValueLargerThanMax(valueCounters)) {
			memory.put(valueCounters, 0L);
			return 0L;
		}

		int length = getSum(valueCounters);
		if (length == N) {
			memory.put(valueCounters, 1L);
			return 1L;
		}

		long result = 0L;
		for (int i = 0; i < 10; i++) {
			int previous = valueCounters.get(i);
			valueCounters.set(i, previous + 1);

			result += getResult(valueCounters);

			valueCounters.set(i, previous);
		}

		memory.put(valueCounters, result);
		return result;
	}

	private static boolean containsValueLargerThanMax(List<Integer> values) {
		for (int v : values) {
			if (v > max) {
				return true;
			}
		}

		return false;
	}

	private static int getSum(List<Integer> values) {
		int sum = 0;

		for (int v : values) {
			sum += v;
		}

		return sum;
	}

	private static List<Integer> getValueCounters() {
		List<Integer> values = new ArrayList<Integer>(10);

		for (int i = 0; i < 10; i++) {
			values.add(0);
		}

		return values;
	}
}

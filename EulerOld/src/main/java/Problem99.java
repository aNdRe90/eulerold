import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.StringTokenizer;


public class Problem99 
{
	/*
	 * Comparing two numbers written in index form like 211 and 37 is not difficult, 
	 * as any calculator would confirm that 2^11 = 2048 < 3^7 = 2187.

		However, confirming that 632382^518061 > 519432^525806 would be much more difficult, 
		as both numbers contain over three million digits.

		Using base_exp.txt (right click and 'Save Link/Target As...'), a 22K text file 
		containing one thousand lines with a base/exponent pair on each line, determine 
		which line number has the greatest numerical value.

		NOTE: The first two lines in the file represent the numbers in the example given above.
	 */

	
	/*
	 * SOLUTION:
	 * a1^b1 > a2^b2    <=>   b1*log(a1) > b2*log(a2)
	 */
	public static void main(String[] args)
	{
		readPowers();
		Collections.sort(powers, new Comparator<Power>()
				{
					@Override
					public int compare(Power arg0, Power arg1) 
					{
						double val0 = (double)arg0.exponent*Math.log(arg0.base);
						double val1 = (double)arg1.exponent*Math.log(arg1.base);
						
						if(val0>val1)
							return 1;
						else if(val0<val1)
							return -1;
						else
							return 0;
					}					
				});
		
		System.out.println(powers.getLast().line);
	}
	
	
	private static void readPowers()
	{
		BufferedReader f = null;
		try
		{
			f = new BufferedReader(new FileReader("base_exp.txt"));
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
		String line = null;
		
		int lineNumber = 1;
		do
		{
			try 
			{
				line = f.readLine();
			} 
			catch(IOException ee) { ee.printStackTrace(); }
			
			if(line==null)
				break;
			
			StringTokenizer st = new StringTokenizer(line, ",");
			powers.add(new Power(Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()), lineNumber++));
		}
		while(true);
	}
	private static LinkedList<Power> powers = new LinkedList<Power>();
}

class Power
{
	public Power(int base, int exponent, int line)
	{
		this.base = base;
		this.exponent = exponent;
		this.line = line;
	}
	
	public String toString()
	{
		return base+"^"+exponent+"\t"+line;
	}
	
	public int base;
	public int exponent;
	public int line;
}



import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;

import MyStuff.PolynomialSolver;

public class Problem131 {
	/*
	 * There are some prime values, p, for which there exists 
	 * a positive integer, n, such that the expression n^3 + n^2 x p 
	 * is a perfect cube.
	 * 
	 * For example, when p = 19, 8^3 + 8^2�19 = 12^3.
	 * 
	 * What is perhaps most surprising is that for each prime 
	 * with this property the value of n is unique, 
	 * and there are only four such primes below one-hundred.
	 * 
	 * How many primes below one million have this remarkable property?
	 */
	
	/* SOLUTION:
	 * n^3 + n^2 x p = x^3
	 * x must be >n so:
	 * 
	 * n^3 + n^2 x p = (n+k)^3  where k>0
	 * 
	 * Solving it for n we get:
	 * n^2*(3k-p) + n*(3k^2) + (k^3) = 0
	 * 
	 * delta = k^2(4kp - 3k^2)
	 * sqrtDelta = k*sqrt(k*(4p - 3k))
	 * 
	 * delta>0 if k<4/3p
	 * 
	 * n = ( -3k^2 +- k*sqrt(k*(4p - 3k)) ) / ( 6k - 2p )
	 * 
	 * We want n to be positive integer so by analyzing I found that
	 * the only way to get it is to have (guarantees n>0):
	 * 
	 * k<p/3   and n = ( -3k^2 - k*sqrt(k*(4p - 3k)) ) / ( 6k - 2p )
	 * 
	 * so n = ( 3k^2 + k*sqrt(k*(4p - 3k)) ) / ( 2p - 6k )
	 * 
	 * For every prime I check for every k=1,2,...,floor[p/3]
	 * the delta to be a square number first and then n to be an integer.
	 */	
	public static void main(String[] args) {
		final long primeBound = 1000000L;
		int result = 0;
		
		for(long p=2; p<primeBound; p++) {
			if(MyStuff.MyMath.isPrime(p)) {
				System.out.println(p);
						
				long kBound = p/3L;				
				for(long k=1; k<=kBound; k++) {
					long delta = k*(4*p - 3*k);
					
					if(MyStuff.MyMath.isSquareNumber(delta)) {
						long numerator = k*(3*k + (long)Math.sqrt(delta));
						long denominator = 2*p - 6*k;
						
						if(denominator>0L && numerator%denominator==0L) {
							result++;
						}
					}
				}
			}
		}
		System.out.println(result);
	}
}

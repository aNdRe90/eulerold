
public class Problem4 
{
	/*
	 * A palindromic number reads the same both ways. 
	 * The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 � 99.
	 * Find the largest palindrome made from the product of two 3-digit numbers.
	 */
	public static void main(String[] args) 
	{
		int maxPalindromeValue = 0;
		
		for(int i=999; i>99; i--)
			for(int j=999; j>99; j--)
			{
				int product = i*j;
				if(product>maxPalindromeValue && isPalindrome(Integer.toString(product)))
				{
					maxPalindromeValue = product;
				}				
			}
		
		System.out.println(maxPalindromeValue);
	}
	
	public static boolean isPalindrome(String s)
	{
		int halfLength = s.length()/2;
		int counter = 0;
		
		for(int i = 0; i<halfLength; i++)
		{
			if(s.charAt(i)==s.charAt(s.length()-1-i))
				counter++;
		}
		
		if(counter==halfLength)
			return true;
		else
			return false;
	}
}



import java.util.ArrayList;
import java.util.List;

import MyStuff.Prime;

public class Problem196 {
	/*
	 * Build a triangle from all positive integers in the following way:
	 * 
	 *  1				1st row
	 *  2  3
	 *  4  5  6
	 *  7  8  9 10
	 *  11 12 13 14 15
	 *  16 17 18 19 20 21
	 *  22 23 24 25 26 27 28
	 *  29 30 31 32 33 34 35 36
	 *  37 38 39 40 41 42 43 44 45
	 *  46 47 48 49 50 51 52 53 54 55
	 *  56 57 58 59 60 61 62 63 64 65 66
	 *  
	 *  . . .
	 *  
	 *  Each positive integer has up to eight neighbours in the triangle.
	 *  
	 *  A set of three primes is called a prime triplet if one of the three 
	 *  primes has the other two as neighbours in the triangle.
	 *  
	 *  For example, in the second row, the prime numbers 2 and 3 are 
	 *  elements of some prime triplet.
	 *  
	 *  If row 8 is considered, it contains two primes which are elements 
	 *  of some prime triplet, i.e. 29 and 31.
	 *  If row 9 is considered, it contains only one prime which is an 
	 *  element of some prime triplet: 37.
	 *  
	 *  Define S(n) as the sum of the primes in row n which are elements 
	 *  of any prime triplet.
	 *  Then S(8)=60 and S(9)=37.
	 *  
	 *  You are given that S(10000)=950007619.
	 *  Find  S(5678027) + S(7208785).
	 */
	public static void main(String[] args) {
		long result = S(5678027) + S(7208785);
		System.out.println("RESULT = " + result);
	}

	private static long S(int row) {
		long sum = 0L;

		for (int column = 1; column <= row; column++) {
			if ((column % 1000) == 0) {
				System.out.println(row + ", " + column);
			}

			long current = getElement(row, column);
			if (Prime.isPrime(current)
					&& isPartOfPrimeTriplet(row, column, current)) {
				sum += current;
			}
		}

		return sum;
	}

	private static boolean isPartOfPrimeTriplet(int row, int column,
			long current) {
		List<Coordinates> primesAbove = getPrimesAbove(row, column);
		List<Coordinates> primesBelow = getPrimesAbove(row + 2, column);

		if ((primesAbove.size() + primesBelow.size()) > 1) {
			return true;
		}

		for (Coordinates primeAbove : primesAbove) {
			if (hasPrimeNeighbourExcept(primeAbove.row, primeAbove.column,
					current)) {
				return true;
			}
		}

		for (Coordinates primeBelow : primesBelow) {
			if (hasPrimeNeighbourExcept(primeBelow.row, primeBelow.column,
					current)) {
				return true;
			}
		}

		return false;
	}

	private static boolean hasPrimeNeighbourExcept(int row, int column,
			long exception) {
		Long primeToBe = getElement(row - 1, column - 1);
		if ((primeToBe != null) && (primeToBe != exception)
				&& Prime.isPrime(primeToBe)) {
			return true;
		}

		primeToBe = getElement(row - 1, column);
		if ((primeToBe != null) && (primeToBe != exception)
				&& Prime.isPrime(primeToBe)) {
			return true;
		}

		primeToBe = getElement(row - 1, column + 1);
		if ((primeToBe != null) && (primeToBe != exception)
				&& Prime.isPrime(primeToBe)) {
			return true;
		}

		primeToBe = getElement(row + 1, column - 1);
		if ((primeToBe != null) && (primeToBe != exception)
				&& Prime.isPrime(primeToBe)) {
			return true;
		}

		primeToBe = getElement(row + 1, column);
		if ((primeToBe != null) && (primeToBe != exception)
				&& Prime.isPrime(primeToBe)) {
			return true;
		}

		primeToBe = getElement(row + 1, column + 1);
		if ((primeToBe != null) && (primeToBe != exception)
				&& Prime.isPrime(primeToBe)) {
			return true;
		}

		return false;
	}

	private static List<Coordinates> getPrimesAbove(int row, int column) {
		List<Coordinates> primesAbove = new ArrayList<Coordinates>(3);

		Long primeToBe = getElement(row - 1, column - 1);
		if ((primeToBe != null) && Prime.isPrime(primeToBe)) {
			primesAbove.add(new Coordinates(row - 1, column - 1));
		}

		primeToBe = getElement(row - 1, column);
		if ((primeToBe != null) && Prime.isPrime(primeToBe)) {
			primesAbove.add(new Coordinates(row - 1, column));
		}

		primeToBe = getElement(row - 1, column + 1);
		if ((primeToBe != null) && Prime.isPrime(primeToBe)) {
			primesAbove.add(new Coordinates(row - 1, column + 1));
		}

		return primesAbove;
	}

	private static Long getElement(int row, int column) {
		if ((row <= 0) || (column <= 0) || (column > row)) {
			return null;
		}

		long firstInRow = getFirstInRow(row);
		return firstInRow + (column - 1);
	}

	private static long getFirstInRow(long n) {
		return ((((n * n) + n) / 2L) - n) + 1L;
	}

	private static class Coordinates {
		private final int row;
		private final int column;

		private Coordinates(int r, int c) {
			this.row = r;
			this.column = c;
		}
	}
}

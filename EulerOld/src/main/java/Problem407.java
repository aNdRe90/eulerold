

import java.math.BigInteger;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import MyStuff.Factorizator;
import MyStuff.Prime;

public class Problem407 {
	/*
	 * If we calculate a^2 mod 6 for 0 <= a <= 5 we get: 0,1,4,3,4,1.
	 * 
	 * The largest value of a such that a^2 = a mod 6 is 4.
	 * Let's call M(n) the largest value of a < n such that a2 = a (mod n).
	 * So M(6) = 4.
	 * 
	 * Find sum(M(n)) for 1 <= n <= 10^7.
	 */

	//NOT SOLVED!!!!

	private static final long N = (long) 1e7;
	private static final long sqrtN = (long) Math.sqrt(N);
	private static final List<Long> primes = Prime.getPrimesUnder(sqrtN + 1L);
	private static final Map<Long, Set<BigInteger>> quadraticResiduesModPrimes = getQuadraticResidues(primes);
	private static final Map<Long, BigInteger> quadraticResiduesModPrimesProducts = getProducts(quadraticResiduesModPrimes);

	public static void main(String[] args) {
		long sum = 0L;
		for (long n = N; n > 1L; n--) {
			if ((n % 10000L) == 0L) {
				System.out.println(n);
			}

			//long Mbrute = bruteForce(n);
			long M = getM(n);

			//if (M != Mbrute) {
			//	System.out.println("BLADDDD!!!!");
			//}
			sum += M;
		}

		System.out.println("RESULT = " + sum);
		//		BigInteger sum = BigInteger.ZERO;
		//
		//		for (long n = 35L; n <= N; n++) {
		//			//System.out.println(quadraticResiduesModPrimes.get(1103L).size());
		//			long Mbrute = bruteForce(n);
		//			BigInteger M = M(n);
		//			if ((n % 10000L) == 0L) {
		//				System.out.println(n);
		//			}
		//
		//			sum = sum.add(M);
		//		}
		//
		//		System.out.println("RESULT = " + sum);
	}

	private static long getM(long n) {
		long max = (long) Math.sqrt(n - 1L);

		for (long y = ((2L * n) - 3L); y > 0L; y -= 2L) {
			long value = ((y * y) - 1L) / 4L;
			if ((value % n) == 0L) {
				long result = (1L + y) / 2L;
				return max > result ? max : result;
			}
		}

		return -1L;
	}

	private static Map<Long, BigInteger> getProducts(
			Map<Long, Set<BigInteger>> map) {
		Map<Long, BigInteger> products = new HashMap<Long, BigInteger>();

		for (Long key : map.keySet()) {
			BigInteger product = getProduct(map.get(key));
			products.put(key, product);
		}

		return products;
	}

	private static BigInteger getProduct(Set<BigInteger> set) {
		BigInteger product = BigInteger.ONE;

		for (BigInteger b : set) {
			product = product.multiply(b);
		}

		return product;
	}

	private static long bruteForce(long n) {
		long minValue = (long) Math.sqrt(n - 1L);
		long max = minValue;

		for (long i = minValue + 1L; i <= n; i++) {
			long current = (i * i) % n;

			if (current == i) {
				if (current > max) {
					max = current;
				}
			}
		}

		return max;
	}

	private static BigInteger M(long n) {
		if (Prime.isPrime(n)) {
			if (n > sqrtN) {
				return getMaxResult(n);
			}

			return getMax(quadraticResiduesModPrimes.get(n));
		}

		if (n == 2L) {
			return BigInteger.ONE;
		}

		if (n == 1L) {
			return BigInteger.ZERO;
		}

		Set<Long> primeFactors = Factorizator.getPrimeDivisors(n, primes);
		LinkedList<Long> primeFactorsList = new LinkedList<Long>(primeFactors);

		if (primeFactorsList.size() == 1) {
			return getMax(quadraticResiduesModPrimes.get(primeFactorsList
					.getFirst()));
		}

		long largestPrime = primeFactorsList.removeLast();
		for (long i = 0L; i < n; i += largestPrime) {

		}

		//		Iterator<Long> iter = primeFactorsList.descendingIterator();
		//		while (iter.hasNext()) {
		//			long prime = iter.next();
		//			possibilities = getMoreAccuratePossibilities(prime, possibilities);
		//		}
		//
		//		long M = getMaxFromResultsAmongPossibilities(possibilities, n);
		//		return M;

		return BigInteger.ZERO;
	}

	private static BigInteger getMax(Set<BigInteger> set) {
		BigInteger max = BigInteger.valueOf(Long.MIN_VALUE);

		for (BigInteger v : set) {
			if (v.compareTo(max) > 0) {
				max = v;
			}
		}

		return max;
	}

	private static long getMaxFromResultsAmongPossibilities(
			LinkedList<Long> possibilities, long n) {
		Iterator<Long> iter = possibilities.descendingIterator();

		while (iter.hasNext()) {
			long possibleResult = iter.next();
			if (((possibleResult * possibleResult) % n) == possibleResult) {
				return possibleResult;
			}
		}

		return -1L;
	}

	private static LinkedList<Long> getMoreAccuratePossibilities(long prime,
			LinkedList<Long> possibilities) {
		if (prime == 2L) {
			return possibilities;
		}

		Set<BigInteger> modPrimeQuadraticResidues = quadraticResiduesModPrimes
				.get(prime);
		List<Long> toRemove = new LinkedList<Long>();

		for (long possibility : possibilities) {
			long modulo = possibility % prime;

			if (!modPrimeQuadraticResidues.contains(modulo)) {
				toRemove.add(possibility);
			}
		}

		possibilities.removeAll(toRemove);
		return possibilities;
	}

	private static LinkedList<Long> getInitialPossibilities(long largestPrime,
			long n) {
		LinkedList<Long> possibilities = new LinkedList<Long>();

		for (long i = 0L; i < n; i += largestPrime) {
			possibilities.add(i);
		}

		return possibilities;
	}

	private static Map<Long, Set<BigInteger>> getQuadraticResidues(
			List<Long> primes) {
		int size = primes.size();
		Map<Long, Set<BigInteger>> quadraticResidues = new HashMap<Long, Set<BigInteger>>(
				size);

		for (int i = 0; i < size; i++) {
			long prime = primes.get(i);
			Set<BigInteger> residuesModPrime = getResiduesMod(prime);
			quadraticResidues.put(prime, residuesModPrime);
		}

		return quadraticResidues;
	}

	private static Set<BigInteger> getResiduesMod(long prime) {
		Set<BigInteger> residues = new HashSet<BigInteger>();
		//System.out.println(prime);

		//long minValue = (long) Math.sqrt(prime);
		long maxValue = prime / 2L;

		for (long i = 0L; i <= maxValue; i++) {
			BigInteger val = BigInteger.valueOf((i * i) % prime);
			residues.add(val);
		}

		return residues;
	}

	private static BigInteger getMaxResult(long prime) {
		long minValue = (long) Math.sqrt(prime);
		long max = minValue;

		long maxValue = prime / 2L;

		for (long i = minValue + 1L; i <= maxValue; i++) {
			long current = ((i * i) % prime);
			if ((current == i) && (current > max)) {
				max = current;
			}
		}

		return BigInteger.valueOf(max);
	}
}

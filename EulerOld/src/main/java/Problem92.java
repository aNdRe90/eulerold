import java.util.Iterator;
import java.util.LinkedList;


public class Problem92 
{
	/*
	 * A number chain is created by continuously adding the square of the digits in a number 
	 * to form a new number until it has been seen before.
	 * 
	 * For example,
	 * 
	 * 44 -> 32 -> 13 -> 10 -> 1 -> 1
	 * 85 -> 89 -> 145 -> 42 -> 20 -> 4 -> 16 -> 37 -> 58 -> 89
	 * 
	 * Therefore any chain that arrives at 1 or 89 will become stuck in an endless loop. 
	 * What is most amazing is that EVERY starting number will eventually arrive at 1 or 89.
	 * 
	 * How many starting numbers below ten million will arrive at 89?
	 */
	
	/* 1-7 digit number can have sum of squares of its digits equal to [1; 7*81] = [1; 567]
	 */
	public static void main(String[] args) 
	{
		final int N = 10000000;
		final int MAX = 567;
		
		LinkedList<Integer> numbersLeadingToEndIn89 = new LinkedList<Integer>();
		
		for(int i=1; i<=MAX; i++)
		{
			long previous = (long)i;
			while(true)
			{
				long next = getNextNumber(previous);
				
				if(next==89)
				{
					numbersLeadingToEndIn89.add(new Integer(i));
					break;
				}
				else if(next==1)
					break;

				previous = next;
			}
		}
		
		Iterator<Integer> iter = numbersLeadingToEndIn89.iterator();
		int[] endingIn89 = new int[numbersLeadingToEndIn89.size()];
		
		int i=0;
		while(iter.hasNext())
			endingIn89[i++] = iter.next().intValue();
		
		
		int resultSum = 0;
		for(int j=1; j<N; j++)
		{
			System.out.println(j);
			
			long next = getNextNumber(j);
			for(int k=0; k<endingIn89.length; k++)
			{
				if(next==endingIn89[k])
				{
					resultSum++;
					break;
				}
				else if(next<endingIn89[k])
					break;
			}
		}
		
		System.out.println(resultSum);
	}
	
	private static long getNextNumber(long previous)
	{
		String s = Long.toString(previous);
		long next = 0L;
		
		for(int i=0; i<s.length(); i++)
			next += ( (s.charAt(i)-0x30)*(s.charAt(i)-0x30) );
		
		return next;
	}
}

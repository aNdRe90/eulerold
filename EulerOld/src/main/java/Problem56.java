import java.math.BigInteger;


public class Problem56 
{
	/*
	 * A googol (10^100) is a massive number: one followed by one-hundred zeros; 
	 * 100^100 is almost unimaginably large: one followed by two-hundred zeros. 
	 * Despite their size, the sum of the digits in each number is only 1.
	 * 
	 * Considering natural numbers of the form, a^b, where a, b < 100, what is the maximum digital sum?
	 */
	public static void main(String[] args) 
	{
		long maxDigitSum = 0L;
		BigInteger hundred = new BigInteger("100");
		for(BigInteger i = BigInteger.ONE; i.compareTo(hundred)<0; i=i.add(BigInteger.ONE))
			for(int j=1; j<100; j++)
			{
				long currentDigitSum = getDigitSum(i.pow(j));
				if(currentDigitSum>maxDigitSum)
					maxDigitSum = currentDigitSum;
			}
		
		System.out.println(maxDigitSum);
	}
	
	public static long getDigitSum(BigInteger n)
	{
		String s = n.toString();
		long sum = 0L;
		int size = s.length();
		for(int i=0; i<size; i++)
			sum += (s.charAt(i)-0x30);
		
		return sum;
	}
}

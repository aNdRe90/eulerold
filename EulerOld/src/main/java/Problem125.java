

import java.util.LinkedList;
import java.util.List;

public class Problem125 {
	/*
	 * The palindromic number 595 is interesting because it can be written 
	 * as the sum of consecutive squares: 6^2 + 7^2 + 8^2 + 9^2 + 10^2 + 11^2 + 12^2.
	 * 
	 * There are exactly eleven palindromes below one-thousand that can be written 
	 * as consecutive square sums, and the sum of these palindromes is 4164. 
	 * Note that 1 = 0^2 + 1^2 has not been included as this problem is concerned 
	 * with the squares of positive integers.
	 * 
	 * Find the sum of all the numbers less than 10^8 that are both palindromic 
	 * and can be written as the sum of consecutive squares.
	 */
	public static void main(String[] args) {
		final int N = 100000000;
		long sum = 0L;		
		List<Integer> palindromicNumbers = getPalindromicNumbersBelow(N);

		int counter=0;
		for(int number : palindromicNumbers) {
			if( (counter++)%100==0)
				System.out.println(counter);
			
			if(canBeWrittenAsSumOfConsecutiveSquares(number))
				sum += (long) number;
		}
		
		System.out.println(sum);
	}

	private static boolean canBeWrittenAsSumOfConsecutiveSquares(int number) {
		int maxSquareBase = (int) Math.sqrt(number) - 1;
		
		for(int base=1; base<=maxSquareBase; base++) {
			if(canBeWrittenAsSumOfConsecutiveSquaresStartingFromBase(number, base))
				return true;
		}		
		
		return false;
	}

	private static boolean canBeWrittenAsSumOfConsecutiveSquaresStartingFromBase(
									int number, int base) {
		int sum = base*base;
		
		for(int currentBase=base+1; ; currentBase++) {
			sum += (currentBase*currentBase);
			if(sum>number)
				break;
			if(sum==number)
				return true;
		}
		
		return false;
	}

	private static List<Integer> getPalindromicNumbersBelow(int n) {
		List<Integer> palindromicNumbers = new LinkedList<Integer>(); 
		for(int i=1; i<n; i++)
			if(MyStuff.MyMath.isPalindrome(i))
				palindromicNumbers.add(i);
		
		return palindromicNumbers;
	}
}

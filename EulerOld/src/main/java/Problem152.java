

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import MyStuff.BigFraction;

public class Problem152 {
	/*
	 * There are several ways to write the number 1/2 as a sum of inverse 
	 * squares using distinct integers.
	 * 
	 * For instance, the numbers {2,3,4,5,7,12,15,20,28,35} can be used:
	 * 
	 * 1/2 = 1/2^2 + 1/3^2 + 1/4^2 + 1/5^2 + 1/7^2 + 1/12^2 + 1/15^2 + 1/20^2 + 1/28^2 + 1/35^2
	 * 
	 * In fact, only using integers between 2 and 45 inclusive, 
	 * there are exactly three ways to do it, the remaining two being: 
	 * {2,3,4,6,7,9,10,20,28,35,36,45} and {2,3,4,6,7,9,12,15,28,30,35,36,45}.
	 * 
	 * How many ways are there to write the number 1/2 as a sum of inverse squares 
	 * using distinct integers between 2 and 80 inclusive?
	 */

	/*
	 * NOT SOLVED !!!
	 */
	private static final int N = 45;
	private static final BigFraction fractionMin = new BigFraction(
			BigInteger.ONE, BigInteger.valueOf(N * N));
	private static final BigFraction fractionZero = new BigFraction(
			BigInteger.ZERO);
	private static List<BigFraction> fractions = getFractions(N);
	private static List<BigFraction> sumBounds = getSumBounds(fractions);

	private static Long[][] memory = new Long[N + 1][N + 1];

	//private static Map<Integer, Map<BigFraction, Long>> memory = new HashMap<Integer, Map<BigFraction, Long>>();

	//	private static final int lastIndex = N - 15;
	//	private static List<Set<BigFraction>> fractionsPossibleFromLastIndex = getFractionsPossibleFromIndex(
	//			fractions, lastIndex);

	public static void main(String[] args) {
		BigFraction expectedSum = new BigFraction(BigInteger.ONE,
				BigInteger.valueOf(2));

		long result = getResult(0, expectedSum);
		System.out.println("RESULT = " + result);
	}

	//	private static List<Set<BigFraction>> getFractionsPossibleFromIndex(
	//			List<BigFraction> fractions, int index) {
	//		List<Set<BigFraction>> possibilities = new ArrayList<Set<BigFraction>>(
	//				N - lastIndex);
	//		for (int i = 0; i < (N - lastIndex); i++) {
	//			possibilities.add(new HashSet<BigFraction>());
	//		}
	//
	//		int[] bases = getBases();
	//		VariationNumber var = new VariationNumber(bases);
	//		var.increment();
	//
	//		while (!var.isOverflow()) {
	//			int[] values = var.getValues();
	//
	//			BigFraction result = getSumOfFractions(values);
	//			int indexOfFirst1 = getIndexOfFirst1(values);
	//			possibilities.get(indexOfFirst1).add(result);
	//			var.increment();
	//		}
	//
	//		return possibilities;
	//	}

	private static int getIndexOfFirst1(int[] values) {
		for (int i = 0; i < values.length; i++) {
			if (values[i] == 1) {
				return i;
			}
		}

		return -1;
	}

	private static BigFraction getSumOfFractions(int[] values) {
		BigFraction sum = new BigFraction(BigInteger.ZERO);

		for (int i = 0; i < values.length; i++) {
			if (values[i] == 1) {
				sum = sum.add(fractions.get(i));
				sum = sum.reduce();
			}
		}

		return sum;
	}

	//	private static int[] getBases() {
	//		int[] bases = new int[N - lastIndex];
	//
	//		for (int i = 0; i < bases.length; i++) {
	//			bases[i] = 2;
	//		}
	//
	//		return bases;
	//	}

	private static long getResult(int indexStart, BigFraction expectedSum) {
		//System.out.println(minStartIndex + ", " + expectedSum);
		//		Long resultFromMemory = getFromMemory(indexStart, expectedSum);
		//		if (resultFromMemory != null) {
		//			System.out.println("GGG");
		//			return resultFromMemory;
		//		}

		if (expectedSum.compareTo(fractionZero) == 0) {
			//putToMemory(indexStart, expectedSum, 1L);
			return 1L;
		}

		if (expectedSum.compareTo(fractionMin) < 0) {
			//putToMemory(indexStart, expectedSum, 0L);
			return 0L;
		}

		int minStartIndex = getMinStartIndex(indexStart, expectedSum);
		if (minStartIndex == -1) {
			//putToMemory(indexStart, expectedSum, 0L);
			return 0L;
		}

		int maxStartIndex = getMaxStartIndex(expectedSum);
		long result = 0L;

		System.out.println(minStartIndex + ", " + maxStartIndex);
		if (memory[minStartIndex][maxStartIndex] != null) {
			return memory[minStartIndex][maxStartIndex];
		}

		for (int i = minStartIndex; i <= maxStartIndex; i++) {
			BigFraction current = fractions.get(i);
			BigFraction newSum = expectedSum.subtract(current).reduce();

			BigFraction maxPossibleSum = sumBounds.get(i + 1);
			if (newSum.compareTo(maxPossibleSum) > 0) {
				//putToMemory(indexStart, expectedSum, 0L);
				return 0L;
			}
			//			if (newSum.compareTo(fractionZero) < 0) {
			//				continue;
			//				//break;
			//			}

			result += getResult(i + 1, newSum);
		}

		memory[minStartIndex][maxStartIndex] = result;
		//putToMemory(indexStart, expectedSum, result);
		return result;
	}

	//	private static void putToMemory(int indexStart, BigFraction expectedSum,
	//			long result) {
	//		Map<BigFraction, Long> map = memory.get(indexStart);
	//		if (map == null) {
	//			map = new HashMap<BigFraction, Long>();
	//			memory.put(indexStart, map);
	//		}
	//
	//		map.put(expectedSum, result);
	//	}
	//
	//	private static Long getFromMemory(int indexStart, BigFraction expectedSum) {
	//		Map<BigFraction, Long> map = memory.get(indexStart);
	//		if (map == null) {
	//			return null;
	//		}
	//
	//		return map.get(expectedSum);
	//	}

	private static int getMinStartIndex(int indexStart, BigFraction expectedSum) {
		int size = sumBounds.size();

		for (int i = indexStart; i < size; i++) {
			BigFraction currentFraction = fractions.get(i);
			if (currentFraction.compareTo(expectedSum) < 0) {
				return i;
			}
		}

		return -1;
	}

	private static int getMaxStartIndex(BigFraction expectedSum) {
		int size = sumBounds.size();

		for (int i = 0; i < size; i++) {
			BigFraction currentSum = sumBounds.get(i);
			if (currentSum.compareTo(expectedSum) < 0) {
				return i - 1;
			}
		}

		return -1;
	}

	private static List<BigFraction> getSumBounds(List<BigFraction> fractions) {
		int size = fractions.size();
		List<BigFraction> sumBounds = new ArrayList<BigFraction>(size);

		BigFraction sum = new BigFraction(BigInteger.ZERO);
		for (int i = size - 1; i >= 0; i--) {
			sum = (sum.add(fractions.get(i))).reduce();
			sumBounds.add(new BigFraction(sum));
		}

		Collections.reverse(sumBounds);
		return sumBounds;
	}

	private static List<BigFraction> getFractions(int n) {
		List<BigFraction> fractions = new ArrayList<BigFraction>(n - 1);

		for (int i = 2; i <= n; i++) {
			BigFraction f = new BigFraction(BigInteger.ONE,
					BigInteger.valueOf(i * i));
			fractions.add(f);
		}

		return fractions;
	}
}



import MyStuff.Fraction;

public class Problem207 {
	/*
	 * For some positive integers k, 
	 * there exists an integer partition of the form   4^t = 2^t + k,	 * 
	 * where 4^t, 2^t, and k are all positive integers and t is a real number.
	 * 
	 * The first two such partitions are 4^1 = 2^1 + 2 and 
	 * 4^1.5849625... = 2^1.5849625... + 6.
	 * 
	 * Partitions where t is also an integer are called perfect.
	 * For any m >= 1 let P(m) be the proportion of such partitions 
	 * that are perfect with k <= m.
	 * 
	 * Thus P(6) = 1/2.
	 * 
	 * In the following table are listed some values of P(m)
	 * 
	 *    P(5) = 1/1
	 *    P(10) = 1/2
	 *    P(15) = 2/3
	 *    P(20) = 1/2
	 *    P(25) = 1/2
	 *    P(30) = 2/5
	 *    
	 *    ...
	 *    P(180) = 1/4
	 *    P(185) = 3/13
	 *    
	 * Find the smallest m for which P(m) < 1/12345
	 */

	/*SOLUTION
	 * t is of form  log_2(x)
	 * then 
	 * 4^t = 2^t + k   is of form   x^2 = x + k
	 * 
	 * x^2 - x - k = 0
	 * delta = 1 + 4k
	 * 
	 * x = 0.5*(1+sqrt(1+4k))
	 * if x is a power of 2   log_2(x) is integer.
	 * 
	 * That`s all:)
	 */
	private static final double log2 = Math.log(2.0);
	private static final Fraction limitRatio = new Fraction(1L, 12345L);

	public static void main(final String[] args) {
		long tRealCounter = 0L;
		long tIntegerCounter = 0L;
		long minM = -1L;

		for (long k = 1L;; k++) {
			if ((k % 1048576L) == 0L) {
				System.out.println(k);
			}

			long delta = (4L * k) + 1L;
			if (MyStuff.MyMath.isSquareNumber(delta)) {
				long sqrtDelta = (long) Math.sqrt(delta);
				if ((sqrtDelta % 2L) == 0L) {
					continue;
				} else {
					long x = (1L + sqrtDelta) / 2L;
					if (isPowerOf2(x)) {
						tIntegerCounter++;
					} else {
						tRealCounter++;
					}
				}

				Fraction currentRatio = new Fraction(tIntegerCounter,
						tRealCounter + tIntegerCounter);

				if (currentRatio.compareTo(limitRatio) < 0) {
					minM = k;
					break;
				}
			}
		}

		System.out.println("RESULT = " + minM);
	}

	private static boolean isPowerOf2(final long x) {
		double logX = Math.log(x);

		long exp = (long) (logX / log2);
		long pow = (long) Math.pow(2.0, exp);

		return x == pow;
	}
}

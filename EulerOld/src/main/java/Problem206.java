

public class Problem206 {
	/*
	 * Find the unique positive integer whose square has the form 
	 * 1_2_3_4_5_6_7_8_9_0,
	 * where each �_� is a single digit.
	 */
	public static void main(final String[] args) {
		long minN = (long) 1e9 + 30L;
		long maxN = (long) (Math.sqrt(2.0) * 1e9);

		//System.out.println(isRequiredPattern(1122334455667788900L));

		for (long n = minN; n < maxN;) {
			long n2 = n * n;
			if (isRequiredPattern(n2)) {
				System.out.println("RESULT = " + n2 + " (" + n + "^2)");
				break;
			}

			if ((n % 100L) == 30L) {
				n += 40L;
			} else { //ends with 70
				n += 60L;
			}
		}
	}

	private static boolean isRequiredPattern(final long n) {
		String s = Long.toString(n);

		return (s.charAt(0) == '1') && (s.charAt(2) == '2')
				&& (s.charAt(4) == '3') && (s.charAt(6) == '4')
				&& (s.charAt(8) == '5') && (s.charAt(10) == '6')
				&& (s.charAt(12) == '7') && (s.charAt(14) == '8')
				&& (s.charAt(16) == '9');
	}
}

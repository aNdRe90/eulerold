

public class Problem197 {
	/*
	 * Given is the function f(x) = [ 2^(30.403243784-x^2) ] x 10^-9
	 * ( [] is the floor-function),
	 * 
	 * the sequence u(n) is defined by u(0) = -1 and u(n+1) = f(u(n)).
	 * Find u(n) + u(n+1) for n = 10^12.
	 * 
	 * Give your answer with 9 digits after the decimal point.
	 */

	/*SOLUTION
	 * While debugging i saw that from a certain value
	 * of n (=518) u(n) and u(n+1) are constant and only start swaping.
	 * 
	 * So if we fall into the loop
	 * u(n) = ...,a,b,a,b,a,b,a,b,a,b   
	 * the result is a+b. 
	 */
	private static final long N = (long) 1e12;
	private static double exp = 30.403243784;

	public static void main(final String[] args) {
		double evenNResult = -1.0; //n=0 and all n even
		double oddNResult = f(evenNResult); //n=1 and all n odd

		for (long n = 2L; n <= N; n += 2L) {
			if ((n % 1000000L) == 0L) {
				System.out.println(n);
			}

			double newEvenNResult = f(oddNResult);
			double newOddNResult = f(newEvenNResult);

			if ((newEvenNResult == evenNResult)
					&& (newOddNResult == oddNResult)) {
				System.out.println("Breaking at n=" + n);
				System.out.println("RESULT = "
						+ (newEvenNResult + newOddNResult));
				break;
			} else {
				evenNResult = newEvenNResult;
				oddNResult = newOddNResult;
			}
		}
	}

	private static double f(final double current) {
		double result = Math.pow(2.0, exp - (current * current));
		result = 1e-9 * Math.floor(result);

		return result;
	}
}

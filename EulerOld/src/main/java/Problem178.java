

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Problem178 {
	/*
	 * Consider the number 45656.
	 * It can be seen that each pair of consecutive digits of 45656 
	 * has a difference of one.
	 * A number for which every pair of consecutive digits has a difference 
	 * of one is called a step number.
	 * 
	 * A pandigital number contains every decimal digit from 0 to 9 at least once.
	 * How many pandigital step numbers less than 10^40 are there? 
	 */

	private static Map<MemoryItem, Long> memory = new HashMap<MemoryItem, Long>();

	public static void main(String[] args) {
		final int N = 40;
		Set<Integer> pandigitalInfo = new HashSet<Integer>();
		long result = 0L;

		for (int length = 9; length < N; length++) {
			for (int i = 1; i <= 9; i++) {
				System.out.println("Length = " + length + ", i = " + i);
				pandigitalInfo.add(i);
				result += getResult(i, length, pandigitalInfo);
				pandigitalInfo.remove(i);
			}
		}

		System.out.println("RESULT = " + result);
	}

	private static long getResult(int currentDigit, int length,
			Set<Integer> pandigitalInfo) {
		Long resultFromMemory = getFromMemory(currentDigit, length,
				pandigitalInfo);
		if (resultFromMemory != null) {
			return resultFromMemory;
		}

		if (length == 1) {
			long result = 0L;
			if (pandigitalInfo.size() == 10) {
				if (currentDigit > 0) {
					result += 1L;
				}
				if (currentDigit < 9) {
					result += 1L;
				}
			} else {
				if ((currentDigit > 0)
						&& (pandigitalInfo.size() == 9)
						&& !pandigitalInfo.contains(new Integer(
								currentDigit - 1))) {
					result += 1L;
				}
				if ((currentDigit < 9)
						&& (pandigitalInfo.size() == 9)
						&& !pandigitalInfo.contains(new Integer(
								currentDigit + 1))) {
					result += 1L;
				}
			}

			putToMemory(currentDigit, length, pandigitalInfo, result);
			return result;
		}

		int minLength = getMinLength(pandigitalInfo, currentDigit);
		if (length < minLength) {
			putToMemory(currentDigit, length, pandigitalInfo, 0L);
			return 0L;
		}

		long result = 0L;
		if (currentDigit > 0) {
			int newDigit = currentDigit - 1;
			boolean contains = pandigitalInfo.contains(newDigit);

			if (!contains) {
				pandigitalInfo.add(newDigit);
			}

			result += getResult(newDigit, length - 1, pandigitalInfo);

			if (!contains) {
				pandigitalInfo.remove(newDigit);
			}
		}

		if (currentDigit < 9) {
			int newDigit = currentDigit + 1;
			boolean contains = pandigitalInfo.contains(newDigit);

			if (!contains) {
				pandigitalInfo.add(newDigit);
			}
			result += getResult(newDigit, length - 1, pandigitalInfo);

			if (!contains) {
				pandigitalInfo.remove(newDigit);
			}
		}

		putToMemory(currentDigit, length, pandigitalInfo, result);
		return result;
	}

	private static int getMinLength(Set<Integer> pandigitalInfo,
			int currentDigit) {
		int maxNeededDigit = getMaxNotContainedIn(pandigitalInfo);
		int minNeededDigit = getMinNotContainedIn(pandigitalInfo);

		int minLength = (maxNeededDigit != -1) && (minNeededDigit != -1) ? Math
				.max(maxNeededDigit - currentDigit, currentDigit
						- minNeededDigit) : -1;

		return minLength;
	}

	private static int getMinNotContainedIn(Set<Integer> values) {
		for (int i = 0; i <= 9; i++) {
			if (!values.contains(i)) {
				return i;
			}
		}

		return -1;
	}

	private static int getMaxNotContainedIn(Set<Integer> values) {
		for (int i = 9; i >= 0; i--) {
			if (!values.contains(i)) {
				return i;
			}
		}

		return -1;
	}

	private static Long getFromMemory(int currentDigit, int length,
			Set<Integer> pandigitalInfo) {
		MemoryItem memoryItem = new MemoryItem(currentDigit, length,
				pandigitalInfo);

		Long result = memory.get(memoryItem);
		return result;
	}

	private static void putToMemory(int currentDigit, int length,
			Set<Integer> pandigitalInfo, long result) {
		MemoryItem memoryItem = new MemoryItem(currentDigit, length,
				pandigitalInfo);
		memory.put(memoryItem, result);
	}

	private static class MemoryItem {
		private static int[] primes = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29 };
		private final int currentDigit;
		private final int length;
		private final Set<Integer> pandigitalInfo;

		@Override
		public int hashCode() {
			int result = (this.length * 12345) + (this.currentDigit * 54321);

			int i = 0;
			for (int v : this.pandigitalInfo) {
				result *= primes[i++] * v;
			}
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			MemoryItem other = (MemoryItem) obj;
			if (this.currentDigit != other.currentDigit) {
				return false;
			}
			if (this.length != other.length) {
				return false;
			}
			if (this.pandigitalInfo == null) {
				if (other.pandigitalInfo != null) {
					return false;
				}
			} else if (!this.pandigitalInfo.equals(other.pandigitalInfo)) {
				return false;
			}
			return true;
		}

		public MemoryItem(int currentDigit, int length,
				Set<Integer> pandigitalInfo) {
			super();
			this.currentDigit = currentDigit;
			this.length = length;
			this.pandigitalInfo = new HashSet<Integer>(pandigitalInfo);
		}

		@Override
		public String toString() {
			return this.pandigitalInfo + ", " + this.length + ", "
					+ this.currentDigit;
		}
	}
}

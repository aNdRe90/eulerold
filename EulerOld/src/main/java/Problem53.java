import java.math.BigInteger;


public class Problem53 
{
	/*
	 * There are exactly ten ways of selecting three from five, 12345:

		123, 124, 125, 134, 135, 145, 234, 235, 245, and 345

		In combinatorics, we use the notation, 5C3 = 10.

		In general,
		nCr = n! / r!(n-r)!	, where r <= n, n! = nx(n-1)x...x3x2x1, and 0! = 1.

		It is not until n = 23, that a value exceeds one-million: 23C10 = 1144066.
		How many, not necessarily distinct, 
		values of  nCr, for 1 <= n <= 100, are greater than one-million?	 
	*/
	public static void main(String[] args) 
	{
		BigInteger million = BigInteger.valueOf(1000000L);
		int counter = 0;
		
		for(int n=1; n<101; n++)
			for(int r=1; r<=n; r++)  //could be r<n as C(n,n)==1, always
			{
				if(MyMath.binomialCoefficient(n, r).compareTo(million)>0)
					counter++;
			}
		
		System.out.println(counter);
	}
	
	
}

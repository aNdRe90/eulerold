import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import MyStuff.MyMath;


public class Problem88new {
	/*
	 * A natural number, N, that can be written as the sum and product of a given set of at least two natural numbers, 
	 * {a1, a2, ... , ak} is called a product-sum number: N = a1 + a2 + ... + ak = a1 � a2 � ... � ak.
	 * 
	 * For example, 6 = 1 + 2 + 3 = 1 � 2 � 3.
	 * 
	 * 	For a given set of size, k, we shall call the smallest N with this property a minimal product-sum number. 
	 * The minimal product-sum numbers for sets of size, k = 2, 3, 4, 5, and 6 are as follows.
	 * 
	 * k=2: 4 = 2 � 2 = 2 + 2
	 * k=3: 6 = 1 � 2 � 3 = 1 + 2 + 3
	 * k=4: 8 = 1 � 1 � 2 � 4 = 1 + 1 + 2 + 4
	 * k=5: 8 = 1 � 1 � 2 � 2 � 2 = 1 + 1 + 2 + 2 + 2
	 * k=6: 12 = 1 � 1 � 1 � 1 � 2 � 6 = 1 + 1 + 1 + 1 + 2 + 6
	 * 
	 * Hence for 2<=k<=6, the sum of all the minimal product-sum numbers is 4+6+8+12 = 30; 
	 * note that 8 is only counted once in the sum.
	 * 
	 * In fact, as the complete set of minimal product-sum numbers for 2<=k<=12 is {4, 6, 8, 12, 15, 16}, the sum is 61.
	 * 
	 * What is the sum of all the minimal product-sum numbers for 2<=k<=12000?
	 */
	public static void main(String[] args) {	
		divisorsArray = getDivisorsArray(range+200);
		alreadyGotDivisors = new ArrayList<Long>();
		
		for(int i=4; ; i++) {
			//if(i==N)
			//{
			//	int a = 3;
			//}
			if(allKfilledUpTo(N))
				break;
			System.out.println(i);
			if(! MyStuff.MyMath.isPrime(i)) {
				//alreadyGotDivisors = new ArrayList<Long>();	
				checkNumber(i);		
			}
		}
		
		long sum = getResultSum();		
		System.out.println(sum);
	}

	private static long getResultSum() {
		HashSet<Integer> set = new LinkedHashSet<Integer>(kMinimalValues.length);
		long sum = 0L;
		for(int i=2; i<=N; i++) {
			int before = set.size();
			set.add(kMinimalValues[i]);
			if(before<set.size())
				sum += kMinimalValues[i];
		}
		
		return sum;
	}

	private static void checkNumber(int i) {
		//if(divisorsArray.get(i)==null) {
		//	alreadyGotDivisors.clear();
		//	return;
		//}
		
		ArrayList<Long> divisors = new ArrayList<Long>(divisorsArray.get(i));
		List<ArrayList<Long>> pairs = getPairsOfDivisors(divisors);
		
		markResultsFromPairs(pairs);
		//leavePairsWithLastFactorNotPrime(pairs);
		
		for(ArrayList<Long> factors : pairs) {
			int index = (int) factors.get(1).longValue();
			if(divisorsArray.get(index)!=null) {
				alreadyGotDivisors.add(factors.get(0));
				
				checkNumber(index);
			}		
			//markResultsFromPairs(pairs);
		}
		if(! alreadyGotDivisors.isEmpty())
			alreadyGotDivisors.remove(alreadyGotDivisors.size()-1);
	}

	private static void markResultsFromPairs(List<ArrayList<Long>> pairs) {
		int number = (int) (pairs.get(0).get(0)*pairs.get(0).get(1));
		number *= getProductOfNumbers(alreadyGotDivisors);
		
		for(ArrayList<Long> factors : pairs) {
			ArrayList<Long> temp = new ArrayList<Long>(alreadyGotDivisors);
			temp.addAll(factors);
			//System.out.print(temp);
			
			
			int sumOfFactors = getSumOfNumbers(factors) + getSumOfNumbers(alreadyGotDivisors);		
			int k = number - sumOfFactors + factors.size() + alreadyGotDivisors.size();
			//System.out.println(k);
			
			if(k<=N && (kMinimalValues[k]==0 || number<kMinimalValues[k]))
				kMinimalValues[k] = number;		
		}
	}

	private static int getProductOfNumbers(ArrayList<Long> numbers) {
		int product = 1;
		for(long n : numbers)
			product *= n;
		
		return product;
	}

	private static int getSumOfNumbers(ArrayList<Long> factors) {
		int sum = 0;
		for(long f : factors)
			sum += f;
		
		return sum;
	}

	

	private static List<ArrayList<Long>> getPairsOfDivisors(ArrayList<Long> divisors) {
		long number = divisors.get(0)*divisors.get(divisors.size()-1);
		List<ArrayList<Long>> toBeReturned = new ArrayList<ArrayList<Long>>();	

		//toBeReturned.add(new ArrayList<Long>(divisors));
		
		while(divisors.size()>1) {
			ArrayList<Long> pair = new ArrayList<Long>(2);
			pair.add(divisors.remove(0));
			pair.add(divisors.remove(divisors.size()-1));
			
			toBeReturned.add(pair);
		}		
		
		if(divisors.size()==1) {
			ArrayList<Long> pair = new ArrayList<Long>(2);
			long temp = divisors.get(0);
			int howMany = (int) (Math.log((double)number)/Math.log((double)temp));
			for(int i=0; i<howMany; i++)
				pair.add(temp);
			
			toBeReturned.add(pair);
		}
		
		return toBeReturned;
	}

	private static boolean allKfilledUpTo(int n) {
		for(int i=2; i<=n; i++)
			if(kMinimalValues[i]==0)
				return false;
		
		return true;
	}
	
	private static List<ArrayList<Long>> getDivisorsArray(int range) {
		List<ArrayList<Long>> divisors = new ArrayList<ArrayList<Long>>(range);
		divisors.add(null);
		divisors.add(null);
		divisors.add(null);
		divisors.add(null);
		for(long i=4; i<=range; i++) {
			if(MyStuff.MyMath.isPrime(i)) {
				divisors.add(null);
				continue;
			}
			divisors.add(getArrayOfDivisorsForNumber(i));
		}
		
		return divisors;
	}
	
	private static ArrayList<Long> getArrayOfDivisorsForNumber(Long i) {
		long[] divs = MyStuff.MyMath.getDivisors(i);
		Arrays.sort(divs);
		ArrayList<Long> innerDivisors = new ArrayList<Long>(divs.length);
		for(long divisor : divs)
			innerDivisors.add(divisor);
		
		innerDivisors.remove(0);
		int size = innerDivisors.size();
		if(size>1)
			innerDivisors.remove(innerDivisors.size()-1);
		
		return innerDivisors;
	}

	private static final int N = 12000;
	private static final int range = 40000;
	private static int[] kMinimalValues = new int[N+1];
	private static List<ArrayList<Long>> divisorsArray;
	private static ArrayList<Long> alreadyGotDivisors;
}

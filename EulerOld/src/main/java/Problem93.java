import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import MyStuff.PermutationGenerator;


public class Problem93 {
	/*
	 * By using each of the digits from the set, {1, 2, 3, 4}, exactly once, 
	 * and making use of the four arithmetic operations (+, -, *, /) and brackets/parentheses, 
	 * it is possible to form different positive integer targets.
	 * 
	 * For example,
	 * 8 = (4 * (1 + 3)) / 2
	 * 14 = 4 * (3 + 1 / 2)
	 * 19 = 4 * (2 + 3) - 1
	 * 36 = 3 * 4 * (2 + 1)
	 * 
	 * Note that concatenations of the digits, like 12 + 34, are not allowed.
	 * 
	 * Using the set, {1, 2, 3, 4}, it is possible to obtain thirty-one different target numbers 
	 * of which 36 is the maximum, and each of the numbers 1 to 28 can be obtained before 
	 * encountering the first non-expressible number.
	 * 
	 * Find the set of four distinct digits, a < b < c < d, for which the longest set 
	 * of consecutive positive integers, 1 to n, can be obtained, 
	 * giving your answer as a string: abcd.
	 */
	
	/*
	 * SOLUTION:
	 * _ _ X o _ _ X _ o _ X _ _ o X _ _
	 * where X is a digit, _ could be a bracket (start or end)
	 * and o is an operation +*-/
	 * There are 11 positions of brackets:
	 * _ _ X o _ _ X _ o _ X _ _ o X _ _     no brackets
	 * _ ( X o _ _ X ) o _ X _ _ o X _ _
	 * _ ( X o _ _ X _ o _ X ) _ o X _ _
	 * _ _ X o _ ( X _ o _ X ) _ o X _ _
	 * _ _ X o _ ( X _ o _ X _ _ o X ) _
	 * _ _ X o _ _ X _ o ( X _ _ o X ) _
	 * _ ( X o _ _ X ) o ( X _ _ o X ) _
	 * ( ( X o _ _ X ) o _ X ) _ o X _ _
	 * _ ( X o _ ( X _ o _ X ) ) o X _ _
	 * _ _ X o ( ( X _ o _ X ) _ o X ) _
	 * _ _ X o _ ( X _ o ( X _ _ o X ) )
	 */
	public static void main(String[] args) {
		//System.out.print(getValueRepresentedByExpression("3*4*(2+1)"));
		int longestSetLength = 0;	
		String result = null;
		initializeBracketsPositions();
		initializeOperations();		
		//System.out.println(getSetLengthForDigits(1,2,3,4));
		
		for(int a=1; a<10; a++)
			for(int b=a+1; b<10; b++)
				for(int c=b+1; c<10; c++)
					for(int d=c+1; d<10; d++) {
						int setLength = getSetLengthForDigits(a,b,c,d);
						if(setLength>longestSetLength) {
							longestSetLength = setLength;
							result = ""+a+b+c+d;
						}
					}
		
		System.out.println(result);
	}

	private static void initializeOperations() {
		char[] operators = { '+', '-', '*', '/' };
		
		for(int i=0; i<4; i++)
			for(int j=0; j<4; j++)
				for(int k=0; k<4; k++) {
						ArrayList<Character> operation = new ArrayList<Character>(3);
						operation.add(operators[i]);
						operation.add(operators[j]);
						operation.add(operators[k]);
						operations.add(operation);
					}			
	}

	private static void initializeBracketsPositions() {	
		brackets.add(new ArrayList<int[]>());    //no brackets
		brackets.add(getArrayListOfBracketOn(1,7));
		brackets.add(getArrayListOfBracketOn(1,11));
		brackets.add(getArrayListOfBracketOn(5,11));
		brackets.add(getArrayListOfBracketOn(5,15));
		brackets.add(getArrayListOfBracketOn(9,15));
		
		brackets.add(getArrayListOfBracketsOn(1,7, 9,15));
		brackets.add(getArrayListOfBracketsOn(0,7, 1,11));
		brackets.add(getArrayListOfBracketsOn(1,11, 5,12));
		brackets.add(getArrayListOfBracketsOn(4,11, 5,15));
		brackets.add(getArrayListOfBracketsOn(5,15, 9,16));
	}

	private static ArrayList<int[]> getArrayListOfBracketsOn(int i, int j, int k, int l) {
		ArrayList<int[]> positions = new ArrayList<int[]>(2);
		positions.add(getArrayForBracketOn(i,j));
		positions.add(getArrayForBracketOn(k,l));
		return positions;
	}

	private static ArrayList<int[]> getArrayListOfBracketOn(int i, int j) {
		ArrayList<int[]> positions = new ArrayList<int[]>(1);
		positions.add(getArrayForBracketOn(i,j));
		return positions;
	}

	private static int[] getArrayForBracketOn(int i, int j) {
		int[] temp = new int[2];
		temp[0] = i;
		temp[1] = j;
		
		return temp;
	}

	private static int getSetLengthForDigits(int a, int b, int c, int d) {
		Set<Integer> numbersObtained = new HashSet<Integer>();
		int[] digits = { a, b, c, d };
		
		
		for(ArrayList<int[]> positionsOfBrackets : brackets) {
			StringBuffer mathExpression = new StringBuffer("                 "); //17 spaces
			PermutationGenerator permGen = new PermutationGenerator(4, true);
			
			while(permGen.hasMore()) {
				int[] permutation = permGen.getNext();
				putDigits(digits[permutation[0]],digits[permutation[1]],
						  digits[permutation[2]],digits[permutation[3]], mathExpression);
				putBrackets(positionsOfBrackets, mathExpression);
				
				for(ArrayList<Character> operation : operations) {
					putOperation(operation, mathExpression);
					String finalExpression = trimSpaces(mathExpression.toString());
					double value = getValueRepresentedByExpression(finalExpression);
					if(isInteger(value))
						numbersObtained.add( (int) value);
				}
			}
		}
		
		return getFirstNotObtainedNumber(numbersObtained) - 1;
	}
	
	private static int getFirstNotObtainedNumber(Set<Integer> numbersObtained) {
		List<Integer> numbers = new LinkedList<Integer>(numbersObtained);
		Collections.sort(numbers);
		
		int previous = 0;
		for(int num : numbers) {
			if( num>0 && (num-previous)>1 )
				return previous+1;
			
			previous = num;
		}
		
		return -1;
	}

	private static boolean isInteger(double value) {
		return ( (double)Math.round(value)==value );
	}

	private static String trimSpaces(String s) {
		StringBuffer buf = new StringBuffer("");
		for(int i=0; i<s.length(); i++) {
			char currentChar = s.charAt(i);
			if(currentChar!=' ')
				buf.append(currentChar);
		}
		
		return buf.toString();
	}

	private static void putDigits(int a, int b, int c, int d, StringBuffer mathExpression) {
		//2,6,10,14 - positions for digits
		mathExpression.setCharAt(2, (char) (a+0x30));
		mathExpression.setCharAt(6, (char) (b+0x30));
		mathExpression.setCharAt(10, (char) (c+0x30));
		mathExpression.setCharAt(14, (char) (d+0x30));
	}

	private static void putOperation(ArrayList<Character> operation, StringBuffer mathExpression) {
		//3,8,13 - positions for operators
		mathExpression.setCharAt(3, operation.get(0));
		mathExpression.setCharAt(8, operation.get(1));
		mathExpression.setCharAt(13, operation.get(2));
	}

	private static void putBrackets(ArrayList<int[]> positionsOfBrackets, StringBuffer mathExpression) {
		for(int[] position : positionsOfBrackets) {
			mathExpression.setCharAt(position[0], '(');
			mathExpression.setCharAt(position[1], ')');
		}
	}

	private static double getValueRepresentedByExpression(String expression) {
		String RNP = getReversePolishNotationFromExpression(expression);
		LinkedList<Object> stack = new LinkedList<Object>();
		int length = RNP.length();
		
		for(int i=0; i<length; i++) {
			char currentChar = RNP.charAt(i);
			handleRPN(stack, currentChar);
		}
		
		return (Double) stack.removeLast();
	}

	private static void handleRPN(LinkedList<Object> stack, char currentChar) {
		if(Character.isDigit(currentChar))
			stack.add((double) (currentChar-0x30));
		if(isOperator(currentChar)) {
			double a = (Double) stack.removeLast();
			double b = (Double) stack.removeLast();
			stack.add(getValueForOperation(a,b,currentChar));
		}
	}

	private static Object getValueForOperation(double a, double b, char currentChar) {
		if(currentChar=='-')
			return b-a;
		if(currentChar=='+')
			return b+a;
		if(currentChar=='*')
			return b*a;
		if(currentChar=='/')
			return b/a;
		
		return 0.0;
	}

	private static String getReversePolishNotationFromExpression(String expression) {
		return RPN(expression);
	}

	private static String RPN(String expression) {
		//http://pl.wikipedia.org/wiki/Odwrotna_notacja_polska
		StringBuffer out = new StringBuffer("");
		LinkedList<Character> stack = new LinkedList<Character>();
		
		for(int i=0; i<expression.length(); i++) {
			char c = expression.charAt(i);
			handleCharacter(c, stack, out);
		}
		
		while(!stack.isEmpty())
			out.append(stack.removeLast());
		
		return out.toString();
	}

	private static void handleCharacter(char c, LinkedList<Character> stack, StringBuffer out) {
		if(Character.isDigit(c))
			out.append(c);
		if(c=='(')
			stack.add(c);
		if(c==')')
			handleRightBracket(')', stack, out);		
		if(isOperator(c))
			handleOperator(c, stack, out);
	}

	private static void handleOperator(char c, LinkedList<Character> stack, StringBuffer out) {
		if(stack.isEmpty()) {
			stack.add(c);
			return;
		}
		
		char fromStack = stack.getLast();
		if(getOperatorPriority(fromStack)<getOperatorPriority(c))
			stack.add(c);
		else {
			while(!stack.isEmpty()) {
				char stacked = stack.getLast();
				if(getOperatorPriority(stacked)<getOperatorPriority(c))
					break;
				else
					out.append(stack.removeLast());
			}
			stack.add(c);
		}
	}

	private static int getOperatorPriority(char c) {
		if(c=='(')
			return 0;
		if(c=='+' || c=='-' || c==')')
			return 1;
		if(c=='*' || c=='/' || c=='%')
			return 2;
		
		return -1;
	}

	private static void handleRightBracket(char c, LinkedList<Character> stack, StringBuffer out) {
		while(true) {
			char fromStack = stack.removeLast();
			if(fromStack!='(')
				out.append(fromStack);
			else
				break;
		}
	}

	private static boolean isOperator(char c) {
		return c=='+' || c=='-' || c=='*' || c=='/';
	}

	private static List<ArrayList<int[]>> brackets = new LinkedList<ArrayList<int[]>>();
	private static List<ArrayList<Character>> operations = new LinkedList<ArrayList<Character>>();
}

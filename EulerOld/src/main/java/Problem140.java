

import java.math.BigInteger;

import diophante.equations.GeneralisedPellsEquationSolver;

public class Problem140 {
	/*
	 * Consider the infinite polynomial series AG(x) = x*G(1) + x^2*G(2) + x^3*G(3) + ..., 
	 * where G(k) is the kth term of the second order recurrence relation 
	 * G(k) = G(k-1) + G(k-2), G(1) = 1 and G(2) = 4; that is, 1, 4, 5, 9, 14, 23, ... .
	 * 
	 * For this problem we shall be concerned with values of x for which AG(x) 
	 * is a positive integer.
	 * 
	 * The corresponding values of x for the first five natural numbers are shown below:
	 * 
	 * AG(x) = 1    => x = (sqrt(5)-1)/4
	 * AG(x) = 2    => x = 2/5
	 * AG(x) = 3    => x = (sqrt(22)-2)/6
	 * AG(x) = 4    => x = (sqrt(137)-5)/14
	 * AG(x) = 5    => x = 1/2
	 * 
	 * We shall call AG(x) a golden nugget if x is rational, 
	 * because they become increasingly rarer; for example, 
	 * the 20th golden nugget is 211345365.
	 * 
	 * Find the sum of the first thirty golden nuggets.
	 */
	
	/*
	 * SOLUTION:
	 * Linear recursion solving  -> pdf in /ROZNE
	 * G(k) = (15-sqrt(5))/10 * ((1+sqrt(5))/2)^k  + (15+sqrt(5))/10 * ((1-sqrt(5))/2)^k 
	 * 
	 * Solving the AG(x) = sum(i to infinity) [x^k*G(k)] similiar to problem 137 I got:
	 * AG(x) = (3x^2 + x)/(1 - x - x^2)
	 * 
	 * (3x^2 + x)/(1 - x - x^2) = k   where k is integer ... gives us:
	 * (3+k)x^2 + (1+k)x + (-k) = 0
	 * 
	 * delta = 5k^2 + 14k + 1
	 * 
	 * 
	 * 5k^2 + 14k + 1 = n^2   (n is integer) <=> 5k^2 + 14k + 1 - n^2 = 0
	 * x = (-1-k + n) / 2*(3+k)
	 * 
	 * deltaK = 176 + 20n^2
	 * 
	 * 176 + 20n^2 = m^2   (m is integer)
	 * k = (-14 + m) / 10
	 * 
	 * m^2 - 20n^2 = 176		Generalised Pell`s equation with D = 20, k = 176
	 * 
	 * We need to find such solution to this equation that:
	 * k=(-14 + m) / 10   is integer 
	 * AND x = (-1-k + n) / 2*(3+k)   is a rational number
	 */
	
	public static void main(String[] agrs) {
		GeneralisedPellsEquationSolver pell = new GeneralisedPellsEquationSolver(20L, 176L);
		int goldenNuggetsFound = 0;
		final int goldenNuggetsToFind = 30;
		BigInteger sum = BigInteger.ZERO;
		BigInteger fourteen = BigInteger.valueOf(14L);
		BigInteger two = BigInteger.valueOf(2L);
		BigInteger three = BigInteger.valueOf(3L);
		
		while(true) {
			BigInteger[] mn = pell.getNextSolution();			
			BigInteger m = mn[0];
			BigInteger n = mn[1];
			
			BigInteger[] kAndRemainder = m.subtract(fourteen).
										 divideAndRemainder(BigInteger.TEN); 
			
			if(kAndRemainder[1].compareTo(BigInteger.ZERO)==0) {
				BigInteger k = kAndRemainder[0];
				
				BigInteger numerator = n.subtract(k).subtract(BigInteger.ONE);
				BigInteger denominator = two.multiply(three.add(k));
				
				BigInteger[] xAndRemainder 
								= numerator.divideAndRemainder(denominator);
				
				if(xAndRemainder[1].compareTo(BigInteger.ZERO)!=0) { //x is rational
					goldenNuggetsFound++;
					sum = sum.add(k);
				}
			}
			
			if(goldenNuggetsFound==goldenNuggetsToFind) {
				break;
			}
		}
				
		System.out.println(sum.toString());
	}
}



import java.util.ArrayList;
import java.util.List;

import MyStuff.Prime;

public class Problem204 {
	/*
	 * A Hamming number is a positive number which has no prime factor larger than 5.
	 * So the first few Hamming numbers are 1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 15.
	 * There are 1105 Hamming numbers not exceeding 10^8.
	 * 
	 * We will call a positive number a generalised Hamming number of type n, 
	 * if it has no prime factor larger than n.
	 * 
	 * Hence the Hamming numbers are the generalised Hamming numbers of type 5.
	 * 
	 * How many generalised Hamming numbers of type 100 are there which don't exceed 10^9?
	 */

	private static List<Integer> primes = getPrimesUnder(100);

	public static void main(final String[] args) {
		final int N = (int) 1e9;
		int sum = 1; //1 already counted

		for (int n = 2; n <= N; n++) {
			if ((n % 32768) == 0) {
				System.out.println(n);
			}

			if (isGeneraliseHamingNumber100(n)) {
				sum++;
			}
		}

		System.out.println("RESULT = " + sum);
	}

	private static boolean isGeneraliseHamingNumber100(final int n) {
		int nCopy = n;

		for (int prime : primes) {
			while ((nCopy % prime) == 0) {
				nCopy /= prime;
			}
		}

		return nCopy == 1;
	}

	private static List<Integer> getPrimesUnder(final int n) {
		List<Integer> primes = new ArrayList<Integer>(25);
		primes.add(2);

		for (int i = 3; i < n; i += 2) {
			if (Prime.isPrime(i)) {
				primes.add(i);
			}
		}

		return primes;
	}
}

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;


public class Problem95 {
	/*
	 * The proper divisors of a number are all the divisors excluding the number itself. 
	 * For example, the proper divisors of 28 are 1, 2, 4, 7, and 14. 
	 * As the sum of these divisors is equal to 28, we call it a perfect number.
	 * 
	 * Interestingly the sum of the proper divisors of 220 is 284 and the sum of 
	 * the proper divisors of 284 is 220, forming a chain of two numbers. 
	 * For this reason, 220 and 284 are called an amicable pair.
	 * 
	 * Perhaps less well known are longer chains. For example, starting with 12496, 
	 * we form a chain of five numbers:
	 * 
	 * 12496 => 14288 => 15472 => 14536 => 14264 (=> 12496 => ...)
	 * 
	 * Since this chain returns to its starting point, it is called an amicable chain.
	 * 
	 * Find the smallest member of the longest amicable chain with no element exceeding one million.
	 */
	
	//UGLY BRUTE FORCE RUNNING IN ALMOST 1h
	public static void main(String[] args) {
		long longestLength = 0L;
		long longestSmallest = Long.MAX_VALUE;
		long range = 1000000L;
		
		for(long i=2; i<range; i++) {
			if(i%1000==0)
				System.out.println(i);
			if(i==5916)
			{
				int c = 21;
			}
			
			List<Long> chain = getAmicableChain(i);
			if(chain.size()>=longestLength) {
				long smallestToBe = getSmallestMemberInAmicableChain(chain);
				
				if(longestLength==chain.size()) {
					if(smallestToBe<longestSmallest)
						longestSmallest = smallestToBe;
				}
				else {
					longestSmallest = smallestToBe; 				
					longestLength = chain.size();
				}
			}
		}
		
		System.out.println(longestSmallest);
	}

	private static List<Long> getAmicableChain(long n) {
		long chainMemberValueLimit = 1000000L;
		List<Long> chain = new ArrayList<Long>(50);
		chain.add(n);
		long previousInChain = n;
		
		while(true) {
			if(chainStartingNumbers.contains(previousInChain))
				return new ArrayList<Long>(1);
			
			long nextInChain = getNextAmicableChainMember(previousInChain);
			
			if(previousInChain==1L || nextInChain>chainMemberValueLimit || chainStartingNumbers.contains(nextInChain))
				return new ArrayList<Long>(1);
			
			int index = chain.indexOf(nextInChain);
			if(index!=-1) {
				chainStartingNumbers.add(chain.get(index));
				return chain.subList(index, chain.size());
			}
			
			chain.add(nextInChain);
			previousInChain = nextInChain;
		}
	}

	private static List<Long> getInitializedSumsOfProperDivisors(int range) {
		List<Long> sums = new ArrayList<Long>((int)range);
		sums.add(0L);
		sums.add(0L);
		
		for(int i=2; i<range; i++) {
			if(i%1000==0)
				System.out.println(i);
			sums.add(getSumOfDivisors(MyStuff.MyMath.getDivisors(i)) - i);
		}
		System.out.println("FINISHED INITIALIZING");
		return sums;
	}

	private static Long getSumOfDivisors(long[] divisors) {
		long sum = 0L;
		for(long div : divisors) 
			sum += div;
		
		return sum;
	}

	private static long getSmallestMemberInAmicableChain(List<Long> chain) {
		long smallest = Long.MAX_VALUE;
		for(long n : chain)
			if(n<smallest)
				smallest = n;
		
		return smallest;
	}

	private static long getSmallestChainMemberStartingFrom(int index, List<Long> chain) {
		long smallest = Long.MAX_VALUE;
		int size = chain.size();
		
		for(int i=index; i<size; i++) {
			long current = chain.get(i);
			if(current<smallest)
				smallest = current;
		}
		
		return smallest;
	}

	private static long getSmallestChainMember(Set<Long> chain) {
		long smallest = Long.MAX_VALUE;
		for(long n : chain) 
			if(n<smallest)
				smallest = n;
		
		return smallest;
	}

	private static long getNextAmicableChainMember(long n) {
		return getSumOfDivisors(MyStuff.MyMath.getDivisors(n)) - n;
	}

	private static List<Long> sumOfProperDivisors;
	private static Set<Long> chainStartingNumbers = new HashSet<Long>();
}

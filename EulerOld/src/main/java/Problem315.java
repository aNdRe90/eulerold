

import java.util.ArrayList;
import java.util.List;

public class Problem315 {
	/*
	 * Sam and Max are asked to transform two digital clocks into two "digital root" clocks.
	A digital root clock is a digital clock that calculates digital roots step by step.

	When a clock is fed a number, it will show it and then it will start the calculation, showing all the intermediate values until it gets to the result.
	For example, if the clock is fed the number 137, it will show: "137" → "11" → "2" and then it will go black, waiting for the next number.

	Every digital number consists of some light segments: three horizontal (top, middle, bottom) and four vertical (top-left, top-right, bottom-left, bottom-right).
	Number "1" is made of vertical top-right and bottom-right, number "4" is made by middle horizontal and vertical top-left, top-right and bottom-right. Number "8" lights them all.

	The clocks consume energy only when segments are turned on/off.
	To turn on a "2" will cost 5 transitions, while a "7" will cost only 4 transitions.

	Sam and Max built two different clocks.

	Sam's clock is fed e.g. number 137: the clock shows "137", then the panel is turned off, then the next number ("11") is turned on, then the panel is turned off again and finally the last number ("2") is turned on and, after some time, off.
	For the example, with number 137, Sam's clock requires:
	"137" 	: 	(2 + 5 + 4) × 2 = 22 transitions ("137" on/off).
	"11" 	: 	(2 + 2) × 2 = 8 transitions ("11" on/off).
	"2" 	: 	(5) × 2 = 10 transitions ("2" on/off).
	For a grand total of 40 transitions.

	Max's clock works differently. Instead of turning off the whole panel, it is smart enough to turn off only those segments that won't be needed for the next number.
	For number 137, Max's clock requires:
	"137"

		:

		2 + 5 + 4 = 11 transitions ("137" on)
	7 transitions (to turn off the segments that are not needed for number "11").
	"11"


		:


		0 transitions (number "11" is already turned on correctly)
	3 transitions (to turn off the first "1" and the bottom part of the second "1";
	the top part is common with number "2").
	"2"

		:

		4 tansitions (to turn on the remaining segments in order to get a "2")
	5 transitions (to turn off number "2").
	For a grand total of 30 transitions.

	Of course, Max's clock consumes less power than Sam's one.
	The two clocks are fed all the prime numbers between A = 107 and B = 2×107.
	Find the difference between the total number of transitions needed by Sam's clock and that needed by Max's one.
	 */

	public static void main(String[] args) {
		final long fromN = (long) 1e7 + 1L;
		final long toN = (2L * (long) 1e7) - 1L;

		List<Long> primes = MyStuff.Prime.getPrimesUnder((long) Math.sqrt(toN));
		Clock sam = new SamClock(8);
		Clock max = new MaxClock(8);

		for (long n = fromN; n <= toN; n++) {
			if ((n % 100000L) == 0L) {
				System.out.println(n);
			}

			if (isPrime(n, primes)) {
				//if (Prime.isPrime(n)) {
				sam.feedNumber(n);
				max.feedNumber(n);

			}
		}

		System.out.println("RESULT = "
				+ (sam.getTransitions() - max.getTransitions()));
	}

	private static boolean isPrime(long n, List<Long> primes) {
		long sqrtN = (long) Math.sqrt(n);

		for (long prime : primes) {
			if (sqrtN < prime) {
				break;
			}

			if ((n % prime) == 0) {
				return false;
			}
		}

		return true;
	}
}

abstract class Clock {
	protected List<boolean[]> display;
	protected long transitions = 0L;
	protected final int displaySize;

	//boolean[] = {top, middle, bottom, topleft, topright, bottomleft, bottomright}

	public Clock(int displaySize) {
		display = new ArrayList<boolean[]>(displaySize);
		this.displaySize = displaySize;

		for (int i = 0; i < displaySize; i++) {
			display.add(new boolean[] { false, false, false, false, false,
					false, false });
		}
	}

	public long getTransitions() {
		return transitions;
	}

	public void feedNumber(long number) {
		long currentValue = number;

		while (true) {
			turnOnValue(getDisplayWithNumber(currentValue));

			long nextValue = getNextValue(currentValue);
			if (currentValue == nextValue) {
				turnOffDisplay();
				break;
			} else {
				turnOffValue(getDisplayWithNumber(nextValue));

				currentValue = nextValue;
			}
		}
	}

	protected void clearDisplay() {
		display.clear();

		for (int i = 0; i < displaySize; i++) {
			display.add(new boolean[] { false, false, false, false, false,
					false, false });
		}
	}

	private void turnOffDisplay() {
		for (boolean[] b : display) {
			transitions += getTransitions(b);
		}

		clearDisplay();
	}

	protected long getTransitions(boolean[] array) {
		long sum = 0L;

		for (boolean b : array) {
			if (b) {
				sum++;
			}
		}

		return sum;
	}

	protected abstract void turnOnValue(List<boolean[]> displayValue);

	protected abstract void turnOffValue(List<boolean[]> nextValue);

	private long getNextValue(long currentValue) {
		long sum = 0L;

		long n = currentValue;

		while (n > 0L) {
			sum += n % 10L;
			n /= 10L;
		}

		return sum;
	}

	private List<boolean[]> getDisplayWithNumber(long number) {
		List<boolean[]> displayWithNumber = new ArrayList<boolean[]>();

		long n = number;

		while (n > 0L) {
			int digit = (int) (n % 10L);
			displayWithNumber.add(getDisplayDigit(digit));

			n /= 10L;
		}

		return displayWithNumber;
	}

	private boolean[] getDisplayDigit(int digit) {
		boolean[] displayDigit = new boolean[7];

		switch (digit) {
		case 0:
			boolean[] b0 = { true, false, true, true, true, true, true };
			displayDigit = b0;
			break;
		case 1:
			boolean[] b1 = { false, false, false, false, true, false, true };
			displayDigit = b1;
			break;
		case 2:
			boolean[] b2 = { true, true, true, false, true, true, false };
			displayDigit = b2;
			break;
		case 3:
			boolean[] b3 = { true, true, true, false, true, false, true };
			displayDigit = b3;
			break;
		case 4:
			boolean[] b4 = { false, true, false, true, true, false, true };
			displayDigit = b4;
			break;
		case 5:
			boolean[] b5 = { true, true, true, true, false, false, true };
			displayDigit = b5;
			break;
		case 6:
			boolean[] b6 = { true, true, true, true, false, true, true };
			displayDigit = b6;
			break;
		case 7:
			boolean[] b7 = { true, false, false, true, true, false, true };
			displayDigit = b7;
			break;
		case 8:
			boolean[] b8 = { true, true, true, true, true, true, true };
			displayDigit = b8;
			break;
		case 9:
			boolean[] b9 = { true, true, true, true, true, false, true };
			displayDigit = b9;
			break;
		}

		return displayDigit;
	}
}

class SamClock extends Clock {

	public SamClock(int displaySize) {
		super(displaySize);
	}

	@Override
	protected void turnOnValue(List<boolean[]> valueToDisplay) {
		for (boolean[] b : valueToDisplay) {
			transitions += getTransitions(b);
		}

		display = valueToDisplay;
	}

	@Override
	protected void turnOffValue(List<boolean[]> nextValue) {
		for (boolean[] b : display) {
			transitions += getTransitions(b);
		}

		clearDisplay();
	}

}

class MaxClock extends Clock {

	public MaxClock(int displaySize) {
		super(displaySize);
	}

	@Override
	protected void turnOnValue(List<boolean[]> valueToDisplay) {
		int size = valueToDisplay.size();

		for (int i = 0; i < size; i++) {
			boolean[] currentDigit = display.get(i);
			boolean[] nextDigit = valueToDisplay.get(i);

			boolean[] commonPart = and(currentDigit, nextDigit);

			transitions += (getTransitions(nextDigit) - getTransitions(commonPart));
		}

		display = valueToDisplay;
	}

	private boolean[] and(boolean[] a, boolean[] b) {
		if (a.length != b.length) {
			return null;
		}

		boolean[] and = new boolean[a.length];

		for (int i = 0; i < and.length; i++) {
			and[i] = a[i] & b[i];
		}

		return and;
	}

	@Override
	protected void turnOffValue(List<boolean[]> nextValue) {
		int nextValueSize = nextValue.size();

		for (int i = 0; i < nextValueSize; i++) {
			boolean[] currentDigit = display.get(i);
			boolean[] nextDigit = nextValue.get(i);

			boolean[] commonPart = and(currentDigit, nextDigit);

			transitions += (getTransitions(currentDigit) - getTransitions(commonPart));
		}

		int currentSize = display.size();

		for (int i = nextValueSize; i < currentSize; i++) {
			transitions += getTransitions(display.get(i));
		}
	}

}

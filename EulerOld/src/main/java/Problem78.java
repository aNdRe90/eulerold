import java.math.BigInteger;
import java.util.ArrayList;


public class Problem78 
{
	/*
	 * Let p(n) represent the number of different ways in which n coins can be separated into piles. 
	 * For example, five coins can separated into piles in exactly seven different ways, so p(5)=7.

		OOOOO
		OOOO   O
		OOO   OO
		OOO   O   O
		OO   OO   O
		OO   O   O   O
		O   O   O   O   O
		
		Find the least value of n for which p(n) is divisible by one million.
	 */
	
	/*
	 * SOLUTION: http://en.wikipedia.org/wiki/Partition_%28number_theory%29
	 */
	public static void main(String[] args) 
	{
		String million = "000000";
		values[0] = BigInteger.ONE;
		values[1] = BigInteger.ONE;
		
		for(int i=2; i<=N; i++) {
			if((i%1000)==0)
				System.out.println(i);
			String s = p(i).toString();
			if(s.length()>6 && s.substring(s.length()-6).equals(million))
			{
				System.out.println("\n n="+i);
				System.out.println("p(n)="+s);
				break;
			}
		}			
	}
	
	//http://en.wikipedia.org/wiki/Pentagonal_number_theorem
	private static BigInteger p(int n)
	{		
		if(values[n]==null)
		{
			
			BigInteger sum = BigInteger.ZERO;
			for(int i=1; ;i++)
			{
				int generalisedPentagonal = getNthGeneralisedPentagonalNumber(i);
				int nextIndex = n - generalisedPentagonal;
				if(nextIndex<0)
					break;
				
				if((i-1)%4<2)
					sum = sum.add(p(nextIndex));
				else
					sum = sum.subtract(p(nextIndex));
			}
			
			values[n] = sum;
		}		
		
		return values[n];
	}
	
	private static final int N = 1000000;
	private static BigInteger[] values = new BigInteger[N+1];
	
	private static int getNthGeneralisedPentagonalNumber(int n)
	{
		int m = 0;
		int step = -1;
		for(int i=0; i<n; i++) {
			step = i+1;
			int currentStep = step;
			if(i%2!=0)
				currentStep = -currentStep;
			
			m += currentStep;
		}
			
			
		return (3*m*m - m)/ 2;
	}
}

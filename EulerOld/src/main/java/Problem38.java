
public class Problem38 
{
	/*
	 * Take the number 192 and multiply it by each of 1, 2, and 3:

    	192 � 1 = 192
    	192 � 2 = 384
    	192 � 3 = 576

		By concatenating each product we get the 1 to 9 pandigital, 192384576. 
		We will call 192384576 the concatenated product of 192 and (1,2,3)

		The same can be achieved by starting with 9 and multiplying 
		by 1, 2, 3, 4, and 5, giving the pandigital, 918273645, 
		which is the concatenated product of 9 and (1,2,3,4,5).

		What is the largest 1 to 9 pandigital 9-digit number that can be formed 
		as the concatenated product of an integer with (1,2, ... , n) where n > 1?	 */
	
	
	/*
	 * SOLUTION:
	 * 
	 * if n >1 then our integer X must be maximum 5-digits.
	 * WHY?  
	 * minimal n is 2:
	 * X * 1 = 5-digits
	 * X * 2 = (5-6)-digits    5+5=10, 5+6>10 (!)
	 * 
	 * So only values 1 <= X < 50 000 will have to be checked.
	 */
	public static void main(String[] args) 
	{
		int max = 0;
		
		for(int i = 1; i<50000; i++)
		{
			int current = getConcatenatedPandigitalProduct(i);
			if(current>max)
				max = current;
		}
		
		System.out.println(max);
	}
	
	public static int getConcatenatedPandigitalProduct(int value)
	{
		StringBuffer s = new StringBuffer(Integer.toString(value));
		int n = 2;
		
		while(true)
		{
			String toAppend = Integer.toString(value*n);
			
			if( (s.length()+toAppend.length()) < 10) 
				s.append(toAppend);
			else
				break;
			
			++n;
		}
		
		String result = s.toString();
		if(s.length()==9 && isPandigital(result))
			return Integer.parseInt(result);
		else
			return 0;
	}
	
	public static boolean isPandigital(String num)
	{
		int length = num.length();
		if(length<1 || length>9)
			return false;
		
		
		char[] chars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
		for(int i=1; i<=length; i++)
		{
			int counter = 0;
			char currentChar = chars[i];
			for(int j=0; j<length; j++)
				if(num.charAt(j)==currentChar)
					counter++;
			
			if(counter!=1)
				return false;
		}	
		return true;
	}
}

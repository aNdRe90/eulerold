

import java.util.Arrays;

public class Problem220 {
	/*
	 * Let D0 be the two-letter string "Fa". For n>=1, derive Dn from Dn-1 
	 * by the string-rewriting rules:
	 * 
	 * "a" -> "aRbFR"
	 * "b" -> "LFaLb"
	 * 
	 * Thus, D0 = "Fa", D1 = "FaRbFR", D2 = "FaRbFRRLFaLbFR", and so on.
	 * 
	 * These strings can be interpreted as instructions to a computer graphics program, 
	 * with "F" meaning "draw forward one unit", "L" meaning "turn left 90 degrees", 
	 * "R" meaning "turn right 90 degrees", and "a" and "b" being ignored. 
	 * The initial position of the computer cursor is (0,0), pointing up towards (0,1).
	 * 
	 * Then Dn is an exotic drawing known as the Heighway Dragon of order n. 
	 * For example, D10 is shown below; counting each "F" as one step, 
	 * the highlighted spot at (18,16) is the position reached after 500 steps.
	 * 
	 * What is the position of the cursor after 10^12 steps in D50 ?
	 * Give your answer in the form x,y with no spaces.
	 */

	//BRUTE FORCE:(
	public static void main(String[] args) {
		final long steps = (long) 1e12;

		long[] position = new long[] { 0L, 1L };
		long[] vectorMove = new long[] { 0L, 1L };

		long x = -1L;
		long y = -1L;

		for (long i = 1L; i < steps; i++) {
			if ((i % 100000000L) == 0L) {
				System.out.println(i);
			}

			if ((((i & -i) << 1) & i) != 0L) { //ith turn is L
				x = -vectorMove[1];
				y = vectorMove[0];
				vectorMove[0] = x;
				vectorMove[1] = y;
			} else { //ith turn is R
				x = vectorMove[1];
				y = -vectorMove[0];
				vectorMove[0] = x;
				vectorMove[1] = y;
			}

			position[0] += vectorMove[0];
			position[1] += vectorMove[1];
		}
		//		StringBuffer D10 = D(10);
		//
		//		int length = D10.length();
		//		int indexPrev = 0;
		//		int indexNext = D10.indexOf("F", indexPrev + 1);
		//
		//		while (indexNext != -1) {
		//			System.out.println(D10.substring(indexPrev, indexNext));
		//			indexPrev = indexNext;
		//			indexNext = D10.indexOf("F", indexPrev + 1);
		//		}
		//
		//		System.out.println(D10.substring(indexPrev));
		//
		//		long[] coordinates = getCoordinatesAfterSteps(D10, 500);
		//
		System.out.println(Arrays.toString(position));
	}

	private static long[] getCoordinatesAfterSteps(StringBuffer txt,
			int maxSteps) {
		long[] position = getStartingPosition();

		char[] directions = { 'N', 'E', 'S', 'W' };
		int way = 0;
		int steps = 0;
		int length = txt.length();

		for (int i = 0; i < length; i++) {
			char c = txt.charAt(i);

			if (c == 'F') {
				if (steps == maxSteps) {
					break;
				}
				position = getNewPosition(position, directions[way]);
				steps++;
				//System.out.println(steps);
			} else if (c == 'L') {
				way--;
				way = way < 0 ? directions.length + way : way;
			} else if (c == 'R') {
				way++;
				way = way >= directions.length ? way % directions.length : way;
			}
		}

		return position;
	}

	private static long[] getNewPosition(long[] position, char direction) {
		if (direction == 'N') {
			position[1]++;
		} else if (direction == 'S') {
			position[1]--;
		} else if (direction == 'E') {
			position[0]++;
		} else if (direction == 'W') {
			position[0]--;
		}

		return position;
	}

	private static long[] getStartingPosition() {
		long[] position = new long[2];
		position[0] = 0L;
		position[1] = 0L;

		return position;
	}

	private static StringBuffer D(int n) {
		StringBuffer txt = new StringBuffer("Fa");
		String aReplace = "aRbFR";
		String bReplace = "LFaLb";

		for (int i = 1; i <= n; i++) {
			for (int j = 0; j < txt.length(); j++) {
				char c = txt.charAt(j);

				if (c == 'a') {
					txt.deleteCharAt(j);
					txt.insert(j, aReplace);
					j += aReplace.length() - 1;
				}
				if (c == 'b') {
					txt.deleteCharAt(j);
					txt.insert(j, bReplace);
					j += bReplace.length() - 1;
				}
			}

			System.out.println(txt);
		}

		return txt;
	}
}

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;

//import edu.jas.arith.Roots;

import MyStuff.Cycle;
import MyStuff.MyMath;
import MyStuff.BigSquareRoot;;

public class Problem66
{
	/*
	 * Consider quadratic Diophantine equations of the form:

		x^2 � Dy^2 = 1

	For example, when D=13, the minimal solution in x is 649^2 � 13�180^2 = 1.

	It can be assumed that there are no solutions in positive integers when D is square.

	By finding minimal solutions in x for D = {2, 3, 5, 6, 7}, we obtain the following:

	3^2 � 2�2^2 = 1
	2^2 � 3�1^2 = 1
	9^2 � 5�4^2 = 1
	5^2 � 6�2^2 = 1
	8^2 � 7�3^2 = 1

	Hence, by considering minimal solutions in x for D <= 7, the largest x is obtained when D=5.

	Find the value of D <= 1000 in minimal solutions of x for which the largest value of x is obtained.

	 */
	public static void main(String[] args) 
	{
		int maxD = 1000;		
		int minD = 2;
		long before = System.currentTimeMillis();
		
		BigInteger maxMinX = BigInteger.ZERO;
		BigInteger maxMinY = BigInteger.ZERO;
		int maxMinD = 0;
		
		
		BigSquareRoot sqrtGenerator = new BigSquareRoot();
		sqrtGenerator.setScale(100);
		for(int D=minD; D<=maxD; D++)
		{
			//if(D==181)
			//	continue;
			if(!MyMath.isSquareNumber(D))
			{
				System.out.println("D = "+D);
				BigInteger currentX = getMinX(D);
				BigInteger currentY = sqrtGenerator.get(new BigDecimal(currentX)).toBigInteger();
				
				System.out.println("(x,y) = ("+currentX.toString()+", "+currentY.toString()+")\n");
				
				if(currentX.compareTo(maxMinX)>0)
				{
					maxMinX = currentX;
					maxMinY = currentY;
					maxMinD = D;
				}
			}
		}
		
		long after = System.currentTimeMillis();
		System.out.println("\nSOLUTION:\nD = "+maxMinD+"  (x,y) = ("+maxMinX.toString()+", "+maxMinY.toString()+")\n");	
		System.out.println("TIME = "+(after-before)+"ms");
	}
	
	public static BigInteger A(int k, int[] q)
	{		
		if(k>q.length)
			return null;
		
		if(k==-2)
			return BigInteger.ZERO;
		if(k==-1)
			return BigInteger.ONE;
		
		BigInteger qk = BigInteger.valueOf(q[k]);
		BigInteger b1 = null;
		BigInteger b2 = null;		
		
		if((k-1)>=0 && valuesA[k-1]==null)
			valuesA[k-1] = A(k-1, q);
		if((k-2)>=0 && valuesA[k-2]==null)
			valuesA[k-2] = A(k-2, q);
		
		if(k==0)
		{
			b1 = BigInteger.ONE;
			b2 = BigInteger.ZERO;
			if(valuesA[k]==null)
				valuesA[k] = b1.multiply(qk).add(b2);
			return valuesA[k];
		}		
		
		if(k==1)
		{
			b1 = valuesA[0];
			b2 = BigInteger.ONE;
			if(valuesA[k]==null)
				valuesA[k] = b1.multiply(qk).add(b2);
			return valuesA[k];
		}

		
		b1 = valuesA[k-1];
		b2 = valuesA[k-2];

		BigInteger result = qk.multiply(b1).add(b2);		
		return result;
	}
	
	public static BigInteger B(int k, int[] q)
	{		
		if(k>q.length)
			return null;
		
		if(k==-2)
			return BigInteger.ONE;
		if(k==-1)
			return BigInteger.ZERO;
		
		BigInteger qk = BigInteger.valueOf(q[k]);
		BigInteger b1 = null;
		BigInteger b2 = null;		
		
		if((k-1)>=0 && valuesB[k-1]==null)
			valuesB[k-1] = B(k-1, q);
		if((k-2)>=0 && valuesB[k-2]==null)
			valuesB[k-2] = B(k-2, q);
		
		if(k==0)
		{
			b1 = BigInteger.ZERO;
			b2 = BigInteger.ONE;
			if(valuesB[k]==null)
				valuesB[k] = b1.multiply(qk).add(b2);
			return valuesB[k];
		}		
		
		if(k==1)
		{
			b1 = valuesB[0];
			b2 = BigInteger.ZERO;
			if(valuesB[k]==null)
				valuesB[k] = b1.multiply(qk).add(b2);
			return valuesB[k];
		}

		
		b1 = valuesB[k-1];
		b2 = valuesB[k-2];

		BigInteger result = qk.multiply(b1).add(b2);		
		return result;
	}
	
	public static BigInteger getMinX(int D)
	{
		int[] q = getContinuedFraction(D);
		int[] cycle = Cycle.getCycle(q);
		int cycleLength = cycle.length;
		
		
		BigInteger x = null;
		if(cycleLength%2==1)
		{			
			valuesA = new BigInteger[2*cycleLength];
			valuesB = new BigInteger[2*cycleLength];
			x = A(2*cycleLength-1, q);
			//xy[1] = B(2*cycleLength-1, q);
		}
		else
		{
			valuesA = new BigInteger[cycleLength];
			valuesB = new BigInteger[cycleLength];
			x = A(cycleLength-1, q);
			//xy[1] = B(cycleLength-1, q);
		}		
		
		return x;
	}
	
	//VEEERY TRICKY BUT WORKS:)   //from problem 64
	//gets the of period of continued fraction of sqrt(n)
	public static int[] getContinuedFraction(int n)     
	{
		double sqrtN = Math.sqrt((double)n);

		int a0 = (int) Math.sqrt((double)n);
		int inNumerator = 1;		//in the numerator
		int inDenominator = -a0;  //in the denominator
		
		int[] buf = new int[howMuchToInvestigate+1];
		buf[0] = a0;
		for(int i=0; i<howMuchToInvestigate; i++)
		{
			int tempNum = inNumerator;
			int tempDen = inDenominator;
			
			inDenominator = n - inDenominator*inDenominator;
			if(inDenominator%tempNum==0)
			{
				inDenominator /= tempNum;
				tempNum = 1;
			}	
			inNumerator = tempNum*tempDen*(-1);
						
			double allValue = (sqrtN+(double) inNumerator) / (double)inDenominator;  
			buf[i+1] = (int) allValue;
			
			inNumerator -= buf[i+1]*inDenominator;
			
			int temp = inDenominator;
			inDenominator = inNumerator;
			inNumerator = temp;
		}
		
		return buf;
	}		
		
	private static int howMuchToInvestigate = 1001;   //last a(n) = a(howMuchToInvestigate)
	private static BigInteger[] valuesA = null;
	private static BigInteger[] valuesB = null;
}

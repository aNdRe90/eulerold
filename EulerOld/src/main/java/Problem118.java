

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import MyStuff.CombinationGenerator;
import MyStuff.PermutationGenerator;

public class Problem118 {
	/*
	 * Using all of the digits 1 through 9 and concatenating them freely 
	 * to form decimal integers, different sets can be formed. 
	 * Interestingly with the set {2,5,47,89,631}, all of the elements 
	 * belonging to it are prime.
	 * 
	 * How many distinct sets containing each of the digits one through nine 
	 * exactly once contain only prime elements?
	 */
	public static void main(String[] args) {
		List<Integer> digits = getAllPositiveDigits();
		fillPrimeSetsWithSingleElementSets();
		fillPrimeSets(digits, 1);		
		//saveSetsToFile("D:\\sets.txt");
		
		System.out.println(sets.size());
	}
	
	private static void saveSetsToFile(String filename) {
		FileWriter writer = null;
		
		try {
			writer = new FileWriter(filename);
			for(LinkedList<Integer> set : sets)
				writer.write(set.toString()+"\n");
			
			writer.close();
		} catch (IOException e) {e.printStackTrace();}
	}

	private static void fillPrimeSetsWithSingleElementSets() {
		PermutationGenerator permGen = new PermutationGenerator(9, false);
		while(permGen.hasMore()) {
			int[] permutation = permGen.getNext();
			int number = getNumberFromDigits(permutation);
			
			if(MyStuff.MyMath.isPrime(number)) {
				LinkedList<Integer> set = new LinkedList<Integer>();
				set.add(number);
				sets.add(set);
			}
		}
	}

	private static int getNumberFromDigits(int[] permutation) {
		StringBuffer buffer = new StringBuffer("");
		
		for(int i=0; i<permutation.length; i++)
			buffer.append(permutation[i]);
		
		return Integer.parseInt(buffer.toString());
	}

	private static void fillPrimeSets(List<Integer> digits, int elementMinLength) {
		int amountOfDigits = digits.size();
		if(amountOfDigits==0) {
			LinkedList<Integer> temp = new LinkedList<Integer>(currentSet);
			Collections.sort(temp);
			sets.add(temp);
			currentSet.removeLast();
			return;
		}
		
		int setsSizeBefore = sets.size();
		
		for(int elementLength=1; elementLength<=amountOfDigits; elementLength++) {
			CombinationGenerator combGen = new CombinationGenerator(digits.size(), elementLength);
			
			while(combGen.hasMore()) {				
				int[] combination = combGen.getNext();
				PermutationGenerator permGen = new PermutationGenerator(combination.length, true);
				
				while(permGen.hasMore()) {
					int[] permutation = permGen.getNext();
					int[] permutatedCombination = getPermutatedCombination(permutation, combination);					
					int numberFromCombination = getNumber(digits, permutatedCombination);				
					
					if(MyStuff.MyMath.isPrime(numberFromCombination)) {
						currentSet.add(numberFromCombination);
						List<Integer> newDigits = getDigitsWithoutTheseFrom(digits, numberFromCombination);
						fillPrimeSets(newDigits, 1);
					}					
				}
				
			}
		}
		
		if(sets.size()>setsSizeBefore && !currentSet.isEmpty())
			currentSet.removeLast();
		else if(sets.size()==setsSizeBefore && !currentSet.isEmpty())
			currentSet.removeLast();
	}

	private static int[] getPermutatedCombination(int[] permutation, int[] combination) {
		int[] permutatedCombination = new int[combination.length];
		
		for(int i=0; i<combination.length; i++)
			permutatedCombination[i] = combination[permutation[i]];
		
		return permutatedCombination;
	}

	private static List<Integer> getDigitsWithoutTheseFrom(List<Integer> digits, int number) {
		List<Integer> newDigits = new LinkedList<Integer>(digits);
		
		String numberString = Integer.toString(number);
		int length = numberString.length(); 
		
		for(int i=0; i<length; i++)
			newDigits.remove(Integer.valueOf(numberString.charAt(i) - 0x30));
			
		return newDigits;
	}

	private static List<Integer> getAllPositiveDigits() {
		List<Integer> digits = new ArrayList<Integer>();
		for(int i=1; i<10; i++)
			digits.add(i);
		
		return digits;
	}

	/*private static List<LinkedList<Integer>> getPrimeSets(List<Integer> digits) {
		List<LinkedList<Integer>> primeSets = new LinkedList<LinkedList<Integer>>();		
		int amountOfDigits = digits.size();
		
		for(int elementLength=1; elementLength<=amountOfDigits; elementLength++) {
			List<Integer> primesOfThisLengthAndDigits = getAllPrimes(elementLength, digits);
			primeSets.addAll(getPrimeSetsWhereFirstElementIsFrom(primesOfThisLengthAndDigits));
		}
		
		return primeSets;
	}

	private static List<LinkedList<Integer>> getPrimeSetsWhereFirstElementIsFrom(
								List<Integer> primes) {
		List<LinkedList<Integer>> primeSets = new LinkedList<LinkedList<Integer>>();
		
		for(int prime : primes) {
			List<Integer> primeSet = new LinkedList<Integer>();
			primeSet.add(prime);
			primeSet.addAll();
		}
	}

	private static List<Integer> getAllPrimes(int elementLength, List<Integer> digits) {
		List<Integer> primes = new LinkedList<Integer>();
		
		CombinationGenerator combGen = new CombinationGenerator(digits.size(), elementLength);
		while(combGen.hasMore()) {
			int[] combination = combGen.getNext();
			int numberFromCombination = getNumber(digits, combination);
			if(MyStuff.MyMath.isPrime(numberFromCombination))
				primes.add(numberFromCombination);
		}
		
		return primes;
	} */

	private static int getNumber(List<Integer> digits, int[] combination) {
		StringBuffer buffer = new StringBuffer("");
		
		for(int i=0; i<combination.length; i++)
			buffer.append(digits.get(combination[i]));
		
		return Integer.parseInt(buffer.toString());
	}
	
	private static Set<LinkedList<Integer>> sets = new HashSet<LinkedList<Integer>>();
	private static LinkedList<Integer> currentSet = new LinkedList<Integer>();
}

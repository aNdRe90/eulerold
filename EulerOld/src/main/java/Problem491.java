import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import MyStuff.CombinationGenerator;


public class Problem491 {
	/*
	 * We call a positive integer double pandigital if it uses all the digits 0 to 9 
	 * exactly twice (with no leading zero). 
	 * For example, 40561817703823564929 is one such number.
	 * 
	 * How many double pandigital numbers are divisible by 11?
	 */
	
	/*
	 * Number is divisible by 11 if the alternating sum of the digits in the number is divisible by 11.
	 * For example 2728. 2-7+2-8 = -11  is divisible by 11 so 2728 is too.
	 * 
	 * In our case double pandigital number is divisible by 11 only if its 20 digits can be split
	 * into 2 subset of equal sizes that sums differ by 0, 11, 22, 33, ...
	 * Turns out that sum of 0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9 is 90, so they can only be split into
	 * subsets of size 10 that differs by 0,22,44,66,88.
	 * 
	 * So the sizes of subset are:  45-45, 34-56, 23-67, 12-78, 1-89
	 * 
	 * We will generate all that first element starts the number (leading zero case).
	 * So subsets of size 10 with element sums 89, 78, 67, 56, 45, 34, 23, 12, 1.
	 * 
	 * For each of them we count the number of possible digit places (unique permutations). It is 10!/(2*doubles) where
	 * doubles is how many time digit appears twice in subset.
	 * 
	 * So for [0, 1, 3, 3, 5, 5, 6, 7, 7, 8] its 10!/(2*3).
	 * 
	 * Additionally we need to handle leading zero case. No matter if there is 1 or 2 zeros, the procedure is same:
	 * count possible digit places for values without one zero. e.g.
	 * 
	 * 0,0,2,2				WE TRIM number of possible digit places for [0,2,2] so 3!/2 = 3  ( {0,0,2,2}, {0,2,0,2}, {0,2,2,0} )
	 * 0,2,0,2
	 * 2,0,0,2
	 * 2,0,2,0
	 * 2,2,0,0
	 * 0,2,2,0
	 * 
	 * Finally we iterate over every subset adding to result a product of unique permutation number for subset starting the number
	 * (with leading zero case) times unique permutation number of other subset creating the number 
	 * (here we can only take 10!/(2*doubles).
	 * 
	 * It`s easy to show that number of doubles in first subset is always the same 
	 * as number of doubles in the other half of the number.
	 */	
	public static void main(String[] args) {
		assurePreconditions();
	}

	private static void assurePreconditions() {		
		Set<Subset> subsets = getAllPossibleSubsetsOnOddPositions();
		long result = 0L;
		long factorial10 = 3628800L;
		
		for(Subset subset : subsets) {
			if(!subset.isEmpty()) {
				int otherHalfDoubles = subset.doubles;
				
				long divider = (long) Math.pow(2.0, otherHalfDoubles);
				result += subset.uniquePermutations*(factorial10 / divider);
			}
		}
		
		System.out.println("RESULT = " + result);
	}

	private static Set<Subset> getAllPossibleSubsetsOnOddPositions() {
		int values[] = { 0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9 };
		
		Set<Subset> subsets = getSubsetsWithSumAndSize(89, 10, values);
		subsets.addAll(getSubsetsWithSumAndSize(78, 10, values));
		subsets.addAll(getSubsetsWithSumAndSize(67, 10, values));
		subsets.addAll(getSubsetsWithSumAndSize(56, 10, values));
		subsets.addAll(getSubsetsWithSumAndSize(45, 10, values));
		subsets.addAll(getSubsetsWithSumAndSize(34, 10, values));
		subsets.addAll(getSubsetsWithSumAndSize(23, 10, values));
		subsets.addAll(getSubsetsWithSumAndSize(12, 10, values));
		subsets.addAll(getSubsetsWithSumAndSize(1, 10, values));
		
		return subsets;
	}

	private static Set<Subset> getSubsetsWithSumAndSize(int expectedSubsetSum, int expectedSubsetSize, int[] values) {
		Set<Subset> subsets = new HashSet<Subset>();
		CombinationGenerator combGen = new CombinationGenerator(values.length, expectedSubsetSize);
		
		while(combGen.hasMore()) {
			int[] indexes = combGen.getNext();
			List<Integer> subsetValues = getSubset(indexes, values);
			int subsetSum = getSubsetSum(subsetValues);
			
			if(subsetSum == expectedSubsetSum && subsetValues.size() == expectedSubsetSize) {
				subsets.add(new Subset(subsetValues));
			}
		}
		
		System.out.println(subsets.size() + " SUBSETS FOR SUM " + expectedSubsetSum + " AND SIZE " + expectedSubsetSize + ":\n" + subsets);
		return subsets;
	}

	private static int getSubsetSum(List<Integer> subset) {
		int sum = 0;
		
		for(int i : subset) {
			sum += i;
		}
		
		return sum;
	}

	private static List<Integer> getSubset(int[] indexes, int[] values) {
		List<Integer> subset = new ArrayList<Integer>();
		
		for(int i=0; i<indexes.length; i++) {
			subset.add(values[indexes[i]]);
		}
		
		return subset;
	}
	
	private static class Subset
	{
		List<Integer> values;
		long uniquePermutations;
		int doubles;

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((values == null) ? 0 : values.hashCode());
			return result;
		}

		public boolean isEmpty() {
			return values.isEmpty();
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Subset other = (Subset) obj;
			if (values == null) {
				if (other.values != null)
					return false;
			} else if (!equalValuesInSubset(values, other.values))
				return false;
			return true;
		}

		private boolean equalValuesInSubset(List<Integer> values1,
				List<Integer> values2) {
			List<Integer> copy = new ArrayList<Integer>(values1);
			copy.removeAll(values2);
			
			return copy.size() == 0;
		}
		
		@Override
		public String toString() {
			return values.toString() + "{" + uniquePermutations + "}";
		}

		public Subset(List<Integer> values) {
			super();
			this.values = values;
			
			if(values.size() != 10) {
				throw new IllegalArgumentException();
			}
			
			uniquePermutations = getUniquePermutations(values);
			doubles = getDoubles(values);
		}

		private int getDoubles(List<Integer> values) {
			int doubles = 0;
			int previous = values.get(0);
			int size = values.size();
			
			for(int i=1; i<size; i++) {
				int next = values.get(i);
				if(previous==next) {
					doubles++;
				}
				
				previous = next;
			}
			
			return doubles;
		}

		private long getUniquePermutations(List<Integer> values) {
			if(values.size() == 0) {
				return 0L;
			}			
			
			int zeros = getNumberOfZeros(values);
			long uniquePermutations = getUniquePermutationsWithoutZerosHandling(values);
			
			if(zeros>0) {
				uniquePermutations -= getUniquePermutationsWithoutZerosHandling(values.subList(1, values.size()));
			}
			
			
			return uniquePermutations;
		}

		private long getUniquePermutationsWithoutZerosHandling(List<Integer> values) {
			long uniquePermutations = factorial(values.size());
			int previous = values.get(0);
			int size = values.size();
			
			for(int i=1; i<size; i++) {
				int next = values.get(i);
				if(previous==next) {
					uniquePermutations /= 2L;
				}
				
				previous = next;
			}
			
			return uniquePermutations;
		}

		private int getNumberOfZeros(List<Integer> values) {
			int zeros = 0;
			
			for(int i : values) {
				if(i==0) {
					zeros++;
				} else {
					break;
				}
			}
			
			return zeros;
		}
		
		private long factorial(long n) {
			long factorial = 1L;
			
			for(long i=2L; i<=n; i++) {
				factorial *= i;
			}
			
			return factorial;
		}
	}
}
//	public static void main(String[] args) {	
//		int[] digitUses = getDigitUses();
		
//		for(int a20=0; a20<10; a20++) {
//			digitUses[a20]--;
//			solveProblem(1, a20, 0, digitUses, Integer.toString(a20));
//			digitUses[a20]++;
//		}
//	}

	
//	private static void solveProblem(int length, int b, int residue, int[] digitUses, String numberB) {
//		if(length==19) {
////			String s = new StringBuffer(numberB).reverse().toString();
////			BigInteger doublePandigital = new BigInteger(s).multiply(eleven);
//			
//			if(digitUses[b] == 1 && residue == 0) {
//				result++;
//				if(result%10000==0) {
//					System.out.println(result);
//					String s = new StringBuffer(numberB).reverse().toString();
//					BigInteger doublePandigital = new BigInteger(s).multiply(eleven);
//					System.out.println("11x" + s + " = " + doublePandigital);
//				}
//			}
//			
//			return;
//		}
//		
//		
//		for(int nextB=0; nextB<10; nextB++) {
//			int sum = nextB + b + residue;
//			int nextA = sum % 10;
//			int nextResidue = sum / 10;
//			
//			if(digitUses[nextA] > 0) {
//				digitUses[nextA]--;
//				solveProblem(length + 1, nextB, nextResidue, digitUses, numberB
//						+ nextB);
//				digitUses[nextA]++;
//			}			
//		}
//	}
//
//	private static int[] getDigitUses() {
//		int[] digitUses = new int[10];
//		
//		for(int i=0; i<digitUses.length; i++) {
//			digitUses[i] = 2;
//		}
//		
//		return digitUses;
//	}

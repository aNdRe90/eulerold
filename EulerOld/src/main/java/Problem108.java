

import MyStuff.VariationNumber;

public class Problem108 {
	/*
	 * In the following equation x, y, and n are positive integers.
	 * 
	 * 1/x + 1/y = 1/n
	 * 
	 * For n = 4 there are exactly three distinct solutions:
	 * 
	 * 1/5 + 1/20 = 1/4
	 * 1/6 + 1/12 = 1/4
	 * 1/8 + 1/8  = 1/4
	 * 
	 * What is the least value of n for which the number of 
	 * distinct solutions exceeds one-thousand?
	 * 
	 * NOTE: This problem is an easier version of problem 110; 
	 * it is strongly advised that you solve this one first.
	 */
	
	/*
	 * SOLUTION:
	 * 1/x + 1/y = 1/n
	 * yn + xn = xy
	 * (n+l)n + (n+k)n = (n+l)(n+k)     //k=1,2,...,n    l=1,2,...,n
	 * n^2 + nl + n^2 + nk = n^2 + nk + nl + kl
	 * n^2 = kl
	 * 
	 * k,l  are divisors of n^2 and k*l = n^2
	 * 
	 * n^2 is a square number, so its have odd number of divisors e.g
	 * 16  = 1,2,4,8,16
	 * (k,l) = (1,16), (2,8), (4,4)   - 3 solutions   to problem  1/x + 1/y = 1/4
	 * 									(x,y) = { (4+1,4+16), (4+2, 4+8), (4+4, 4+4) }
	 * 
	 * So for n we have  numberOfDivisors(n^2)/2 + 1 solutions for problem 1/x + 1/y = 1/n.
	 * 
	 * numberOfDivisors(n^2)/2 + 1 >1000
	 * numberOfDivisors(n^2) >= 2000
	 * 
	 * (exp1+1)*(exp2+1)*...*(expi+1) = numberOfDivisors(n^2) >= 2000
	 * 
	 * expiMIN = 1, so   2^i >= 2000    =>  iMAX = 11
	 * 
	 * expMAX = ceil( 2000^(1/i) )
	 * 
	 */
	public static void main(String[] args) {		
		int minProduct = Integer.MAX_VALUE;
		int iMAX = (int) Math.ceil(Math.log(range)/Math.log(2.0));
		int[] minExponents = null;// new int[iMAX];
		int[] primes = getPrimesArray(iMAX);
		long minResultToBe = Long.MAX_VALUE;
		
		for(int i=iMAX; i>1; i--) {
			int[] bases = getBases(i);			
			VariationNumber number = new VariationNumber(bases);
			
			while(!number.isOverflow()) {
				int[] exponents = incrementByOne(number.getValues());
				
				if(everyExponentEven(exponents)) {
					long resultToBe = getValueFromPrimesAndExponents(
											primes, getExponentsDividedBy2(exponents));
					
					int product = getProductOfValues(incrementByOne(exponents));
					
					if(product>=range && resultToBe<minResultToBe) {
						minResultToBe = resultToBe;
						minExponents = exponents;
					}
				}						
				
				number.increment();
			}
		}
		
		System.out.println(minResultToBe);		
	}

	private static int[] getExponentsDividedBy2(int[] exponents) {
		int[] exp = new int[exponents.length];
		
		for(int i=0; i<exp.length; i++)
			exp[i] = exponents[i]/2;
		
		return exp;
	}

	private static boolean everyExponentEven(int[] exponents) {
		int counter = 0;
		
		for(int e : exponents)
			if(e%2==0)
				counter++;
		
		return counter==exponents.length;
	}

	private static long getValueFromPrimesAndExponents(int[] primes, int[] exponents) {
		long value = 1L;
		
		for(int i=0; i<exponents.length; i++)
			value *= (long) Math.pow(primes[i], exponents[i]);
		
		return value;
	}

	private static int getProductOfValues(int[] values) {
		int product = 1;
		for(int v : values)
			product *= v;
		
		return product;
	}

	private static int[] incrementByOne(int[] values) {
		int[] newValues = new int[values.length];
		
		for(int i=0; i<values.length; i++)
			newValues[i] = values[i]+1;
		
		return newValues;
	}

	private static int[] getPrimesArray(int howMany) {
		int[] primes = new int[howMany];
		int i=0;
		
		for(int n=2; i<howMany; n++) {
			if(MyStuff.MyMath.isPrime(n))
				primes[i++] = n;
		}
		
		return primes;
	}

	private static int[] getBases(int i) {
		int expMAX = (int) Math.ceil( Math.pow((double)range, 1.0/(double)i) );
		int[] bases = new int[i];
		for(int j=0; j<bases.length; j++)
			bases[j] = expMAX;
		
		return bases;
	}
	
	private static int range = 2000;
}

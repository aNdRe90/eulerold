import java.math.BigDecimal;
import java.math.BigInteger;
import MyStuff.BigSquareRoot;
import MyStuff.Cycle;


public class Problem100 
{
	/*
	 * If a box contains twenty-one coloured discs, composed of fifteen blue discs and six red discs, 
	 * and two discs were taken at random, it can be seen that the probability of taking two blue discs, 
	 * P(BB) = (15/21)�(14/20) = 1/2.
	 * 
	 * The next such arrangement, for which there is exactly 50% chance of taking two blue discs at random, 
	 * is a box containing eighty-five blue discs and thirty-five red discs.
	 * 
	 * By finding the first arrangement to contain over 1012 = 1,000,000,000,000 discs in total, 
	 * determine the number of blue discs that the box would contain.
	 */
	
	
	/*
	 * SOLUTION:
	 * Having total number of discs "x" and blue discs number "a" following equation must be satisfied:
	 * 
	 * a/x * (a-1)/(x-1) = 1/2    =>   2a^2 - 2a + x - x^2 = 0
	 * DeltaA = 4 - 8(x-x^2) = 8x^2 - 8x + 4 = 4(2x^2 - 2x + 1) = 4m^2   where m is an integer.
	 * Then a = (2+2m)/4 = (1+m)/2
	 * 
	 * Having furhter:
	 * m^2 = 2x^2 - 2x + 1    =>  2x^2 - 2x + 1 - m^2 = 0
	 * DeltaM = 4 - 8(1-m^2) = 8m^2 - 4 = 4(2m^2 - 1)
	 * Then x = (2 + 2*sqrt(2m^2 - 1) )/4 =  (1+sqrt(2m^2-1) )/2
	 * 
	 * Knowing that x>10^12 we have:
	 * 1 + sqrt(2m^2-1) > 2*10^12   =>   2m^2 - 1 > 4*10^24 + 1 - 4*10^12   =>  m > sqrt( 2*10^24 + 1 - 2*10^12 )
	 * =>  m > sqrt(2)*sqrt(10^24 - 10^12)   =>  m > sqrt(2)*10^6*sqrt(10^12-1)
	 * 
	 * So starting with minimal m meeting this criteria, we must find first m, where sqrt(2m^2-1) is integer.
	 * Then our result will be 0.5*(1+m)
	 * 
	 * However going further we can say that:
	 * 2m^2 - 1 = k^2  =>  k^2 - 2m^2 = -1   
	 * 
	 * And find the solution of this diophantine equation for minimum m > sqrt( 2*10^24 + 1 - 2*10^12 )
	 */
	public static void main(String[] args)
	{
		q[0] = 1;
		for(int i=1; i<q.length; i++)
			q[i] = 2;		
		
		BigDecimal minM = new BigDecimal(Math.sqrt(2));
		minM = minM.multiply(new BigDecimal("1000000.0"));
		minM = minM.multiply(new BigDecimal(Math.sqrt(1000000000000.0-1.0)));		
		BigInteger minMint = minM.toBigInteger();
		
		for(int k=1; ;k++)
		{
			BigInteger mk = solveDiophantic(k);
			if(mk.compareTo(minMint)>0)
			{
				minMint = mk;
				break;
			}
		}
		
		BigInteger a = BigInteger.ONE.add(minMint);
		a = a.divide(new BigInteger("2"));
		
		
		System.out.println(a.toString());
		
		/*BigSquareRoot sqrtGen = new BigSquareRoot();
		sqrtGen.setMaxIterations(30);
		sqrtGen.setScale(30);
		
		while(true)
		{
			System.out.println(m.toString());
			
			BigDecimal square = new BigDecimal(m.multiply(m).shiftLeft(1).subtract(BigInteger.ONE));
			BigDecimal sqrt = sqrtGen.get(square);
			if(sqrtGen.getError().equals(BigDecimal.ZERO))
			{
				BigInteger temp = sqrt.toBigInteger().shiftRight(1).shiftLeft(1);
				if(temp.equals(sqrt.toBigInteger()))
				{
					m = temp;
					break;
				}
				
			}
			else
				m = m.add(BigInteger.ONE);
		}*/
		
	}
	
	public static BigInteger solveDiophantic(int n)
	{
		//ANALOGY TO PROBLEM 66
		//sqrt(2) = [1; (2)]			
			
		BigInteger m = null;
		BigInteger k = null;
		//if(cycleLength%2==1)
		//{			
			//valuesA = new BigInteger[2*cycleLength];
		valuesB = new BigInteger[2*n*cycleLength];
		valuesA = new BigInteger[2*n*cycleLength];
		k = A((2*n-1)*cycleLength-1, q);
		m = B((2*n-1)*cycleLength-1, q);
			//xy[1] = B(2*cycleLength-1, q);
		//}
		//else
		//{
			//valuesA = new BigInteger[cycleLength];
			//valuesB = new BigInteger[cycleLength];
			//k = B(cycleLength-1, q);
				//xy[1] = B(cycleLength-1, q);
		//}		
			
		return m;
		
	}
	
	public static BigInteger A(int k, int[] q)
	{		
		if(k>q.length)
			return null;
		
		if(k==-2)
			return BigInteger.ZERO;
		if(k==-1)
			return BigInteger.ONE;
		
		BigInteger qk = BigInteger.valueOf(q[k]);
		BigInteger b1 = null;
		BigInteger b2 = null;		
		
		if((k-1)>=0 && valuesA[k-1]==null)
			valuesA[k-1] = A(k-1, q);
		if((k-2)>=0 && valuesA[k-2]==null)
			valuesA[k-2] = A(k-2, q);
		
		if(k==0)
		{
			b1 = BigInteger.ONE;
			b2 = BigInteger.ZERO;
			if(valuesA[k]==null)
				valuesA[k] = b1.multiply(qk).add(b2);
			return valuesA[k];
		}		
		
		if(k==1)
		{
			b1 = valuesA[0];
			b2 = BigInteger.ONE;
			if(valuesA[k]==null)
				valuesA[k] = b1.multiply(qk).add(b2);
			return valuesA[k];
		}

		
		b1 = valuesA[k-1];
		b2 = valuesA[k-2];

		BigInteger result = qk.multiply(b1).add(b2);		
		return result;
	}
	
	public static BigInteger B(int k, int[] q)
	{		
		if(k>q.length)
			return null;
		
		if(k==-2)
			return BigInteger.ONE;
		if(k==-1)
			return BigInteger.ZERO;
		
		BigInteger qk = BigInteger.valueOf(q[k]);
		BigInteger b1 = null;
		BigInteger b2 = null;		
		
		if((k-1)>=0 && valuesB[k-1]==null)
			valuesB[k-1] = B(k-1, q);
		if((k-2)>=0 && valuesB[k-2]==null)
			valuesB[k-2] = B(k-2, q);
		
		if(k==0)
		{
			b1 = BigInteger.ZERO;
			b2 = BigInteger.ONE;
			if(valuesB[k]==null)
				valuesB[k] = b1.multiply(qk).add(b2);
			return valuesB[k];
		}		
		
		if(k==1)
		{
			b1 = valuesB[0];
			b2 = BigInteger.ZERO;
			if(valuesB[k]==null)
				valuesB[k] = b1.multiply(qk).add(b2);
			return valuesB[k];
		}

		
		b1 = valuesB[k-1];
		b2 = valuesB[k-2];

		BigInteger result = qk.multiply(b1).add(b2);		
		return result;
	}
	
	private static BigInteger[] valuesA = null;
	private static BigInteger[] valuesB = null;
	private static final int N = 10000;
	private static int[] q = new int[N+1];
	private static final int cycleLength = 1;
}

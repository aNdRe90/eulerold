

import java.util.Arrays;

public class Problem189 {
	/*
	 * Consider the following configuration of 64 triangles:
	 * 
	 * We wish to colour the interior of each triangle with one of three colours: 
	 * red, green or blue, so that no two neighbouring triangles have the same colour. 
	 * Such a colouring shall be called valid. Here, two triangles are said to be 
	 * neighbouring if they share an edge.
	 * 
	 * Note: if they only share a vertex, then they are not neighbours.
	 * For example, here is a valid colouring of the above grid:
	 * 
	 * A colouring C' which is obtained from a colouring C by rotation or reflection is 
	 * considered distinct from C unless the two are identical.
	 * 
	 * How many distinct valid colourings are there for the above configuration?
	 */
	private static long solutions = 0L;
	private static final int ROWS = 4; //8;
	private static final int N = 16; //64;
	private static final char[][] triangle = new char[ROWS][];

	static {
		int rowLength = 1;
		for (int row = 0; row < ROWS; row++) {
			triangle[row] = new char[rowLength];
			rowLength += 2;
		}
	}

	public static void main(String[] args) {
		int n = 1;
		solve(n);

		System.out.println("RESULT = " + solutions);
	}

	//a(n) = 2n-1
	//S(n) = (1 + 2n - 1)n/2 = n^2

	private static void solve(int n) {
		if (triangle[0][0] == 'g') {
			//System.out.println("STOP! = " + solutions);
		}

		if (n > N) {
			solutions++;
			System.out.print(Arrays.toString(triangle[0]));
			System.out.print(Arrays.toString(triangle[1]));
			System.out.print(Arrays.toString(triangle[2]));
			System.out.print(Arrays.toString(triangle[3]));
			System.out.println();

			if ((solutions % 10000000L) == 0L) {
				System.out.println(solutions);
			}

			return;
		}

		int row = (int) Math.sqrt(n);
		int indexInRow = n - (row * row) - 1;

		if ((row * row) == n) {
			row--;
			indexInRow = triangle[row].length - 1;
		}

		char[] colors = getPossibleColors(row, indexInRow);
		for (char color : colors) {
			triangle[row][indexInRow] = color;
			solve(n + 1);
		}
	}

	private static char[] getPossibleColors(int row, int indexInRow) {
		if (indexInRow == 0) {
			return new char[] { 'r', 'g', 'b' };
		}

		if ((indexInRow % 2) == 0) {
			if (triangle[row][indexInRow - 1] == 'r') {
				return new char[] { 'g', 'b' };
			}
			if (triangle[row][indexInRow - 1] == 'g') {
				return new char[] { 'r', 'b' };
			}
			if (triangle[row][indexInRow - 1] == 'b') {
				return new char[] { 'r', 'g' };
			}
		} else {
			if ((triangle[row][indexInRow - 1] == 'r')
					&& (triangle[row - 1][indexInRow - 1] == 'r')) {
				return new char[] { 'g', 'b' };
			}
			if ((triangle[row][indexInRow - 1] == 'r')
					&& (triangle[row - 1][indexInRow - 1] == 'g')) {
				return new char[] { 'b' };
			}
			if ((triangle[row][indexInRow - 1] == 'r')
					&& (triangle[row - 1][indexInRow - 1] == 'b')) {
				return new char[] { 'g' };
			}

			if ((triangle[row][indexInRow - 1] == 'g')
					&& (triangle[row - 1][indexInRow - 1] == 'r')) {
				return new char[] { 'b' };
			}
			if ((triangle[row][indexInRow - 1] == 'g')
					&& (triangle[row - 1][indexInRow - 1] == 'g')) {
				return new char[] { 'r', 'b' };
			}
			if ((triangle[row][indexInRow - 1] == 'g')
					&& (triangle[row - 1][indexInRow - 1] == 'b')) {
				return new char[] { 'r' };
			}

			if ((triangle[row][indexInRow - 1] == 'b')
					&& (triangle[row - 1][indexInRow - 1] == 'r')) {
				return new char[] { 'g' };
			}
			if ((triangle[row][indexInRow - 1] == 'b')
					&& (triangle[row - 1][indexInRow - 1] == 'g')) {
				return new char[] { 'r' };
			}
			if ((triangle[row][indexInRow - 1] == 'b')
					&& (triangle[row - 1][indexInRow - 1] == 'b')) {
				return new char[] { 'r', 'g' };
			}
		}

		return null;
	}
}

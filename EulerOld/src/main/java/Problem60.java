import java.util.ArrayList;
import java.util.Arrays;

import MyStuff.*;

public class Problem60 
{
	/*
	 * The primes 3, 7, 109, and 673, are quite remarkable. 
	 * By taking any two primes and concatenating them in any order 
	 * the result will always be prime. 
	 * For example, taking 7 and 109, both 7109 and 1097 are prime. 
	 * The sum of these four primes, 792, represents the lowest sum for a set of four primes with this property.
	 * 
	 * Find the lowest sum for a set of five primes 
	 * for which any two primes concatenate to produce another prime.
	 */
	public static void main(String[] args) 
	{
		//System.out.println(areAbleToConcatenatePrimes(673,109));
		ArrayList<Integer> primes = new ArrayList<Integer>(1000);
		primes.add(new Integer(2));
		
		for(int i=3; i<10000 ; i+=2) 
		{
			if(MyMath.isPrime(i))			
				primes.add(new Integer(i));			
		}	
		
		int length = primes.size();
		long sum = 0L;
		int[] fiveIndex = new int[5];
		int[] five = new int[5];

		for(fiveIndex[0]=0; fiveIndex[0]<length; fiveIndex[0]++)
		{
			five[0] = primes.get(fiveIndex[0]);
			for(fiveIndex[1]=fiveIndex[0]+1; fiveIndex[1]<length; fiveIndex[1]++)
			{
				five[1] = primes.get(fiveIndex[1]);
				if(!areAbleToConcatenatePrimes(five[0], five[1]))
					continue;
				
				for(fiveIndex[2]=fiveIndex[1]+1; fiveIndex[2]<length; fiveIndex[2]++)
				{
					five[2] = primes.get(fiveIndex[2]);
					if(!areAbleToConcatenatePrimes(five[0], five[2]))
						continue;
					if(!areAbleToConcatenatePrimes(five[1], five[2]))
						continue;
					
					for(fiveIndex[3]=fiveIndex[2]+1; fiveIndex[3]<length; fiveIndex[3]++)
					{
						five[3] = primes.get(fiveIndex[3]);
						if(!areAbleToConcatenatePrimes(five[0], five[3]))
							continue;
						if(!areAbleToConcatenatePrimes(five[1], five[3]))
							continue;
						if(!areAbleToConcatenatePrimes(five[2], five[3]))
							continue;
						
						for(fiveIndex[4]=fiveIndex[3]+1; fiveIndex[4]<length; fiveIndex[4]++)
						{
							five[4] = primes.get(fiveIndex[4]);
							if(!areAbleToConcatenatePrimes(five[0], five[4]))
								continue;
							if(!areAbleToConcatenatePrimes(five[1], five[4]))
								continue;
							if(!areAbleToConcatenatePrimes(five[2], five[4]))
								continue;
							if(!areAbleToConcatenatePrimes(five[3], five[4]))
								continue;
							
							//FOUND
							sum = five[0] + five[1] + five[2] + five[3] + five[4];
							fiveIndex[0] = length;
							fiveIndex[1] = length;
							fiveIndex[2] = length;
							fiveIndex[3] = length;
							break;
						}
					}
				}
			}
		}
		
		System.out.println(sum);
	}
	
	public static boolean areSetOfFivePrimes(int[] primes)
	{
		CombinationGenerator comb = new CombinationGenerator(5,2);
		while(comb.hasMore())
		{
			int[] nextComb = comb.getNext();
			if(!areAbleToConcatenatePrimes(primes[nextComb[0]], primes[nextComb[1]]))
				return false;
		}
		return true;
	}
	
	public static boolean areAbleToConcatenatePrimes(int a, int b)
	{
		String first = Integer.toString(a)+Integer.toString(b);
		if(MyMath.isPrime(Long.parseLong(first)))
		{
			String second = Integer.toString(b)+Integer.toString(a);
			if(MyMath.isPrime(Long.parseLong(second)))
				return true;
			else
				return false;
		}
		else
			return false;
	}
}

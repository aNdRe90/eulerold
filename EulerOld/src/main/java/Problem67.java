import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.BitSet;
import java.util.StringTokenizer;


public class Problem67 
{
	/*
	 * By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.

						 3
						7 4
					   2 4 6
					  8 5 9 3

		That is, 3 + 7 + 4 + 9 = 23.

		Find the maximum total from top to bottom of the triangle in file triangle.txt (100 rows)
	 */
	
	/*
	 * SOLUTION:
	 *  				 3
						7 4
					   2 4 6
					  8 5 9 3
					  
					  
		Going from down to up, we create new triangle where a value represents max sum starting from it.
						 		X(0,0)
					X(1,0)     				X(1,1)
			X(2,0)		   		X(2,1)       		   X(2,2)
  X(3,0)				X(3,1)				X(3,2)				 X(3,3)
					      
		Last row i repeated: 8 5 9 3
		The one before that is created by a pattern:   X(i,j) = X(i,j) + max{ X(i+1,j), X(i+1,j+1) }
		
		This way we can compute the maximum sum very fast and the sum is equal to X(0,0).
							   23
		 					20    19
		 				 10    13    15
					   8    5     9      3
					   
		If we want to get the path we start from the top and take the grater values:
		23 -> 20 	-> 13 	-> 9
			  Left	Right 	Right
	 */
	public static void main(String[] args) 
	{		
		readTriangle();
		
		
		for(int i=0; i<rows; i++)
		{
			subTriangleMaxValues[i] = new int[i+1];
			for(int j=0; j<subTriangleMaxValues[i].length; j++)
			{
				if(i==(rows-1))
					subTriangleMaxValues[i][j] = triangle[i][j];
				else
					subTriangleMaxValues[i][j] = -1;
			}
		}
		
		for(int row=(rows-2); row>=0; row--)
		{
			for(int col=0; col<=row; col++)
			{
				int left = subTriangleMaxValues[row+1][col];
				int right = subTriangleMaxValues[row+1][col+1];
				int larger = left>right ? left : right;
				
				subTriangleMaxValues[row][col] = larger + triangle[row][col];
			}
		}
		
		System.out.println(subTriangleMaxValues[0][0]);
	}
	
	
	private static void readTriangle()
	{
		BufferedReader f = null;
		try
		{
			f = new BufferedReader(new FileReader(filename));
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
		String line = null;
		
		int row = 0;
		do
		{
			try 
			{
				line = f.readLine();
			} 
			catch(IOException ee) { ee.printStackTrace(); }
			
			if(line==null)
				break;
			
			triangle[row] = new int[row+1];
			StringTokenizer st = new StringTokenizer(line);
			
			int i = 0;
			while(st.hasMoreTokens())
			{
				triangle[row][i++] = Integer.parseInt(st.nextToken());
			}
			
			row++;		
			if(row==rows)
				break;
		}
		while(true);
	}
	
	private static final int rows = 100;
	private static int[][] triangle = new int[rows][];	
	private static int[][] subTriangleMaxValues = new int[rows][];	   //max for tree starting in [x][y]
	private static final String filename = "triangle.txt";
}


public class Problem37 
{
	/*
	 * The number 3797 has an interesting property. Being prime itself, it is possible to 
	 * continuously remove digits from left to right, and remain prime at each stage: 
	 * 3797, 797, 97, and 7. Similarly we can work from right to left: 3797, 379, 37, and 3.
	 * 
	 * Find the sum of the only eleven primes that are both truncatable 
	 * from left to right and right to left.
	 * 
	 * NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.
	 */
	
	public static void main(String[] args) 
	{		
		long sum = 0L;
		int found = 0;
		
		long i = 11;
		while(found<11)
		{
			if(isBothTruncatable(i))
			{
				sum += i;
				++found;
			}
			++i;
		}
		
		System.out.println(sum);
	}
	
	public static boolean isBothTruncatable(long number)
	{
		if(MyMath.isPrime(number))			
			return isFromLeftTruncatable(number) && isFromRightTruncatable(number);		
		else
			return false;
	}
	
	private static boolean isFromLeftTruncatable(long number)
	{
		StringBuffer s = new StringBuffer(Long.toString(number));
		int length = s.length();
		
		for(int i=0; i<(length-1); i++)
		{
			s.deleteCharAt(0);
			if(!MyMath.isPrime(Long.parseLong(s.toString())) )
				return false;
		}
		return true;
	}
	
	private static boolean isFromRightTruncatable(long number)
	{
		StringBuffer s = new StringBuffer(Long.toString(number));
		int length = s.length();
		
		for(int i=0; i<(length-1); i++)
		{
			s.deleteCharAt(s.length()-1);
			if(!MyMath.isPrime(Long.parseLong(s.toString())) )
				return false;
		}
		return true;
	}
}

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import MyStuff.Factorizator;
import MyStuff.IOUtility;
import MyStuff.Prime;

public class Problem500 {
	/*
	 * 
	 * The number of divisors of 120 is 16.
	 * In fact 120 is the smallest number having 16 divisors.
	 * 
	 * Find the smallest number with 2^500500 divisors.
	 * Give your answer modulo 500500507.
	 */
	//500500th primes is 7376507
	private static final boolean saveNumberToFile = false;
	private static final String filename = "D:/smallestWith2^to500500Divisors.txt";
	
	private static final long divisorBase = 2L;
	private static Long minPower = divisorBase-1L;
	private static final long minDivisorPowerDifference = getNextPossiblePower(minPower) - minPower;
	
	public static void main(String[] args) {
		final long divisorPower = 500500L; //divisorBase^divisorPower divisors
		final long modulo = 500500507L;
		
		long before = System.currentTimeMillis();
		long result = solveProblem(divisorPower, modulo);
		long after = System.currentTimeMillis();
		
		System.out.println("RESULT = " + result);
		System.out.println("Time = " + (after - before)*0.001 + "s");
		//bruteForceApproach(divisorPower, divisorBase);
	}

	private static long solveProblem(long divisorPower, long modulo) {
		List<Long> bases = new ArrayList<Long>();
		List<Long> powers = new ArrayList<Long>();
		bases.add(2L);
		powers.add(minPower);
		
		computeSolutionFillingTheBasesAndPowersOfTheResult(bases, powers, divisorPower);
		return getModuloFinalResult(bases, powers, modulo);
	}

	private static void computeSolutionFillingTheBasesAndPowersOfTheResult(
			List<Long> bases, List<Long> powers, long iterations) {
		for(long i=2L; i<=iterations; i++) {
			performStep(i, bases, powers);
			if(i%1000L == 0L) {
				System.out.println(i + "/" + iterations);
			}
		}
		
		System.out.println(iterations + "/" + iterations);
		
		if (saveNumberToFile) {
			String number = getFormattedNumber(bases, powers);
			//String number = getNumber(bases, powers);
			IOUtility.saveToFile("Smallest number with 2^" + iterations
					+ " divisors is " + number,
					filename);
		}
	}
	
	private static String getNumber(List<Long> bases, List<Long> powers) {
		BigInteger number = BigInteger.ONE;
				
		int size = bases.size();		
		for(int i=0; i<size; i++) {
			long power = powers.get(i);
			BigInteger base = BigInteger.valueOf(bases.get(i));

			for (int j = 0; j < power; j++) {
				number = number.multiply(base);
			}
		}
		
		return number.toString();
	}

	private static String getFormattedNumber(List<Long> bases, List<Long> powers) {
		StringBuffer buffer = new StringBuffer("");
		
		int size = bases.size();
		for(int i=0; i<size; i++) {
			buffer.append(bases.get(i));
			buffer.append('^');
			buffer.append(powers.get(i));
			buffer.append("*");
			if(i%500 == 0) {
				buffer.append('\n');
			}
		}
		
		buffer.deleteCharAt(buffer.length()-1);
		return buffer.toString();
	}

	private static void performStep(long currentDivisorPower, List<Long> bases, List<Long> powers) {
		long nextBase = Prime.getNextPrime(bases.get(bases.size()-1));
		int baseIndexToIncreaseItsPower = -1;
		long powerDeltaToAdd = -1L;
		
		int size = bases.size();
		for(int i=0; i<size; i++) {
			long currentBase = bases.get(i);
			long currentPower = powers.get(i);
			long nextPossiblePower = getNextPossiblePower(currentPower);
			long powerDelta = nextPossiblePower - currentPower;

			if(isMultiplierLowerThanNextBaseWithLowestPower(currentBase, powerDelta, nextBase)) {
				baseIndexToIncreaseItsPower = i;
				powerDeltaToAdd = powerDelta;
			} else if(powerDelta == minDivisorPowerDifference) {
				break;
			}
		}	
		
		if(baseIndexToIncreaseItsPower != -1) {
			long oldPower = powers.get(baseIndexToIncreaseItsPower);
			powers.set(baseIndexToIncreaseItsPower, oldPower + powerDeltaToAdd);
		} else {
			bases.add(nextBase);
			powers.add(minPower);
		}
	}

	private static boolean isMultiplierLowerThanNextBaseWithLowestPower(long multiplierBase,
			long multiplierPower, long nextBase) {
		double lnNextBase = Math.log(nextBase);
		double lnMultiplierBase = Math.log(multiplierBase);
		
		return ((double)multiplierPower)*lnMultiplierBase < lnNextBase;
	}

	private static long getNextPossiblePower(long currentPower) {
		return divisorBase*(currentPower + 1L) - 1L;
	}

	private static long getModuloFinalResult(List<Long> bases, List<Long> powers, long modulo) {
		System.out.println("Computing modulo ...");
		long result = 1L;
		int size = bases.size();
		
		for(int i=0; i<size; i++) {
			long power = powers.get(i);
			long base = bases.get(i);

			for (int j = 0; j < power; j++) {
				result *= base;
				result = result % modulo;
			}
		}
		
		return result;
	}

	private static void bruteForceApproach(final long divisorPower) {
		long result = getBruteForceResult(divisorPower);
		String factorized = getFactorizedNumber(result);
		System.out.println(result + ", " + factorized);
	}
	
	private static String getFactorizedNumber(long result) {
		List<Long> data = Factorizator.getPrimeFactorizationWithPowers(result);
		StringBuffer buffer = new StringBuffer("");
		
		for(int i=0; i<data.size(); i+=2) {
			buffer.append(data.get(i));
			buffer.append('^');
			buffer.append(data.get(i+1));
			buffer.append('*');
		}
		
		buffer.deleteCharAt(buffer.length()-1);
		return buffer.toString();
	}

	private static long getBruteForceResult(long divisorPower) {
		long divisorsExpected = (long) Math.pow(divisorBase, divisorPower);
		
		long n = 2L;
		while(true) {
			if(!Prime.isPrime(n)) {
				long divisors = Factorizator.getNumberOfDivisors(n);
				if(divisors==divisorsExpected) {
					break;
				}
			}
			
			n++;
			if(n%10000L == 0L) {
				System.out.println("Current = " + n);
			}
		}
		
		return n;
	}	
}
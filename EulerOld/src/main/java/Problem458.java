
public class Problem458 {
	/*
	 * Consider the alphabet A made out of the letters of the word "project": A={c,e,j,o,p,r,t}.
	 * Let T(n) be the number of strings of length n consisting of letters from A that 
	 * do not have a substring that is one of the 5040 permutations of "project".
	 * 
	 * T(7)=77-7!=818503.
	 * Find T(1012). Give the last 9 digits of your answer.
	 */
	public static void main(String[] args) {
		String alphabet = "cejoprt";
		
//		long result = MyStuff.MyMath.binaryExpMod(7L, 1000000000000L, 9L);
//		System.out.println(result);
	}

	private static long factorial(long n) {
		long factorial = 1L;
		
		for(long i=2L; i<=n; i++) {
			factorial *= i;
		}
		
		return factorial;
	}
}

import java.util.HashSet;
import java.util.Iterator;


public class Problem32
{
	/*
	 * We shall say that an n-digit number is pandigital if it makes use of all the digits 
	 * 1 to n exactly once; for example, the 5-digit number, 15234, is 1 through 5 pandigital.
	 * 
	 * The product 7254 is unusual, as the identity, 39 � 186 = 7254, 
	 * containing multiplicand, multiplier, and product is 1 through 9 pandigital.
	 * 
	 * Find the sum of all products whose multiplicand/multiplier/product identity 
	 * can be written as a 1 through 9 pandigital.
	 * HINT: Some products can be obtained in more than one way so be sure to only 
	 * include it once in your sum.
	 */
	
	
	/*
	 * SOLUTION:
	 * max{n,m}-digit number  <= n-digit number * m-digit number  <= 2*max{n,m}-digit number
	 * 
	 * Therefore:    n+m+max{n,m} <10
	 * 
	 * (n,m) = (1,4), (1,3), (1,2), (1,1), (2,3), (2,2), (3,3) 
	 */
	public static void main(String[] args) 
	{
		HashSet<Integer> products = new HashSet<Integer>();
		
		for(int n=1; n<10; n++)
			for(int m=1; m<10000; m++)
			{
				int product = n*m;
				String temp = Integer.toString(n)+Integer.toString(m)+Integer.toString(product);
				if(temp.length()==9 && isPandigital(temp))
				{
					products.add(Integer.valueOf(product));
				}
			}
		
		for(int n=10; n<1000; n++)
			for(int m=100; m<1000; m++)
			{
				int product = n*m;
				String temp = Integer.toString(n)+Integer.toString(m)+Integer.toString(product);
				if(temp.length()==9 && isPandigital(temp))
				{
					products.add(Integer.valueOf(product));
				}
			}
		
		long sum = 0L;
		Iterator<Integer> iter = products.iterator();
		while(iter.hasNext())
			sum += (long) iter.next().intValue();
		
		System.out.println(sum);
	}
	
	public static boolean isPandigital(String num)
	{
		int length = num.length();
		if(length<1 || length>9)
			return false;
		
		
		char[] chars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
		for(int i=1; i<=length; i++)
		{
			int counter = 0;
			char currentChar = chars[i];
			for(int j=0; j<length; j++)
				if(num.charAt(j)==currentChar)
					counter++;
			
			if(counter!=1)
				return false;
		}	
		return true;
	}
}



import java.math.BigInteger;

public class Problem168 {
	/*
	 * Consider the number 142857. 
	 * We can right-rotate this number by moving the last digit (7) 
	 * to the front of it, giving us 714285.
	 * 
	 * It can be verified that 714285=5x142857.
	 * This demonstrates an unusual property of 142857: 
	 * it is a divisor of its right-rotation.
	 * 
	 * Find the last 5 digits of the sum of all integers n, 
	 * 10 < n < 10^100, that have this property.
	 */
	private static final int N = 100;
	private static BigInteger[] tenPowers = getTenPowers(N);

	public static void main(String[] args) {
		BigInteger sum = BigInteger.ZERO;
		BigInteger modulo = BigInteger.valueOf(100000L);

		for (int length = 2; length <= N; length++) {
			System.out.println(length);

			for (int x = 1; x < 10; x++) {
				for (int first = 1; first < 10; first++) {
					int[] number = new int[length];
					number[0] = first;

					int value = 0;
					int carry = 0;

					for (int position = 1; position < length; position++) {
						value = (number[position - 1] * x) + carry;
						number[position] = value % 10;
						carry = value / 10;
					}

					if ((number[length - 1] != 0)
							&& (((number[length - 1] * x) + carry) == number[0])) {
						sum = sum.add(getValue(number));
						sum = sum.mod(modulo);
					}
				}
			}
		}

		System.out.println("RESULT = " + sum);
	}

	private static BigInteger[] getTenPowers(int n) {
		BigInteger[] powers = new BigInteger[n];

		for (int i = 0; i < n; i++) {
			powers[i] = BigInteger.TEN.pow(i);
		}

		return powers;
	}

	private static BigInteger getValue(int[] number) {
		BigInteger value = BigInteger.ZERO;

		for (int i = 0; i < number.length; i++) {
			BigInteger addend = BigInteger.valueOf(number[i]);
			addend = addend.multiply(tenPowers[i]);
			value = value.add(addend);
		}

		return value;
	}
}

package numbermind;

public class NumberMindHint implements Comparable<NumberMindHint> {
	private final StringBuffer sequence;
	private final int correctCounter;

	public NumberMindHint(StringBuffer sequence, int correctCounter) {
		super();
		this.sequence = sequence;
		this.correctCounter = correctCounter;
	}

	public StringBuffer getSequence() {
		return this.sequence;
	}

	public int getCorrectCounter() {
		return this.correctCounter;
	}

	@Override
	public int compareTo(NumberMindHint other) {
		return this.correctCounter - other.correctCounter;
	}

	@Override
	public String toString() {
		return this.sequence.toString() + " [" + this.correctCounter + "]";
	}
}

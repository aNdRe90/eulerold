package numbermind;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import MyStuff.CombinationGenerator;

public class NumberMindSolver {
	private static NumberMindHint[] hints;
	private static int resultLength;
	private static StringBuffer result;
	private static int xxx = 0;
	private static List<List<int[]>> allCombinations;

	public static StringBuffer solveBetter(NumberMindHint[] instance) {
		if ((instance == null) || (instance.length == 0)) {
			throw new IllegalArgumentException(
					"No instance given! (null or empty hints)");
		}

		//		if (!hintsOfSameLength(instance)) {
		//			throw new IllegalArgumentException(
		//					"Incorrect instance - sequences of different length found!");
		//		}

		int sequenceLength = instance[0].getSequence().length();
		hints = instance;
		resultLength = sequenceLength;
		result = null;
		Arrays.sort(instance);

		int maxCorrectCounter = instance[instance.length - 1]
				.getCorrectCounter();
		int minCorrectCounter = instance[0].getCorrectCounter();
		if (minCorrectCounter == 0) {
			minCorrectCounter = 1;
		}

		allCombinations = getAllCombinations(sequenceLength, instance,
				minCorrectCounter, maxCorrectCounter);

		for (int i = 0; i < hints.length; i++) {
			System.out.println(i);
			int correctCounter = hints[i].getCorrectCounter();
			if (correctCounter == 0) {
				continue;
			}

			StringBuffer buf = new StringBuffer(sequenceLength);
			for (int k = 0; k < sequenceLength; k++) {
				buf.append('x');
			}

			List<int[]> combinations = allCombinations.get(correctCounter
					- minCorrectCounter);

			for (int[] combination : combinations) {
				for (int pos : combination) {
					buf.setCharAt(pos, hints[i].getSequence().charAt(pos));
				}

				computeBetter(i + 1, buf, minCorrectCounter);
			}
		}

		return result;
	}

	private static void computeBetter(int n, StringBuffer resultSequence,
			int minCorrectCounter) {
		if (result != null) {
			return;
		}

		if ((hints.length == n) && hasOnlyDigits(resultSequence)) {
			if (checkResult(hints, resultSequence)) {
				result = resultSequence;
				return;
			}
		}

		for (int i = n; i < hints.length; i++) {
			int correctCounter = hints[i].getCorrectCounter();
			List<int[]> combinations = allCombinations.get(correctCounter
					- minCorrectCounter);

			for (int[] combination : combinations) {
				boolean skip = false;
				StringBuffer newResultSequence = new StringBuffer(
						resultSequence);

				for (int pos : combination) {
					char c = resultSequence.charAt(pos);
					char c2 = hints[i].getSequence().charAt(pos);

					if ((c != 'x') && (c != c2)) {
						skip = true;
						break;
					}

					newResultSequence.setCharAt(pos, c2);
				}

				if (skip) {
					continue;
				}

				if (incorrectPartialResult(newResultSequence)) {
					continue;
				}

				computeBetter(i + 1, newResultSequence, minCorrectCounter);
			}
		}
	}

	private static boolean incorrectPartialResult(StringBuffer newResultSequence) {
		int length = hints[0].getSequence().length();

		for (NumberMindHint hint : hints) {
			StringBuffer sequence = hint.getSequence();
			int counter = 0;

			int xs = 0;
			for (int i = 0; i < length; i++) {
				char c = newResultSequence.charAt(i);

				if (c == sequence.charAt(i)) {
					counter++;
				}

				if (c == 'x') {
					xs++;
				}
			}

			int correctCounter = hint.getCorrectCounter();
			if ((counter > correctCounter) || (correctCounter > (counter + xs))) {
				return true;
			}
		}

		return false;
	}

	private static boolean hasOnlyDigits(StringBuffer sequence) {
		int length = sequence.length();

		for (int i = 0; i < length; i++) {
			if (!Character.isDigit(sequence.charAt(i))) {
				return false;
			}
		}

		return true;
	}

	private static List<List<int[]>> getAllCombinations(int sequenceLength,
			NumberMindHint[] instance, int minCorrectCounter,
			int maxCorrectCounter) {

		List<List<int[]>> combinations = new ArrayList<List<int[]>>(
				(maxCorrectCounter - minCorrectCounter) + 1);

		for (int i = minCorrectCounter; i <= maxCorrectCounter; i++) {
			List<int[]> list = new LinkedList<int[]>();

			CombinationGenerator combGen = new CombinationGenerator(
					sequenceLength, i);
			while (combGen.hasMore()) {
				int[] next = combGen.getNext();
				list.add(Arrays.copyOf(next, next.length));
			}

			combinations.add(list);
		}

		return combinations;
	}

	public static StringBuffer solveNEW(ArrayList<NumberMindHint> instance) {
		if ((instance == null) || (instance.size() == 0)) {
			throw new IllegalArgumentException(
					"No instance given! (null or empty hints)");
		}

		if (!hintsOfSameLength(instance)) {
			throw new IllegalArgumentException(
					"Incorrect instance - sequences of different length found!");
		}

		int sequenceLength = getSequenceLength(instance);
		result = getInitialResultSequence(sequenceLength);
		resultLength = sequenceLength;

		Collections.sort(instance);

		List<Set<Character>> digitsRemoved = getEmptyDigitsRemoved(sequenceLength);
		List<NumberMindHint> forbiddenHints = getForbiddenHints(instance);

		digitsRemoved = updateDigitsRemoved(digitsRemoved, instance);
		StringBuffer resultSequence = getInitialResultSequence(sequenceLength);

		compute(instance, digitsRemoved, resultSequence, forbiddenHints);

		return result;
	}

	private static List<NumberMindHint> getForbiddenHints(
			ArrayList<NumberMindHint> instance) {
		List<NumberMindHint> forbiddenHints = new LinkedList<NumberMindHint>();

		for (NumberMindHint hint : instance) {
			if (hint.getCorrectCounter() == 0) {
				forbiddenHints.add(new NumberMindHint(new StringBuffer(hint
						.getSequence()), hint.getCorrectCounter()));
			}
		}

		return forbiddenHints;
	}

	private static List<Set<Character>> updateDigitsRemoved(
			List<Set<Character>> digitsRemoved,
			ArrayList<NumberMindHint> instance) {
		for (int i = 0; i < instance.size(); i++) {
			NumberMindHint hint = instance.get(i);
			int correctCounter = hint.getCorrectCounter();

			if (correctCounter == 0) {
				StringBuffer sequence = hint.getSequence();
				int sequenceLength = sequence.length();

				for (int j = sequenceLength - 1; j >= 0; j--) {
					digitsRemoved.get(j).add(sequence.charAt(j));
				}

				instance.remove(i);
				i--;
			}
		}

		return digitsRemoved;
	}

	private static void compute(ArrayList<NumberMindHint> instance,
			List<Set<Character>> digitsRemoved, StringBuffer resultSequence,
			List<NumberMindHint> forbiddenHints) {
		int sequenceLength = getSequenceLength(instance);
		int difference = resultLength - sequenceLength;

		for (NumberMindHint hint : instance) {
			if (isForbidden(hint, forbiddenHints)) {
				continue;
			}

			CombinationGenerator combGen = new CombinationGenerator(
					sequenceLength, hint.getCorrectCounter());

			while (combGen.hasMore()) {
				int[] combination = combGen.getNext();
				combination = updateCombination(combination, difference);

				StringBuffer newResultSequence = getNewResultSequence(
						resultSequence, hint, combination, difference);
				if (!validCombination(combination, newResultSequence)) {
					continue;
				}

				result = getResultSuffix(newResultSequence);
				List<Set<Character>> newDigitsRemoved = getNewDigitsRemoved(
						digitsRemoved, newResultSequence, hint);

				ArrayList<NumberMindHint> newInstance = getNewInstance(
						instance, newResultSequence, forbiddenHints);
				//newDigitsRemoved = updateDigitsRemoved(newDigitsRemoved,
				//		newInstance);

				Collections.sort(newInstance);

				if (isCorrect(newInstance)) {
					compute(newInstance, newDigitsRemoved, newResultSequence,
							forbiddenHints);
				}
			}
		}
	}

	private static boolean validCombination(int[] combination,
			StringBuffer resultSequence) {
		for (int i : combination) {
			if (resultSequence.charAt(i) == 'x') {
				return false;
			}
		}

		return true;
	}

	private static int[] updateCombination(int[] combination, int difference) {
		for (int i = 0; i < combination.length; i++) {
			combination[i] += difference;
		}

		return combination;
	}

	private static StringBuffer getResultSuffix(StringBuffer newResultSequence) {
		int length = newResultSequence.length();

		for (int i = 0; i < length; i++) {
			char c = newResultSequence.charAt(i);

			if (c != 'x') {
				result.setCharAt(i, c);
			}

		}
		return result;
	}

	private static boolean isForbidden(NumberMindHint hint,
			List<NumberMindHint> forbiddenHints) {
		int length = hint.getSequence().length();

		for (NumberMindHint forbiddenHint : forbiddenHints) {
			for (int i = 0; i < length; i++) {
				if (forbiddenHint.getSequence().charAt(i) == hint.getSequence()
						.charAt(i)) {
					return true;
				}
			}
		}

		return false;
	}

	private static boolean isCorrect(ArrayList<NumberMindHint> instance) {
		for (NumberMindHint hint : instance) {
			if (hint.getSequence().length() < hint.getCorrectCounter()) {
				return false;
			}
		}

		return true;
	}

	private static ArrayList<NumberMindHint> getNewInstance(
			ArrayList<NumberMindHint> instance, StringBuffer resultSequence,
			List<NumberMindHint> forbiddenHints) {
		int length = resultSequence.length();
		ArrayList<NumberMindHint> newInstance = new ArrayList<NumberMindHint>(
				instance.size());

		for (NumberMindHint hint : instance) {
			StringBuffer newSequence = getNewSequence(hint.getSequence(),
					resultSequence);
			int newCorrectCounter = getNewCorrectCounter(hint.getSequence(),
					hint.getCorrectCounter(), resultSequence);
			NumberMindHint newHint = new NumberMindHint(newSequence,
					newCorrectCounter);
			newInstance.add(newHint);
		}

		return newInstance;
	}

	private static int getNewCorrectCounter(StringBuffer sequence,
			int correctCounter, StringBuffer resultSequence) {
		int length = sequence.length();
		int newCorrectCounter = correctCounter;

		for (int i = 0; i < length; i++) {
			char c = resultSequence.charAt(i);
			if ((c != 'x') && (c == sequence.charAt(i))) {
				newCorrectCounter--;
			}
		}

		return newCorrectCounter;
	}

	private static StringBuffer getNewSequence(StringBuffer sequence,
			StringBuffer resultSequence) {
		StringBuffer newSequence = new StringBuffer("");
		int length = sequence.length();

		for (int i = 0; i < length; i++) {
			if (resultSequence.charAt(i) == 'x') {
				newSequence.append(sequence.charAt(i));
			}
		}

		return newSequence;
	}

	private static List<Set<Character>> getNewDigitsRemoved(
			List<Set<Character>> digitsRemoved, StringBuffer resultSequence,
			NumberMindHint hint) {
		int length = hint.getSequence().length();
		List<Set<Character>> newDigitsRemoved = new ArrayList<Set<Character>>(
				resultSequence.length());

		for (int i = 0; i < length; i++) {
			newDigitsRemoved.add(new HashSet<Character>(digitsRemoved.get(i)));

			char c = resultSequence.charAt(i);
			if (c == 'x') {
				newDigitsRemoved.get(i).add(hint.getSequence().charAt(i));
			}
		}

		return newDigitsRemoved;
	}

	private static StringBuffer getNewResultSequence(
			StringBuffer resultSequence, NumberMindHint hint,
			int[] combination, int difference) {
		StringBuffer newResultSequence = new StringBuffer(resultSequence);
		StringBuffer hintSequence = hint.getSequence();

		for (int i : combination) {
			newResultSequence.setCharAt(i, hintSequence.charAt(i - difference));
		}

		return newResultSequence;
	}

	private static int getSequenceLength(ArrayList<NumberMindHint> instance) {
		return instance == null ? -1 : instance.get(0).getSequence().length();
	}

	private static StringBuffer getInitialResultSequence(int sequenceLength) {
		StringBuffer resultSequence = new StringBuffer(sequenceLength);

		for (int i = 0; i < sequenceLength; i++) {
			resultSequence.append('x');
		}

		return resultSequence;
	}

	private static List<Set<Character>> getInitialDigitsRemoved(
			ArrayList<NumberMindHint> instance, int sequenceLength) {
		List<Set<Character>> digitsRemoved = getEmptyDigitsRemoved(sequenceLength);

		for (int i = 0; i < instance.size(); i++) {
			NumberMindHint hint = instance.get(i);
			int correctCounter = hint.getCorrectCounter();

			if (correctCounter == 0) {
				StringBuffer sequence = hint.getSequence();

				for (int j = 0; j < sequenceLength; j++) {
					digitsRemoved.get(j).add(sequence.charAt(j));
				}

				instance.remove(i);
				i--;
			}
		}

		return digitsRemoved;
	}

	private static List<Set<Character>> getEmptyDigitsRemoved(int sequenceLength) {
		List<Set<Character>> digitsRemoved = new ArrayList<Set<Character>>(
				sequenceLength);

		for (int i = 0; i < sequenceLength; i++) {
			digitsRemoved.add(new HashSet<Character>());
		}

		return digitsRemoved;
	}

	public static StringBuffer solve(NumberMindHint[] instance) {
		if ((instance == null) || (instance.length == 0)) {
			throw new IllegalArgumentException(
					"No instance given! (null or empty hints)");
		}

		//		if (!hintsOfSameLength(instance)) {
		//			throw new IllegalArgumentException(
		//					"Incorrect instance - sequences of different length found!");
		//		}

		int sequenceLength = instance[0].getSequence().length();
		Arrays.sort(instance);
		List<Set<Character>> digitsAvailable = getAllDigits(sequenceLength);

		//List<Integer> currentCorrectCounters = getArrayOfLength(instance.length);

		hints = instance;
		resultLength = sequenceLength;
		result = null;

		List<Integer> currentCorrectCounters = new ArrayList<Integer>(
				hints.length);
		for (NumberMindHint hint : hints) {
			currentCorrectCounters.add(0);
		}

		computeX(new StringBuffer(sequenceLength), digitsAvailable,
				currentCorrectCounters);

		return result;
	}

	private static void computeX(StringBuffer resultSequence,
			List<Set<Character>> digitsAvailable,
			List<Integer> currentCorrectCounters) {
		if (result != null) {
			return;
		}

		int length = resultSequence.length();

		if (length == resultLength) {
			if (xxx++ == 100000) {
				xxx = 0;
				System.out.println(resultSequence);
			}
			if (isFinalCorrectCounters(currentCorrectCounters)) {
				result = resultSequence;
			}

			return;
		}

		Set<Character> chars = digitsAvailable.get(length);
		List<Set<Character>> newDigitsAvailable = null;
		for (char c : chars) {
			if ((newDigitsAvailable != null)
					&& !newDigitsAvailable.get(length).contains(c)) {
				continue;
			}

			newDigitsAvailable = new ArrayList<Set<Character>>(
					digitsAvailable.size());
			for (int j = 0; j < resultLength; j++) {
				Set<Character> newChars = new TreeSet<Character>(
						digitsAvailable.get(j));
				newDigitsAvailable.add(newChars);
			}

			List<Integer> newCorrectCounters = new ArrayList<Integer>(
					currentCorrectCounters);
			StringBuffer newResultSequence = new StringBuffer(
					resultSequence.toString() + c);

			for (int i = 0; i < hints.length; i++) {
				int correctCounter = hints[i].getCorrectCounter();
				int newCounter = currentCorrectCounters.get(i);

				if (hints[i].getSequence().charAt(length) == c) {
					newCounter++;
				}

				if (newCounter > correctCounter) {
					continue;
				}

				if (newCounter == correctCounter) {
					for (int j = length + 1; j < resultLength; j++) {
						Set<Character> oldChars = newDigitsAvailable.get(j);
						Set<Character> newChars = new TreeSet<Character>(
								oldChars);
						newChars.remove(hints[i].getSequence().charAt(j));
						newDigitsAvailable.set(j, newChars);
					}
				}

				newCorrectCounters.set(i, newCounter);
			}

			computeX(newResultSequence, newDigitsAvailable, newCorrectCounters);
		}
	}

	private static boolean isFinalCorrectCounters(
			List<Integer> currentCorrectCounters) {
		for (int i = 0; i < hints.length; i++) {
			if (hints[i].getCorrectCounter() != currentCorrectCounters.get(i)) {
				return false;
			}
		}

		return true;
	}

	private static List<Set<Character>> getAllDigits(int n) {
		List<Set<Character>> digitsAvailable = new ArrayList<Set<Character>>(n);

		for (int i = 0; i < n; i++) {
			Set<Character> list = getDigitSet();
			digitsAvailable.add(list);
		}

		return digitsAvailable;
	}

	private static Set<Character> getDigitSet() {
		Set<Character> set = new TreeSet<Character>();

		for (char c = '0'; c <= '9'; c++) {
			set.add(c);
		}

		return set;
	}

	private static List<Integer> getArrayOfLength(int length) {
		List<Integer> array = new ArrayList<Integer>(length);
		for (int i = 0; i < length; i++) {
			array.add(null);
		}

		return array;
	}

	private static void compute(StringBuffer resultSequence,
			List<Integer> currentCorrectCounters) {
		if (result != null) {
			return;
		}

		int length = resultSequence.length();

		if (length == resultLength) {
			System.out.println(resultSequence);
			if (equalCounters(currentCorrectCounters)) {
				result = resultSequence;
			}
			return;
		}

		for (char c = '0'; c <= '9'; c++) {
			List<Integer> newCorrectCounters = updateCorrectCounter(
					currentCorrectCounters, resultSequence, c);
			if (newCorrectCounters != null) {
				StringBuffer newResultSequence = new StringBuffer(
						resultSequence.toString() + c);
				compute(newResultSequence, newCorrectCounters);
			}
		}
	}

	private static boolean equalCounters(List<Integer> currentCorrectCounters) {
		int size = currentCorrectCounters.size();

		for (int i = 0; i < size; i++) {
			if (currentCorrectCounters.get(i) != hints[i].getCorrectCounter()) {
				return false;
			}
		}

		return true;
	}

	private static List<Integer> updateCorrectCounter(
			List<Integer> correctCounters, StringBuffer resultSequence, char c) {
		List<Integer> counters = new ArrayList<Integer>(correctCounters);
		int posistion = resultSequence.length();
		int sequenceLength = hints[0].getSequence().length();
		int positionsLeft = sequenceLength - posistion;

		for (int i = 0; i < hints.length; i++) {
			if (counters.get(i) == null) {
				counters.set(i, 0);
			}

			if (hints[i].getSequence().charAt(posistion) == c) {
				int correctCounter = hints[i].getCorrectCounter();

				int newCounter = counters.get(i) + 1;
				if (newCounter > correctCounter) {
					return null;
				}

				if ((newCounter + positionsLeft) < correctCounter) {
					return null;
				}

				counters.set(i, newCounter);
			}
		}

		return counters;
	}

	public static boolean checkResult(NumberMindHint[] instance,
			StringBuffer resultSequence) {
		if ((instance == null) || (instance.length == 0)) {
			throw new IllegalArgumentException(
					"No instance given! (null or empty hints)");
		}

		if (resultSequence == null) {
			return false;
		}

		//		if (!hintsOfSameLength(instance)) {
		//			throw new IllegalArgumentException(
		//					"Incorrect instance - sequences of different length found!");
		//		}

		int length = instance[0].getSequence().length();

		for (NumberMindHint hint : instance) {
			StringBuffer sequence = hint.getSequence();
			int counter = 0;

			for (int i = 0; i < length; i++) {
				if (resultSequence.charAt(i) == sequence.charAt(i)) {
					counter++;
				}
			}

			if (counter != hint.getCorrectCounter()) {
				return false;
			}
		}

		return true;
	}

	private static List<List<Character>> initialRemovalOfDigits(
			List<List<Character>> digitsPossible,
			NumberMindHint[] sortedInstance) {
		for (NumberMindHint element : sortedInstance) {
			if (element.getCorrectCounter() == 0) {
				removeDigits(digitsPossible, element.getSequence());
			} else {
				break;
			}
		}

		return digitsPossible;
	}

	private static void removeDigits(List<List<Character>> digitsPossible,
			StringBuffer sequence) {
		int length = sequence.length();

		for (int i = 0; i < length; i++) {
			digitsPossible.get(i).remove((Character) sequence.charAt(i));
		}
	}

	private static boolean hintsOfSameLength(ArrayList<NumberMindHint> instance) {
		int length = -1;
		for (NumberMindHint element : instance) {
			int newLength = element.getSequence().length();

			if (length == -1) {
				length = newLength;
			} else if (length != newLength) {
				return false;
			}
		}

		return length > -1 ? true : false;
	}

	private static List<List<Character>> getInitialDigitsPossible(int n) {
		List<List<Character>> digitsPossible = new ArrayList<List<Character>>(n);

		for (int i = 0; i < n; i++) {
			LinkedList<Character> allDigits = getAllDigits();
			digitsPossible.add(allDigits);
		}

		return digitsPossible;
	}

	private static LinkedList<Character> getAllDigits() {
		LinkedList<Character> allDigits = new LinkedList<Character>();

		for (char c = '0'; c <= '9'; c++) {
			allDigits.add(c);
		}

		return allDigits;
	}
}

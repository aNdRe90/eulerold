

import java.util.Set;

import MyStuff.Factorizator;
import MyStuff.Prime;

public class Problem357 {
	/*
	 * Consider the divisors of 30: 1,2,3,5,6,10,15,30.
	 * It can be seen that for every divisor d of 30, d+30/d is prime.
	 * 
	 * Find the sum of all positive integers n not exceeding 100 000 000
	 * such that for every divisor d of n, d+n/d is prime.
	 */
	public static void main(String[] args) {
		final long N = (long) 1e8;
		Set<Long> primes = Prime.getSetOfPrimesUnder(N + 2L);
		//long bruteForceResult = bruteForce(N);
		long result = 1L; //taking 1 a priori :)

		for (long n = 2L; n <= N; n += 2L) {
			if ((n % 10000L) == 0L) {
				System.out.println(n);
			}

			if (!primes.contains(n + 1L)) {
				//if (!Prime.isPrime(n + 1L)) {
				continue;
			}

			long sqrt = (long) Math.sqrt(n);

			boolean allPrime = true;
			for (long divLower = 2L; divLower <= sqrt; divLower++) {
				if ((n % divLower) == 0L) {
					long divUpper = n / divLower;

					if (!primes.contains(divLower + divUpper)) {
						//if (!Prime.isPrime(divLower + divUpper)) {
						allPrime = false;
						break;
					}
				}
			}

			if (allPrime) {
				result += n;
			}
		}

		System.out.println("RESULT = " + result);
		//System.out.println("BRUTE RESULT = " + bruteForceResult);
	}

	public static long bruteForce(long N) {
		long sum = 0L;

		for (long n = 1L; n <= N; n++) {
			Set<Long> divisors = Factorizator.getDivisors(n); // nie dziala!

			boolean allPrime = true;
			for (long divisor : divisors) {
				if (!Prime.isPrime(divisor + (n / divisor))) {
					allPrime = false;
					break;
				}
			}

			if (allPrime) {
				sum += n;
			}
		}

		return sum;
	}
}

package MyStuff;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class GraphMatrix {
	//edge = int[3]    [0] - vertex from, [1] - vertex to, [2] - weight
	private ArrayList<ArrayList<Integer>> matrix;
	private int rows;
	private int columns;
	private boolean directed;
	
	public static void main(String[] args) {
		ArrayList<ArrayList<Integer>> matrix = new ArrayList<ArrayList<Integer>>();
		for(int i=0; i<7; i++) {
			matrix.add(new ArrayList<Integer>());
			for(int j=0; j<7; j++)
				matrix.get(i).add(Integer.valueOf(0));
		}
		
		GraphMatrix g = new GraphMatrix(matrix, false);
		g.addEdge(0,1,16);
		g.addEdge(0,2,12);
		g.addEdge(0,3,21);
		g.addEdge(1,4,20);
		g.addEdge(1,3,17);
		g.addEdge(2,3,28);
		g.addEdge(2,5,31);
		g.addEdge(3,4,18);
		g.addEdge(3,5,19);
		g.addEdge(3,6,23);
		g.addEdge(4,6,11);
		g.addEdge(5,6,27);
		long w1 = g.getSumOfWeights();
		
		GraphMatrix mst = g.getMinimumSpanningTree();
		long w2 = mst.getSumOfWeights();
		int a = 0;
	}
	
	public GraphMatrix(ArrayList<ArrayList<Integer>> matrix, boolean directed) {
		this.directed = directed;
		rows = matrix.size();
		if(rows<1)
			throw new IllegalArgumentException("Given matrix is empty.");
		
		columns = matrix.get(0).size();
		for(int i=1; i<rows; i++)
			if(matrix.get(i).size()!=columns)
				throw new IllegalArgumentException("Given matrix is not square.");
		
		this.matrix = getCopyOfMatrix(matrix);
	}
	
	public GraphMatrix() {
		matrix = new ArrayList<ArrayList<Integer>>();
		rows = 0;
		columns = 0;
	}
	
	/**
	 * 
	 * @return array where [0][1]  are numbers of vertice, [2] is weight of edge
	 */
	public List<int[]> getEdges() {
		List<int[]> edges = new LinkedList<int[]>();
		
		for(int i=0; i<rows; i++) {
			int j = directed ? 0 : i+1;
			
			for(; j<columns; j++) {
				int weight = matrix.get(i).get(j);
				if(weight!=0) {
					int[] edge = new int[3];
					edge[0] = i;
					edge[1] = j;
					edge[2] = weight;
					edges.add(edge);
				}
			}
		}
		
		return edges;
	}
	
	public long getSumOfWeights() {
		long sum = 0L;
		
		for(int i=0; i<rows; i++) {
			int j = directed ? 0 : i+1;
			
			for(; j<columns; j++)
				sum += matrix.get(i).get(j);
		}
		
		return sum;
	}
	
	public GraphMatrix getMinimumSpanningTree() {
		if(directed || rows<2)
			return null;
		
		GraphMatrix mst = getMstInitializedGraphMatrix();
		boolean[] verticeTaken = new boolean[rows];
		verticeTaken[0] = true;
		
		while(! areAllElementsTrue(verticeTaken)) {
			List<int[]> edges = getEdgesFromTakenVerticeToNontakenVertice(verticeTaken); 
			int[] edge = getMinimumWeightEdge(edges);
			mst.addEdge(edge);
			verticeTaken[edge[1]] = true;
		}
		
		return mst;
	}	
	
	private void addEdge(int... edge) {
		if(edge.length!=3 || containsInvalidVertexNumbers(edge))
			return;
		
		matrix.get(edge[0]).set(edge[1], edge[2]);
		if(!directed)
			matrix.get(edge[1]).set(edge[0], edge[2]);
	}

	private boolean containsInvalidVertexNumbers(int[] edge) {
		return edge[0]<0 || edge[0]>=rows || edge[1]<0 || edge[1]>=rows;
	}

	private int[] getMinimumWeightEdge(List<int[]> edges) {
		int[] minEdge = null;
		int minWeight = Integer.MAX_VALUE;
		int size = edges.size();
		
		for(int i=0; i<size; i++) {
			int[] edge = edges.get(i);
			if(edge[2]<minWeight) {
				minWeight = edge[2];
				minEdge = edge;
			}
		}
		
		return minEdge;
	}

	private List<int[]> getEdgesFromTakenVerticeToNontakenVertice(boolean[] verticeTaken) {
		List<int[]> edges = new ArrayList<int[]>((rows*(rows-1))/2);
		
		for(int i=0; i<rows; i++) {
			if(verticeTaken[i]) {
				edges.addAll(getEdgesFromVertexToItsNontakenNeighbours(i, verticeTaken));
			}
		}
			
		return edges;
	}

	private List<int[]> getEdgesFromVertexToItsNontakenNeighbours(int v, boolean[] verticeTaken) {
		List<int[]> edges = new LinkedList<int[]>();
		
		for(int i=0; i<columns; i++)
			if(!verticeTaken[i] && i!=v) {
				int weight = matrix.get(v).get(i);
				if(weight!=0) {
					int[] edge = new int[3];
					edge[0] = v;
					edge[1] = i;
					edge[2] = matrix.get(v).get(i);
					edges.add(edge);
				}				
			}
		
		return edges;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer("");
		
		for(int i=0; i<rows; i++) {
			for(int j=0; j<columns; j++) {
				buffer.append(matrix.get(i).get(j));
				buffer.append('\t');
			}
			buffer.append('\n');
		}
		
		return buffer.toString();
	}

	private GraphMatrix getMstInitializedGraphMatrix() {
		return new GraphMatrix(getArrayListWithZeros(rows, columns), false);
	}

	private boolean areAllElementsTrue(boolean[] values) {
		for(boolean b : values)
			if(!b)
				return false;
		
		return true;
	}

	private ArrayList<ArrayList<Integer>> getArrayListWithZeros(int rows, int columns) {
		ArrayList<ArrayList<Integer>> matrix = new ArrayList<ArrayList<Integer>>(rows);
		
		for(int i=0; i<rows; i++) {
			matrix.add(new ArrayList<Integer>(columns));
			for(int j=0; j<columns; j++)
				matrix.get(i).add(Integer.valueOf(0));
		}		
		
		return matrix;
	}

	public void addVertex() {
		for(int i=0; i<rows; i++)
			matrix.get(i).add(Integer.valueOf(0));
		
		matrix.add(new ArrayList<Integer>(columns+1));
		
		for(int i=0; i<=columns; i++)
			matrix.get(rows).add(Integer.valueOf(0));		
		
		rows++;
		columns++;
	}
	
	public int getNumberOfVertice() {
		return rows;
	}

	private ArrayList<ArrayList<Integer>> getCopyOfMatrix(
								ArrayList<ArrayList<Integer>> matrix) {
		
		ArrayList<ArrayList<Integer>> copy = new ArrayList<ArrayList<Integer>>(rows);
		
		for(int i=0; i<rows; i++) {
			copy.add(new ArrayList<Integer>(columns));
			for(int j=0; j<columns; j++)
				copy.get(i).add(new Integer(matrix.get(i).get(j)));
		}
		
		return copy;
	}
}

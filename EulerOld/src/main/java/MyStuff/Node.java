package MyStuff;

import java.util.List;

public abstract class Node implements Cloneable {
	protected Node parent;
	protected int treeLevel;
	
	public abstract List<Node> getChildrenNodes();
	public abstract Node clone();
	public abstract boolean isResultNode();
	
	public Node getParent() {
		return parent;
	}
	
	public void setParent(Node parent) {
		this.parent = parent;
	}	
	
	public int getTreeLevel() {
		return treeLevel;
	}

	public void setTreeLevel(int treeLevel) {
		this.treeLevel = treeLevel;
	}
}

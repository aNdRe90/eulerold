package MyStuff;

import java.util.Arrays;

public class PythagoreanTripple implements Comparable<PythagoreanTripple> {
	private long a;
	private long b;
	private long c;
	
	public PythagoreanTripple(long a, long b, long c) {
		super();
		
		long[] array = new long[] {a,b,c};
		Arrays.sort(array);
		
		this.a = array[0];
		this.b = array[1];
		this.c = array[2];
	}
	
	public long getA() {
		return a;
	}

	public long getB() {
		return b;
	}

	public long getC() {
		return c;
	}

	public long[] asArray() {
		return new long[] { a , b , c };
	}
	
	@Override
	public String toString() {
		return "[a = " + a + ", b = " + b + ", c = " + c + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (a ^ (a >>> 32));
		result = prime * result + (int) (b ^ (b >>> 32));
		result = prime * result + (int) (c ^ (c >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PythagoreanTripple other = (PythagoreanTripple) obj;
		if (a != other.a)
			return false;
		if (b != other.b)
			return false;
		if (c != other.c)
			return false;
		return true;
	}

	@Override
	public int compareTo(PythagoreanTripple other) {
		long difference = this.a - other.a;
		
		if(difference==0L) {
			difference = this.b - other.b;
			
			if(difference==0L) {
				difference = this.c - other.c;
			}
		}
		
		if(difference==0L) {
			return 0;
		}
		
		return difference>0L ? 1 : -1;
	}
}

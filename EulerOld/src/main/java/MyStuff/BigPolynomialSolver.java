package MyStuff;

import java.math.BigDecimal;

public class BigPolynomialSolver {
	private static final BigDecimal defaultStartingX = BigDecimal.ZERO;
	private static final int defaultIterations = 100;

	public static void main(final String[] args) {
		BigDecimal[] coefficients = { BigDecimal.valueOf(1.0),
				BigDecimal.valueOf(-5.0), BigDecimal.valueOf(6.0) };
		System.out.println(solve(coefficients, BigDecimal.valueOf(4.0)));
	}

	public static BigDecimal solve(final BigDecimal[] coefficients,
			final BigDecimal xStart, final int iterations) {
		if ((coefficients == null) || (coefficients.length < 1)) {
			throw new IllegalArgumentException("Invalid coefficients.");
		}

		BigDecimal x = new BigDecimal(xStart.toString());
		for (int i = 0; i < iterations; i++) {
			x = x.subtract(getValueFromCoefficients(coefficients, x).divide(
					getDerivativeValueFromCoefficients(coefficients, x),
					BigDecimal.ROUND_HALF_UP));
		}

		return x;
	}

	public static BigDecimal solve(final BigDecimal[] coefficients,
			final BigDecimal xStart) {
		return solve(coefficients, xStart, defaultIterations);
	}

	public static BigDecimal solve(final BigDecimal[] coefficients,
			final int iterations) {
		return solve(coefficients, defaultStartingX, iterations);
	}

	public static BigDecimal solve(final BigDecimal[] coefficients) {
		return solve(coefficients, defaultStartingX, defaultIterations);
	}

	private static BigDecimal getValueFromCoefficients(
			final BigDecimal[] coefficients, final BigDecimal x) {
		BigDecimal value = BigDecimal.ZERO;
		for (int i = 0; i < coefficients.length; i++) {
			value = value.add(x.pow(i)).multiply(
					coefficients[coefficients.length - i - 1]);
		}

		return value;
	}

	private static BigDecimal getDerivativeValueFromCoefficients(
			final BigDecimal[] coefficients, final BigDecimal x) {
		BigDecimal[] coefficientsDerivative = new BigDecimal[coefficients.length - 1];
		for (int i = 0; i < coefficientsDerivative.length; i++) {
			coefficientsDerivative[i] = coefficients[i].multiply(BigDecimal
					.valueOf(coefficients.length - i - 1));
		}

		return getValueFromCoefficients(coefficientsDerivative, x);
	}
}

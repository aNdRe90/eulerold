package MyStuff;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class IOUtility {
	public static void saveToFile(String text, String filename) {
		FileWriter out;
		try {
			out = new FileWriter(new File(filename), true);
			out.write(text);		
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static BufferedWriter getWriter(final File file, final boolean append) {
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(file, append));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return writer;
	}

	public static BufferedReader getReader(final File file) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(file));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return reader;
	}
}

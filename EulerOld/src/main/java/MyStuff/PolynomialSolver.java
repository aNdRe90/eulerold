package MyStuff;


public class PolynomialSolver {
	private static final double defaultStartingX = 0.0;
	private static final int defaultIterations = 100;

	public static void main(final String[] args) {
		double[] coefficients = { 1.0, -5.0, 6.0 };
		System.out.println(solve(coefficients, 4.0));
	}

	public static double solve(final double[] coefficients,
			final double xStart, final int iterations) {
		if ((coefficients == null) || (coefficients.length < 1)) {
			throw new IllegalArgumentException("Invalid coefficients.");
		}

		double x = xStart;
		for (int i = 0; i < iterations; i++) {
			x = x
					- (getValueFromCoefficients(coefficients, x) / getDerivativeValueFromCoefficients(
							coefficients, x));
		}

		return x;
	}

	public static double solve(final double[] coefficients, final double xStart) {
		return solve(coefficients, xStart, defaultIterations);
	}

	public static double solve(final double[] coefficients, final int iterations) {
		return solve(coefficients, defaultStartingX, iterations);
	}

	public static double solve(final double[] coefficients) {
		return solve(coefficients, defaultStartingX, defaultIterations);
	}

	private static double getValueFromCoefficients(final double[] coefficients,
			final double x) {
		double value = 0.0;
		for (int i = 0; i < coefficients.length; i++) {
			value += Math.pow(x, i) * coefficients[coefficients.length - i - 1];
		}

		return value;
	}

	private static double getDerivativeValueFromCoefficients(
			final double[] coefficients, final double x) {
		double[] coefficientsDerivative = new double[coefficients.length - 1];
		for (int i = 0; i < coefficientsDerivative.length; i++) {
			coefficientsDerivative[i] = coefficients[i]
					* (coefficients.length - i - 1);
		}

		return getValueFromCoefficients(coefficientsDerivative, x);
	}
}

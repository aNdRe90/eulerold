package MyStuff;

import java.util.HashMap;
import java.util.Map;

public class RomanNumbers {
	public static void main(String[] args) {
		System.out.println(arabicToRoman(692));
	}
	
	/**
	 * Converts given roman number to its arabic representation.
	 * @param roman number written in roman letters (I,V,X,L,C,D,M) - not necessarily in its shortest form.
	 * For example IVIII would result in 7 as well as VII.
	 * @return arabic integer for given roman number or -1 when incorrect string given as parameter.
	 */
	public static int romanToArabic(String roman) {
		if(isValidRomanNumber(roman)) {
			return getArabicNumber(roman);
		}
		else 
			return -1;
	}
	
	/**
	 * Converts given integer to its shortest representation in roman numerals.
	 * @param arabic number to convert
	 * @return null if given number<1 or number>10000, otherwise its shortest representation in roman numerals.
	 */
	public static String arabicToRoman(int arabic) {
		if(arabic<1 || arabic>10000)
			return null;
		
		StringBuffer roman = new StringBuffer("");
		handle1000part(arabic, roman);		
		arabic = arabic%1000;		
		if(arabic!=0)
			handle500part(arabic, roman);		
		
		return roman.toString();
	}

	private static void handle500part(int arabic, StringBuffer roman) {
		if(arabic>=900) {
			roman.append("CM");
			arabic -= 900;
		}
		else if(arabic>=500) {
			roman.append('D');
			arabic -= 500;
		}
		else if(arabic>=400) {
			roman.append("CD");
			arabic -= 400;
		}
		
		if(arabic!=0)
			handle100part(arabic, roman);
	}
	
	private static void handle100part(int arabic, StringBuffer roman) {
		int amountOfC = arabic/100;
		for(int i=0; i<amountOfC; i++)
			roman.append('C');
		
		arabic = arabic%100;
		if(arabic!=0)
			handle50part(arabic, roman);
	}

	private static void handle50part(int arabic, StringBuffer roman) {
		if(arabic>=90) {
			roman.append("XC");
			arabic -= 90;
		}
		else if(arabic>=50) {
			roman.append('L');
			arabic -= 50;
		}
		else if(arabic>=40) {
			roman.append("XL");
			arabic -= 40;
		}
		
		if(arabic!=0)
			handle10part(arabic, roman);
	}

	private static void handle10part(int arabic, StringBuffer roman) {
		int amountOfX = arabic/10;
		for(int i=0; i<amountOfX; i++)
			roman.append('X');
		
		arabic = arabic%10;
		if(arabic!=0)
			handle1part(arabic, roman);
	}

	private static void handle1part(int arabic, StringBuffer roman) {
		if(arabic==9)
			roman.append("IX");
		else if(arabic>=5) {
			roman.append('V');
			arabic -= 5;
			for(int i=0; i<arabic; i++)
				roman.append('I');
		}
		else if(arabic==4)
			roman.append("IV");
		else if(arabic<4)
			for(int i=0; i<arabic; i++)
				roman.append('I');
	}

	private static void handle1000part(int arabic, StringBuffer roman) {
		int amountOfM = arabic/1000;
		for(int i=0; i<amountOfM; i++)
			roman.append('M');
	}

	private static int getArabicNumber(String roman) {
		int length = roman.length();
		int previousValue = arabicValues.get(roman.charAt(0));
		int resultNumber = previousValue;
		int nextValue = 0;
		
		for(int i=1; i<length; i++) {
			nextValue = arabicValues.get(roman.charAt(i));
			if(nextValue>previousValue)
				resultNumber = resultNumber + nextValue - 2*previousValue;
			else
				resultNumber += nextValue;
			
			previousValue = nextValue;
		}
		
		return resultNumber;
	}

	private static boolean isValidRomanNumber(String roman) {
		if(roman==null || roman.length()==0)
			return false;
		
		if(containsForbiddenLetter(roman))
			return false;
		
		if(! isInProperOrder(roman))
			return false;
		
		return true;				
	}

	private static boolean isInProperOrder(String roman) {
		char previous = roman.charAt(0);
		char next = 'x';
		int lowestValue = arabicValues.get(previous);		
		int lastValue = arabicValues.get(previous);
		int length = roman.length();
		
		for(int i=1; i<length; i++) {
			next = roman.charAt(i);
			
			if(isCorrectSubtractingRule(previous, next))
				lowestValue = getSubtractingRuleValue(previous, next);		
			else {
				lastValue = arabicValues.get(next);
				if(! isCorrectOrder(previous, next, lowestValue))
					return false;
			}				
			
			previous = next;			
			if(lowestValue>lastValue)
				lowestValue = lastValue;
			else if(lowestValue<lastValue)
				return false;
		}
		
		return true;
	}

	private static int getSubtractingRuleValue(char previous, char next) {
		return arabicValues.get(next) - arabicValues.get(previous);
	}

	private static boolean isCorrectOrder(char previous, char next, int lowestValue) {
		int previousValue = arabicValues.get(previous);
		int nextValue = arabicValues.get(next);
		
		if(nextValue>previousValue)
			return false;
		
		return true;
	}

	private static boolean isCorrectSubtractingRule(char previous, char next) {
		if(previous!='I' && previous!='X' && previous!='C')
			return false;
		
		if(previous=='I')
			return next=='V' || next =='X';

		if(previous=='X')
			return next=='L' || next =='C';

		if(previous=='C')
			return next=='D' || next =='M';
		
		return false;
	}

	private static boolean containsForbiddenLetter(String roman) {
		int length = roman.length();
		
		for(int i=0; i<length; i++) {
			char currentChar = roman.charAt(i);
			if(isForbiddenCharacter(currentChar))
				return true;
		}
		
		return false;
	}

	private static boolean isForbiddenCharacter(char c) {
		return c!='I' && c!='V' && c!='X' && c!='L' && c!='C' && c!='D' && c!='M';
	}
	
	private static Map<Character, Integer> arabicValues = new HashMap<Character, Integer>();
	private static Map<Integer, Character> romanValues = new HashMap<Integer, Character>();
	static {
		arabicValues.put('I', 1);
		arabicValues.put('V', 5);
		arabicValues.put('X', 10);
		arabicValues.put('L', 50);
		arabicValues.put('C', 100);
		arabicValues.put('D', 500);
		arabicValues.put('M', 1000);
		
		romanValues.put(1, 'I');
		romanValues.put(5, 'V');
		romanValues.put(10, 'X');
		romanValues.put(50, 'L');
		romanValues.put(100, 'C');
		romanValues.put(500, 'D');
		romanValues.put(1000, 'M');
	}
}

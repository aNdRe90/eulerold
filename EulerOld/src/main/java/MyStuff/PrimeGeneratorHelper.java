package MyStuff;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class PrimeGeneratorHelper {
	public static long countPrimesIn(final File[] files) {
		long sum = 0L;
		for (File file : files) {
			sum += countPrimesIn(file);
		}

		return sum;
	}
	
	public static long countPrimesIn(File file) {
		if (!file.exists() || file.isDirectory()) {
			throw new IllegalArgumentException(
					"File either doesn`t exist or is a directory.");
		}
		System.out.println("Reading... " + file.getName());
		return getNumberOfPrimesInFile(file);
	}

	private static long getNumberOfPrimesInFile(final File file) {
		long sum = 0L;
		BufferedReader reader = IOUtility.getReader(file);

		try {
			try {
				String line = null;
				while ((line = reader.readLine()) != null) {
					String[] linePrimes = line.split(";");
					sum += linePrimes.length;
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				reader.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return sum;
	}
	
	public static void removeDuplicates(File[] files) {
		for (File file : files) {
			removeDuplicates(file);
		}
	}

	public static void removeDuplicates(File file) {
		if (!file.exists() || file.isDirectory()) {
			throw new IllegalArgumentException(
					"File either doesn`t exist or is a directory.");
		}
		
		System.out.println("Removing duplicates... " + file.getName());
		
		BufferedReader reader = IOUtility.getReader(file);
		String fileWithNoDuplicatesName = file.getAbsolutePath();
		int lastDot = fileWithNoDuplicatesName.length() - 4;
		fileWithNoDuplicatesName = fileWithNoDuplicatesName.substring(0, lastDot) + "_no_duplicates.txt";
		
		BufferedWriter writer = IOUtility.getWriter(new File(fileWithNoDuplicatesName), false);
		List<String> primesForLine = new ArrayList<String>(PrimeGenerator.primesInLine);
		String previous = "";
		try {
			try {
				String line = null;
				while ((line = reader.readLine()) != null) {
					String[] linePrimes = line.split(";");

					int i = 0;
					if(previous.equals("")) {
						previous = linePrimes[0];
						primesForLine.add(previous);
						i = 1;
					}
					
					for (; i<linePrimes.length; i++) {
						String next = linePrimes[i];

						if(!next.equals(previous)) {
							primesForLine.add(next);
							previous = next;
						} else {
							System.out.println("DUPLICATE IS " + next);
							continue;
						}

						if(primesForLine.size() == PrimeGenerator.primesInLine) {
							writeLineOfPrimes(writer, primesForLine);
							primesForLine.clear();
						}
					}
				}
				
				writeLineOfPrimes(writer, primesForLine);
				primesForLine.clear();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				reader.close();
				writer.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void writeLineOfPrimes(BufferedWriter writer,
			List<String> primesForLine) throws IOException {
		StringBuffer outLine = new StringBuffer("");
		for(int j=0; j<primesForLine.size(); j++) {
			outLine.append(primesForLine.get(j));
			if(j== primesForLine.size() - 1) {
				outLine.append("\n");
			} else {
				outLine.append(";");
			}
			
		}
		writer.write(outLine.toString());
	}
}
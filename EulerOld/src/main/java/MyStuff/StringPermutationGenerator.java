package MyStuff;

public class StringPermutationGenerator 
{
	public StringPermutationGenerator(String permut)
	{
		permutation = new String(permut);
		generator = new PermutationGenerator(permut.length(), true);
	}
	
	public boolean hasMore()
	{
		return generator.hasMore();
	}
	
	public String getNext()
	{
		int[] perm = generator.getNext();
		StringBuffer buf = new StringBuffer(permutation.length());
		for(int i=0; i<perm.length; i++)
			buf.append(permutation.charAt(perm[i]));
		
		return buf.toString();
	}

	private String permutation;
	private PermutationGenerator generator;
}

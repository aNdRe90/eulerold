package MyStuff;

import java.math.BigDecimal;

public class BigExtremum {
	private final BigDecimal x;
	private final BigDecimal value;
	private final boolean maximum;

	public BigExtremum(final BigDecimal x, final BigDecimal value,
			final boolean maximum) {
		super();
		this.x = new BigDecimal(x.toString());
		this.value = new BigDecimal(value.toString());
		this.maximum = maximum;
	}

	public BigDecimal getX() {
		return this.x;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public boolean isMaximum() {
		return this.maximum;
	}

	@Override
	public String toString() {
		if (isMaximum()) {
			return "Maximum at " + this.x + " = " + this.value;
		} else {
			return "Minimum at " + this.x + " = " + this.value;
		}
	}
}

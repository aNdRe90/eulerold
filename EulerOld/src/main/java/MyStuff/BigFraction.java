package MyStuff;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

/**
 * Represents a big fraction with numerator and denominator of BigInteger type.
 * Implements adding, subtracting, dividing, multiplying, raising to a given
 * power, reducing and reversing.
 * 
 * @author Andrzej *
 */
public class BigFraction implements Comparable<BigFraction> {
	public static void main(String[] args) {
		BigFraction a = new BigFraction(BigInteger.ONE, BigInteger.valueOf(2));
		BigFraction b = new BigFraction(BigInteger.ONE, BigInteger.valueOf(3));

		System.out.println(a.subtract(b));
	}

	/**
	 * Creates a big fraction with given numerator and denominator. If given
	 * denominator==0, ArithmeticException is thrown. * IT DOES NOT REDUCE IT.
	 * 
	 * @param num
	 *            numerator
	 * @param den
	 *            denominator
	 */
	public BigFraction(final BigInteger num, final BigInteger den) {
		if (den.compareTo(BigInteger.ZERO) == 0) {
			throw new ArithmeticException("Zero in denominator!");
		}

		this.numerator = new BigInteger(num.toString());
		this.denominator = new BigInteger(den.toString());

		if ((this.numerator.compareTo(BigInteger.ZERO) < 0)
				&& (this.denominator.compareTo(BigInteger.ZERO) < 0)) {
			this.numerator = this.numerator.multiply(this.minusOne);
			this.denominator = this.denominator.multiply(this.minusOne);
		}
	}

	/**
	 * Creates a big fraction with given numerator (fraction from BigInteger
	 * value).
	 * 
	 * @param val
	 *            numerator (denominator==1)
	 */
	public BigFraction(final BigInteger val) {
		this.numerator = new BigInteger(val.toString());
		this.denominator = BigInteger.ONE;
	}

	/**
	 * Creates a big fraction identical to the one given as parameter. * IT DOES
	 * NOT REDUCE IT.
	 * 
	 * @param f
	 *            big fraction to copy.
	 */
	public BigFraction(final BigFraction f) {
		this.numerator = new BigInteger(f.numerator.toString());
		this.denominator = new BigInteger(f.denominator.toString());
	}

	/**
	 * Reverses the big fraction exchanging the values of numerator and
	 * denominator. It does not change this object - only returns reversed big
	 * fraction. * IT DOES NOT REDUCE IT.
	 * 
	 * @return big fraction created by reversing this big fraction.
	 */
	public BigFraction reverse() {
		return new BigFraction(new BigInteger(this.denominator.toString()),
				new BigInteger(this.numerator.toString()));
	}

	/**
	 * Adds a given big fraction to this big fraction. * IT DOES NOT REDUCE IT.
	 * 
	 * @param f
	 *            big fraction to add
	 * @return Big fraction being a sum of this and given big fraction.
	 */
	public BigFraction add(final BigFraction f) {
		return new BigFraction(this.numerator.multiply(f.denominator).add(
				f.numerator.multiply(this.denominator)),
				this.denominator.multiply(f.denominator));
	}

	/**
	 * Subtracts a given big fraction from this big fraction. * IT DOES NOT
	 * REDUCE IT.
	 * 
	 * @param f
	 *            big fraction to subtract
	 * @return Big fraction being a difference between this and given big
	 *         fraction.
	 */
	public BigFraction subtract(final BigFraction f) {
		return new BigFraction(this.numerator.multiply(f.denominator).subtract(
				f.numerator.multiply(this.denominator)),
				this.denominator.multiply(f.denominator));
	}

	/**
	 * Multiplies a given big fraction by this big fraction. * IT DOES NOT
	 * REDUCE IT.
	 * 
	 * @param f
	 *            big fraction to multiply
	 * @return Big fraction being a product of this and given big fraction.
	 */
	public BigFraction multiply(final BigFraction f) {
		return new BigFraction(this.numerator.multiply(f.numerator),
				this.denominator.multiply(f.denominator));
	}

	/**
	 * Divides this big fraction by a given big fraction. If given big fraction
	 * has numerator==0, ArithmeticException is thrown. * IT DOES NOT REDUCE IT.
	 * 
	 * @param f
	 *            big fraction to divide by
	 * @return Big fraction being a quotient of this and given fbig raction.
	 */
	public BigFraction divide(final BigFraction f) {
		if (f.numerator.compareTo(BigInteger.ZERO) == 0) {
			throw new ArithmeticException("Dividing by zero!");
		}

		return new BigFraction(this.numerator.multiply(f.denominator),
				this.denominator.multiply(f.numerator));
	}

	/**
	 * Raises the big fraction to the power of given value. * IT DOES NOT REDUCE
	 * IT.
	 * 
	 * @param n
	 * @return Big fraction being a nth power of this big fraction.
	 */
	public BigFraction pow(int n) {
		if (n == 0) {
			this.numerator = BigInteger.ONE;
			this.denominator = BigInteger.ONE;
		} else if (n < 0) {
			reverse();
			n *= -1;
		}

		BigInteger tempN = new BigInteger(this.numerator.toString());
		BigInteger tempD = new BigInteger(this.denominator.toString());

		BigFraction power = new BigFraction(tempN, tempD);
		for (int i = 1; i < n; i++) {
			power = power.multiply(this);
		}

		return power;
	}

	/**
	 * Hash code function where hash is equal to denominator*17 + numerator*13.
	 */
	@Override
	public int hashCode() {
		return this.denominator.multiply(new BigInteger("17"))
				.add(this.numerator.multiply(new BigInteger("13"))).intValue();
	}

	/**
	 * Reduces the big fraction. Reducing makes sure that numerator and
	 * denominator don`t have any common divisor.
	 * 
	 * @return this after operation
	 */
	public BigFraction reduce() {
		BigInteger greatestCommonDivisor = BigInteger.ZERO;
		while (true) {
			greatestCommonDivisor = MyMath
					.GCD(this.denominator, this.numerator);
			if (greatestCommonDivisor.equals(BigInteger.ONE)) {
				break;
			}

			this.denominator = this.denominator.divide(greatestCommonDivisor);
			this.numerator = this.numerator.divide(greatestCommonDivisor);
		}

		if ((this.numerator.compareTo(BigInteger.ZERO) < 0)
				&& (this.denominator.compareTo(BigInteger.ZERO) < 0)) {
			this.numerator = this.numerator.multiply(this.minusOne);
			this.denominator = this.denominator.multiply(this.minusOne);
		}
		return this;
	}

	/**
	 * Converts a fraction to String of form "numerator/denominator" e.g. 1/3.
	 */
	@Override
	public String toString() {
		return this.numerator.toString() + "/" + this.denominator.toString()
				+ "  (" + toDouble() + ")";
	}

	/**
	 * Compares this fraction with a given object and returns true if they are
	 * equal.
	 */
	@Override
	public boolean equals(final Object other) {
		BigFraction otherFraction = (BigFraction) other;
		return (this.numerator.compareTo(otherFraction.numerator) == 0)
				&& (this.denominator.compareTo(otherFraction.denominator) == 0);
	}

	/**
	 * Compares this big fraction with another big fraction. Returns: 1 if this
	 * big fraction is greater than given big fraction. -1 if this big fraction
	 * is lower than given big fraction. 0 if both big fraction represents the
	 * same value.
	 */
	@Override
	public int compareTo(final BigFraction other) {
		boolean thisNegative = (this.denominator.compareTo(BigInteger.ZERO) < 0)
				|| (this.numerator.compareTo(BigInteger.ZERO) < 0);
		boolean otherNegative = (other.denominator.compareTo(BigInteger.ZERO) < 0)
				|| (other.numerator.compareTo(BigInteger.ZERO) < 0);

		if (thisNegative && !otherNegative) {
			return -1;
		}

		if (!thisNegative && otherNegative) {
			return 1;
		}

		if (this.denominator.compareTo(other.denominator) == 0) {
			return this.numerator.compareTo(other.numerator);
		}

		//BigInteger commonDenominator = denominator.multiply(other.denominator);
		BigInteger newNumeratorThis = this.numerator
				.multiply(other.denominator);
		BigInteger newNumeratorOther = other.numerator
				.multiply(this.denominator);

		return newNumeratorThis.subtract(newNumeratorOther).compareTo(
				BigInteger.ZERO);
	}

	/**
	 * Converts a big fraction to BigDecimal value;
	 * 
	 * @return BigDecimal of the same value as the big fraction.
	 */
	public BigDecimal toBigDecimal(final int decimalPoints) {
		BigDecimal quotient = new BigDecimal(this.numerator);
		quotient = quotient.divide(new BigDecimal(this.denominator),
				decimalPoints, RoundingMode.HALF_UP);
		return quotient;
	}

	public BigInteger getNumerator() {
		return new BigInteger(this.numerator.toString());
	}

	public BigInteger getDenominator() {
		return new BigInteger(this.denominator.toString());
	}

	public BigDecimal toBigDecimalExact() {
		BigDecimal quotient = new BigDecimal(this.numerator);
		quotient = quotient.divide(new BigDecimal(this.denominator));
		return quotient;
	}

	/**
	 * Converts a fraction to double value;
	 * 
	 * @return double of the same value as the fraction.
	 */
	public double toDouble() {
		return toBigDecimal(15).doubleValue();
	}

	public BigInteger toBigInteger() {
		return this.numerator.divide(this.denominator);
	}

	/**
	 * Tells is a fraction is proper (GCD(numerator, denominator)==1)
	 * 
	 * @return
	 */
	public boolean isProper() {
		return MyMath.GCD(this.denominator, this.numerator).equals(
				BigInteger.ONE);
	}

	private BigInteger numerator;
	private BigInteger denominator;
	private final BigInteger minusOne = new BigInteger("-1");
}

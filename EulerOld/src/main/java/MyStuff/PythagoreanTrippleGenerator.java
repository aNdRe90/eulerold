package MyStuff;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class PythagoreanTrippleGenerator {
	public static void main(String[] args) {
		List<PythagoreanTripple> tripples = getPrimitivePythagoreanTripples(130L);
		//System.out.println(tripples.size());
		
		for(PythagoreanTripple t : tripples) {
			System.out.println(t.toString());
		}
	}
	
	public static List<PythagoreanTripple> 
				getPrimitivePythagoreanTripples(long maxSideLength) {
		Set<PythagoreanTripple> primitives 
				= new HashSet<PythagoreanTripple>();
		
		for(long n=1L; (n*n + (n+1)*(n+1))<=maxSideLength ;n++) {
			//System.out.println(n);
			for(long m=(n+1); (n*n + m*m)<=maxSideLength ;m+=2L) {
				if(MyStuff.MyMath.GCD(n, m)==1L) {
					long a = m*m - n*n;
					long b = 2*m*n;
					long c = m*m + n*n;
					
					primitives.add(new PythagoreanTripple(a,b,c));
				}
			}
		}
		
		List<PythagoreanTripple> primitivesList 
					= new ArrayList<PythagoreanTripple>(primitives);
		//Collections.sort(primitivesList);
		
		return primitivesList;
	}

	public static List<PythagoreanTripple> 
						getPythagoreanTripples(long maxSideLength) {
		List<PythagoreanTripple> primitives 
				= getPrimitivePythagoreanTripples(maxSideLength);
		List<PythagoreanTripple> tripples 
					= new LinkedList<PythagoreanTripple>(primitives);
		
		for(PythagoreanTripple primitive : primitives) {
			for(long k=2; ;k++) {
				if(primitive.getC()*k>maxSideLength) {
					break;
				}
				
				PythagoreanTripple newTripple = 
						new PythagoreanTripple( primitive.getA()*k,
									  			primitive.getB()*k, 
									  			primitive.getC()*k);
				tripples.add(newTripple);
			}
		}		
		
		Collections.sort(tripples);
		return tripples;
	}
}

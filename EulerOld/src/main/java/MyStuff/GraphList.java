package MyStuff;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

public class GraphList 
{
	/**
	 * Creates graph with given number of vertice numbered from 0.
	 * @param vert
	 */
	public GraphList(int vert)
	{
		vertice = vert;
		for(int i=0; i<vertice; i++)
			list.add(new HashSet<Edge>(vertice));
	}
	
	public void addEdge(int vert1, int vert2, int weight)
	{
		if(vert1<0 || vert1>=vertice || vert2<0 || vert2>=vertice)
			throw new IllegalArgumentException("Wrong vertex number.");
		
		list.get(vert1).add(new Edge(vert2, weight));
		//list.get(vert2).add(new Edge(vert1, weight));
	}
	
	/**
	 * Adds new vertex and assigns it a next number.
	 */
	public void addVertex()
	{
		list.add(new HashSet<Edge>(++vertice));
	}
	
	public int getShortestPath(int vertStart, int vertEnd)
	{
		//INITIALIZATION-----------------------------------------------------------
		int[] distances = new int[vertice];
		int[] previousVertex = new int[vertice];
		boolean[] nodesTaken = new boolean[vertice];
		
		for(int i=0; i<vertice; i++)
		{
			distances[i] = Integer.MAX_VALUE;
			previousVertex[i] = -1; //undefined
		}		
		distances[vertStart] = 0;
		//--------------------------------------------------------------------------
		
		
		while(true)
		{
			int currentNodeNumber = -1;
			int minDistance = Integer.MAX_VALUE;
			
			for(int i=0; i<distances.length; i++) 
			{
				int tempDistance = distances[i];
				if(!nodesTaken[i] && tempDistance<minDistance)
				{
					minDistance = tempDistance;
					currentNodeNumber = i;
				}
			}
			
			if(minDistance==Integer.MAX_VALUE || currentNodeNumber==vertEnd)
				break;
			
			nodesTaken[currentNodeNumber] = true;
			
			Iterator<Edge> iter = list.get(currentNodeNumber).iterator();
			
			while(iter.hasNext())
			{				
				Edge next = iter.next();
				if(nodesTaken[next.endVertex])
					continue;
					
				int distanceRootToNeighbour = minDistance + next.weight;
				if(distanceRootToNeighbour<distances[next.endVertex])
				{
					distances[next.endVertex] = distanceRootToNeighbour;
					previousVertex[next.endVertex] = currentNodeNumber;
				}
			}	
			
			int counter = 0;
			for(int j=0; j<nodesTaken.length; j++)
				if(nodesTaken[j])
					counter++;
			
			if(counter==nodesTaken.length)
				break;
		}
		
		return distances[vertEnd];
	}
	
	private LinkedList<HashSet<Edge>> list = new LinkedList<HashSet<Edge>>();
	private int vertice;  
	private int edges = 0;
	
	private class Edge
	{
		public Edge(int endVert, int w)
		{
			endVertex = endVert;
			weight = w;
		}
		
		public int hashCode()
		{
			return 171*weight+1417*endVertex;
		}
		
		public boolean equals(Object o)
		{
			Edge other = (Edge) o;
			return weight==other.weight && endVertex==other.endVertex;
		}
		
		public String toString()
		{
			return "End: "+endVertex+"  Weight: "+weight;
		}
		
		public int weight;
		public int endVertex;
	}
}

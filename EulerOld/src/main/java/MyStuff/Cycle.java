package MyStuff;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class Cycle
{
	public static void main(String[] args)
	{
		int[] s = {1, 1, 1, 1, 2, 1, 1, 1, 1, 2, 1};
		int[] c = getCycle(s);
		int a = 0;
	}
	
	/**
	 * Returns recurring cycle in given String.
	 * @param cycle String probably containing a cycle
	 * @return String with cycle or null if no cycle found.
	 */	
	public static String getCycle(String txt)
	{
		//String cycle = b.toString().substring(2);  //just decimal part
		//if(cycle.length()%2==0)
		//	cycle = cycle.substring(0, cycle.length()-1);
		String original = new String(txt);
		String cycleCopy = new String(txt);
		do
		{
			scale = txt.length();
			maxCycleLength = scale/2;
			if(maxCycleLength<0)
				maxCycleLength = 0;
			txt = getEstimatedCycle(txt);
			
			if(txt!=null)
			{
				cycleCopy = new String(txt);
				if(txt.length()%2==0)
					txt += ""+txt.charAt(0);				
			}
				
		}
		while(txt!=null);   //going out the loop only if cycle==null, 
							  //so we keep copy of the last "non-null" cycle
		
		char temp = cycleCopy.charAt(0);
		int counter = 1;
		for(int i=1; i<cycleCopy.length(); i++)
			if(cycleCopy.charAt(i)==temp)
				counter++;
		
		if(counter==cycleCopy.length())
			return new String(""+temp);
		else
		{
			if(cycleCopy.equals(original))
				return null;
			else
				return cycleCopy;
		}
	}
	
	private static String getEstimatedCycle(String cyclePart)
	{
		//if(decFractionPart.length()%2==0)
		//	decFractionPart = decFractionPart.substring(0, decFractionPart.length()-1);
		//System.out.println(decFractionPart.charAt(0));
		String cycleToBe = new String(""); //?
		//int cycleStartingPoint = -1;
		
		boolean isCycle = false;
		for(int i=maxCycleLength; i>0; i--)
		{			
			for(int cycleStart=0; ((scale-cycleStart)/2)>=i  ; cycleStart++)
			{
				cycleToBe = cyclePart.substring(cycleStart, cycleStart+i);
				
				int previousStart = cycleStart+i;
				while(true)
				{
					int nextStart = previousStart+i;
					
					if(nextStart>scale)
					{
						String rest = cyclePart.substring(previousStart);
						int restLen = rest.length();
						
						int counter = 0;
						for(int k=0; k<restLen; k++)
							if(rest.charAt(k)==cycleToBe.charAt(k))
								counter++;

						if(counter==restLen && (counter!=0 || previousStart==scale))
						{
							isCycle = true;
							i = 0;   //out the loop
							cycleStart = Integer.MAX_VALUE;  //out the loop
							break;
						}
						else break;						
					}
					else if(nextStart==scale)
					{
						if(!cycleToBe.equals(cyclePart.substring(previousStart)))
							break;
					}
					else
					{
						if(!cycleToBe.equals(cyclePart.substring(previousStart, nextStart)))
							break;
					}										
					
					previousStart = nextStart;
				}
			}
		}
		
		if(isCycle)
			return cycleToBe;
		else
			return null;
	}
	
	/**
	 * Returns recurring cycle in given int array.
	 * @param cycle int array probably containing a cycle
	 * @return int array with cycle or null if no cycle found.
	 */	
	public static int[] getCycle(int[] cycle)
	{
		int[] original = new int[cycle.length];
		for(int i=0; i<original.length; i++)
			original[i] = cycle[i];
		
		int[] cycleCopy = new int[cycle.length];
		for(int i=0; i<cycleCopy.length; i++)
			cycleCopy[i] = cycle[i];
		
		do
		{
			scale = cycle.length;
			maxCycleLength = scale/2;
			if(maxCycleLength<0)
				maxCycleLength = 0;
			cycle = getEstimatedCycle(cycle);
			
			if(cycle!=null)
			{
				cycleCopy = new int[cycle.length];
				for(int i=0; i<cycleCopy.length; i++)
					cycleCopy[i] = cycle[i];
				
				if(cycle.length%2==0)
				{
					cycle = new int[cycleCopy.length+1];
					for(int i=0; i<cycleCopy.length; i++)
						cycle[i] = cycleCopy[i];
					
					cycle[cycleCopy.length] = cycleCopy[0];
				}		
				//else
				//{
				//	cycle = new int[cycleCopy.length+2];
				//	for(int i=0; i<cycleCopy.length; i++)
				//		cycle[i] = cycleCopy[i];
					
				//	cycle[cycleCopy.length] = cycleCopy[0];
				//	cycle[cycleCopy.length+1] = cycleCopy[1];
				//}					
			}
				
		}
		while(cycle!=null);   //going out the loop only if cycle==null, 
							  //so we keep copy of the last "non-null" cycle
		
		int temp = cycleCopy[0];
		int counter = 1;
		for(int i=1; i<cycleCopy.length; i++)
			if(cycleCopy[i]==temp)
				counter++;
		
		if(counter==cycleCopy.length)
		{
			int[] r = new int[1];
			r[0] = temp;
			return r;
		}
		else
		{			
			if(cycleCopy.length==original.length)
			{
				int counter2 = 0;
				for(int i=0; i<cycleCopy.length; i++)
					if(cycleCopy[i]==original[i])
						counter2++;
				
				if(counter2==cycleCopy.length)
					return null;
				else
					return cycleCopy;
			}
			return cycleCopy;
		}
	}
	
	private static int[] getEstimatedCycle(int[] cyclePart)
	{	
		//System.out.println(decFractionPart.charAt(0));
		ArrayList<Integer> cycleToBe = new ArrayList<Integer>(100); //?
		//int cycleStartingPoint = -1;
		
		boolean isCycle = false;
		for(int i=maxCycleLength; i>0; i--)
		{			
			for(int cycleStart=0; ((scale-cycleStart)/2)>=i  ; cycleStart++)
			{
				cycleToBe = new ArrayList<Integer>(i);
				for(int j=cycleStart; j<(cycleStart+i); j++)
					cycleToBe.add(new Integer(cyclePart[j]));
				
				int previousStart = cycleStart+i;
				while(true)
				{
					int nextStart = previousStart+i;
					if(nextStart>scale)
					{
						if(previousStart>cyclePart.length)
							break;
						
						int[] rest = new int[cyclePart.length - previousStart];
						for(int k=0; k<rest.length; k++)
							rest[k] = cyclePart[k+previousStart];
						
						int counter = 0;
						for(int k=0; k<rest.length; k++)
							if(rest[k]==cycleToBe.get(k).intValue())
								counter++;
						
						if(counter==rest.length && (counter!=0 || previousStart==scale))
						{
							isCycle = true;
							i = 0;   //out the loop
							cycleStart = Integer.MAX_VALUE;  //out the loop
							break;
						}						
					}
					else if(nextStart==scale)
					{
						int counter = 0;
						int m=0;
						for(int k=previousStart; k<cyclePart.length; k++)
						{
							if(cycleToBe.get(m++).intValue()==cyclePart[k])
								counter++;
						}					
						if(counter!=(nextStart-previousStart))
							break;
					}
					else
					{
						int counter = 0;
						int m=0;
						for(int k=previousStart; k<nextStart; k++)
						{
							if(cycleToBe.get(m++).intValue()==cyclePart[k])
								counter++;
						}					
						if(counter!=(nextStart-previousStart))
							break;
					}				
					
					previousStart = nextStart;
				}
			}
		}
		
		if(isCycle)
		{
			int[] r = new int[cycleToBe.size()];
			for(int i=0; i<r.length; i++)
				r[i] = cycleToBe.get(i).intValue();
			
			return r;
		}
		else
			return null;
	}
	
	private static final int initialScale = 2003;
	private static int scale = initialScale;
	private static int maxCycleLength = -1;
}

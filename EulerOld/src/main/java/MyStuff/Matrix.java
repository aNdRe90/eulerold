package MyStuff;

public class Matrix {
	public static void main(String[] args) {
		double[][] A = { {3,4,2,1}, {0,8,-4,0}, {1,-2,3,0}, {2,2,7,-9} };
		double[] B = { 21, 4, 6, -9 };
		Matrix matrixA = new Matrix(A);
		
		int columns = matrixA.getColumns();
		double determinant = matrixA.getDeterminant();
		for(int i=0; i<columns; i++)
			System.out.println(matrixA.replaceColumn(i, B).getDeterminant()/determinant);
		
		//System.out.println(matrix.getDeterminant());
	}
	
	private double[][] matrix;
	private int rows;
	private int columns;
	
	public Matrix() {
		
	}
	
	public Matrix(double[][] matrix) {
		rows = matrix.length;
		columns = matrix[0].length;
		
		this.matrix = new double[rows][columns];	
		for(int row=0; row<rows; row++) {
			for(int column=0; column<columns; column++)
				this.matrix[row][column] = matrix[row][column];
		}
	}
	
	public Matrix(Matrix other) {
		rows = other.rows;
		columns = other.columns;
		
		this.matrix = new double[rows][columns];	
		for(int row=0; row<rows; row++) {
			for(int column=0; column<columns; column++)
				this.matrix[row][column] = other.matrix[row][column];
		}
	}
	
	public Matrix(int rows, int columns) {
		if(rows<1 || columns<1)
			throw new IllegalArgumentException(
					"Not proper size of matrix! \nRows="+rows+", Columns="+columns);
		
		this.rows = rows;
		this.columns = columns;
		
		matrix = new double[rows][columns];
	}
	
	public Matrix multiply(double constant) {
		Matrix result = new Matrix(matrix);
		
		for(int row=0; row<rows; row++) {
			for(int column=0; column<columns; column++)
				result.matrix[row][column] *= constant;
		}
		
		return result;
	}
	
	public Matrix add(Matrix m) {
		if(rows!=m.rows || columns!=m.columns)
			throw new IllegalArgumentException("Wrong size of matrix to subtract!");
		
		Matrix result = new Matrix(matrix);
		
		for(int row=0; row<rows; row++) {
			for(int column=0; column<columns; column++)
				result.matrix[row][column] = matrix[row][column] + m.matrix[row][column];
		}
		
		return result;
	}
	
	public Matrix transpose() {
		Matrix result = new Matrix(rows, columns);
		
		for(int column=0; column<columns; column++) {
			result.setColumn(column, this.getColumn(column, matrix));
		}		
		
		return result;
	}
	
	public Matrix invert() {
		double determinant = getDeterminant();
		if(determinant==0.0)
			throw new IllegalArgumentException("Determinant=0, cannot invert the matrix!");
		
		Matrix result = new Matrix(matrix);
		
		for(int row=0; row<rows; row++) {
			for(int column=0; column<columns; column++) {
				Matrix reducedMatrix = getReducedMatrix(row, column, matrix);
				double value = reducedMatrix.getDeterminant()*matrix[row][column];
				if((row+column)%2==1)
					value = -value;
				
				result.setValue(row, column, value);
			}
		}
		
		return result.transpose().multiply(1.0/determinant);
	}
	
	//NOT TESTED - but should work:)
	public Matrix multiply(Matrix m) {
		if(columns!=m.rows)
			throw new IllegalArgumentException("Wrong size of matrix to multiply!");
		
		Matrix result = new Matrix(rows, m.columns);
		
		for(int row=0; row<rows; row++) {
			for(int column=0; column<m.columns; column++) {
				double[] columnValues = getColumn(column, m.matrix);
				double value = getDotProduct(matrix[row], columnValues);
				result.setValue(row, column, value);
			}
		}
		
		return result;
	}
	
	private double getDotProduct(double[] a, double[] b) {
		if(a.length!=b.length)
			throw new IllegalArgumentException("Wrong size of arrays to get dot product from!");
		
		double dotProduct = 0L;
		for(int i=0; i<a.length; i++)
			dotProduct += (a[i]*b[i]);
			
		return dotProduct;
	}

	private double[] getColumn(int column, double[][] matrix) {
		double[] columnValues = new double[matrix.length];
		
		int k=0;
		for(int i=0; i<matrix.length; i++)
			columnValues[i] = matrix[i][column];
		
		return columnValues;
	}

	public Matrix subtract(Matrix m) {
		if(rows!=m.rows || columns!=m.columns)
			throw new IllegalArgumentException("Wrong size of matrix to add!");
		
		Matrix result = new Matrix(matrix);
		
		for(int row=0; row<rows; row++) {
			for(int column=0; column<columns; column++)
				result.matrix[row][column] = matrix[row][column] - m.matrix[row][column];
		}
		
		return result;
	}
	
	public void setMatrixValues(double[][] matrix) {
		rows = matrix.length;
		columns = matrix[0].length;
		
		this.matrix = new double[rows][columns];	
		for(int row=0; row<rows; row++) {
			for(int column=0; column<columns; column++)
				this.matrix[row][column] = matrix[row][column];
		}
	}
	
	public double getDeterminant() {
		if(rows!=columns)
			throw new IllegalArgumentException("Matrix is not square!");
		
		return getDeterminantForMatrix(matrix);
	}
	
	public int getRows() {
		return rows;
	}
	
	public int getColumns() {
		return columns;
	}
	
	public Matrix replaceColumn(int column, double[] values) {
		Matrix result = new Matrix(matrix);
		result.setColumn(column, values);
		
		return result;
	}
	
	public Matrix replaceRow(int row, double[] values) {
		Matrix result = new Matrix(matrix);
		result.setRow(row, values);
		
		return result;
	}
	
	private void setColumn(int column, double[] values) {
		if(column<0 || column>=columns)
			throw new IllegalArgumentException("Wrong number of column to replace!");
		
		if(values.length!=rows)
			throw new IllegalArgumentException("Wrong amount of values given!");
		
		for(int row=0; row<rows; row++)
			matrix[row][column] = values[row];
	}
	
	public void setValue(int row, int column, double value) {
		if(row<0 || row>=rows || column<0 || column>=columns)
			throw new IllegalArgumentException(
					"Wrong place to put the value!\nRow="+row+", Column="+column);
		
		matrix[row][column] = value;		
	}
	
	public double getValue(int row, int column) {
		if(row<0 || row>=rows || column<0 || column>=columns)
			throw new IllegalArgumentException(
					"Wrong place to get value from!\nRow="+row+", Column="+column);
		
		return matrix[row][column];
	}
	
	private void setRow(int row, double[] values) {
		if(row<0 || row>=rows )
			throw new IllegalArgumentException("Wrong number of row to replace!");
		
		if(values.length!=columns)
			throw new IllegalArgumentException("Wrong amount of values given!");
		
		for(int column=0; column<columns; column++)
			matrix[row][column] = values[column];
	}
	
	private double getDeterminantForMatrix(double[][] matrix) {
		if(matrix[0].length<4)
			return getDeterminantForOrderLowerThan4(matrix, matrix[0].length);
		
		int columns = matrix[0].length;
		double determinant = 0L;
		long constant = 1L;
		
		for(int column=0; column<columns; column++) {
			Matrix reducedMatrix = getReducedMatrix(0, column, matrix);
			determinant += constant*matrix[0][column]*getDeterminantForMatrix(reducedMatrix.matrix);
			
			constant = -constant;
		}

		return determinant;
	}

	private Matrix getReducedMatrix(int rowExcluded, int columnExcluded, double[][] matrix) {
		int rows = matrix.length;
		int columns = matrix[0].length;
		
		if(rows==1 && columns==1)
			return null;
		
		double[][] reducedMatrix = new double[rows-1][columns-1];
		
		int i=0;
		
		for(int row=0; row<rows; row++, i++) {
			if(row==rowExcluded) {
				i--;
				continue;
			}
			
			int j=0;
			for(int column=0; column<columns; column++, j++) {
				if(column==columnExcluded) {
					j--;
					continue;
				}				
				
				reducedMatrix[i][j] = matrix[row][column];
			}
		}
		
		return new Matrix(reducedMatrix);
	}

	private double getDeterminantForOrderLowerThan4(double[][] matrix, int order) {
		if(order==1)
			return matrix[0][0];
		
		if(order==2)
			return matrix[0][0]*matrix[1][1] - matrix[1][0]*matrix[0][1];
		
		if(order==3) {
			double temp1 = matrix[0][0]*matrix[1][1]*matrix[2][2];
			double temp2 = matrix[1][0]*matrix[2][1]*matrix[0][2];
			double temp3 = matrix[2][0]*matrix[0][1]*matrix[1][2];
			double temp4 = matrix[0][2]*matrix[1][1]*matrix[2][0];
			double temp5 = matrix[1][2]*matrix[2][1]*matrix[0][0];
			double temp6 = matrix[2][2]*matrix[0][1]*matrix[1][0];
			
			return temp1 + temp2 + temp3 - temp4 - temp5 - temp6;
		}
			//return matrix[0][0]*matrix[1][1]*matrix[2][2] + matrix[1][0]*matrix[2][1]*matrix[0][2]
			//	 + matrix[2][0]*matrix[0][1]*matrix[1][2] - matrix[0][2]*matrix[1][1]*matrix[2][0]
			//	 - matrix[1][2]*matrix[2][1]*matrix[0][0] - matrix[2][2]*matrix[0][1]*matrix[1][0];
					
		return 0.0;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer("");
		
		for(int row=0; row<rows; row++) {
			for(int column=0; column<columns; column++)
				buffer.append(matrix[row][column]+", \t");
			
			buffer.append('\n');
		}		
		
		return buffer.toString();
	}
}

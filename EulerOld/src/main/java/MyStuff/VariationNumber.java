package MyStuff;

public class VariationNumber {
	private int parts;
	private int[] bases;
	private int[] values;
	boolean overflow;
	
	public VariationNumber(int[] bases) {
		for(int i=0; i<bases.length; i++)
			if(bases[i]<0)
				throw new IllegalArgumentException("bases contains value<0");
		
		this.parts = bases.length;
		this.bases = bases.clone();
		values = new int[parts];		
	}
	
	public boolean isOverflow() {
		return overflow;
	}
	
	public void increment() {
		values[0]++;
		if(values[0]>=bases[0]) {
			values[0] = 0;
			moveCarry(1);
		}
	}
	
	public void resetNumber() {
		overflow = false;
		for(int i=0; i<values.length; i++)
			values[i] = 0;
	}
	
	private void moveCarry(int position) {
		if(position==parts) {
			overflow = true;
			return;
		}
		
		values[position]++;
		if(values[position]>=bases[position]) {
			values[position] = 0;
			moveCarry(position+1);
		}
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer("");
		
		for(int i=0; i<values.length; i++) {
			buffer.append(values[i]);
			buffer.append(',');
		}
		
		return buffer.toString();
	}

	public int[] getValues() {
		return values.clone();
	}
}

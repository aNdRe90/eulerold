package MyStuff;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Factorizator {
	public static void main(final String[] args) {
		//List<Long> primes = Prime.getPrimesUnder(50000000L);

		for (int i = 0; i < 100000000; i++) {
			if ((i % 1000) == 0) {
				System.out.println(i);
			}
			Set<Long> divisors = getDivisors(i);
			int c = 0;
		}
	}

	public static Set<Long> getDivisorsBetter(final long v) {
		Set<Long> divisors = new TreeSet<Long>();

		List<Long> factorization = getPrimeFactorizationWithPowers(v);
		int size = factorization.size();
		long[] primes = new long[size / 2];
		int[] bases = new int[size / 2];

		for (int i = 0, k = 0; i < size; i += 2, k++) {
			primes[k] = factorization.get(i);
			bases[k] = factorization.get(i + 1).intValue() + 1;
		}

		VariationNumber var = new VariationNumber(bases);
		while (!var.isOverflow()) {
			int[] currentPowers = var.getValues();
			long divisor = getValue(primes, currentPowers);
			divisors.add(divisor);
			var.increment();
		}

		return divisors;
	}

	private static Long getValue(long[] primes, int[] powers) {
		if (primes.length != powers.length) {
			return null;
		}

		long value = 1L;
		for (int i = 0; i < primes.length; i++) {
			value *= ((long) Math.pow(primes[i], powers[i]));
		}

		return value;
	}

	/**
	 * Function returns the prime divisors of the given number.
	 * 
	 * @param v
	 * @return prime divisors
	 * 
	 *         e.g. for 180 which after prime factorization is equal to
	 *         2^2*3^2*5 we would get only {2,3,5} if you want to know the
	 *         exponents as well, call getPrimeFactorization() and you will get
	 *         {2,2,3,3,5} for 180.
	 * 
	 *         For n<2 it returns null.
	 */
	public static Set<Long> getPrimeDivisors(final long v) {
		Set<Long> primeDivisors = new HashSet<Long>();
		getPrimeFactorization(v, primeDivisors);

		return primeDivisors;
	}

	public static Set<Long> getPrimeDivisors(final long v, List<Long> primes) {
		Set<Long> primeDivisors = new HashSet<Long>();
		getPrimeFactorization(v, primeDivisors, primes);

		return primeDivisors;
	}

	/**
	 * Function prime factorizes the number
	 * 
	 * @param n
	 *            number to factorize
	 * @return array of prime factors e.g. for given 180, array has elements {
	 *         2, 2, 3, 3, 5} as the prime factorization of 180 is: 180 =
	 *         2^2*3^2*5 Notice that the same values will always be adjacent.
	 * 
	 *         For n<2 it returns [1].
	 */
	public static List<Long> getPrimeFactorization(final long v) {
		List<Long> primeDivisors = new LinkedList<Long>();
		getPrimeFactorization(v, (Collection<Long>) primeDivisors);

		return primeDivisors;
	}

	public static List<Long> getPrimeFactorization(final long v,
			List<Long> primes) {
		List<Long> primeDivisors = new LinkedList<Long>();
		getPrimeFactorization(v, primeDivisors, primes);

		return primeDivisors;
	}

	private static void getPrimeFactorization(long v,
			Collection<Long> collection, List<Long> primes) {
		if (v < 2L) {
			return;
		}

		if (Prime.isPrime(v)) {
			collection.add(v);
			return;
		}

		boolean end = false;
		for (long prime : primes) {
			if (prime > (long) Math.sqrt(v)) {
				break;
			}

			while (true) {
				if (v == 1L) {
					end = true;
					break;
				}

				if ((v % prime) == 0L) {
					v /= prime;

					collection.add(prime);
				} else if (Prime.isPrime(v)) {
					collection.add(v);
					end = true;
					break;
				} else {
					break;
				}
			}

			if (end) {
				break;
			}
		}
	}

	/**
	 * Function computes all the divisors of the given number including 1 and
	 * the number itself. Divisors are not sorted in any way! But they do not
	 * repeat themselves. NOTE: function checks 2^n possibilities, where n =
	 * size of array returned by getPrimeFactorization() function. So it can
	 * take far too much time for big numbers.
	 * 
	 * @param n
	 *            number which divisors to return
	 * @return set of divisors. For n<2 it returns empty set.
	 */
	public static Set<Long> getDivisors(final long n) {
		if (n < 2) {
			return new HashSet<Long>();
		}

		List<Long> primeDivs = getPrimeFactorization(n);
		int size = primeDivs.size();

		Set<Long> divisorsSet = new HashSet<Long>();
		boolean[] bools = new boolean[size];

		int a = 1;
		a = a << size;
		for (int i = 0; i < a; i++) {
			bools = new boolean[size];
			int index = 0;
			int val = i;
			while (val != 0) {
				if ((val % 2) != 0) {
					bools[index] = true;
				}
				++index;
				val = val >>> 1;
			}

			long divisor = 1L;
			for (int j = 0; j < size; j++) {
				if (bools[j]) {
					divisor *= primeDivs.get(j);
				}
			}
			divisorsSet.add(divisor);
		}

		return divisorsSet;
	}

	public static Set<Long> getDivisors(final long n, List<Long> primes) {
		if (n < 2) {
			return new HashSet<Long>();
		}

		List<Long> primeDivs = getPrimeFactorization(n, primes);
		int size = primeDivs.size();

		Set<Long> divisorsSet = new HashSet<Long>();
		boolean[] bools = new boolean[size];

		int a = 1;
		a = a << size;
		for (int i = 0; i < a; i++) {
			bools = new boolean[size];
			int index = 0;
			int val = i;
			while (val != 0) {
				if ((val % 2) != 0) {
					bools[index] = true;
				}
				++index;
				val = val >>> 1;
			}

			long divisor = 1L;
			for (int j = 0; j < size; j++) {
				if (bools[j]) {
					divisor *= primeDivs.get(j);
				}
			}
			divisorsSet.add(divisor);
		}

		return divisorsSet;
	}

	private static void getPrimeFactorization(long v,
			final Collection<Long> collection) {
		if (v < 2L) {
			return;
		}

		if (Prime.isPrime(v)) {
			collection.add(v);
			return;
		}

		boolean end = false;
		long currentPrime = 2L;

		while (true) {
			if (currentPrime > (long) Math.sqrt(v)) {
				break;
			}

			while (true) {
				if (v == 1) {
					end = true;
					break;
				}

				if ((v % currentPrime) == 0L) {
					v /= currentPrime;

					collection.add(currentPrime);
				} else if (Prime.isPrime(v)) {
					collection.add(v);
					end = true;
					break;
				} else {
					break;
				}
			}

			if (end) {
				break;
			}

			currentPrime = Prime.getNextPrime(currentPrime);
		}
	}

	/**
	 * Calculations are based on the theorem:
	 * http://en.wikipedia.org/wiki/Divisor_function saying that if X = p1^m1 *
	 * p2^m2 * ... pn^mn where pi is a prime, and mi integer then number of
	 * divisors = (m1+1)(m2+1)...(mn+1) So it is not calculating the divisors
	 * themselves and is more efficient that way.
	 * 
	 * @param n
	 *            number which number of divisors to calculate.
	 * @return number of divisors including that the 1 and number itself is its
	 *         divisor.
	 * 
	 *         For n<2 it returns 0.
	 */
	public static long getNumberOfDivisors(final long n) {
		if (n < 2) {
			return 0L;
		}

		List<Long> primeFactorsWithPowers = getPrimeFactorizationWithPowers(n);
		int size = primeFactorsWithPowers.size();

		long numberOfDivisors = 1L;
		for (int i = 1; i < size; i += 2) {
			numberOfDivisors *= (primeFactorsWithPowers.get(i) + 1L);
		}

		return numberOfDivisors;
	}

	/**
	 * Function prime factorizes the number returning their primes divisors
	 * along with their powers.
	 * 
	 * @param n
	 *            number to factorize
	 * @return array of prime factors e.g. for given 180, array has elements {
	 *         2, 2, 3, 2, 5, 1} as the prime factorization of 180 is: 180 =
	 *         2^2*3^2*5^1
	 * 
	 *         For n<2 it returns empty list.
	 */
	public static List<Long> getPrimeFactorizationWithPowers(final long n) {
		List<Long> primeDivisors = new LinkedList<Long>();
		getPrimeFactorization(n, (Collection<Long>) primeDivisors);

		int size = primeDivisors.size();
		List<Long> primeDivisorsWithPowers = new ArrayList<Long>(size * 2);
		for (int i = 0; i < size; i++) {
			long prime = primeDivisors.get(i);
			long exp = 1L;

			while (((++i) < size) && (primeDivisors.get(i) == prime)) {
				++exp;
			}

			primeDivisorsWithPowers.add(prime);
			primeDivisorsWithPowers.add(exp);
			i--;
		}

		return primeDivisorsWithPowers;
	}

	public static List<Long> getPrimeFactorizationWithPowers(final long n,
			List<Long> primes) {
		List<Long> primeDivisors = new LinkedList<Long>();
		getPrimeFactorization(n, primeDivisors, primes);

		int size = primeDivisors.size();
		List<Long> primeDivisorsWithPowers = new ArrayList<Long>(size * 2);
		for (int i = 0; i < size; i++) {
			long prime = primeDivisors.get(i);
			long exp = 1L;

			while (((++i) < size) && (primeDivisors.get(i) == prime)) {
				++exp;
			}

			primeDivisorsWithPowers.add(prime);
			primeDivisorsWithPowers.add(exp);
			i--;
		}

		return primeDivisorsWithPowers;
	}

	/**
	 * Function calculates the Euler's totient function phi(n). phi(n) =
	 * phi(p1^k1)*phi(p2^k2)*...*phi(pm^km) where n=p1^k1*p2^k2*...*pm^km is the
	 * prime factorization of n (pi are primes, ki are integer exponentials).
	 * 
	 * Additionally, phi(prime^k) = prime^k - prime^(k-1). So phi(prime) = prime
	 * - 1
	 * 
	 * @param n
	 *            value from 1
	 * @return -1 if n<1, phi(n) otherwise.
	 */
	public static long getEulersTotient(final long n) {
		if (n < 1L) {
			return -1L;
		}
		if (n == 1) {
			return 1L;
		}

		long phi = 1L;
		List<Long> primeDivisorsWithPowers = getPrimeFactorizationWithPowers(n);
		int size = primeDivisorsWithPowers.size();

		for (int i = 0; i < size; i += 2) {
			long prime = primeDivisorsWithPowers.get(i);
			long exp = primeDivisorsWithPowers.get(i + 1);

			phi *= ((long) Math.pow(prime, exp - 1));
			phi *= (prime - 1);
		}

		return phi;
	}
}

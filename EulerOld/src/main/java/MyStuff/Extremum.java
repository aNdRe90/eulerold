package MyStuff;

public class Extremum {
	private final double x;
	private final double value;
	private final boolean maximum;

	public Extremum(final double x, final double value, final boolean maximum) {
		super();
		this.x = x;
		this.value = value;
		this.maximum = maximum;
	}

	public double getX() {
		return this.x;
	}

	public double getValue() {
		return this.value;
	}

	public boolean isMaximum() {
		return this.maximum;
	}

	@Override
	public String toString() {
		if (isMaximum()) {
			return "Maximum at " + this.x + " = " + this.value;
		} else {
			return "Minimum at " + this.x + " = " + this.value;
		}
	}
}

package MyStuff;

import java.util.LinkedList;
import java.util.List;

public class SearchingTree {
	private Node root;
	private final int maxLevel = 1000; 
	
	public SearchingTree(Node root) {
		if(root==null)
			throw new IllegalArgumentException("Root cannot be null.");
		
		this.root = root.clone();
		this.root.setTreeLevel(1);
	}
	
	public Node getBreadthSearchFirstResultNode() {	
		if(root.isResultNode())
			return root.clone();
		
		List<Node> currentLevelNodes = getStartingLevelNodes();
		int level = 1;
		while((level++)<maxLevel) {
			List<Node> nextLevelNodes = new LinkedList<Node>(); 
			for(Node currentNode : currentLevelNodes) {
				List<Node> currentNodeChildren = currentNode.getChildrenNodes();
				Node resultToBe = getResultFrom(currentNodeChildren);
				
				if(resultToBe!=null)
					return resultToBe;
				
				nextLevelNodes.addAll(currentNodeChildren);
			}
			
			currentLevelNodes = nextLevelNodes;
		}
		
		return null;
	}
	
	private List<Node> getStartingLevelNodes() {
		List<Node> levelNodes = new LinkedList<Node>();
		levelNodes.add(root);
		
		return levelNodes;
	}

	private Node getResultFrom(List<Node> nodes) {
		for(Node node : nodes)
			if(node.isResultNode())
				return node;
		
		return null;
	}
}

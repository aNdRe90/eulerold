package MyStuff;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class BaseNumber {
	private final long base;
	private final long logBase;
	private final BigInteger bigBase;
	private List<Long> number;

	public BaseNumber(final long base) {
		this.base = base;
		this.number = new ArrayList<Long>();
		this.number.add(0L);
		this.logBase = (long) Math.log(base);
		this.bigBase = BigInteger.valueOf(this.base);
	}

	public List<Long> getNumber() {
		return new ArrayList<Long>(this.number);
	}

	public void increment() {
		boolean carry = true;
		int position = 0;

		while (carry) {
			if (position == this.number.size()) {
				this.number.add(0L);
			}

			long value = this.number.get(position) + 1L;
			if (value == this.base) {
				this.number.set(position, 0L);
				position++;
			} else {
				this.number.set(position, value);
				carry = false;
			}
		}
	}

	public void add(final long value) {
		List<Long> numberAdded = getNumberFrom(value);
		add(numberAdded);
	}

	public void add(final BigInteger value) {
		List<Long> numberAdded = getNumberFrom(value);
		add(numberAdded);
	}

	private void add(List<Long> numberAdded) {
		long carry = 0L;
		int positions = Math.max(this.number.size(), numberAdded.size());

		numberAdded = fillUpTo(numberAdded, positions);
		this.number = fillUpTo(this.number, positions);

		List<Long> sum = new ArrayList<Long>(positions);

		for (int i = 0; i < positions; i++) {
			long sumValue = this.number.get(i) + numberAdded.get(i) + carry;
			carry = sumValue / this.base;
			sum.set(i, sumValue % this.base);
		}

		this.number = sum;
	}

	private List<Long> fillUpTo(final List<Long> number, final int positions) {
		if (number.size() < positions) {
			int difference = positions - number.size();
			for (int i = 0; i < difference; i++) {
				number.add(0L);
			}
		}

		return number;
	}

	private List<Long> getNumberFrom(final long value) {
		long valueCopy = value;
		int positions = getMaxPower(value) + 1;
		List<Long> number = new ArrayList<Long>(positions);

		for (int i = 0; i < positions; i++) {
			number.add(valueCopy % this.base);
			valueCopy /= this.base;
		}

		return number;
	}

	public int getLength() {
		return this.number.size();
	}

	private int getMaxPower(final double value) {
		return (int) (Math.log(value) / this.logBase);
	}

	private List<Long> getNumberFrom(final BigInteger value) {
		BigInteger valueCopy = new BigInteger(value.toString());
		int positions = getMaxPower(value.doubleValue()) + 1;
		List<Long> number = new ArrayList<Long>(positions);

		for (int i = 0; i < positions; i++) {
			number.add(valueCopy.mod(this.bigBase).longValue());
			valueCopy = valueCopy.divide(this.bigBase);
		}

		return number;
	}

	@Override
	public String toString() {
		return "(base = " + this.base + ") " + this.number.toString();
	}

	public long getValue() {
		long value = 0L;
		long pow = 1L;

		for (long positionValue : this.number) {
			value += positionValue * pow;
			pow *= this.base;
		}

		return value;
	}

	public BigInteger getBigValue() {
		BigInteger value = BigInteger.ZERO;
		BigInteger pow = BigInteger.ONE;

		for (long positionValue : this.number) {
			BigInteger bigpositionValue = BigInteger.valueOf(positionValue);
			value = value.add(bigpositionValue.multiply(pow));
			pow = pow.multiply(this.bigBase);
		}

		return value;
	}
}

package MyStuff;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.sun.org.apache.bcel.internal.generic.LNEG;

public class PrimeGenerator extends Thread {
	private static final char separator = ';';
	public static final int primesInLine = 1000;

	private final String prefix;
	private final String filename;
	private long from;
	private final long to;

	public static void main(final String[] args) {
		PrimeGenerator p1 = new PrimeGenerator("D:/Primes/new/primes1.00x1e11_1.01x1e11.txt", 
				100200000000L, 100300000000L, "1 ->");

		PrimeGenerator p2 = new PrimeGenerator("D:/Primes/new/primes2.00x1e11_2.01x1e11.txt",
				200200000000L, 200300000000L, "2 ->");

		PrimeGenerator p3 = new PrimeGenerator("D:/Primes/new/primes3.00x1e11_3.01x1e11.txt",
				300200000000L, 300300000000L, "3 ->");

		PrimeGenerator p4 = new PrimeGenerator("D:/Primes/new/primes4.00x1e11_4.01x1e11.txt",
				400200000000L, 400300000000L, "4 ->");

//		PrimeGenerator p5 = new PrimeGenerator("D:/Primes/new/primes5.00x1e11_5.01x1e11.txt",
//				500000000000L, 500100000000L, "1 ->");
//
//		PrimeGenerator p6 = new PrimeGenerator("D:/Primes/new/primes6.00x1e11_6.01x1e11.txt",
//				600000000000L, 600100000000L, "1 ->");
//
//		PrimeGenerator p7 = new PrimeGenerator("D:/Primes/new/primes7.00x1e11_7.01x1e11.txt",
//				700000000000L, 700100000000L, "1 ->");
//
//		PrimeGenerator p8 = new PrimeGenerator("D:/Primes/new/primes8.00x1e11_8.01x1e11.txt",
//				800000000000L, 800100000000L, "1 ->");

		p1.start();
		p2.start();
		p3.start();
		p4.start();
//		p5.start();
//		p6.start();
//		p7.start();
//		p8.start();
	}

	public PrimeGenerator(final String filename, final long from,
			final long to, final String prefix) {
		this.filename = filename;
		this.prefix = prefix;
		this.from = from;
		this.to = to;
	}

	public void generateAndSavePrimes() {
		File file = new File(this.filename);
		BufferedWriter writer = IOUtility.getWriter(file, true);
		
		try {
			try {
				performGeneratingAndSaving(writer);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				writer.flush();
				writer.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void performGeneratingAndSaving(BufferedWriter writer)
			throws IOException {
		int line = 0;
		if (this.from == 2L) {
			line++;
			this.from++;
			writer.write("2;");
		}

		if ((this.from % 2L) == 0L) {
			this.from++;
		}

		for (long n = this.from; n <= this.to; n += 2L) {
			if ((n % 1000000L) == 1L) {
				System.out.println(this.prefix + n);
			}

			if (Prime.isPrime(n)) {
				line++;
				writer.write(Long.toString(n));
				if (line == primesInLine) {
					line = 0;
					writer.write('\n');
				} else {
					writer.write(separator);
				}
			}
		}
	}

	public void loadPrimes(final String filename,
			final Collection<Long> collection) {
		File file = new File(filename);
		if (!file.exists()) {
			throw new IllegalArgumentException("File doesn`t exist!");
		}
		BufferedReader reader = IOUtility.getReader(file);

		try {
			try {
				String line = null;
				while ((line = reader.readLine()) != null) {
					String[] linePrimes = line.split(";");
					for (String primeString : linePrimes) {
						collection.add(Long.parseLong(primeString));
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				reader.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void loadPrimes(final String filename,
			final Collection<Long> collection, final long limit) {
		File file = new File(filename);
		if (!file.exists()) {
			throw new IllegalArgumentException("File doesn`t exist!");
		}
		BufferedReader reader = IOUtility.getReader(file);

		try {
			try {
				String line = null;
				while ((line = reader.readLine()) != null) {
					String[] linePrimes = line.split(";");
					boolean out = false;

					for (String primeString : linePrimes) {
						long prime = Long.parseLong(primeString);
						if (prime > limit) {
							out = true;
							break;
						}

						collection.add(prime);
					}

					if (out) {
						break;
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				reader.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		generateAndSavePrimes();
	}
}

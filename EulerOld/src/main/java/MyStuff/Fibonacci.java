package MyStuff;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;

public class Fibonacci {
	public static void main(String[] args) {
		System.out.println(Fibonacci.getNthValue(1000));
	}
	
	private static BigInteger[] values = { 	new BigInteger("1"), new BigInteger("1"),
											new BigInteger("2"), new BigInteger("3"),
											new BigInteger("5"), new BigInteger("8"),
											new BigInteger("13"), new BigInteger("21"),
											new BigInteger("34"), new BigInteger("55"),
											new BigInteger("89"), new BigInteger("144"),
											new BigInteger("233"), new BigInteger("377"),
											new BigInteger("610"), new BigInteger("987") };
	
	private static BigDecimal a 
					= new BigDecimal("1.618033988749894848204586834365638117720309179" +
							"80576286213544862270526046281890244970720720418939113748" +
							"47540880753868917521266338622235369317931800607667263544" +
							"333890865959395829056383226613199282902678806752");
	private static BigDecimal b
					= new BigDecimal("-0.61803398874989484820458683436563811772030917" +
							"98057628621354486227052604628189024497072072041893911374" +
							"84754088075386891752126633862223536931793180060766726354" +
							"433389086595939582905638322661319928290267880675");
	private static BigDecimal invertedSqrt5 
					= new BigDecimal("0.447213595499957939281834733746255247088123671" +
							"92230514485417944908210418512756097988288288167575645499" +
							"39016352301547567008506535448894147727172720243066905417" +
							"733556346383758331622553290645279713161071522701");
	
	public static BigInteger getNthValue(int n) {
		if(n<1)
			return null;
		if(n<=values.length)
			return values[n-1];
		
		BigDecimal result = invertedSqrt5.multiply(a.pow(n, MathContext.UNLIMITED));		
		result = result.setScale(3, BigDecimal.ROUND_HALF_UP);
		
		return result.toBigIntegerExact();
	}
}

package MyStuff;

import java.math.BigDecimal;

public class BigMatrix {
	public static void main(String[] args) {
		double[][] A = { {3,4,2,1}, {0,8,-4,0}, {1,-2,3,0}, {2,2,7,-9} };
		double[] B = { 21, 4, 6, -9 };
		Matrix matrixA = new Matrix(A);
		
		int columns = matrixA.getColumns();
		double determinant = matrixA.getDeterminant();
		for(int i=0; i<columns; i++)
			System.out.println(matrixA.replaceColumn(i, B).getDeterminant()/determinant);
		
		//System.out.println(matrix.getDeterminant());
	}
	
	private BigDecimal[][] matrix;
	private int rows;
	private int columns;
	
	public BigMatrix() {
		
	}
	
	public BigMatrix(BigDecimal[][] matrix) {
		rows = matrix.length;
		columns = matrix[0].length;
		
		this.matrix = new BigDecimal[rows][columns];	
		for(int row=0; row<rows; row++) {
			for(int column=0; column<columns; column++)
				this.matrix[row][column] = new BigDecimal(matrix[row][column].toString());
		}
	}
	
	public BigMatrix(BigMatrix other) {
		rows = other.rows;
		columns = other.columns;
		
		this.matrix = new BigDecimal[rows][columns];	
		for(int row=0; row<rows; row++) {
			for(int column=0; column<columns; column++)
				this.matrix[row][column] = new BigDecimal(other.matrix[row][column].toString());
		}
	}
	
	public BigMatrix(int rows, int columns) {
		if(rows<1 || columns<1)
			throw new IllegalArgumentException(
					"Not proper size of matrix! \nRows="+rows+", Columns="+columns);
		
		this.rows = rows;
		this.columns = columns;
		
		matrix = new BigDecimal[rows][columns];
	}
	
	//NOT TESTED - but should work:)
	public BigMatrix multiply(BigDecimal constant) {
		BigMatrix result = new BigMatrix(matrix);
		
		for(int row=0; row<rows; row++) {
			for(int column=0; column<columns; column++)
				result.matrix[row][column] = result.matrix[row][column].multiply(constant);
		}
		
		return result;
	}
	
	public BigMatrix add(BigMatrix m) {
		if(rows!=m.rows || columns!=m.columns)
			throw new IllegalArgumentException("Wrong size of matrix to subtract!");
		
		BigMatrix result = new BigMatrix(matrix);
		
		for(int row=0; row<rows; row++) {
			for(int column=0; column<columns; column++)
				result.matrix[row][column] = matrix[row][column].add(m.matrix[row][column]);
		}
		
		return result;
	}
	
	public BigMatrix transpose() {
		BigMatrix result = new BigMatrix(rows, columns);
		
		for(int column=0; column<columns; column++) {
			result.setColumn(column, this.getColumn(column, matrix));
		}		
		
		return result;
	}
	
	public BigMatrix invert() {
		BigDecimal determinant = getDeterminant();
		if(determinant.equals(BigDecimal.ZERO))
			throw new IllegalArgumentException("Determinant=0, cannot invert the matrix!");
		
		BigMatrix result = new BigMatrix(matrix);
		
		for(int row=0; row<rows; row++) {
			for(int column=0; column<columns; column++) {
				BigMatrix reducedMatrix = getReducedMatrix(row, column, matrix);
				BigDecimal value = reducedMatrix.getDeterminant().multiply(matrix[row][column]);
				if((row+column)%2==1)
					value = value.negate();
				
				result.setValue(row, column, value);
			}
		}
		
		return result.transpose().multiply(BigDecimal.ONE.divide(determinant, BigDecimal.ROUND_UP));
	}
	
	public BigMatrix multiply(BigMatrix m) {
		if(columns!=m.rows)
			throw new IllegalArgumentException("Wrong size of matrix to multiply!");
		
		BigMatrix result = new BigMatrix(rows, m.columns);
		
		for(int row=0; row<rows; row++) {
			for(int column=0; column<m.columns; column++) {
				BigDecimal[] columnValues = getColumn(column, m.matrix);
				BigDecimal value = getDotProduct(matrix[row], columnValues);
				result.setValue(row, column, value);
			}
		}
		
		return result;
	}
	
	private BigDecimal getDotProduct(BigDecimal[] matrix, BigDecimal[] columnValues) {
		if(matrix.length!=columnValues.length)
			throw new IllegalArgumentException("Wrong size of arrays to get dot product from!");
		
		BigDecimal dotProduct = BigDecimal.ZERO;
		for(int i=0; i<matrix.length; i++)
			dotProduct = dotProduct.add(matrix[i].multiply(columnValues[i]));
			
		return dotProduct;
	}

	private BigDecimal[] getColumn(int column, BigDecimal[][] matrix) {
		BigDecimal[] columnValues = new BigDecimal[matrix.length];
		
		int k=0;
		for(int i=0; i<matrix.length; i++)
			columnValues[i] = new BigDecimal(matrix[i][column].toString());
		
		return columnValues;
	}

	public BigMatrix subtract(BigMatrix m) {
		if(rows!=m.rows || columns!=m.columns)
			throw new IllegalArgumentException("Wrong size of matrix to add!");
		
		BigMatrix result = new BigMatrix(matrix);
		
		for(int row=0; row<rows; row++) {
			for(int column=0; column<columns; column++)
				result.matrix[row][column] = matrix[row][column].subtract(m.matrix[row][column]);
		}
		
		return result;
	}
	
	public void setMatrixValues(BigDecimal[][] matrix) {
		rows = matrix.length;
		columns = matrix[0].length;
		
		this.matrix = new BigDecimal[rows][columns];	
		for(int row=0; row<rows; row++) {
			for(int column=0; column<columns; column++)
				this.matrix[row][column] = new BigDecimal(matrix[row][column].toString());
		}
	}
	
	public BigDecimal getDeterminant() {
		if(rows!=columns)
			throw new IllegalArgumentException("Matrix is not square!");
		
		return getDeterminantForMatrix(matrix);
	}
	
	public int getRows() {
		return rows;
	}
	
	public int getColumns() {
		return columns;
	}
	
	public BigMatrix replaceColumn(int column, BigDecimal[] values) {
		BigMatrix result = new BigMatrix(matrix);
		result.setColumn(column, values);
		
		return result;
	}
	
	public BigMatrix replaceRow(int row, BigDecimal[] values) {
		BigMatrix result = new BigMatrix(matrix);
		result.setRow(row, values);
		
		return result;
	}
	
	private void setColumn(int column, BigDecimal[] bigDecimals) {
		if(column<0 || column>=columns)
			throw new IllegalArgumentException("Wrong number of column to replace!");
		
		if(bigDecimals.length!=rows)
			throw new IllegalArgumentException("Wrong amount of values given!");
		
		for(int row=0; row<rows; row++)
			matrix[row][column] = bigDecimals[row];
	}
	
	public void setValue(int row, int column, BigDecimal value) {
		if(row<0 || row>=rows || column<0 || column>=columns)
			throw new IllegalArgumentException(
					"Wrong place to put the value!\nRow="+row+", Column="+column);
		
		matrix[row][column] = value;		
	}
	
	public BigDecimal getValue(int row, int column) {
		if(row<0 || row>=rows || column<0 || column>=columns)
			throw new IllegalArgumentException(
					"Wrong place to get value from!\nRow="+row+", Column="+column);
		
		return new BigDecimal(matrix[row][column].toString());
	}
	
	private void setRow(int row, BigDecimal[] values) {
		if(row<0 || row>=rows )
			throw new IllegalArgumentException("Wrong number of row to replace!");
		
		if(values.length!=columns)
			throw new IllegalArgumentException("Wrong amount of values given!");
		
		for(int column=0; column<columns; column++)
			matrix[row][column] = values[column];
	}
	
	private BigDecimal getDeterminantForMatrix(BigDecimal[][] matrix) {
		if(matrix[0].length<4)
			return getDeterminantForOrderLowerThan4(matrix, matrix[0].length);
		
		int columns = matrix[0].length;
		BigDecimal determinant = BigDecimal.ZERO;
		BigDecimal constant = BigDecimal.ONE;
		
		for(int column=0; column<columns; column++) {
			BigMatrix reducedMatrix = getReducedMatrix(0, column, matrix);
			determinant = determinant.add(
								constant.multiply(
								matrix[0][column].multiply(
										getDeterminantForMatrix(reducedMatrix.matrix))));
			
			constant = constant.negate();
		}

		return determinant;
	}

	private BigMatrix getReducedMatrix(int rowExcluded, int columnExcluded, BigDecimal[][] matrix) {
		int rows = matrix.length;
		int columns = matrix[0].length;
		
		if(rows==1 && columns==1)
			return null;
		
		BigDecimal[][] reducedMatrix = new BigDecimal[rows-1][columns-1];
		
		int i=0;
		
		for(int row=0; row<rows; row++, i++) {
			if(row==rowExcluded) {
				i--;
				continue;
			}
			
			int j=0;
			for(int column=0; column<columns; column++, j++) {
				if(column==columnExcluded) {
					j--;
					continue;
				}				
				
				reducedMatrix[i][j] = matrix[row][column];
			}
		}
		
		return new BigMatrix(reducedMatrix);
	}

	private BigDecimal getDeterminantForOrderLowerThan4(BigDecimal[][] matrix, int order) {
		if(order==1)
			return matrix[0][0];
		
		if(order==2)
			return matrix[0][0].multiply(matrix[1][1]).subtract(
									matrix[1][0].multiply(matrix[0][1])
															    );
		
		if(order==3) {
			BigDecimal temp1 = matrix[0][0].multiply(matrix[1][1]).multiply(matrix[2][2]);
			BigDecimal temp2 = matrix[1][0].multiply(matrix[2][1]).multiply(matrix[0][2]);
			BigDecimal temp3 = matrix[2][0].multiply(matrix[0][1]).multiply(matrix[1][2]);
			BigDecimal temp4 = matrix[0][2].multiply(matrix[1][1]).multiply(matrix[2][0]);
			BigDecimal temp5 = matrix[1][2].multiply(matrix[2][1]).multiply(matrix[0][0]);
			BigDecimal temp6 = matrix[2][2].multiply(matrix[0][1]).multiply(matrix[1][0]);
			
			return temp1.add(temp2).add(temp3).subtract(temp4).subtract(temp5).subtract(temp6);
		}
			//return matrix[0][0]*matrix[1][1]*matrix[2][2] + matrix[1][0]*matrix[2][1]*matrix[0][2]
			//	 + matrix[2][0]*matrix[0][1]*matrix[1][2] - matrix[0][2]*matrix[1][1]*matrix[2][0]
			//	 - matrix[1][2]*matrix[2][1]*matrix[0][0] - matrix[2][2]*matrix[0][1]*matrix[1][0];
					
		return BigDecimal.ZERO;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer("");
		
		for(int row=0; row<rows; row++) {
			for(int column=0; column<columns; column++)
				buffer.append(matrix[row][column]+", \t");
			
			buffer.append('\n');
		}		
		
		return buffer.toString();
	}
}

package MyStuff;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.StringTokenizer;

/**
 * Class that enables to convert regular math expressions into Revers Polish Notation
 * and calculate its value. For example 2*sqrt(2) + 3^7 = 2189.828427124746
 * 
 * It also enables us to specify variables inside expression and give values for them.
 * 
 * For example we may have 2*x - (y^x)/log10(x)    and give x=10.0, y=2.0
 * We will then get = -1004.0
 * 
 * It also tries to forbid invalid expression from executing telling as
 * pretty specific what is wrong in exception messega that is thrown 
 * if something is not right.
 * 
 * Expressions may include:
 * Functions:
	 * "sqrt" 		- e.g.  sqrt(2)
	 * "sin", 		- e.g.  sin(3)
	 * "cos", 		- e.g.  cos(4)
	 * "tg", 		- e.g.  tg(4)
	 * "ctg", 		- e.g.  ctg(4)
	 * "log",		- e.g.  log(2,4) means base 2 and value 4   (==2)
	 * "sinh", 		- e.g.  sinh(2)
	 * "cosh", 		- e.g.  cosh(8)
	 * "tgh", 		- e.g.  tgh(2)	 means arctg(2) or tg^-1(2)
	 * "ctgh", 		- e.g.  ctgh(4)	 means arcctg(2) or ctg^-1(2)
	 * "ln", 		- e.g.  ln(3)     means logarithm with base e
	 * "log10"		- e.g.  log10     means logarithm with base 10
	 * 
	 * Operators:
	 * + - / * % ^ ( )
	 * 
	 * Numbers with floating point ".":
	 * e.g. 3.0, 22.545, 9
 * @author Andrzej
 */
public class ReversePolishNotation {
	public static void main(String[] args) {
		double v;
		try {
			v = getValueForExpression("-2.8*x - (y^x)/log10(x)", 10, 2);
			System.out.println(v);
		} catch (Exception e) {e.printStackTrace();}		
	}
	
	/**
	 * Computes value for expression. 
	 * Expression may contains:
	 * Functions:
	 * "sqrt" 		- e.g.  sqrt(2)
	 * "sin", 		- e.g.  sin(3)
	 * "cos", 		- e.g.  cos(4)
	 * "tg", 		- e.g.  tg(4)
	 * "ctg", 		- e.g.  ctg(4)
	 * "log",		- e.g.  log(2,4) means base 2 and value 4   (==2)
	 * "sinh", 		- e.g.  sinh(2)
	 * "cosh", 		- e.g.  cosh(8)
	 * "tgh", 		- e.g.  tgh(2)	 means arctg(2) or tg^-1(2)
	 * "ctgh", 		- e.g.  ctgh(4)	 means arcctg(2) or ctg^-1(2)
	 * "ln", 		- e.g. ln(3)     means logarithm with base e
	 * "log10"		- e.g. log10     means logarithm with base 10
	 * 
	 * Operators:
	 * + - / * % ^ ( )
	 * 
	 * Numbers with floating point ".":
	 * e.g. 3.0, 22.545, 9
	 * 
	 * Correct expressions example:
	 * 2*9 - (2^8)*7
	 * 
	 * @param expression
	 * @return
	 * @throws Exception
	 */
	public static double getValueForExpression(String expression) 
			throws Exception {
		if(expression==null) {
			throw new Exception("Expression cannot be null.");
		}
		if(expression.isEmpty()) {
			throw new Exception("Empty expression given!");
		}
		
		String RPN = getRPNForExpression(expression);
		
		return getValueForRPNExpression(RPN);
	}
	
	/**
	 * Computes value for expression. 
	 * Expression may contains:
	 * Functions:
	 * "sqrt" 		- e.g.  sqrt(2)
	 * "sin", 		- e.g.  sin(3)
	 * "cos", 		- e.g.  cos(4)
	 * "tg", 		- e.g.  tg(4)
	 * "ctg", 		- e.g.  ctg(4)
	 * "log",		- e.g.  log(2,4) means base 2 and value 4   (==2)
	 * "sinh", 		- e.g.  sinh(2)
	 * "cosh", 		- e.g.  cosh(8)
	 * "tgh", 		- e.g.  tgh(2)	 means arctg(2) or tg^-1(2)
	 * "ctgh", 		- e.g.  ctgh(4)	 means arcctg(2) or ctg^-1(2)
	 * "ln", 		- e.g. ln(3)     means logarithm with base e
	 * "log10"		- e.g. log10     means logarithm with base 10
	 * 
	 * Operators:
	 * + - / * % ^ ( )
	 * 
	 * Numbers with floating point ".":
	 * e.g. 3.0, 22.545, 9
	 * 
	 * Variables (single letters):
	 * e.g.  a   x   i 
	 * 
	 * Correct expressions example:
	 * 2*x - (y^8)*7
	 * @param expression
	 * @param variables - it is an array with values for given variables.
	 * If expression contains variables   f,r,a  the values will be assigned
	 * variables[0] = a,  variables[1] = f,  variables[2] = r   (alphabeticly)
	 * 
	 * You may use whatever letters you want.
	 * @return
	 * @throws Exception
	 */
	public static double getValueForExpression(String expression, double... variables) 
			throws Exception {
		if(expression==null) {
			throw new Exception("Expression cannot be null.");
		}
		
		if(variables==null) {
			throw new Exception("Variables values array cannot be null.");
		}
		
		if(expression.isEmpty()) {
			throw new Exception("Empty expression given!");
		}
		
		String RPN = getRPNForExpression(expression);
		
		return getValueForRPNExpression(RPN, variables);
	}
	
	/**
	 * Returns value for given RPN Expression which cannot contain any variable.
	 * @param RPN
	 * @return
	 * @throws Exception
	 */
	public static double getValueForRPNExpression(String RPN) throws Exception {
		if(RPN==null) {
			throw new Exception("RPN expression cannot be null.");
		}
		
		if(RPN.isEmpty()) {
			throw new Exception("Empty RPN expression given!");
		}
		
		return getValueForRPNExpression(RPN, new double[0]);
	}
	
	/**
	 * Returns RPN for given expression. Expression may contain variables which
	 * would be included in result.
	 * 
	 * E.g.  for expression a*b   we will get  a b *
	 * @param expression
	 * @return
	 * @throws Exception
	 */
	public static String getRPNForExpression(String expression) throws Exception {
		//http://pl.wikipedia.org/wiki/Odwrotna_notacja_polska
		if(expression==null) {
			throw new Exception("Expression cannot be null.");
		}
		
		expression = trimSpaces(expression);
		firstValidityCheck(expression);
		out = new LinkedList<String>();
		stack = new Stack<Object>();
		variables = new ArrayList<Character>();
		List<String> tokens = getPartsOfExpression(expression);
		
		for(String token : tokens) {
			handleToken(token);
		}
		
		while(!stack.isEmpty()) {
			Object peeked = stack.peek();
			if(isLeftBracket(peeked.toString()) 
					|| isRightBracket(peeked.toString())) {
				throw new Exception("Wrong bracket places in given expression.");
			}
			
			out.add(stack.pop().toString());
		}
		
		Collections.sort(variables);
		return getOutString();
	}
	
	private static void firstValidityCheck(String expression) throws Exception {
		char startingChar = expression.charAt(0);
		char endingChar = expression.charAt(expression.length()-1);
		
		for(int i=0; i<separators.length; i++) {
			if(startingChar==separators[i] && separators[i]!='-'
					&& separators[i]!='(') {
				throw new Exception(startingChar + " is invalid character " +
						"at the beginning of the expression");
			}
			
			if(endingChar==separators[i] && separators[i]!=')') {
				throw new Exception(endingChar + " is invalid character " +
						"at the end of the expression");
			}
		}
	}

	public static double getValueForRPNExpression(String RPN, char[] varsC, double... vars) 
			throws Exception {
		if(RPN==null) {
			throw new Exception("RPN expression cannot be null.");
		}
		
		if(varsC==null) {
			throw new Exception("Variables array cannot be null.");
		}
		
		if(vars==null) {
			throw new Exception("Variables values array cannot be null.");
		}
		
		if(varsC.length!=vars.length) {
			throw new Exception("Wrong number of values for variables." +
					"There are " + varsC.length + " variables and you gave " +
							vars.length + " values for them");
		}
		
		variables = getVariablesFrom(varsC);		
		return getValueForRPNExpression(RPN, vars);
	}
	
	private static List<Character> getVariablesFrom(char[] varsC) {
		List<Character> variables = new ArrayList<Character>(varsC.length);
		
		for(char c : varsC) {
			variables.add(c);
		}
		
		return variables;
	}

	private static double getValueForRPNExpression(String RPN, double... vars)
			throws Exception {
		if(variables!=null && vars.length != variables.size()) {
			throw new Exception("Wrong number of values for variables." +
					"There are " + variables.size() + " variables and you gave " +
							vars.length + " values for them");
		}
		
		if(variables!=null && vars.length==variables.size()) {
			RPN = replaceVariablesWithValues(RPN, vars);
		}
		
		Stack<Object> stackValue = new Stack<Object>();
		StringTokenizer tokenizer = new StringTokenizer(RPN, " ");
		
		while(tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			
			if(isNumber(token)) {
				handleNumberValue(token, stackValue);
			}
			if(isOperator(token)) {
				handleOperatorValue(token, stackValue);
			}
			if(isFunction(token)) {
				handleFunctionValue(token, stackValue);
			}
		}
		
		if(stackValue.size()!=1) {
			throw new Exception("Wrong RPN expression: " + RPN);
		}
		
		return Double.parseDouble(stackValue.pop().toString());
	}

	private static String replaceVariablesWithValues(String RPN, double[] vars) 
			throws Exception {
		StringTokenizer tokenizer = new StringTokenizer(RPN, " ");
		StringBuffer bufRPN = new StringBuffer("");
		
		while(tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			
			if(isVariable(token)) {
				int index = variables.indexOf(token.charAt(0));
				if(index>-1) {
					bufRPN.append(vars[index]);
				}
				else {
//					bufRPN.append(token);
					throw new Exception("Variables given don`t match " +
							"those in given expression.");
				}
			}
			else {
				bufRPN.append(token);
			}
			
			bufRPN.append(" ");
		}
		
		return bufRPN.toString();
	}

	private static void handleFunctionValue(String token, Stack<Object> stack) throws Exception {
		Function f = new Function(token);
		int argumentsNeeded = f.getNumberOfArguments();
		double[] arguments = new double[argumentsNeeded];
		
		boolean validArgumentsNumber = stack.size()>=argumentsNeeded;
		int argumentsGotten = 0;
		
		if(validArgumentsNumber) {
			for(int i=0; i<arguments.length; i++) {
				String popped = stack.pop().toString();
				if(isNumber(popped)) {
					argumentsGotten++;
					arguments[i] = Double.parseDouble(popped);
				}
			}
			
			if(argumentsGotten!=argumentsNeeded) {
				throw new Exception("Wrong number of arguments for function " + f.name +
						" (given: " + argumentsGotten + ", needed: " + argumentsNeeded + ").");
			}
			
			stack.add(f.getResult(arguments));
		}
		else {
			throw new Exception("Wrong number of arguments for function " + f.name +
					"(given: " + stack.size() + ", needed: " + argumentsNeeded + ").");
		}
	}

	private static void handleOperatorValue(String token, Stack<Object> stack) 
			throws Exception {
		if(stack.isEmpty()) {
			return;
		}		
		Number a = new Number(stack.pop().toString());
		if(stack.isEmpty()) {
			throw new Exception("Some error occured - wrong expression?");
		}
		Number b = new Number(stack.pop().toString());
		
		stack.push(Double.toString(b.getResult(a, token.charAt(0))));
	}

	private static void handleNumberValue(String token, Stack<Object> stack) {
		stack.push(token);
	}
	
	private static String trimSpaces(String s) {
		StringBuffer trimmed = new StringBuffer("");
		int length = s.length();
		
		for(int i=0; i<length; i++) {
			char c = s.charAt(i);
			
			if(c!=' ') {
				trimmed.append(c);
			}
		}
		
		return trimmed.toString();
	}

	private static String getOutString() {
		StringBuffer buf = new StringBuffer("");
		
		for(String s : out) {
			buf.append(s);
			buf.append(" ");
		}
		
		return buf.toString();
	}

	private static List<String> getPartsOfExpression(String expression) 
			throws Exception {
		List<String> parts = new LinkedList<String>();
		List<Integer> separatorIndexes = getSeparatorIndexes(expression);
		
		int indexStart = 0;
		int indexEnd;
		
		for(int operatorIndex : separatorIndexes) {
			indexEnd = operatorIndex;
			
			if(indexStart<indexEnd) {
				parts.add(expression.substring(indexStart, indexEnd));
			}
			parts.add(expression.charAt(indexEnd)+"");
			
			indexStart = indexEnd + 1;
		}		
		
		if(indexStart<expression.length()) {
			parts.add(expression.substring(indexStart));		
		}
		
		return fixMinusesIn(parts);
	}

	private static List<String> fixMinusesIn(List<String> parts) 
			throws Exception {
		List<String> partsArray = new ArrayList<String>(parts);
		
		for(int i=0; i<partsArray.size(); i++) {
			if(partsArray.get(i).equals("-")) {
				checkMinus(i, partsArray);
			}
		}
		
		return partsArray;
	}

	private static void checkMinus(int index, List<String> partsArray) 
			throws Exception {
		int size = partsArray.size();
		
		if(index==(size-1)) {
			throw new Exception("Minus at the end of expression.");
		}
		else if(index==0) {
			String b = partsArray.get(1);
			if(isNumber(b)) {
				partsArray.remove(0);
				partsArray.set(0, "-"+b);
			}
			
			if(isFunction(b)) {   //-function  ==  0-function
				partsArray.add(0, "0");
			}
		}
		else {
			String a = partsArray.get(index-1);
			String b = partsArray.get(index+1);
			if(isNumber(b) && isLeftBracket(a)) {
				partsArray.remove(index);
				partsArray.set(index, "-"+b);
			}
		}
	}

	private static List<Integer> getSeparatorIndexes(String expression) {
		List<Integer> indexes = new LinkedList<Integer>();
		
		for(int i=0; i<separators.length; i++) {
			int fromIndex = 0;
			int nextIndex = expression.indexOf(separators[i], fromIndex); 
			
			while(nextIndex!=-1) {
				indexes.add(nextIndex);			
				fromIndex = nextIndex+1;
				
				nextIndex = expression.indexOf(separators[i], fromIndex); 
			}
		}
		
		Collections.sort(indexes);
		return indexes;
	}

	private static void handleToken(String token) throws Exception {
		if(isNumber(token)) {
			handleNumber(token);
		}
		else if(isFunction(token)) {
			handleFunction(token);
		}
		else if(isFunctionArgumentSeparator(token)) {
			handleFunctionArgumentSeparator(token);
		}
		else if(isVariable(token)) {
			handleVariable(token);		
		}
		else if(isOperator(token)) {
			handleOperator(token);
		}
		else if(isLeftBracket(token)) {
			handleLeftBracket(token);
		}
		else if(isRightBracket(token)) {
			handleRightBracket(token);
		}
		else { //incorrect expression
			throw new Exception("Expression is invalid." +
					" Cannot parse token: " + token);
		}
	}

	private static void handleOperator(String token) {
		if(isLeftSideOperator(token)) {
			handleLeftSideOperator(token);			
		}
		else if(isRightSideOperator(token)) {
			handleRightSideOperator(token);
		}
	}
	
	private static void handleRightSideOperator(String token) {
		int tokenPriority = getPriority(token);
		
		while( !stack.isEmpty() ) {
			String peeked = stack.peek().toString();
			
			if(isOperator(peeked)) {
				int peekedPriority = getPriority(peeked);
				if(tokenPriority<peekedPriority) {
					out.add(stack.pop().toString());
				}
				else {
					break;
				}
			}
			else {
				break;
			}
		}
		
		stack.push(token);
	}

	private static void handleLeftSideOperator(String token) {
		int tokenPriority = getPriority(token);
		
		while( !stack.isEmpty() ) {
			String peeked = stack.peek().toString();
			
			if(isOperator(peeked)) {
				int peekedPriority = getPriority(peeked);
				if(tokenPriority<=peekedPriority) {
					out.add(stack.pop().toString());
				}
				else {
					break;
				}
			}
			else {
				break;
			}
		}
		
		stack.push(token);
	}

	private static boolean isRightSideOperator(String token) {
		if(token.length()!=1) {
			return false;
		}
		char operatorToBe = token.charAt(0);
		int length = rightSideOperators.length;
		
		for(int i=0; i<length; i++) {
			if(operatorToBe==rightSideOperators[i]) {
				return true;
			}
		}
		
		return false;
	}

	private static boolean isLeftSideOperator(String token) {
		if(token.length()!=1) {
			return false;
		}
		char operatorToBe = token.charAt(0);
		int length = leftSideOperators.length;
		
		for(int i=0; i<length; i++) {
			if(operatorToBe==leftSideOperators[i]) {
				return true;
			}
		}
		
		return false;
	}
	
	private static int getPriority(String operator) {
		if(operator.equals("(")) {
			return 0;
		}
		
		if(operator.equals("+") || operator.equals("-") 
				|| operator.equals(")")) {
			return 1;
		}
		
		if(operator.equals("*") || operator.equals("/") 
				|| operator.equals("%") || operator.equals("^")) {
			return 2;
		}
		
		return -1;
	}

	private static boolean isOperator(String token) {
		if(token.length()!=1) {
			return false;
		}
		char operatorToBe = token.charAt(0);
		int length = operators.length;
		
		for(int i=0; i<length; i++) {
			if(operatorToBe==operators[i]) {
				return true;
			}
		}
		
		return false;
	}

	private static void handleRightBracket(String token) throws Exception {
		boolean leftBracketFound = false;
		
		while(!stack.isEmpty()) {
			if(isOperator(stack.peek().toString())) {
				out.add(stack.pop().toString());
			}
			
			if(stack.peek().equals(leftBracket)) {
				stack.pop();
				leftBracketFound = true;
				
				if( !stack.isEmpty() && isFunction(stack.peek().toString())) {
					out.add(stack.pop().toString());
				}
				break;
			}
		}
		
		if(!leftBracketFound) {
			throw new Exception("Wrong usage of brackets - " +
					"no left one found for some right brakcet.");
		}
	}

	private static void handleLeftBracket(String token) {
		stack.push(token);
	}

	private static void handleVariable(String token) {
		Character variable = token.charAt(0);
		out.add(variable.toString());
		
		if(!variables.contains(variable)) {
			variables.add(variable);
		}
	}

	private static void handleFunctionArgumentSeparator(String token) throws Exception {		
		boolean leftBracketFound = false;
		
		while(!stack.isEmpty()) {
			if(stack.peek().equals(leftBracket)) {
				leftBracketFound = true;
				break;
			}
			
			out.add(stack.pop().toString());
		}
		
		if(!leftBracketFound) {
			throw new Exception("Wrong usage of function argument separator: " 
								+ functionArgumentsSeparator + " or brackets of function.");
		}
	}

	private static void handleFunction(String token) {
		stack.push(new Function(token));
	}

	private static void handleNumber(String token) {
		out.add(token);
	}

	private static boolean isVariable(String token) {
		return token.length()==1 && Character.isLetter(token.charAt(0));
	}

	private static boolean isRightBracket(String token) {
		return token.length()==1 && token.charAt(0)==')';
	}

	private static boolean isLeftBracket(String token) {
		return token.length()==1 && token.charAt(0)=='(';
	}

	private static boolean isFunctionArgumentSeparator(String token) {
		return token.length()==1 && token.charAt(0)==functionArgumentsSeparator;
	}

	private static boolean isFunction(String token) {
		for(int i=0; i<functions.length; i++) {
			if(token.equals(functions[i])) {
				return true;
			}
		}
		
		return false;
	}

	private static boolean isNumber(String token) {
		int length = token.length();
		if(length==0) {
			return false;
		}
		
		if(length==1) {
			return Character.isDigit(token.charAt(0));
		}
		
		for(int i=1; i<(length-1); i++) {
			char c = token.charAt(i);
			if( !Character.isDigit(c) && c!='.') {
				return false;
			}
		}
		
		return Character.isDigit(token.charAt(length-1));
	}
	
	private static String[] functions = {"sqrt", "sin", "cos", "tg", "ctg", "log",
										 "sinh", "cosh", "tgh", "ctgh", "ln", "log10"};
	private static char functionArgumentsSeparator = ',';
	
	private static char[] separators = {'+', '/', '-', '*', '^', '%', '(', ')', ','};
	private static char[] operators = {'+', '/', '-', '*', '^', '%'};
	private static char[] leftSideOperators = {'+', '/', '-', '*', '%'};
	private static char[] rightSideOperators = {'^'};
	
	private static List<String> out;
	private static Stack<Object> stack;
	private static List<Character> variables;
	
	private static String leftBracket = "(";
	
	private static class Number {
		private double v;
		
		public Number(String s) {
			this.v = Double.parseDouble(s);
		}
		
		@Override
		public String toString() {
			return Double.toString(v);
		}
		
		public double getResult(Number other, char operator) {
			if(operator=='+') {
				return this.v + other.v;
			}
			
			if(operator=='-') {
				return this.v - other.v;
			}
			
			if(operator=='*') {
				return this.v * other.v;
			}
			
			if(operator=='/') {
				return this.v / other.v;
			}
			
			if(operator=='^') {
				return Math.pow(this.v, other.v);
			}
			
			if(operator=='%') {
				return this.v % other.v;
			}
			
			throw new IllegalArgumentException(
					"Operator unrecognized: " + operator);
		}
	}
	
	private static class Function {
		private String name;
		
		public Function(String name) {
			if(!isFunction(name)) {
				throw new IllegalArgumentException(name + " is not a " +
						"valid function name.");
			}
			
			this.name = name;
		}
		
		public double getResult(double... arguments) {
			if(argumentsValid(arguments)) {
				return getValueFor(arguments);
			}
			else {
				throw new IllegalArgumentException(
						arguments.length + " is wrong number of arguments for " +
								"funtion " + name);
			}
		}
		
		public int getNumberOfArguments() {
			if(name.equals("log")) {
				return 2;
			}
			else {
				return 1;
			}
		}
		
		@Override
		public String toString() {
			return name;
		}

		private double getValueFor(double[] arguments) {
			if(name.equals("sqrt")) {
				if(arguments[0]<0.0) {
					throw new IllegalArgumentException("Wrong value for sqrt(x):" +
							" x=" + arguments[0]);
				}
				return Math.sqrt(arguments[0]);
			}
			if(name.equals("sin")) {
				return Math.sin(arguments[0]);
			}
			if(name.equals("cos")) {
				return Math.cos(arguments[0]);
			}
			if(name.equals("tg")) {
				return Math.tan(arguments[0]);
			}
			if(name.equals("ctg")) {
				return Math.cos(arguments[0]) / Math.sin(arguments[0]);
			}
			if(name.equals("sinh")) {
				return Math.sinh(arguments[0]);
			}
			if(name.equals("cosh")) {
				return Math.cosh(arguments[0]);
			}
			if(name.equals("tgh")) {
				return Math.tanh(arguments[0]);
			}
			if(name.equals("ctgh")) {
				return Math.cosh(arguments[0]) / Math.sinh(arguments[0]);
			}
			if(name.equals("log")) {
				if(arguments[0]<=0.0 || arguments[1]<=0.0 || arguments[1]==1.0) {
					throw new IllegalArgumentException("Wrong values for log(a,b): " +
							"a=" + arguments[1] + ", b=" + arguments[0]);
				}
				
				return Math.log(arguments[0]) / Math.log(arguments[1]);
			}
			if(name.equals("log10")) {
				if(arguments[0]<=0.0) {
					throw new IllegalArgumentException("Wrong value for log10(x):" +
							" x=" + arguments[0]);
				}
				return Math.log10(arguments[0]);
			}
			if(name.equals("ln")) {
				if(arguments[0]<=0.0) {
					throw new IllegalArgumentException("Wrong value for ln(x):" +
							" x=" + arguments[0]);
				}
				return Math.log(arguments[0]);
			}
			
			return -1.0;
		}

		private boolean argumentsValid(double[] arguments) {
			if(name.equals("log")) {
				return arguments.length==2;
			}
			else {
				return arguments.length==1;
			}
		}
	}
}

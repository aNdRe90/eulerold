package MyStuff;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BigPolynomial {
	private final BigDecimal[] coefficients;
	private final int defaultIterations = 100;
	private static final BigDecimal epsilon = BigDecimal.valueOf(0.001);
	private final int scale = 15;
	private final RoundingMode rounding = RoundingMode.HALF_UP;
	private final BigDecimal zero = BigDecimal.valueOf(0.0000000001);

	public static void main(final String[] args) {
		BigPolynomial a = new BigPolynomial(new BigDecimal[] {
				BigDecimal.valueOf(4.0), BigDecimal.valueOf(18.0),
				BigDecimal.valueOf(-7.5), BigDecimal.valueOf(1.0) });
		BigPolynomial b = new BigPolynomial(new BigDecimal[] {
				BigDecimal.valueOf(4.0), BigDecimal.valueOf(0.0),
				BigDecimal.valueOf(5.0) });

		//		System.out.println("a = " + a);
		//		System.out.println("b = " + b);
		//		System.out.println("a+b = " + a.add(b));
		//		System.out.println("a-b = " + a.subtract(b));
		//		System.out.println("b-a = " + b.subtract(a));
		//		System.out.println("a*b = " + a.multiply(b));
		//		System.out.println("b*a = " + b.multiply(a));
		//		System.out.println("a` = " + a.getDerivative());
		//		System.out.println("b` = " + b.getDerivative());
		System.out.println("a = " + a);
		System.out.println("a` = " + a.getDerivative());
		System.out.println(a.getDerivative().getValue(BigDecimal.valueOf(3.0)));
	}

	public BigPolynomial(final int maxPower) {
		if (maxPower < 0) {
			throw new IllegalArgumentException(
					"Polynomial cannot have negative powers!");
		}

		this.coefficients = new BigDecimal[maxPower + 1];

		for (int i = 0; i < this.coefficients.length; i++) {
			this.coefficients[i] = BigDecimal.valueOf(0.0);
		}
	}

	public BigPolynomial(final BigDecimal... coefficients) {
		this.coefficients = new BigDecimal[coefficients.length];

		for (int i = 0; i < this.coefficients.length; i++) {
			this.coefficients[i] = new BigDecimal(coefficients[i].toString());
		}
	}

	public BigPolynomial(final BigPolynomial other) {
		this.coefficients = new BigDecimal[other.coefficients.length];

		for (int i = 0; i < this.coefficients.length; i++) {
			this.coefficients[i] = new BigDecimal(
					other.coefficients[i].toString());
		}
	}

	public BigPolynomial add(final BigPolynomial other) {
		int max = Math.max(this.coefficients.length, other.coefficients.length);
		BigDecimal[] coefficientsSum = new BigDecimal[max];

		for (int i = 0; i < max; i++) {
			BigDecimal a = i < this.coefficients.length ? this.coefficients[i]
					: BigDecimal.ZERO;
			BigDecimal b = i < other.coefficients.length ? other.coefficients[i]
					: BigDecimal.ZERO;

			coefficientsSum[i] = a.add(b).setScale(this.scale, this.rounding);
		}

		return new BigPolynomial(coefficientsSum);
	}

	public BigPolynomial subtract(final BigPolynomial other) {
		int max = Math.max(this.coefficients.length, other.coefficients.length);
		BigDecimal[] coefficientsDifference = new BigDecimal[max];

		for (int i = 0; i < max; i++) {
			BigDecimal a = i < this.coefficients.length ? this.coefficients[i]
					: BigDecimal.ZERO;
			BigDecimal b = i < other.coefficients.length ? other.coefficients[i]
					: BigDecimal.ZERO;

			coefficientsDifference[i] = a.subtract(b).setScale(this.scale,
					this.rounding);
		}

		return new BigPolynomial(coefficientsDifference);
	}

	public BigPolynomial multiply(final BigPolynomial other) {
		int length = (this.coefficients.length + other.coefficients.length) - 1;
		BigDecimal[] coefficientsProduct = new BigDecimal[length];

		for (int i = 0; i < other.coefficients.length; i++) {
			for (int j = 0; j < this.coefficients.length; j++) {
				BigDecimal temp = other.coefficients[i].multiply(
						this.coefficients[j]).setScale(this.scale,
						this.rounding);

				if (coefficientsProduct[i + j] == null) {
					coefficientsProduct[i + j] = temp;
				} else {
					coefficientsProduct[i + j] = coefficientsProduct[i + j]
							.add(temp).setScale(this.scale, this.rounding);
				}

			}
		}

		return new BigPolynomial(coefficientsProduct);
	}

	public BigPolynomial getDerivative() {
		BigDecimal[] coefficientsDerivative = new BigDecimal[this.coefficients.length - 1];
		for (int i = 0; i < coefficientsDerivative.length; i++) {
			coefficientsDerivative[i] = this.coefficients[i + 1]
					.multiply(BigDecimal.valueOf(i + 1));
		}

		return new BigPolynomial(coefficientsDerivative);
	}

	public BigDecimal getRootNear(BigDecimal x, final int iterations) {
		if ((this.coefficients == null) || (this.coefficients.length < 1)) {
			throw new IllegalArgumentException("Invalid coefficients.");
		}

		BigPolynomial derivative = getDerivative();
		for (int i = 0; i < iterations; i++) {
			System.out.println(i);
			BigDecimal temp = derivative.getValue(x);
			if (temp.compareTo(BigDecimal.ZERO) == 0) {
				break;
			}
			x = x.subtract(getValue(x).divide(temp, this.scale, this.rounding))
					.setScale(this.scale, this.rounding);
		}

		return x;
	}

	public BigDecimal getRootNear(final BigDecimal x) {
		return getRootNear(x, this.defaultIterations);
	}

	public BigDecimal getValue(final BigDecimal x) {
		//horner`s schema
		BigDecimal value = this.coefficients[this.coefficients.length - 1];

		for (int i = this.coefficients.length - 2; i >= 0; i--) {
			value = this.coefficients[i].add(
					value.multiply(x).setScale(this.scale, this.rounding))
					.setScale(this.scale, this.rounding);
		}

		return value;
	}

	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer("[");

		for (int i = this.coefficients.length - 1; i >= 0; i--) {
			buf.append(this.coefficients[i]);
			if (i > 0) {
				buf.append("x^");
				buf.append(i);
				buf.append(", ");
			} else {
				buf.append(']');
			}
		}

		return buf.toString();
	}

	public BigPolynomial pow(int n) {
		if (n < 0) {
			throw new IllegalArgumentException(
					"Polynomial cannot be raised to a power < 1!");
		}

		BigPolynomial pow = new BigPolynomial(this);

		for (int i = 1; i < n; i++) {
			pow = pow.multiply(this);
		}

		return pow;
	}

	public BigExtremum getExtremumBetween(BigDecimal from, BigDecimal to) {
		BigDecimal lower = from.compareTo(to) < 0 ? from : to;
		BigDecimal upper = from.compareTo(to) < 0 ? to : from;

		BigPolynomial derivative = getDerivative();
		BigDecimal zeroX = derivative.getRootNear((upper.subtract(lower)
				.setScale(this.scale, this.rounding)).divide(
				BigDecimal.valueOf(2.0), this.scale, this.rounding));

		if (((zeroX.compareTo(lower) < 0) || (zeroX.compareTo(upper) > 0))) {
			return null;
		}

		//incorrectly calculated root by a newtons method
		if (derivative.getValue(zeroX).compareTo(this.zero) > 0) {
			return null;
		}

		BigDecimal valueAtZeroX = getValue(zeroX);

		if (((getValue(zeroX.subtract(epsilon).setScale(this.scale,
				this.rounding)).compareTo(valueAtZeroX)) < 0)
				&& ((getValue(zeroX.add(epsilon).setScale(this.scale,
						this.rounding)).compareTo(valueAtZeroX)) < 0)) {
			return new BigExtremum(zeroX, valueAtZeroX, true);
		}

		if (((getValue(zeroX.subtract(epsilon).setScale(this.scale,
				this.rounding)).compareTo(valueAtZeroX)) > 0)
				&& ((getValue(zeroX.add(epsilon).setScale(this.scale,
						this.rounding)).compareTo(valueAtZeroX)) > 0)) {
			return new BigExtremum(zeroX, valueAtZeroX, false);
		}

		return null;
	}

	public BigExtremum getMaxValueBetween(BigDecimal from, BigDecimal to) {
		BigDecimal lower = from.compareTo(to) < 0 ? from : to;
		BigDecimal upper = from.compareTo(to) < 0 ? to : from;

		BigExtremum extremum = getExtremumBetween(from, to);
		if ((extremum != null) && extremum.isMaximum()) {
			return extremum;
		}

		BigDecimal valueLower = getValue(lower);
		BigDecimal valueUpper = getValue(upper);

		if (valueLower.compareTo(valueUpper) > 0) {
			return new BigExtremum(lower, valueLower, true);
		} else {
			return new BigExtremum(upper, valueUpper, true);
		}
	}
}

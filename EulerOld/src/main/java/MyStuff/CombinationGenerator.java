package MyStuff;

//I HAVE IT FROM THE INTERNET - NOT MINE!!! ~~Andrzej

//--------------------------------------
//Systematically generate combinations.
//--------------------------------------

import java.math.BigInteger;
import java.util.Arrays;

public class CombinationGenerator {

	private final int[] a;
	private final int n;
	private final int r;
	private BigInteger numLeft;
	private final BigInteger total;

	public static void main(String[] args) {
		CombinationGenerator c = new CombinationGenerator(10, 6);
		int[] temp = null;
		while (c.hasMore()) {
			temp = c.getNext();
			System.out.println(Arrays.toString(temp));
		}
	}

	//------------
	// Constructor
	//------------

	public CombinationGenerator(int n, int r) {
		if (r > n) {
			throw new IllegalArgumentException();
		}
		if (n < 1) {
			throw new IllegalArgumentException();
		}
		this.n = n;
		this.r = r;
		this.a = new int[r];
		BigInteger nFact = getFactorial(n);
		BigInteger rFact = getFactorial(r);
		BigInteger nminusrFact = getFactorial(n - r);
		this.total = nFact.divide(rFact.multiply(nminusrFact));
		reset();
	}

	//------
	// Reset
	//------

	public void reset() {
		for (int i = 0; i < this.a.length; i++) {
			this.a[i] = i;
		}
		this.numLeft = new BigInteger(this.total.toString());
	}

	//------------------------------------------------
	// Return number of combinations not yet generate
	//------------------------------------------------

	public BigInteger getNumLeft() {
		return this.numLeft;
	}

	//-----------------------------
	// Are there more combinations?
	//-----------------------------

	public boolean hasMore() {
		return this.numLeft.compareTo(BigInteger.ZERO) == 1;
	}

	//------------------------------------
	// Return total number of combinations
	//------------------------------------

	public BigInteger getTotal() {
		return this.total;
	}

	//------------------
	// Compute factorial
	//------------------

	private static BigInteger getFactorial(int n) {
		BigInteger fact = BigInteger.ONE;
		for (int i = n; i > 1; i--) {
			fact = fact.multiply(new BigInteger(Integer.toString(i)));
		}
		return fact;
	}

	//--------------------------------------------------------
	// Generate next combination (algorithm from Rosen p. 286)
	//--------------------------------------------------------

	public int[] getNext() {

		if (this.numLeft.equals(this.total)) {
			this.numLeft = this.numLeft.subtract(BigInteger.ONE);
			return this.a;
		}

		int i = this.r - 1;
		while (this.a[i] == ((this.n - this.r) + i)) {
			i--;
		}
		this.a[i] = this.a[i] + 1;
		for (int j = i + 1; j < this.r; j++) {
			this.a[j] = (this.a[i] + j) - i;
		}

		this.numLeft = this.numLeft.subtract(BigInteger.ONE);
		return this.a;

	}
}

package MyStuff;

public class Cipher 
{
	public static byte[] decipherXOR(byte[] encrypted, byte[] key)
	{
		if(key==null || key.length==0 || encrypted==null || encrypted.length==0)
			return null;
		
		byte[] deciphered = new byte[encrypted.length];
		
		for(int i=0, j=0; i<deciphered.length; i++)
		{
			deciphered[i] = (byte) (encrypted[i] ^ key[j++]);
			if(j==key.length)
				j = 0;
		}
		
		return deciphered;
	}
	
	public static byte[] encryptXOR(byte[] original, byte[] key)
	{
		if(key==null || key.length==0 || original==null || original.length==0)
			return null;
		
		byte[] encrypted = new byte[original.length];
		
		for(int i=0, j=0; i<encrypted.length; i++)
		{
			encrypted[i] = (byte) (original[i] ^ key[j++]);
			if(j==key.length)
				j = 0;
		}
		
		return encrypted;
	}
	
	private static String[] commonEnglishWords = 
		{ 
		  "the", "be", "two", "of", "and", "a", "in", "that", "have", "i",
		  "it", "for", "not", "on", "with", "he", "as", "you", "do", "at",
		  "this", "but", "his", "by", "from", "they", "we", "say", "her", "she",
		  "or", "an", "will", "my", "one", "all", "would", "there", "their", "what",
		  "so", "up", "out", "if", "about", "who", "get", "which", "go", "me",
		  "when", "make", "can", "like", "time", "no", "just", "him", "know", "take",
		  "person", "into", "year", "your", "good", "some", "could", "them", "see", "other",
		  "than", "then", "now", "look", "only", "come", "its", "over", "think", "also", 
		  "back", "after", "use", "two", "how", "our", "work", "first", "well", "way",
		  "even", "new", "want", "beacause", "any", "these", "give", "day", "most", "us"
		};
}


package MyStuff;

import java.util.Arrays;

public class Polynomial {
	private final double[] coefficients;
	private static final double epsilon = 0.001;
	private static final double zero = 0.0000000001;
	private static final int defaultIterations = 100;

	public static void main(final String[] args) {
		//means x^3 + 2x + 3
		Polynomial a = new Polynomial(new double[] { 4.0, 18.0, -7.5, 1.0 });

		Polynomial b = new Polynomial(new double[] { -500, 0, 1 });
		System.out.println(b.getRootNear(10000000.0));

		//		System.out.println("a = " + a);
		//		System.out.println("b = " + b);
		//		System.out.println("a+b = " + a.add(b));
		//		System.out.println("a-b = " + a.subtract(b));
		//		System.out.println("b-a = " + b.subtract(a));
		//		System.out.println("a*b = " + a.multiply(b));
		//		System.out.println("b*a = " + b.multiply(a));
		//		System.out.println("a` = " + a.getDerivative());
		//		System.out.println("b` = " + b.getDerivative());

		System.out.println("b = " + b);
		System.out.println("b` = " + b.getDerivative());
		System.out.println(b.getValue(3.0));
	}

	public Polynomial(final int maxPower) {
		if (maxPower < 0) {
			throw new IllegalArgumentException(
					"Polynomial cannot have negative powers!");
		}

		this.coefficients = new double[maxPower + 1];
	}

	/**
	 * Polynomial a = new Polynomial(new double[] { 3, 2, 0, 1}); //means x^3 +
	 * 2x + 3
	 * 
	 * @param coefficients
	 */
	public Polynomial(final double... coefficients) {
		this.coefficients = Arrays.copyOf(coefficients, coefficients.length);
	}

	public Polynomial(final Polynomial p) {
		this.coefficients = Arrays
				.copyOf(p.coefficients, p.coefficients.length);
	}

	public Polynomial add(final Polynomial other) {
		int max = Math.max(this.coefficients.length, other.coefficients.length);
		double[] coefficientsSum = new double[max];

		for (int i = 0; i < max; i++) {
			double a = i < this.coefficients.length ? this.coefficients[i]
					: 0.0;
			double b = i < other.coefficients.length ? other.coefficients[i]
					: 0.0;

			coefficientsSum[i] = a + b;
		}

		return new Polynomial(coefficientsSum);
	}

	public Polynomial subtract(final Polynomial other) {
		int max = Math.max(this.coefficients.length, other.coefficients.length);
		double[] coefficientsDifference = new double[max];

		for (int i = 0; i < max; i++) {
			double a = i < this.coefficients.length ? this.coefficients[i]
					: 0.0;
			double b = i < other.coefficients.length ? other.coefficients[i]
					: 0.0;

			coefficientsDifference[i] = a - b;
		}

		return new Polynomial(coefficientsDifference);
	}

	public Polynomial multiply(final Polynomial other) {
		int length = (this.coefficients.length + other.coefficients.length) - 1;
		double[] coefficientsProduct = new double[length];

		for (int i = 0; i < other.coefficients.length; i++) {
			for (int j = 0; j < this.coefficients.length; j++) {
				coefficientsProduct[i + j] += other.coefficients[i]
						* this.coefficients[j];
			}
		}

		return new Polynomial(coefficientsProduct);
	}

	public Polynomial getDerivative() {
		double[] coefficientsDerivative = new double[this.coefficients.length - 1];
		for (int i = 0; i < coefficientsDerivative.length; i++) {
			coefficientsDerivative[i] = this.coefficients[i + 1] * (i + 1);
		}

		return new Polynomial(coefficientsDerivative);
	}

	public Polynomial pow(final int n) {
		if (n < 0) {
			throw new IllegalArgumentException(
					"Polynomial cannot be raised to a power < 1!");
		}

		Polynomial pow = new Polynomial(this);

		for (int i = 1; i < n; i++) {
			pow = pow.multiply(this);
		}

		return pow;
	}

	public double getRootNear(double x, final int iterations) {
		if ((this.coefficients == null) || (this.coefficients.length < 1)) {
			throw new IllegalArgumentException("Invalid coefficients.");
		}

		Polynomial derivative = getDerivative();
		for (int i = 0; i < iterations; i++) {
			x = x - (getValue(x) / derivative.getValue(x));
		}

		return x;
	}

	public double getRootNear(final double x) {
		return getRootNear(x, Polynomial.defaultIterations);
	}

	public double getValue(final double x) {
		//horner`s schema
		double value = this.coefficients[this.coefficients.length - 1];

		for (int i = this.coefficients.length - 2; i >= 0; i--) {
			value = this.coefficients[i] + (value * x);
		}

		return value;
	}

	public Extremum getExtremumBetween(double from, double to) {
		double lower = from < to ? from : to;
		double upper = from < to ? to : from;

		Polynomial derivative = getDerivative();
		double zeroX = derivative.getRootNear((upper - lower) / 2.0);

		if ((zeroX < lower) || (zeroX > upper)) {
			return null;
		}

		//incorrectly calculated root by a newtons method
		if (derivative.getValue(zeroX) > zero) {
			return null;
		}

		double valueAtZeroX = getValue(zeroX);

		if ((getValue(zeroX - Polynomial.epsilon) < valueAtZeroX)
				&& (getValue(zeroX + Polynomial.epsilon) < valueAtZeroX)) {
			return new Extremum(zeroX, valueAtZeroX, true);
		}

		if ((getValue(zeroX - Polynomial.epsilon) > valueAtZeroX)
				&& (getValue(zeroX + Polynomial.epsilon) > valueAtZeroX)) {
			return new Extremum(zeroX, valueAtZeroX, false);
		}

		return null;
	}

	public Extremum getMaxValueBetween(double from, double to) {
		double lower = from < to ? from : to;
		double upper = from < to ? to : from;

		Extremum extremum = getExtremumBetween(from, to);
		if ((extremum != null) && extremum.isMaximum()) {
			return extremum;
		}

		double valueLower = getValue(lower);
		double valueUpper = getValue(upper);

		if (valueLower > valueUpper) {
			return new Extremum(lower, valueLower, true);
		} else {
			return new Extremum(upper, valueUpper, true);
		}
	}

	public Extremum getMinValueBetween(double from, double to) {
		double lower = from < to ? from : to;
		double upper = from < to ? to : from;

		Extremum extremum = getExtremumBetween(from, to);
		if ((extremum != null) && !extremum.isMaximum()) {
			return extremum;
		}

		double valueLower = getValue(lower);
		double valueUpper = getValue(upper);

		if (valueLower < valueUpper) {
			return new Extremum(lower, valueLower, true);
		} else {
			return new Extremum(upper, valueUpper, true);
		}
	}

	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer("[");

		for (int i = this.coefficients.length - 1; i >= 0; i--) {
			buf.append(this.coefficients[i]);
			if (i > 0) {
				buf.append("x^");
				buf.append(i);
				buf.append(", ");
			} else {
				buf.append(']');
			}
		}

		return buf.toString();
	}
}

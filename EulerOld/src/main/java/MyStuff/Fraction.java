package MyStuff;

import java.util.List;

/**
 * Represents a fraction with numerator and denominator of LONG type. Implements
 * adding, subtracting, dividing, multiplying, raising to a given power,
 * reducing and reversing.
 * 
 * @author Andrzej *
 */
public class Fraction implements Comparable<Fraction> {
	public static void main(final String[] args) {
		Fraction test = new Fraction(11, 12);
		System.out.println(test.pow(2));
	}

	/**
	 * Creates a fraction with given numerator and denominator. If given
	 * denominator==0, ArithmeticException is thrown. * IT DOES NOT REDUCE IT.
	 * 
	 * @param num
	 *            numerator
	 * @param den
	 *            denominator
	 */
	public Fraction(final long num, final long den) {
		if (den == 0L) {
			throw new ArithmeticException("Zero in denominator!");
		}

		this.numerator = num;
		this.denominator = den;

		if ((this.numerator < 0) && (this.denominator < 0)) {
			this.numerator *= -1;
			this.denominator *= -1;
		}
	}

	/**
	 * Creates a fraction with given numerator (fraction from long value).
	 * 
	 * @param val
	 *            numerator (denominator==1)
	 */
	public Fraction(final long val) {
		this.numerator = val;
		this.denominator = 1L;
	}

	/**
	 * Creates a fraction identical to the one given as parameter. * IT DOES NOT
	 * REDUCE IT.
	 * 
	 * @param f
	 *            fraction to copy.
	 */
	public Fraction(final Fraction f) {
		this.numerator = f.numerator;
		this.denominator = f.denominator;
	}

	/**
	 * Reverses the fraction exchanging the values of numerator and denominator.
	 * It does not change this object - only returns reversed fraction. * IT
	 * DOES NOT REDUCE IT.
	 * 
	 * @return fraction created by reversing this fraction.
	 */
	public Fraction reverse() {
		return new Fraction(this.denominator, this.numerator);
	}

	/**
	 * Adds a given fraction to this fraction. * IT DOES NOT REDUCE IT.
	 * 
	 * @param f
	 *            fraction to add
	 * @return Fraction being a sum of this and given fraction.
	 */
	public Fraction add(final Fraction f) {
		return new Fraction((this.numerator * f.denominator)
				+ (f.numerator * this.denominator), this.denominator
				* f.denominator);
	}

	/**
	 * Subtracts a given fraction from this fraction. * IT DOES NOT REDUCE IT.
	 * 
	 * @param f
	 *            fraction to subtract
	 * @return Fraction being a difference between this and given fraction.
	 */
	public Fraction subtract(final Fraction f) {
		return new Fraction((this.numerator * f.denominator)
				- (f.numerator * this.denominator), this.denominator
				* f.denominator);
	}

	/**
	 * Multiplies a given fraction by this fraction. * IT DOES NOT REDUCE IT.
	 * 
	 * @param f
	 *            fraction to multiply
	 * @return Fraction being a product of this and given fraction.
	 */
	public Fraction multiply(final Fraction f) {
		return new Fraction(this.numerator * f.numerator, this.denominator
				* f.denominator);
	}

	/**
	 * Divides this fraction by a given fraction. If given fraction has
	 * numerator==0, ArithmeticException is thrown. * IT DOES NOT REDUCE IT.
	 * 
	 * @param f
	 *            fraction to divide by
	 * @return Fraction being a quotient of this and given fraction.
	 */
	public Fraction divide(final Fraction f) {
		if (f.numerator == 0L) {
			throw new ArithmeticException("Dividing by zero!");
		}

		return new Fraction(this.numerator * f.denominator, this.denominator
				* f.numerator);
	}

	/**
	 * Raises the fraction to the power of given value. * IT REDUCES IT ALL WAY
	 * LONG
	 * 
	 * @param n
	 * @return Fraction being a nth power of this fraction.
	 */
	public Fraction pow(int n) {
		if (n == 0) {
			this.numerator = 1L;
			this.denominator = 1L;
		} else if (n < 0) {
			reverse();
			n *= -1;
		}

		Fraction power = new Fraction(this.numerator, this.denominator);
		for (int i = 1; i < n; i++) {
			power = power.multiply(this);
		}

		return power;
	}

	/**
	 * Reduces the fraction. Reducing makes sure that numerator and denominator
	 * don`t have any common divisor.
	 * 
	 * @return this after operation
	 */
	public Fraction reduce() {
		long greatestCommonDivisor = 0L;
		while (true) {
			greatestCommonDivisor = MyMath
					.GCD(this.denominator, this.numerator);
			if (greatestCommonDivisor == 1L) {
				break;
			}

			this.denominator /= greatestCommonDivisor;
			this.numerator /= greatestCommonDivisor;
		}

		if ((this.numerator < 0) && (this.denominator < 0)) {
			this.numerator *= -1;
			this.denominator *= -1;
		}
		return this;
	}

	/**
	 * Converts a fraction to String of form "numerator/denominator" e.g. 1/3.
	 */
	@Override
	public String toString() {
		return this.numerator + "/" + this.denominator;
	}

	/**
	 * Hash code function where hash is equal to denominator*17 + numerator*13.
	 */
	@Override
	public int hashCode() {
		return (int) ((this.denominator * 17) + (this.numerator * 13));
	}

	/**
	 * Compares this fraction with a given object and returns true if they are
	 * equal.
	 */
	@Override
	public boolean equals(final Object other) {
		if (other == null) {
			return false;
		}

		if (!(other instanceof Fraction)) {
			return false;
		}

		Fraction otherFraction = (Fraction) other;
		return (this.numerator == otherFraction.numerator)
				&& (this.denominator == otherFraction.denominator);
	}

	/**
	 * Compares this fraction with another fraction. Returns: 1 if this fraction
	 * is greater than given fraction. -1 if this fraction is lower than given
	 * fraction. 0 if both fraction represents the same value.
	 */
	@Override
	public int compareTo(final Fraction other) {
		if (this.denominator == other.denominator) {
			long difference = this.numerator - other.numerator;

			if (difference > 0) {
				return 1;
			} else if (difference == 0) {
				return 0;
			} else {
				return -1;
			}
		}

		boolean thisNegative = (this.denominator < 0) || (this.numerator < 0);
		boolean otherNegative = (other.denominator < 0)
				|| (other.numerator < 0);

		if (thisNegative && !otherNegative) {
			return -1;
		}

		if (!thisNegative && otherNegative) {
			return 1;
		}

		//long commonDenominator = denominator*other.denominator;
		long newNumeratorThis = this.numerator * other.denominator;
		long newNumeratorOther = other.numerator * this.denominator;

		long difference = newNumeratorThis - newNumeratorOther;

		if (difference > 0) {
			return 1;
		} else if (difference == 0) {
			return 0;
		} else {
			return -1;
		}
	}

	/**
	 * Converts a fraction to double value;
	 * 
	 * @return double of the same value as the fraction.
	 */
	public double toDouble() {
		return (double) this.numerator / (double) this.denominator;
	}

	/**
	 * Tells if fraction can be written as a decimal expansion with finite
	 * decimal part number of digits.
	 * 
	 * Fraction is terminating when its denominator is of form: 2^x * 5^y where
	 * x>=0 and y>=0
	 * 
	 * @return
	 */
	public boolean isTerminatingDecimal() {
		if (this.denominator == 1L) {
			return true;
		}

		List<Long> primeDivisors = Factorizator
				.getPrimeFactorization(this.denominator);

		for (long primeDivisor : primeDivisors) {
			if ((primeDivisor != 2L) && (primeDivisor != 5L)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Tells is a fraction is proper (GCD(numerator, denominator)==1)
	 * 
	 * @return
	 */
	public boolean isProper() {
		return MyMath.GCD(this.denominator, this.numerator) == 1L;
	}

	public long getNumerator() {
		return this.numerator;
	}

	public long getDenominator() {
		return this.denominator;
	}

	private long numerator;
	private long denominator;
}


public class Problem35 
{
	/*
	 * The number, 197, is called a circular prime because all 
	 * rotations of the digits: 197, 971, and 719, are themselves prime.
	 * 
	 * There are thirteen such primes below 100: 
	 * 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.
	 * 
	 * How many circular primes are there below one million?	 */
	
	public static void main(String[] args) 
	{
		int sum = 4;  //2,3,5,7
		
		for(int i=11; i<1000000; i++)
			if(isCircularPrime(i))
				++sum;
			
		System.out.println(sum);
	}
	
	public static boolean isCircularPrime(int number)
	{
		if(number<10 || !MyMath.isPrime(number))
			return false;
		
		StringBuffer s = new StringBuffer(Integer.toString(number));
		int length = s.length();
		
		for(int i=0; i<(length-1); i++)
		{
			char first = s.charAt(0);
			for(int j=0; j<(length-1); j++)
			{
				s.setCharAt(j, s.charAt(j+1));
			}
			s.setCharAt(length-1, first);
			
			if(!MyMath.isPrime(Long.parseLong(s.toString())))
				return false;
		}
		
		return true;
	}
}



import java.math.BigInteger;

import diophante.equations.ProperPellsEquationSolver;

public class Problem139 {
	/*
	 * Let (a, b, c) represent the three sides of a right angle triangle 
	 * with integral length sides. It is possible to place four such triangles 
	 * together to form a square with length c.
	 * 
	 * For example, (3, 4, 5) triangles can be placed together to form 
	 * a 5 by 5 square with a 1 by 1 hole in the middle and it can be seen 
	 * that the 5 by 5 square can be tiled with twenty-five 1 by 1 squares.
	 * 
	 * However, if (5, 12, 13) triangles were used then the hole 
	 * would measure 7 by 7 and these could not be used to tile the 13 by 13 square.
	 * 
	 * Given that the perimeter of the right triangle is less 
	 * than one-hundred million, how many Pythagorean triangles 
	 * would allow such a tiling to take place?
	 */
	
	/*
	 * SOLUTION:
	 * 
	 * a^2 + (a+k)^2 = (kL)^2
	 * 2a^2 + 2ak + k^2 = (kL)^2
	 * (L^2 - 1)k^2 + (-2a)k + (-2a^2) = 0
	 * 
	 * delta = 4a^2 + 8a^2(L^2 - 1) = 4a^2 (2L^2 - 1)
	 * 
	 * k = (2a +- 2a*sqrt(2L^2 - 1) )/ 2(L^2 - 1)   =  a(1+sqrt(2L^2 - 1))/(L^2 - 1)
	 * 
	 * 2L^2 - 1 = x^2
	 * x^2 - 2L^2 = -1
	 * 
	 */
	public static void main(String[] args) {
		final long maxPerimeter = 100000000L;
		long result = 0L;
		ProperPellsEquationSolver solver = new ProperPellsEquationSolver(2L, -1L);
		
		while(true) {
			BigInteger[] xy = solver.getNextSolution();
			long x = xy[0].longValue();
			long L = xy[1].longValue();
			
			if(L>(maxPerimeter/2)) {
				break;
			}
			if(L==1L) {
				continue;
			}
			
			long numerator = 1 + x;
			long denominator = L*L - 1;
			
			long boundA = (maxPerimeter*(L-1)) / (2*L - 1 + x);
			
			for(long a=1; a<=boundA; a++) {
				if(a%100000==0) {
					System.out.println(a + ", " + boundA);
				}
				if( (a*numerator)%denominator==0) {
					result++;
				}
			}
		}
		
		System.out.println(result);
	}
}

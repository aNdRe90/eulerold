

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import MyStuff.Prime;

public class Problem203 {
	/*
	 * The binomial coefficients nCk can be arranged in triangular form, 
	 * Pascal's triangle, like this:
	 * 
	 * 	1	
	 * 	1		1	
	 * 	1		2		1	
	 * 	1		3		3		1	
	 * 	1		4		6		4		1	
	 * 	1		5		10		10		5		1	
	 * 	1		6		15		20		15		6		1	
	 * 1		7		21		35		35		21		7		1
	 * 
	 * .........
	 * 
	 * It can be seen that the first eight rows of Pascal's triangle 
	 * contain twelve distinct numbers: 1, 2, 3, 4, 5, 6, 7, 10, 15, 20, 21 and 35.
	 * 
	 * A positive integer n is called squarefree if no square of a prime divides n. 
	 * Of the twelve distinct numbers in the first eight rows of Pascal's triangle, 
	 * all except 4 and 20 are squarefree. 
	 * The sum of the distinct squarefree numbers in the first eight rows is 105.
	 * 
	 * Find the sum of the distinct squarefree numbers 
	 * in the first 51 rows of Pascal's triangle.
	 */
	public static void main(final String[] args) {
		final int N = 51;
		long sum = 0L;

		Set<Long> pascalValues = getDistinctPascalValuesUpToRow(N);
		long max = getMaxValue(pascalValues);

		long sqrtMax = (long) Math.sqrt(max);
		List<Long> primeSquares = getPrimeSquaresUnder(sqrtMax);

		int size = pascalValues.size();
		int counter = 0;
		for (long pascalValue : pascalValues) {
			System.out.println((++counter) + "/" + size);

			if (isSquareFree(pascalValue, primeSquares)) {
				sum += pascalValue;
			}
		}

		System.out.println(sum);
	}

	private static boolean isSquareFree(final long pascalValue,
			final List<Long> primeSquares) {
		for (long primeSquare : primeSquares) {
			if (primeSquare > pascalValue) {
				break;
			}

			if ((pascalValue % primeSquare) == 0L) {
				return false;
			}
		}

		return true;
	}

	private static List<Long> getPrimeSquaresUnder(final long sqrtMax) {
		List<Long> primeSquares = new ArrayList<Long>(1024);
		primeSquares.add(4L);

		for (long i = 3L; i < sqrtMax; i += 2L) {
			if (Prime.isPrime(i)) {
				primeSquares.add(i * i);
			}
		}

		return primeSquares;
	}

	private static long getMaxValue(final Set<Long> values) {
		long max = Long.MIN_VALUE;

		for (long v : values) {
			if (v > max) {
				max = v;
			}
		}

		return max;
	}

	private static Set<Long> getDistinctPascalValuesUpToRow(final int row) {
		Set<Long> pascalValues = new HashSet<Long>();

		for (int n = 0; n < row; n++) {
			int maxIndex = n / 2;

			for (int i = 0; i <= maxIndex; i++) {
				pascalValues.add(MyStuff.MyMath.binomialCoefficient(n, i)
						.longValue());
			}
		}

		return pascalValues;
	}
}

import java.util.HashSet;
import MyStuff.Fraction;
import MyStuff.MyMath;

public class Problem72 
{
	/*
	 * Consider the fraction, n/d, where n and d are positive integers. 
	 * If n<d and HCF(n,d)=1, it is called a reduced proper fraction.
	 * 
	 * If we list the set of reduced proper fractions for d <= 8 in ascending order of size, we get:
	 * 
	 * 1/8, 1/7, 1/6, 1/5, 1/4, 2/7, 1/3, 3/8, 2/5, 3/7, 1/2, 4/7, 3/5, 5/8, 2/3, 5/7, 3/4, 4/5, 5/6, 6/7, 7/8
	 * 
	 * It can be seen that there are 21 elements in this set.
	 * 
	 * How many elements would be contained in the set of reduced proper fractions for d <= 1,000,000?
	 * 
	 */
	
	/*
	 * We need to find the sum of phi(d)
	 */
	public static void main(String[] args) 
	{
		//HashSet<Fraction> set = new HashSet<Fraction>(10000);
		long howMany = 0;
		Solver one = new Solver(2, 600000);
		Solver two = new Solver(600001, 1000000);
		
		one.start();
		two.start();
		while(!one.finished || !two.finished)
		{
			try
			{
				Thread.sleep(1000);
			}
			catch(Exception e)
			{ e.printStackTrace(); }
		}
		
		howMany += one.getCounter();
		howMany += two.getCounter();
		System.out.println(howMany);
	}

}

class Solver extends Thread
{
	public Solver(int start, int end)
	{
		this.start = start;
		this.end = end;
	}
	
	public void run()
	{
		for(int d=start; d<=end; d++)
		{
			if(d%10000==0)
				System.out.println(d);
			counter += MyMath.getPhi(d);
		}
		finished = true;
	}
	
	public long getCounter() { return counter; }
	
	private int start;
	private int end;
	public boolean finished = false;
	private long counter = 0L;
}

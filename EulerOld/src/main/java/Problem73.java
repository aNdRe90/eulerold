import java.util.HashSet;

import MyStuff.Fraction;


public class Problem73 
{
	/*
	 * Consider the fraction, n/d, where n and d are positive integers. 
	 * If n<d and HCF(n,d)=1, it is called a reduced proper fraction.
	 * 
	 * If we list the set of reduced proper fractions for d <= 8 in ascending order of size, we get:
	 * 
	 * 1/8, 1/7, 1/6, 1/5, 1/4, 2/7, 1/3, 3/8, 2/5, 3/7, 1/2, 4/7, 3/5, 5/8, 2/3, 5/7, 3/4, 4/5, 5/6, 6/7, 7/8
	 * 
	 * It can be seen that there are 3 fractions between 1/3 and 1/2.
	 * 
	 * How many fractions lie between 1/3 and 1/2 in the sorted set of reduced proper fractions for d <= 12,000?
	 * 
	 * Note: The upper limit has been changed recently.
	 */

	/*
	 * REQUIRES INCREASING JAVA HEAP SPACE TO ABOUT 512MB
	 */
	public static void main(String[] args) 
	{
		double lowerBound = 1.0/3.0;
		double upperBound = 0.5;
		
		HashSet<Fraction> set = new HashSet<Fraction>(10000000);
		
		for(int d=2; d<=12000; d++)
		{			
			System.out.println(d);
			long nMax = (long) Math.floor(upperBound*(double)d);
			long nMin = (long) Math.floor(lowerBound*(double)d);
			
			for(long n=nMin; n<=nMax; n++)
			{
				Fraction current = new Fraction(n, d).reduce();
				double currentDouble = current.toDouble();
				
				if(currentDouble>lowerBound && currentDouble<upperBound)
					set.add(current);
					
			}
		}
		System.out.println(set.size());
	}
}

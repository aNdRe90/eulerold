import java.math.BigDecimal;
import java.util.ArrayList;


public class Problem33
{
	/*
	 * The fraction 49/98 is a curious fraction, as an inexperienced mathematician 
	 * in attempting to simplify it may incorrectly believe that 49/98 = 4/8, 
	 * which is correct, is obtained by cancelling the 9s.
	 * 
	 * We shall consider fractions like, 30/50 = 3/5, to be trivial examples.
	 * 
	 * There are exactly four non-trivial examples of this type of fraction, 
	 * less than one in value, and containing two digits in the numerator and denominator.
	 * 
	 * If the product of these four fractions is given in its lowest common terms, 
	 * find the value of the denominator.
	 */
	public static void main(String[] args) 
	{
		ArrayList<Pair> pairs = new ArrayList<Pair>();
		
		for(int a=10; a<100; a++)
			for(int b=(a+1); b<100; b++)
			{
				if(a%10==0 && b%10==0)
					continue;
				
				try
				{
					BigDecimal fraction = BigDecimal.valueOf(a).divide(BigDecimal.valueOf(b));
					
					for(int i=0; i<2; i++)
						for(int j=0; j<2; j++)
						{
							BigDecimal simple = simplifiedFraction(a,b,i,j);
							if(simple!=null && fraction.equals(simple))
							{
								pairs.add(new Pair(a,b));
							}
						}
				}
				catch(ArithmeticException e)
				{
					//if here then fraction has non-finite decimal part
					//and we won`t consider it
				}
				
			}
		
		long numerator = 1L;
		long denominator = 1L;
		for(int i=0; i<pairs.size(); i++)
		{
			numerator *= (long) pairs.get(i).a;
			denominator *= (long) pairs.get(i).b;
		}
		
		System.out.println(MyMath.reduceFraction(numerator, denominator)[1]);
	}
	
	public static BigDecimal simplifiedFraction(int a, int b, int whichFromAtoErase, int whichFromBtoErase)
	{
		StringBuffer newA = new StringBuffer(Integer.toString(a));
		StringBuffer newB = new StringBuffer(Integer.toString(b));
		
		if(newA.charAt(whichFromAtoErase)!=newB.charAt(whichFromBtoErase))
			return null;
		
		newA.deleteCharAt(whichFromAtoErase);
		newB.deleteCharAt(whichFromBtoErase);
		
		try
		{
			BigDecimal simplifiedFraction = new BigDecimal(newA.toString());
			simplifiedFraction = simplifiedFraction.divide(new BigDecimal(newB.toString()));
			return simplifiedFraction;
		}
		catch(ArithmeticException e)	{		}		
		
		return null;
	}
}

class Pair
{
	public Pair(int a, int b)
	{
		this.a = a;
		this.b = b;
	}
	public int a;
	public int b;
}



import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import MyStuff.Factorizator;
import MyStuff.Prime;

public class Problem263 {
	/*
	 * Consider the number 6. The divisors of 6 are: 1,2,3 and 6.
	 * 
	 * Every number from 1 up to and including 6 can be written 
	 * as a sum of distinct divisors of 6:
	 * 1=1, 2=2, 3=1+2, 4=1+3, 5=2+3, 6=6.
	 * 
	 * A number n is called a practical number if every number from 1 up to 
	 * and including n can be expressed as a sum of distinct divisors of n.
	 * 
	 * A pair of consecutive prime numbers with a difference of six is called 
	 * a sexy pair (since "sex" is the Latin word for "six"). 
	 * The first sexy pair is (23, 29).
	 * 
	 * We may occasionally find a triple-pair, which means three consecutive 
	 * sexy prime pairs, such that the second member of each pair is the first 
	 * member of the next pair.
	 * 
	 * We shall call a number n such that :
	 * 
	 *     (n-9, n-3), (n-3,n+3), (n+3, n+9) form a triple-pair, and
	 *     the numbers n-8, n-4, n, n+4 and n+8 are all practical, 
	 *     
	 * an engineers’ paradise.
	 * 
	 * Find the sum of the first four engineers’ paradises.
	 */

	/*
	 * SOLUTION:
	 * Every prime >3 may be written as 6*a +- 1 where a = 1,2,...
	 * 
	 * 1.5h brute force :)
	 */
	public static void main(String[] args) {
		//System.out.println(isPractical(36));
		final int N = 4;
		List<Long> paradises = new ArrayList<Long>(N);
		LinkedList<Character> lastPrimes = new LinkedList<Character>();
		List<Long> primes = Prime.getPrimesUnder((long) 1e6);
		long counter = 0;

		for (long a = 1L;; a++) {
			//			if (isPractical(a, primes)) {
			//				System.out.println(a);
			//			}

			long x = (6L * a) - 1L;
			long y = x + 2L;

			if ((a % 100000L) == 0L) {
				System.out.println(x);
			}

			boolean xPrime = Prime.isPrime(x);
			boolean yPrime = Prime.isPrime(y);

			if (!xPrime && !yPrime) {
				lastPrimes.clear();
				continue;
			}

			//			if (xPrime && yPrime) {
			//				lastPrimes.clear();
			//				continue;
			//			}

			//char toAdd = '-';
			if (xPrime) {
				if (!lastPrimes.isEmpty() && ('x' != lastPrimes.getLast())) {
					lastPrimes.clear();
				}
				lastPrimes.add('x');

				if (lastPrimes.size() == 4) {
					long n = x - 9L;
					//System.out.println(counter++);

					if (isPractical(n - 8L, primes)
							&& isPractical(n - 4L, primes)
							&& isPractical(n, primes)
							&& isPractical(n + 4L, primes)
							&& isPractical(n + 8L, primes)) {
						paradises.add(n);

						System.out.println("JEEEEEEST! " + n);
						if (paradises.size() == N) {
							break;
						}
					}
					//znaleziono tripple sexy
					lastPrimes.removeFirst();
				}
			}

			if (yPrime) {
				if (!lastPrimes.isEmpty() && ('y' != lastPrimes.getLast())) {
					lastPrimes.clear();
				}
				lastPrimes.add('y');

				if (lastPrimes.size() == 4) {
					long n = y - 9L;
					//System.out.println(counter++);

					if (isPractical(n - 8L, primes)
							&& isPractical(n - 4L, primes)
							&& isPractical(n, primes)
							&& isPractical(n + 4L, primes)
							&& isPractical(n + 8L, primes)) {
						paradises.add(n);

						System.out.println("JEEEEEEST! " + n);
						if (paradises.size() == N) {
							break;
						}
					}
					//znaleziono tripple sexy
					lastPrimes.removeFirst();
				}
			}
		}

		long result = getSum(paradises);
		System.out.println("RESULT = " + result);
	}

	private static boolean isPractical(long n, List<Long> primeList) {
		if ((n < 1L) || ((n % 2L) == 1L)) {
			return false;
		}

		List<Long> primeWithPowers = Factorizator
				.getPrimeFactorizationWithPowers(n, primeList);
		long[] primes = getPrimes(primeWithPowers);
		long[] exps = getExponents(primeWithPowers);

		for (int i = 1; i < primes.length; i++) {
			double prime = primes[i];
			double fancySum = 1.0;

			for (int j = 0; j <= (i - 1); j++) {
				fancySum *= (Math.pow(primes[j], exps[j] + 1.0) - 1.0)
						/ (primes[j] - 1.0);
			}

			fancySum += 1.0;
			if (prime > fancySum) {
				return false;
			}
		}

		return true;
	}

	private static long[] getExponents(List<Long> factorization) {
		int size = factorization.size();
		long[] exponents = new long[size / 2];

		int k = 0;
		for (int i = 1; i < size; i += 2) {
			exponents[k++] = factorization.get(i);
		}

		return exponents;
	}

	private static long[] getPrimes(List<Long> factorization) {
		int size = factorization.size();
		long[] primes = new long[size / 2];

		int k = 0;
		for (int i = 0; i < size; i += 2) {
			primes[k++] = factorization.get(i);
		}

		return primes;
	}

	private static int[] getBases(int size) {
		int[] bases = new int[size];

		for (int i = 0; i < size; i++) {
			bases[i] = 2;
		}

		return bases;
	}

	private static long getSum(List<Long> values) {
		long sum = 0L;

		for (long v : values) {
			sum += v;
		}

		return sum;
	}
}

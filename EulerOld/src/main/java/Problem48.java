
public class Problem48 
{
	/*
	 * The series, 1^1 + 2^2 + 3^3 + ... + 10^10 = 10405071317.

	   Find the last ten digits of the series, 1^1 + 2^2 + 3^3 + ... + 1000^1000.
	 */
	
	/*
	 * SOLUTION:
	 * It is simplier if we operate only on the last ten digits all the time.
	 */
	public static void main(String[] args) 
	{
		long sum = 0;
		for(int i=1; i<1001; i++)
		{
			long valueToAdd = i;
			for(int j=2; j<=i; j++)
			{
				valueToAdd *= i;
				valueToAdd = truncateToLastTenDigits(valueToAdd);
			}
			sum += valueToAdd;
			sum = truncateToLastTenDigits(sum);
		}
		
		System.out.println(sum);
	}
	
	public static long truncateToLastTenDigits(long v)
	{
		String val = Long.toString(v);
		if(val.length()<11)
			return v;
		else
			return Long.parseLong(val.substring(val.length()-10));
	}
}

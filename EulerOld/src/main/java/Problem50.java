import java.util.ArrayList;


public class Problem50 
{
	/*
	 * The prime 41, can be written as the sum of six consecutive primes:
		41 = 2 + 3 + 5 + 7 + 11 + 13

		This is the longest sum of consecutive primes that adds to a prime below one-hundred.

		The longest sum of consecutive primes below one-thousand that adds to a prime, 
		contains 21 terms, and is equal to 953.

		Which prime, below one-million, can be written as the sum of the most consecutive primes?
	 */
	public static void main(String[] args) 
	{
		primes = new ArrayList<Integer>(1000);
		for(int i=2; i<1000000 ; i++)
		{
			if(MyMath.isPrime(i))			
				primes.add(new Integer(i));			
		}
		
		int length = primes.size();
		int result = 0;
		int maxConsecutiveAdded = 0;
		
		for(int startingPrimeIndex = 0; startingPrimeIndex<length; startingPrimeIndex++)
		{			
			int howManyConsecutiveAdded = 0;
			
			int currentSum = 0;
			for(int i=startingPrimeIndex; ; i++)
			{
				int toAdd = primes.get(i).intValue();
				if((currentSum+toAdd)<999983/*max prime<1.000.000*/)
				{
					currentSum += toAdd;
					howManyConsecutiveAdded++;
				}
				else
					break;
			}
			
			if(MyMath.isPrime(currentSum) && howManyConsecutiveAdded>maxConsecutiveAdded)
			{
				maxConsecutiveAdded = howManyConsecutiveAdded;
				result = currentSum;
			}
		}
		
		System.out.println(result);
	}
	
	private static ArrayList<Integer> primes = null;
}

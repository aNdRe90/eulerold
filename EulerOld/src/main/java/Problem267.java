

import java.math.BigDecimal;
import java.math.BigInteger;

public class Problem267 {
	/*
	 * You are given a unique investment opportunity.
	 * Starting with $1 of capital, you can choose a fixed proportion, 
	 * f, of your capital to bet on a fair coin toss repeatedly for 1000 tosses.
	 * 
	 * Your return is double your bet for heads and you lose your bet for tails.
	 * 
	 * For example, if f = 1/4, for the first toss you bet $0.25, and if heads comes 
	 * up you win $0.5 and so then have $1.5. You then bet $0.375 and if the second 
	 * toss is tails, you have $1.125.
	 * 
	 * Choosing f to maximize your chances of having at least $1,000,000,000 
	 * after 1,000 flips, what is the chance that you become a billionaire?
	 * 
	 * All computations are assumed to be exact (no rounding), but give your 
	 * answer rounded to 12 digits behind the decimal point in the form 0.abcdefghijkl.
	 */

	/*
	 * SOLUTION:
	 * At first I thought i need to find bestF solving expected value, it
	 * turned out that EX = max when f=1.0.
	 * Along the way I noticed that the result depends only on number of WIN/LOSE
	 * not on their order (VERY IMPORTANT!)
	 * 
	 * This gives me the clue that the best f to win 1,000,000,000 is the one that
	 * ensures minimum win-flips to get this value.
	 * 
	 * Polynomial for winning is (S + 2Sx) and for losing is (S-Sx).
	 * If S=1:
	 * WIN = (1+2x)
	 * LOSE = (1-x)
	 * 
	 * My BigPolynomial class as well as Polynomial seems not work with such high
	 * powers...
	 * So I tried another way - logarithms.
	 * 
	 * 
	 * *****log(x) = log(e,x)*************
	 * We need to find such w that (1+2x)^w * (1-x)^(1000-w) >= 10^9
	 * w*log(1+2x) + (1000-w)*log(1-x) >= 9log(10)
	 * 
	 * w*log( (1+2x)/(1-x) ) >= 9log(10) - 1000log(1-x)
	 * w >= [ 9log(10) - 1000log(1-x) ] / log( (1+2x)/(1-x) )
	 * 
	 * Let f(x) = [ 9log(10) - 1000log(1-x) ] / log( (1+2x)/(1-x) )
	 * Then f`(x) = 
	 * 		27 log(10)-3000 log(1-x)-1000 (1+2 x) log((1+2 x)/(1-x))
	 * 		---------------------------------------------------------
	 * 		(x-1)(1+2x) * log^2 ( (1+2x)/(1-x)  )
	 * 
	 * And using wolframalfa.com  i get that f`(x) = 0  when 
	 * x = 0.146883922440940676575582401...
	 * 
	 * And it is minimum.
	 */
	public static void main(final String[] args) {
		//		final int tosses = 1000;
		//		final double f = 0.14688392244094067658;
		//
		//		double numerator = (9.0 * Math.log(10.0)) - (1000.0 * Math.log(1 - f));
		//		double denominator = Math.log((1.0 + (2.0 * f)) / (1.0 - f));
		//		int winningsNeeded = (int) Math.ceil(numerator / denominator);
		//
		//		System.out.println(getProbability(winningsNeeded, tosses));
		System.out.println(getProbability(1000, 1e9, 0.146));
	}

	private static double getProbability(int tosses, double prize, double f) {
		double numerator = (Math.log(prize)) - (tosses * Math.log(1 - f));
		double denominator = Math.log((1.0 + (2.0 * f)) / (1.0 - f));
		int winningsNeeded = (int) Math.ceil(numerator / denominator);

		return getProbability(winningsNeeded, tosses);
	}

	//		BigDecimal expectedPrize = BigDecimal.valueOf(1e9);
	//
	//		BigPolynomial winPolynomial = new BigPolynomial(BigDecimal.valueOf(S),
	//				BigDecimal.valueOf(2.0 * S));//new Polynomial(S, 2.0 * S);
	//		BigPolynomial losePolynomial = new BigPolynomial(BigDecimal.valueOf(S),
	//				BigDecimal.valueOf(-S));//new Polynomial(S, -S);
	//
	//		for (int winnings = 900; winnings <= tosses; winnings++) {
	//			System.out.println("Winnings = " + winnings);
	//
	//			BigPolynomial winPow = winPolynomial.pow(winnings);
	//			BigPolynomial losePow = losePolynomial.pow(tosses - winnings);
	//			BigPolynomial current = winPow.multiply(losePow);
	//
	//			BigExtremum extremum = current.getMaxValueBetween(BigDecimal.ZERO,
	//					BigDecimal.ONE);
	//			System.out.println(extremum);
	//			if (extremum.getValue().compareTo(expectedPrize) >= 0) {
	//				double probability = getProbability(winnings, tosses);
	//
	//				System.out.println("BestF = " + extremum.getX());
	//				System.out.println("Min winnings = " + winnings);
	//				System.out.println("Probability to win " + expectedPrize
	//						+ " = " + probability);
	//				break;
	//			}
	//		}
	//	}
	//
	private static double getProbability(int winnings, int tosses) {
		BigInteger p = BigInteger.ZERO;

		for (int i = winnings; i <= tosses; i++) {
			p = p.add(MyStuff.MyMath.binomialCoefficient(tosses, i));
		}

		BigDecimal numerator = new BigDecimal(p);
		BigDecimal denominator = BigDecimal.valueOf(Math.pow(2.0, tosses));

		return numerator.divide(denominator, 15, BigDecimal.ROUND_HALF_UP)
				.doubleValue();
	}
}

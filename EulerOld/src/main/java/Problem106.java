

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import MyStuff.CombinationGenerator;

public class Problem106 {
	/*
	 * Let S(A) represent the sum of elements in set A of size n. 
	 * We shall call it a special sum set if for any two non-empty disjoint subsets, 
	 * B and C, the following properties are true:
	 * 
	 *     S(B) != S(C); that is, sums of subsets cannot be equal.
	 *     If B contains more elements than C then S(B) > S(C).
	 *     
	 * For this problem we shall assume that a given set contains n strictly 
	 * increasing elements and it already satisfies the second rule.
	 * 
	 * Surprisingly, out of the 25 possible subset pairs that can be obtained from 
	 * a set for which n = 4, only 1 of these pairs need to be tested for equality 
	 * (first rule). Similarly, when n = 7, only 70 out of the 966 subset pairs need 
	 * to be tested.
	 * 
	 * For n = 12, how many of the 261625 subset pairs that can be obtained 
	 * need to be tested for equality?
	 * 
	 * NOTE: This problem is related to problems 103 and 105.
	 */
	public static void main(String[] args) {
		int[] set = {1,2,3,4,5,6,7,8,9,10,11,12};
		int counter = 0;
		
		for(int i=2; i<=set.length/2; i++) {
			CombinationGenerator combGenA = new CombinationGenerator(set.length, i);
			
			while (combGenA.hasMore()) {
				List<Integer> positionsToCombinateFrom = getPositionsToCombinateFrom(set.length);
				int[] combinationA = getCombinationFrom(combGenA.getNext(),
						positionsToCombinateFrom);
				positionsToCombinateFrom = removeElementsFromPositions(
						positionsToCombinateFrom, combinationA);

				CombinationGenerator combGenB = new CombinationGenerator(
						set.length - i, i);

				while (combGenB.hasMore()) {
					int[] combinationB = getCombinationFrom(combGenB.getNext(),
							positionsToCombinateFrom);

					int[] subsetA = getSubsetOfElements(set, combinationA);
					int[] subsetB = getSubsetOfElements(set, combinationB);

					if (mustBeCheckedForEquality(subsetA, subsetB))
						counter++;
				}
			}
		}
		
		System.out.println(counter/2);
	}
	
	private static boolean mustBeCheckedForEquality(int[] subsetA, int[] subsetB) {
		if(subsetA.length!=subsetB.length || subsetA.length<1 || subsetB.length<1)
			return false;
		
		int[] higherValues = subsetA[0]>subsetB[0] ? subsetA : subsetB;
		int[] lowerValues = subsetA[0]>subsetB[0] ? subsetB : subsetA;			
		int counter = 0;
		
		for(int i=0; i<subsetA.length; i++) 
			if(higherValues[i]>lowerValues[i])
				counter++;
		
		return counter!=subsetA.length;
	}

	private static int[] getCombinationFrom(int[] combination, 
			List<Integer> positions) {
		int[] combs = combination.clone();

		for (int i = 0; i < combs.length; i++)
			combs[i] = positions.get(combination[i]);

		return combs;
	}

	private static List<Integer> removeElementsFromPositions(
			List<Integer> positions, int[] combination) {

		for (int i = 0; i < combination.length; i++) {
			if (positions.contains(combination[i])) {
				positions.remove(positions.indexOf(combination[i]));
			}
		}

		return positions;
	}

	private static List<Integer> getPositionsToCombinateFrom(int length) {
		List<Integer> positions = new ArrayList<Integer>(length);

		for (int i = 0; i < length; i++)
			positions.add(i);

		return positions;
	}
	
	private static int[] getSubsetOfElements(int[] set, int[] combination) {
		int[] subset = new int[combination.length];
		
		for(int i=0; i<subset.length; i++)
			subset[i] = set[combination[i]];
			
		return subset;
	}
	
	private static int getSumOfElements(int[] set) {
		int sum = 0;
		
		for(int element : set) 
			sum += element;
		
		return sum;
	}
}

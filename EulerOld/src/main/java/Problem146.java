

import MyStuff.Prime;

public class Problem146 {
	/*
	 * The smallest positive integer n for which the numbers n^2+1, n^2+3,
	 * n^2+7, n^2+9, n^2+13, and n^2+27 are consecutive primes is 10. The sum of
	 * all such integers n below one-million is 1242490.
	 * 
	 * What is the sum of all such integers n below 150 million?
	 */

	/*
	 * SOLUTION n^2 + 1 is prime only when n*n is even, so n is even.
	 * 
	 * if n ends with 2: n^2 + 1 = .... 5 CANNOT BE
	 * 
	 * if n ends with 4: n^2 + 1 = .... 7 CAN BE n^2 + 3 = .... 9 CAN BE n^2 + 7
	 * = .... 3 CAN BE n^2 + 9 = .... 5 CANNOT BE
	 * 
	 * if n ends with 6: n^2 + 1 = .... 7 CAN BE n^2 + 3 = .... 9 CAN BE n^2 + 7
	 * = .... 3 CAN BE n^2 + 9 = .... 5 CANNOT BE
	 * 
	 * if n ends with 8: n^2 + 1 = .... 5 CANNOT BE
	 * 
	 * if n ends with 10: CAN BE FOR ALL n^2 + 1, n^2 + 3, n^2 + 7, n^2 + 9, n^2
	 * + 13, n^2 + 27
	 * 
	 * So n is divisble by 10, and I will now write the problem as 100n^2 + 1
	 * where n<15.000.000
	 * 
	 * Moreover, every prime except 2,3 can be respresented by p = 6k +- 1 where
	 * k is integer. Therefore if we have to have p+2 prime (and so on in the
	 * problem), then we need to conclude that 100n^2 + 1 = 6k - 1:
	 * 
	 * 100n^2 + 1 = 6k - 1 50n^2 - 1 = 3k => ONLY IF n%3!=0
	 */
	public static void main(final String[] args) {
		final long N = 150000000L;
		final long nBound = N / 10L;
		long sum = 0L;

		for (long n = 1L; n < nBound; n++) {
			if ((n % 3L) == 0) {
				continue;
			}

			if ((n % 1024L) == 0) {
				System.out.println(n);
			}

			final long n2mult100 = 100L * n * n;
			if (Prime.isPrimeMillerRabin(n2mult100 + 1L, 10)
					&& Prime.isPrimeMillerRabin(n2mult100 + 3L, 10)
					&& Prime.isPrimeMillerRabin(n2mult100 + 7L, 10)
					&& Prime.isPrimeMillerRabin(n2mult100 + 9L, 10)
					&& Prime.isPrimeMillerRabin(n2mult100 + 13L, 10)
					&& !Prime.isPrimeMillerRabin(n2mult100 + 19L, 10)
					&& !Prime.isPrimeMillerRabin(n2mult100 + 21L, 10)
					&& Prime.isPrimeMillerRabin(n2mult100 + 27L, 10)) {
				sum += n;
			}
		}

		System.out.println(sum * 10L);
	}
}

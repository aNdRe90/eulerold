

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import MyStuff.VariationNumber;

public class Problem109 {
	/*
	 * In the game of darts a player throws three darts at a target board 
	 * which is split into twenty equal sized sections numbered one to twenty.
	 * 
	 * The score of a dart is determined by the number of the region that the dart lands in. 
	 * A dart landing outside the red/green outer ring scores zero. 
	 * The black and cream regions inside this ring represent single scores. 
	 * However, the red/green outer ring and middle ring score double and 
	 * treble scores respectively.
	 * 
	 * At the centre of the board are two concentric circles called the bull region, 
	 * or bulls-eye. The outer bull is worth 25 points and the inner bull is a double,
	 * worth 50 points.
	 * 
	 * There are many variations of rules but in the most popular game the players 
	 * will begin with a score 301 or 501 and the first player to reduce their 
	 * running total to zero is a winner. However, it is normal to play a "doubles out" 
	 * system, which means that the player must land a double (including the double bulls-eye 
	 * at the centre of the board) on their final dart to win; any other dart that would reduce 
	 * their running total to one or lower means the score for that set of three darts is "bust".
	 * 
	 * When a player is able to finish on their current score it is called a "checkout" 
	 * and the highest checkout is 170: T20 T20 D25 (two treble 20s and double bull).
	 * 
	 * There are exactly eleven distinct ways to checkout on a score of 6:
	 * 
	 * 	D3 	 
		D1 	D2 	 
		S2 	D2 	 
		D2 	D1 	 
		S4 	D1 	 
		S1 	S1 	D2
		S1 	T1 	D1
		S1 	S3 	D1
		D1 	D1 	D1
		D1 	S2 	D1
		S2 	S2 	D1
		
		Note that D1 D2 is considered different to D2 D1 as they finish on different doubles. 
		However, the combination S1 T1 D1 is considered the same as T1 S1 D1.
		
		In addition we shall not include misses in considering combinations; 
		for example, D3 is the same as 0 D3 and 0 0 D3.
		
		Incredibly there are 42336 distinct ways of checking out in total.
		
		How many distinct ways can a player checkout with a score less than 100?
	 */
	public static void main(String[] args) {
		final int N = 100;
		allFields = getInitializedFieldValues();
		doubleFields = getDoubleFields(allFields);
		
		long sum = 0L;
		for(int i=1; i<N; i++) {
			sum += getWaysOfCheckoutFrom(i);
		}
		
		System.out.println(sum);
	}
	
	private static int numberOfFields = 62;
	private static List<Field> allFields;
	private static List<Field> doubleFields;
	
	private static long getWaysOfCheckoutFrom(int score) {
 		long ways = 0L;
		
		for(Field doubleField : doubleFields) {
			int doubleScore = doubleField.getScore();
			
			if(doubleScore==score)
				ways++;
			else if(doubleScore<score) {
				ways += getWaysWhereScoreXCanBeAchievedInYMoves(score - doubleScore, 2);
				ways += getWaysWhereScoreXCanBeAchievedInYMoves(score - doubleScore, 1);
			}			
			else
				break;
		}		

		return ways;
	}

	private static long getWaysWhereScoreXCanBeAchievedInYMoves(int score, int moves) {		
		//WORKS ONLY IF move==2 or moves==1  !!!
		
		int[] bases = getBasesForMoves(moves);
		VariationNumber number = new VariationNumber(bases);
		long waysDifferent = 0L;
		long waysEqual = 0L;
		
		while(!number.isOverflow()) {
			int[] fieldsHitNumbers = number.getValues();
			Field[] fieldsHit = getFields(fieldsHitNumbers);
			int sumOfScores = getSumOfScoresOnFields(fieldsHit);
			
			if(sumOfScores==score) {
				if(moves==2 && fieldsHitNumbers[0]!=fieldsHitNumbers[1])
					waysDifferent++;
				else 
					waysEqual++;
			}
			
			number.increment();
		}
		
		return waysDifferent/2 + waysEqual;
	}

	private static int getSumOfScoresOnFields(Field[] fieldsHit) {
		int sum = 0;
		
		for(Field f : fieldsHit)
			sum += f.getScore();
		
		return sum;
	}

	private static Field[] getFields(int[] fieldsHitNumbers) {
		Field[] fields = new Field[fieldsHitNumbers.length];
		
		for(int i=0; i<fields.length; i++)
			fields[i] = allFields.get(fieldsHitNumbers[i]);
		
		return fields;
	}

	private static int[] getBasesForMoves(int moves) {
		int[] bases = new int[moves];
		
		for(int i=0; i<bases.length; i++)
			bases[i] = numberOfFields;
		
		return bases;
	}

	private static List<Field> getDoubleFields(List<Field> allFields) {
		List<Field> doubleFields = new ArrayList<Field>(numberOfFields/3);
		
		for(Field f : allFields) {
			int multiplier = f.getMultiplier();
			if(multiplier==2) 
				doubleFields.add(new Field(f.getValue(), multiplier));
		}
		
		return doubleFields;
	}

	private static List<Field> getInitializedFieldValues() {
		List<Field> fields = new ArrayList<Field>(numberOfFields);
		int maxOuterFieldValue = 20;
		int maxMultiplier = 3;
		
		for(int i=1; i<=maxOuterFieldValue; i++)
			for(int j=1; j<=maxMultiplier; j++)
				fields.add(new Field(i, j));
		
		fields.add(new Field(25,1));
		fields.add(new Field(25,2));
		
		return fields;
	}
}

class Field {
	private int value;
	private int multiplier;
	private static Map<Integer, Character> signatures = new HashMap<Integer, Character>();
	static {
		signatures.put(1, 'S');
		signatures.put(2, 'D');
		signatures.put(3, 'T');
	}
	
	public Field(int value, int multiplier) {
		this.value = value;
		this.multiplier = multiplier;
	}
	
	public int getMultiplier() {
		return multiplier;
	}
	
	public int getValue() {
		return value;
	}
	
	public int getScore() {
		return value*multiplier;
	}
	
	@Override
	public String toString() {
		return signatures.get(multiplier) + Integer.toString(value);
	}
}

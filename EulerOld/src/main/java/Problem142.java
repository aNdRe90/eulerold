

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Problem142 {
	/*
	 * Find the smallest x + y + z with integers x > y > z > 0 
	 * such that x + y, x - y, x + z, x - z, y + z, y - z 
	 * are all perfect squares.
	 */
	
	/*
	 * SOLUTION:
	 * y + z = k^2
	 * y - z = l^2
	 * 
	 * thus:   
	 * 		y = (k^2 + l^2)/2
	 * 		z = (k^2 - l^2)/2
	 * 
	 * 
	 * x + z = m^2
	 * x - z = n^2
	 * 
	 * thus:   
	 * 		x = (m^2 + n^2)/2
	 * 		z = (m^2 - n^2)/2
	 * 
	 * x + y = p^2
	 * x - y = r^2
	 * 
	 * thus:   
	 * 		x = (p^2 + r^2)/2
	 * 		y = (p^2 - r^2)/2
	 * 
	 * 
	 * So:
	 * y = (p^2 - r^2)/2 = (k^2 + l^2)/2
	 * x = (m^2 + n^2)/2 = (p^2 + r^2)/2
	 * z = (k^2 - l^2)/2 = (m^2 - n^2)/2
	 * 
	 * Lets have:
	 * a = k^2/2
	 * b = l^2/2
	 * c = m^2/2
	 * d = n^2/2
	 * e = p^2/2
	 * f = r^2/2
	 * 
	 * Then:
	 * y = e-f = a+b		=> a = e-f-b
	 * x = c+d = e+f
	 * z = a-b = c-d		=> e-f-b-b = c-d  => c = e+d-f-2b
	 * 
	 * y = e-f
	 * x = e+f = y+2f
	 * z = c-d = e+d-f-2b-d = e-f-2b = y-2b
	 * 
	 * x = y + r^2
	 * y = (p^2 - r^2)/2
	 * z = y - l^2
	 * 
	 * -----------------------------------------------
	 * x + y = 2y + r^2 = p^2   	IS SQUARE
	 * x - y = y + r^2 - y = r^2	IS SQUARE
	 * 
	 * x + z = y + r^2 + y - l^2 = p^2 - r^2 + r^2 - l^2 = p^2 - l^2   CHECK IF ITS SQUARE!
	 * x - z = y + r^2 - y + l^2 = r^2 + l^2	CHECK IF ITS SQUARE!
	 * 
	 * y + z = 2y - l^2 = p^2 - r^2 - l^2		CHECK IF ITS SQUARE!
	 * y - z = y - y + l^2 = l^s	IS SQUARE
	 * -----------------------------------------------
	 * 
	 * From statements above:
	 * p^2 - l^2 > 0    => p>l
	 * p^2 - r^2 - l^2 >0  => r^2 < p^2 + l^2
	 * 
	 * Then simple brute force for proper bounds given above.
	 */
	private static Set<Long> squares = getSqauresBelow((long) 10e8);
	
	public static void main(String[] args) {		
		long minResult = Long.MAX_VALUE;
		long minX = -1L;
		long minY = -1L;
		long minZ = -1L;
		
		for(long p=1L; p<=1000L; p++) {	
			System.out.println(p);
			
			for(long l=1L; l<p; l++) {
				long p2 = p*p;
				long l2 = l*l;
				for(long r=1L; (r*r) < (p2 - l2); r++) {
					long r2 = r*r;
					if( (p2 - r2)%2==0 && (p2 + r2)%2==0) {
						if(isSquare(p2 - l2) && isSquare(r2 + l2) && isSquare(p2 - l2 - r2)) {
							long y = (p2 - r2)/2L;
							long x = y + r2;
							long z = y - l2;
							if(z<0L) {
								continue;
							}
							
							long result = x+y+z;
							if(result<minResult) {
								minResult = result;
								minX = x;
								minY = y;
								minZ = z;
							}						
						}
					}					
				}
			}
		}
		
		System.out.println("x = " + minX + ", y = " + minY + 
				", z = " + minZ + "\nx+y+z = " + minResult);
	}
	
	private static boolean isSquare(long v) {
		return squares.contains(Long.valueOf(v));
	}

	private static Set<Long> getSqauresBelow(long n) {		
		long bound = (long)Math.sqrt(n);
		Set<Long> squares = new HashSet<Long>((int)bound);
		
		for(long i=1L; i<bound; i++) {
			squares.add(i*i);
		}
		
		
		if(bound*bound < n) {
			squares.add(bound*bound);
		}
		return squares;
	}
}

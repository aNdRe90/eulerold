


public class Problem173 {
	/*
	 * We shall define a square lamina to be a square outline with a square "hole" 
	 * so that the shape possesses vertical and horizontal symmetry. 
	 * 
	 * For example, using exactly thirty-two square tiles we can form two different square laminae:
	 * [watch euler offline for picture]
	 * 
	 * With one-hundred tiles, and not necessarily using all of the tiles at one time, 
	 * it is possible to form forty-one different square laminae.
	 * 
	 * Using up to one million tiles how many different square laminae can be formed?
	 */

	/*SOLUTION:
	 * "onepoint"-width outline (called further 1outline) of size n, contains 4n-4 tiles.
	 * 
	 * If we have a hole of size 1, then outlines outside it are of sizes:
	 * 8, 16, 24, ...     [difference is 8]
	 * 
	 * Then size of n-th 1outline outside hole of size x is: 
	 * a(n) = 4(x+2)-4 + 8(n-1) = 8n + 4x - 4
	 * 
	 * Then sum of n 1outlines ouside the hole of x is:
	 * S(n) = n*(4(x+2)-4  + 8n + 4x - 4)/2 = 4n(n+x)
	 * 
	 * So if we have N tiles, then we can create n laminae where n is max
	 * integer satisfying:
	 * 
	 * 4n^2 + 4xn - N <= 0
	 * delta = 16x^2 + 16N
	 * sqrtDetla = 4*sqrt(x^2 + N)
	 * 
	 * nMax = floor [1/8 * ( 4*sqrt(x^2 + N) - 4x )] = floor[0.5*(sqrt(x^2 + N) - x)]
	 * 
	 * If we have N tiles, then maxSize 1outline satisfies:
	 * 4*maxSize - 4 <= N
	 * maxSize <= (N+4)/4
	 * 
	 * Then max hole possible to cover with one 1outline is of size:  maxSize - 2
	 * so   maxX <= (N-4)/4 
	 */
	public static void main(final String[] args) {
		final long tiles = 1000000L;
		long sum = 0L;
		long maxHoleSize = (tiles - 4L) / 4L;

		for (long holeSize = 1L; holeSize <= maxHoleSize; holeSize++) {
			double numerator = Math.sqrt((holeSize * holeSize) + tiles);
			numerator -= holeSize;
			long nMax = (long) (numerator / 2.0);
			sum += nMax;
		}

		System.out.println(sum);
	}
}

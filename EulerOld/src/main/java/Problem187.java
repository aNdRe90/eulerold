

import java.util.ArrayList;
import java.util.List;

import MyStuff.Prime;

public class Problem187 {
	/*
	 * A composite is a number containing at least two prime factors. 
	 * For example, 15 = 3 x 5; 9 = 3 x 3; 12 = 2 x 2 x 3.
	 * 
	 * There are ten composites below thirty containing precisely two, 
	 * not necessarily distinct, prime factors: 4, 6, 9, 10, 14, 15, 21, 22, 25, 26.
	 * 
	 * How many composite integers, n < 10^8, have precisely two, 
	 * not necessarily distinct, prime factors?
	 */
	private static final int N = (int) 1e8;
	private static final int sqrtN = (int) 1e4;
	private static final List<Integer> primes = loadPrimesUpTo(sqrtN);

	public static void main(final String[] args) {
		int sum = 0;

		for (int n = 4; n < N; n++) {
			if ((n % 1024) == 0) {
				System.out.println(n);
			}

			if (containsPrecisely2PrimeFactors(n)) {
				sum++;
			}
		}

		System.out.println(sum);
	}

	private static List<Integer> loadPrimesUpTo(final int n) {
		List<Integer> primes = new ArrayList<Integer>(n / 2);
		primes.add(2);

		for (int i = 3; i < n; i += 2) {
			if (Prime.isPrime(i)) {
				primes.add(i);
			}
		}

		return primes;
	}

	private static boolean containsPrecisely2PrimeFactors(final int n) {
		for (int prime : primes) {
			if ((n % prime) == 0) {
				if (n == (prime * prime)) {
					return true;
				}

				int a = n / prime;
				return Prime.isPrime(a);
			}
		}

		return false;
	}
}

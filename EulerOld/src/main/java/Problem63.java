import java.math.BigInteger;
import java.util.ArrayList;


public class Problem63 
{
	/*
	 * The 5-digit number, 16807=7^5, is also a fifth power. 
	 * Similarly, the 9-digit number, 134217728=8^9, is a ninth power.
	 * 
	 * How many n-digit positive integers exist which are also an nth power?
	 */
	
	public static void main(String[] args) 
	{
		int maxDigits = 20;
		ArrayList<ArrayList<BigInteger>> powers = new ArrayList<ArrayList<BigInteger>>(maxDigits);
		
		int currentPower = 1;
		long found = 0L;
		
		while(true)
		{
			int foundInThisPower = 0;
			for(long i=1L; ;i++)
			{
				BigInteger toPowerOfCurrent = new BigInteger(i+"").pow(currentPower);
				if(toPowerOfCurrent.toString().length()<currentPower)
					continue;
				else if(toPowerOfCurrent.toString().length()==currentPower)
				{
					found++;
					foundInThisPower++;
				}
				else
					break;
			}
			
			if(foundInThisPower==0)
				break;
			
			currentPower++;
		}
		
		System.out.println(found);
	}
}

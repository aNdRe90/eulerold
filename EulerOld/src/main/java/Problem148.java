

import java.math.BigInteger;
import java.util.List;

import MyStuff.BaseNumber;

public class Problem148 {
	private static final double log7 = Math.log(7.0);
	private static final BigInteger seven = new BigInteger("7");
	private static final long N = (long) 1e9;
	private static final long[] powers = getPowers(N);

	/*
	 * We can easily verify that none of the entries in the first seven rows of
	 * Pascal's triangle are divisible by 7: 1 1 1 1 2 1 1 3 3 1 1 4 6 4 1 1 5
	 * 10 10 5 1 1 6 15 20 15 6 1
	 * 
	 * However, if we check the first one hundred rows, we will find that only
	 * 2361 of the 5050 entries are not divisible by 7.
	 * 
	 * Find the number of entries which are not divisible by 7 in the first one
	 * billion (10^9) rows of Pascal's triangle.
	 */

	public static void main(final String[] args) {
		long sum = 1L;
		BaseNumber baseNumber = new BaseNumber(7L);

		long before = System.currentTimeMillis();
		for (long n = 1L; n < N; n++) {
			if ((n % 32768L) == 0L) {
				System.out.println(n);
			}

			long half = getHalfSize(n);
			baseNumber.increment();

			long[] groups = getGroups(baseNumber.getNumber());
			long halfSum = getSumFromGroupsInArea(groups, half);

			sum += 2L * halfSum;
			if (isCenterNotDivisibleBy7(groups, n)) {
				sum++;
			}
		}
		long after = System.currentTimeMillis();

		System.out.println("RESULT = " + sum);
		System.out.println("TIME = " + (after - before) + "ms");
	}

	private static boolean isCenterNotDivisibleBy7(final long[] groups,
			final long n) {
		if ((n % 2L) == 1L) {
			return false;
		}

		long index = n / 2L;
		long[] indexes = new long[groups.length];

		for (int i = indexes.length - 1; i >= 0; i--) {
			indexes[i] = index / powers[i];
			if (indexes[i] >= groups[i]) {
				return false;
			}

			index = index % powers[i];
		}

		return true;
	}

	private static long[] getPowers(final long n) {
		int logN = (int) (Math.log(n) / log7);
		long[] powers = new long[logN + 1];

		long power = 1L;
		for (int i = 0; i < powers.length; i++) {
			powers[i] = power;
			power *= 7L;
		}

		return powers;
	}

	private static long getSumFromGroupsInArea(final long[] groups, long area) {
		long sum = 0L;

		for (int i = groups.length - 1; i >= 0; i--) {
			long currentGroups = groups[i];
			long currentGroupSize = powers[i];
			long groupsNeeded = area / currentGroupSize;

			long product = getProductToIndex(groups, i - 1);
			if (groupsNeeded >= currentGroups) {
				sum += currentGroups * product;
				break;
			} else {
				sum += groupsNeeded * product;
				area = area % currentGroupSize;
			}
		}

		return sum;
	}

	private static long getProductToIndex(final long[] values, final int n) {
		long product = 1L;

		for (int i = 0; i <= n; i++) {
			product *= values[i];
		}

		return product;
	}

	private static long[] getGroups(final List<Long> number) {
		long[] groups = new long[number.size()];

		int maxIndex = groups.length - 1;
		groups[maxIndex] = number.get(maxIndex);

		for (int i = 0; i < maxIndex; i++) {
			groups[i] = number.get(i) + 1L;
		}

		return groups;
	}

	private static long getHalfSize(final long n) {
		long result = n / 2L;

		if ((n % 2L) == 1) {
			result++;
		}

		return result;
	}

	private static void displayPascalTriangleRow(final int n) {
		final int max = n / 2;
		for (int k = 0; k <= max; k++) {
			final BigInteger v = MyStuff.MyMath.newtonsSymbol(n, k);
			String s = null;
			if (v.mod(seven).compareTo(BigInteger.ZERO) != 0) {
				s = "x ";
			} else {
				s = "o ";
			}

			System.out.print(s);
		}
	}
}

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.StringTokenizer;


public class Problem96 
{
	/*
	 * Su Doku (Japanese meaning number place) is the name given to a popular puzzle concept. 
	 * Its origin is unclear, but credit must be attributed to Leonhard Euler who invented a similar, 
	 * and much more difficult, puzzle idea called Latin Squares. 
	 * The objective of Su Doku puzzles, however, is to replace the blanks (or zeros) in a 9 by 9 grid in such that each row, 
	 * column, and 3 by 3 box contains each of the digits 1 to 9. 
	 * Below is an example of a typical starting puzzle grid and its solution grid.
	 * 
	 * 	0 0 3	0 2 0	6 0 0				4 8 3	9 2 1	6 5 7
		9 0 0	3 0 5	0 0 1				9 6 7	3 4 5	8 2 1
		0 0 1	8 0 6	4 0 0				2 5 1	8 7 6	4 9 3
		
		0 0 8	1 0 2	9 0 0				5 4 8	1 3 2	9 7 6
		7 0 0	0 0 0	0 0 8				7 2 9	5 6 4	1 3 8
		0 0 6	7 0 8	2 0 0				1 3 6	7 9 8	2 4 5
		
		0 0 2	6 0 9	5 0 0				3 7 2	6 8 9	5 1 4
		8 0 0	2 0 3	0 0 9				8 1 4	2 5 3	7 6 9
		0 0 5	0 1 0	3 0 0 				6 9 5  	4 1 7	3 8 2
	 * 
	 * 
	 * A well constructed Su Doku puzzle has a unique solution and can be solved by logic, 
	 * although it may be necessary to employ "guess and test" methods in order to eliminate options 
	 * (there is much contested opinion over this). The complexity of the search determines the difficulty of the puzzle; 
	 * the example above is considered easy because it can be solved by straight forward direct deduction.
	 * 
	 * The 6K text file, sudoku.txt (right click and 'Save Link/Target As...'), 
	 * contains fifty different Su Doku puzzles ranging in difficulty, but all with unique solutions 
	 * (the first puzzle in the file is the example above).
	 * 
	 * By solving all fifty puzzles find the sum of the 3-digit numbers found in the top left corner of each solution grid; 
	 * for example, 483 is the 3-digit number found in the top left corner of the solution grid above.
	 */
	
	public static void main(String[] args) 
	{
		LinkedList<Sudoku> sudokus = new LinkedList<Sudoku>();
		LinkedList<Integer[][]> grids = readGrids("sudoku.txt");

		Iterator<Integer[][]> iter = grids.iterator();
		while(iter.hasNext())
		{
			Integer[][] next = iter.next();
			int[][] tempArray = new int[next.length][];
			
			for(int i=0; i<next.length; i++)
			{
				tempArray[i] = new int[next[i].length];
				for(int j=0; j<next[i].length; j++)
				{
					tempArray[i][j] = next[i][j].intValue();
				}
			}
			
			sudokus.add(new Sudoku(tempArray));
		}
		iter = null;
		
		long sum = 0L;
		Iterator<Sudoku> iterS = sudokus.iterator();
		while(iterS.hasNext())
		{
			//Sudoku s = iterS.next();
			//long before = System.nanoTime();
			//s.solve();
			//long after = System.nanoTime();
			int[][] temp = iterS.next().solve();
			//System.out.println(s);
			//System.out.println("\n\n"+(after-before)+"ns");
			StringBuffer buf = new StringBuffer("");
			buf.append(temp[0][0]);
			buf.append(temp[0][1]);
			buf.append(temp[0][2]);
			
			sum += Integer.parseInt(buf.toString());
		}
		
		System.out.println(sum);
	}
	
	private static LinkedList<Integer[][]> readGrids(String filename)
	{
		LinkedList<Integer[][]> grids = new LinkedList<Integer[][]>();
		BufferedReader f = null;
		
		try	{	f = new BufferedReader(new FileReader(filename));	}
		catch(IOException e)	{	e.printStackTrace();		}
		
		String line = null;	
		
		int row = 1;
		Integer[][] currentGrid = null;
		do
		{
			try {	line = f.readLine();	} 
			catch(IOException ee) { ee.printStackTrace(); }
			
			
			
			if(row%10==1)
			{
				if(currentGrid!=null)
					grids.add(currentGrid);
				
				currentGrid = new Integer[9][9];
				row = 2;
				continue;
			}
			
			if(line==null)
				break;
			for(int i=0, j=0; i<line.length(); i++, j++)
				currentGrid[row-2][i] = line.charAt(i) - 0x30;
			
			row++;
		}
		while(true);
		
		return grids;
	}
}

class Sudoku
{
	public Sudoku(int[][] g)
	{
		int[] valueAppearances = new int[N+1];
		grid = new int[rows][columns];
		for(int i=0; i<rows; i++)
			for(int j=0; j<columns; j++)
			{
				if(grid[i][j]>rows || grid[i][j]<0)
					throw new IllegalArgumentException("Wrong values in given grid!");
				
				grid[i][j] = g[i][j];
				++valueAppearances[grid[i][j]];			
			}
		
		for(int i=1; i<valueAppearances.length; i++)
			values.add(new Value(i, valueAppearances[i]));
				
		int row = 0;
		int col = 0;
		for(int i=0; i<subGridCoordinates.length; i++)
		{
			if(col==N)
			{
				col = 0;
				row += subRows;
			}
			
			subGridCoordinates[i][0] = row;
			subGridCoordinates[i][1] = col;
			
			col += subColumns;
		}
	}
	
	
	//returns null if sudoku is not solvable (in subgrid only one place empty, but cannot be filled)
	public int[][] solve()
	{
		if(solved)
			return grid;
		
		while(true)
		{
			boolean first = firstSieve();
			if(first && isSolved())
				break;
			
			
			boolean second = secondSieve();
			if(second && isSolved())
				break;
			
			if(!first && !second)
			{
				if(isSolveable())
					break;
				else
					return null;
			}
			
			//int a = 0;
		}
		
		if(!solved)
		{
			//time to guess:)
			ArrayList<ArrayList<Integer>> guess = getFirstMinimalRiskPlace();
			int guessRow = guess.get(0).get(0).intValue();
			int guessCol = guess.get(0).get(1).intValue();
			
			ArrayList<Integer> guessPossibilities = guess.get(1);
			createBackup();
			
			int badSolutionCounter = 0;   //how many guessed values are wrong
			for(int i=0; i<guessPossibilities.size(); i++)
			{
				gridBackup[guessRow][guessCol] = guessPossibilities.get(i);
				int[][] newGrid = new Sudoku(gridBackup).solve();   //solve sudoku with guessed value
				if(newGrid==null)
					badSolutionCounter++;
				else
				{
					grid = newGrid;
					break;
				}
			}		
			
			if(badSolutionCounter==guessPossibilities.size())
				return null;
		}
		
		int[][] gridRet = new int[grid.length][];
		for(int i=0; i<grid.length; i++)
		{
			gridRet[i] = new int[grid[i].length];
			for(int j=0; j<grid[i].length; j++)
				gridRet[i][j] = grid[i][j];
		}
		return gridRet;
	}
	
	//possibilities contains possible values for the result place
	private ArrayList<ArrayList<Integer>> getFirstMinimalRiskPlace()
	{
		ArrayList<ArrayList<Integer>> minPlace = new ArrayList<ArrayList<Integer>>(2);
		minPlace.add(new ArrayList<Integer>(2));		//for row and col
		minPlace.add(new ArrayList<Integer>(10));		//for possible values in row,col
		
		
		int[] min = new int[2];   //0 - row, 1 - col
		int minChoices = Integer.MAX_VALUE;
		
		for(int i=0; i<rows; i++)
			for(int j=0; j<columns; j++)
				if(grid[i][j]==0)
				{
					boolean[] rowValues = getRowValues(i);
					boolean[] colValues = getColumnValues(j);
					boolean[] subgridValues = getSubgridValues(i, j);
					//if [x] = true, then this value is in row/column
					
					ArrayList<Integer> tempPossibleValues = new ArrayList<Integer>(10);
					boolean[] placePossibleValues = new boolean[rowValues.length];
					int possibleValues = 0;
					
					for(int k=1; k<rowValues.length; k++)
						if( (!rowValues[k]) && (!colValues[k]) && (!subgridValues[k]) )
						{
							tempPossibleValues.add(new Integer(k));
							++possibleValues;
						}
					
					if(possibleValues<minChoices)
					{
						minChoices = possibleValues;
						min[0] = i;
						min[1] = j;
						minPlace.set(1, tempPossibleValues);
						if(minChoices==2)
						{
							i = rows;
							break;
						}
					}
				}	
		
		minPlace.get(0).add(min[0]);
		minPlace.get(0).add(min[1]);
		
		return minPlace;
	}
	
	/**
	 * Take each digit and put on its MUST BE PLACE. Repeat until no digit can be placed for sure. 
	 * Not enough only for hard sudokus.
	 * @return true if some values were placed and the sieve hepled.
	 */
	private boolean firstSieve()
	{
		boolean helped = false;
		int currentValue;
		int numberOfPlacingCurrentValue = 0;
		Collections.sort(values);
		int valuesWithoutFinding = 0;
		
		while(true)
		{
			//Collections.sort(values);
			Value currentV = values.getFirst();
			
			currentValue = currentV.value;
			numberOfPlacingCurrentValue = 0;   //how many times the current value was placed somewhere during
												   //this search of subgrids
			
			//loop over every subgrid
			for(int i=0; i<subGridCoordinates.length; i++)
			{
				if(!isValueInSubgrid(currentValue, subGridCoordinates[i][0], subGridCoordinates[i][1]))
				{
					//subgrid does not contain current value so look for the one place to put it
					int rowToPlace = -1;    //row of putting the current value if only one place found
					int colToPlace = -1;    //column  ...
					int possiblePlacesInGrid = 0; //how many potential places to put the current value
					
					for(int row = subGridCoordinates[i][0]; row<(subRows+subGridCoordinates[i][0]); row++)
						for(int col = subGridCoordinates[i][1]; col<(subColumns+subGridCoordinates[i][1]); col++)
						{
							if(grid[row][col]==0 && isValuePossibleInPlace(currentValue,row,col))
							{
								++possiblePlacesInGrid;
								if(possiblePlacesInGrid==1)
								{
									rowToPlace = row;
									colToPlace = col;
								}		
								else
								{
									row = subRows+subGridCoordinates[i][0];
									break;
								}
							}
						}				
					
					//found only place to put the current value for sure in this grid
					if(possiblePlacesInGrid==1)
					{
						helped = true;
						valuesWithoutFinding = 0;
						++numberOfPlacingCurrentValue;
						grid[rowToPlace][colToPlace] = currentValue;
						++currentV.repeats;
						if(currentV.repeats==N)
						{
							values.removeFirst();
							break;
						}
					}
				}					
			}
			
			if(numberOfPlacingCurrentValue==0)
			{
				values.remove(); //removes first
				++valuesWithoutFinding;
				//Collections.sort(values);
				values.add(currentV);  //put it at the end
			}
				//++whichValueIsCurrent;
			
			if(values.size()==0)
			{
				//solved = true;
				break;
			}
			
			if(valuesWithoutFinding==values.size())
				break;
			//else
			//	Collections.sort(values);
		}		
		
		return helped;
	}
	
	/**
	 * Search all the empty places and check what values are possible there.
	 * If there is only one possible - put it there.
	 * @return true if some values were placed and the sieve hepled.
	 */
	private boolean secondSieve()
	{
		boolean helped = false;
		
		
		while(true)
		{
			int numberOfPlacingValueForSure = 0;
			
			for(int i=0; i<rows; i++)
				for(int j=0; j<columns; j++)
					if(grid[i][j]==0)
					{
						boolean[] rowValues = getRowValues(i);
						boolean[] colValues = getColumnValues(j);
						boolean[] subgridValues = getSubgridValues(i, j);
						//if [x] = true, then this value is in row/column
						
						boolean[] placePossibleValues = new boolean[rowValues.length];
						int possibleValues = 0;
						int onlyPossibleValue = -1;
						
						for(int k=1; k<rowValues.length; k++)
							if( (!rowValues[k]) && (!colValues[k]) && (!subgridValues[k]) )
							{
								placePossibleValues[k] = true;    // "k" can be placed in this (row,col) = (i,j)
								++possibleValues;
								if(possibleValues==1)
								{
									onlyPossibleValue = k;
								}
								else
									break;
							}
						
						if(possibleValues==1)
						{
							grid[i][j] = onlyPossibleValue;
							Iterator<Value> iter = values.iterator();
							while(iter.hasNext())
							{
								Value currentV = iter.next();
								if(currentV.value==onlyPossibleValue)
								{
									currentV.repeats++;
									break;
								}
							}
							++numberOfPlacingValueForSure;
							helped = true;
						}
					}
			
			if(numberOfPlacingValueForSure==0)
				break;
		}	
		
		return helped;
	}
	
	private boolean isSolved()
	{
		for(int i=0; i<rows; i++)
			for(int j=0; j<columns; j++)
				if(grid[i][j]==0)
					return false;
		
		solved = true;
		return true;
	}
	
	public String toString()
	{
		StringBuffer buf = new StringBuffer("");
		for(int i=0; i<rows; i++)
		{
			for(int j=0; j<(columns-1); j++)
				buf.append(grid[i][j]).append(", ");
			
			buf.append(grid[i][columns-1]).append('\n');
		}
		
		return buf.toString();
	}
	
	private void createBackup()
	{
		gridBackup = new int[rows][columns];
		for(int i=0; i<grid.length; i++)
			for(int j=0; j<grid[i].length; j++)
				gridBackup[i][j] = grid[i][j];
	}
	
	/**
	 * Returns boolean values. If boolean[2] is true - value 2 is in the subgrid etc.
	 * @param row
	 * @param col
	 * @return
	 */
	private boolean[] getSubgridValues(int row, int col)
	{
		int leftTopRow = subRows*(row/subRows);
		int leftTopColumn = subColumns*(col/subColumns);
		
		boolean[] values = new boolean[N+1];
		for(int i = leftTopRow; i<(leftTopRow+subRows); i++)
			for(int j = leftTopColumn; j<(leftTopColumn+subColumns); j++)
			{
				values[grid[i][j]] = true;
			}
		
		return values;				
	}
	
	private boolean isValueInSubgrid(int value, int rowTop, int colLeft)
	{		
		for(int i = 0; i<subRows; i++)
			for(int j = 0; j<subColumns; j++)
			{
				if(grid[rowTop+i][colLeft+j]==value)
					return true;
			}
		
		return false;				
	}
	
	private boolean isSolveable()
	{
		boolean solveable = true;
		
		for(int currentValue = 1; currentValue<=N; currentValue++)
			for(int i=0; i<subGridCoordinates.length; i++)
			{
				if(!isValueInSubgrid(currentValue, subGridCoordinates[i][0], subGridCoordinates[i][1]))
				{
					int possiblePlacesInGrid = 0; //how many potential places to put the current value
					
					for(int row = subGridCoordinates[i][0]; row<(subRows+subGridCoordinates[i][0]); row++)
						for(int col = subGridCoordinates[i][1]; col<(subColumns+subGridCoordinates[i][1]); col++)
						{
							if(grid[row][col]==0 && isValuePossibleInPlace(currentValue,row,col))
							{
								++possiblePlacesInGrid;
								continue;
							}
						}
					
					if(possiblePlacesInGrid==0)
					{
						//value is not in grid and there is no place for it in there = not solveable
						solveable = false;
						currentValue = N+1;
						break;
					}
				}
			}
		
		return solveable;
	}
	
	private boolean isValuePossibleInPlace(int value, int row, int col)
	{
		boolean[] rowValues = getRowValues(row);
		boolean[] colValues = getColumnValues(col);
		
		if( (!rowValues[value]) && (!colValues[value]) )
			return true;
		else
			return false;
	}
	
	private boolean[] getRowValues(int row)
	{
		boolean[] values = new boolean[N+1];
		for(int i = 0; i<columns; i++)
			values[grid[row][i]] = true;
		
		return values;			
	}
	
	private boolean[] getColumnValues(int col)
	{
		boolean[] values = new boolean[N+1];
		for(int i = 0; i<rows; i++)
			values[grid[i][col]] = true;
		
		return values;			
	}
	
	private int[][] grid;
	private int[][] gridBackup = null;
	private final int N = 9;
	private int[][] subGridCoordinates = new int[N][2];
	private final int rows = N;
	private final int columns = N;
	private final int subRows = N/3;
	private final int subColumns = N/3;
	private boolean solved = false;	
	private boolean solvable = true;
	private LinkedList<Value> values = new LinkedList<Value>();
	
	private class Value implements Comparable<Value>
	{
		public Value(int val, int rep)
		{
			value = val;
			repeats = rep;
		}
		
		@Override
		public int compareTo(Value o) 
		{
			//so Value with most repeats will be first in the generic list
			if(this.repeats<o.repeats)
				return 1;
			else if(this.repeats>o.repeats)
				return -1;
			else
			{
				if(this.value>o.value)
					return 1;
				else if(this.value<o.value)
					return -1;
				else
					return 0;
			}
		}
		
		public String toString()
		{
			return value+" ("+repeats+")";
		}
		
		public int value;
		public int repeats;		
	}
}

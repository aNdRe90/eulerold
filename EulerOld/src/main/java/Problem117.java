

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

public class Problem117 {
	/*
	 * Using a combination of black square tiles and oblong tiles chosen from: 
	 * red tiles measuring two units, green tiles measuring three units, 
	 * and blue tiles measuring four units, it is possible to tile a row measuring 
	 * five units in length in exactly fifteen different ways.
	 * 
	 * How many ways can a row measuring fifty units in length be tiled?
	 * 
	 * NOTE: This is related to problem 116.
	 */
	public static void main(String[] args) {
		final int N = 50;
		for(int i=0; i<=N; i++)
			solutions.put(i, new HashMap<Integer, BigInteger>());
		
		System.out.println(getSolutionForRow(N));
	}
	
	private static BigInteger getSolutionForRow(int rowLength) {
		BigInteger preResult = getPreResult(rowLength, -1);
		if(preResult!=null)
			return preResult;
		
		BigInteger result = BigInteger.ONE;		
		if(rowLength<minBlockLength)
			return result;		
		
		for(int blockLength=minBlockLength; blockLength<=maxBlockLength; blockLength++) {
			result = result.add(getSolutionForRowAndBlockLength(rowLength, blockLength));
		}
		
		solutions.get(rowLength).put(-1, result);
		return result;
	}

	private static BigInteger getSolutionForRowAndBlockLength(int rowLength, int blockLength) {
		BigInteger preResult = getPreResult(rowLength, blockLength);
		if(preResult!=null)
			return preResult;
		
		BigInteger result = BigInteger.ZERO;
		int maxStartIndex = rowLength - blockLength;
		
		for(int startIndex=0; startIndex<=maxStartIndex; startIndex++) {
			result = result.add(BigInteger.ONE.multiply(
								getSolutionForRow(maxStartIndex-startIndex)));
		}
		
		solutions.get(rowLength).put(blockLength, result);
		return result;
	}
	
	
	private static BigInteger getPreResult(int rowLength, int blockLength) {
		BigInteger preResult = null;
		
		Map<Integer, BigInteger> temp = solutions.get(rowLength);
		if(temp!=null)
			preResult = temp.get(blockLength);
			
		return preResult;
	}
	
	private static final int minBlockLength = 2;
	private static final int maxBlockLength	= 4;	

	private static Map<Integer, Map<Integer, BigInteger>> solutions 
				= new HashMap<Integer, Map<Integer, BigInteger>>();
}

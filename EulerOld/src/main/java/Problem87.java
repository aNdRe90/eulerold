import java.util.HashSet;
import java.util.LinkedList;

import MyStuff.CombinationGenerator;
import MyStuff.MyMath;

public class Problem87 {
	/*
	 * The smallest number expressible as the sum of a prime square, prime cube, and prime fourth power is 28. 
	 * In fact, there are exactly four numbers below fifty that can be expressed in such a way:

		28 = 2^2 + 2^3 + 2^4
		33 = 3^2 + 2^3 + 2^4
		49 = 5^2 + 2^3 + 2^4
		47 = 2^2 + 3^3 + 2^4

		How many numbers below fifty million can be expressed 
		as the sum of a prime square, prime cube, and prime fourth power?
	 */
	public static void main(String[] args) {	
		final int N = 50000000;
		int largestPrime = largestPrimeWhichSquareIsLowerThan(N);
		long[] primesLower = new long[primes.size()];
		
		int m=0;
		for(int prime : primes)
			primesLower[m++] = prime;
		
		boolean[] values = new boolean[N];
		for(int i=0; i<primesLower.length; i++)
			for(int j=0; j<primesLower.length; j++)
				for(int k=0; k<primesLower.length; k++) {
					long square = primesLower[i]*primesLower[i];
					long cube = primesLower[j]*primesLower[j]*primesLower[j];
					long fourth = primesLower[k]*primesLower[k]*primesLower[k]*primesLower[k];
					long sum = square+cube+fourth;
					
					if(sum<(long)N && sum>0L)
						values[(int)sum] = true;
				}		
		
		int counter = 0;
		for(int i=0; i<values.length; i++)
			if(values[i])
				counter++;
		
		System.out.println(counter);
	}
	
	private static int largestPrimeWhichSquareIsLowerThan(int n) {
		int prime = -1;
		primes.add(2);
		
		for(int i=3; i<n; i+=2) {
			if(MyMath.isPrime(i)) {
				int sum = i*i;
				if(sum<n) {
					prime = i;
					primes.add(i);
				}
				else
					break;
			}
		}
		return prime;
	}
	
	private static LinkedList<Integer> primes = new LinkedList<Integer>();
}

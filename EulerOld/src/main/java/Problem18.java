import java.util.BitSet;
import java.util.StringTokenizer;


public class Problem18 
{
	/*
	 * By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.

						 3
						7 4
					   2 4 6
					  8 5 9 3

		That is, 3 + 7 + 4 + 9 = 23.

		Find the maximum total from top to bottom of the triangle below:

													    75
													  95 64
													17 47 82
												  18 35 87 10
												 20 04 82 47 65
												19 01 23 75 03 34
											   88 02 77 73 07 63 67
											 99 65 04 28 06 16 70 92
											41 41 26 56 83 40 80 70 33
										  41 48 72 33 47 32 37 16 94 29
									     53 71 44 65 25 43 91 52 97 51 14
										70 11 33 28 77 73 17 78 39 68 17 57
									   91 71 52 38 17 14 91 43 58 50 27 29 48
									 63 66 04 68 89 53 67 30 73 16 69 87 40 31
									04 62 98 27 23 09 70 98 73 93 38 53 60 04 23

		NOTE: As there are only 16384 routes, it is possible to solve this problem by trying every route. 
		However, Problem 67, is the same challenge with a triangle containing one-hundred rows; 
		it cannot be solved by brute force, and requires a clever method! ;o)

	 */
	
	public static void main(String[] args) 
	{
		String t = "75 95 64 17 47 82 18 35 87 10 20 04 82 47 65 19 01 23 75 03 34 88 02 77 73 07 63 67 "+
					"99 65 04 28 06 16 70 92 41 41 26 56 83 40 80 70 33	41 48 72 33 47 32 37 16 94 29 "+
					"53 71 44 65 25 43 91 52 97 51 14 70 11 33 28 77 73 17 78 39 68 17 57 "+
					"91 71 52 38 17 14 91 43 58 50 27 29 48	63 66 04 68 89 53 67 30 73 16 69 87 40 31 "+
					"04 62 98 27 23 09 70 98 73 93 38 53 60 04 23";
		
		
		int numberOfInts = 0;
		for(int i=1; i<=rows; i++)
			numberOfInts +=i;
		
		StringTokenizer st = new StringTokenizer(t);
		numbers = new int[numberOfInts];
		for(int i=0; i<numbers.length; i++)
			numbers[i] = Integer.parseInt(st.nextToken());
		
		
		long maxSum = 0L;
		int bound = 1;
		bound <<= (rows-1);
		for(int i=0; i<bound; i++)
		{
			directions = new boolean[rows-1];
			int index = 0;
			int val = i;
			while (val != 0) 
			{
			  if (val % 2 != 0)
			  {
			    directions[index]=true;
			  }
			  ++index;
			  val = val >>> 1;
			}
			long sum = searchTriangle();
			if(sum>maxSum)
			{
				maxSum = sum;
			}		
			System.out.println(i);
		}
		
		System.out.println(maxSum);
	}
	
	public static long searchTriangle()
	{
		int row = 0;
		int col = 0;		
		long sum = numbers[0];
		
		for(int i=0; ; i++)
		{
			row++;
			if(row==rows)
				break;
			
			if(directions[i]) //go right
				col++;
			
			int index = toIndex(row,col);
			sum += (long)numbers[index];
		}
		return sum;
	}
	
	//row [0,...]   col [0,...]
	public static int toIndex(int row, int col)
	{
		if(col>row)
			return -1;
		
		int index = 0;
		for(int i=0; i<row; i++)
			index += (i+1);
		
		return index+col;
	}
	
	public static int[] toRowCol(int index)
	{
		int row = 0;
		int col = 0;
		
		int temp = 0;
		for(int i=0; ;i++)
		{
			temp+=i;
			if((temp+i+1)>index)
			{
				row = i;
				break;
			}
				
		}
		col = index - temp;
		
		int[] ret = new int[2];
		ret[0] = row;
		ret[1] = col;
		return ret;
	}

	private static long LB = 0L;
	private static int[] numbers = null;
	private static final int rows = 15;
	private static boolean[] directions = new boolean[rows-1];
}

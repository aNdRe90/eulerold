import java.math.BigInteger;

import MyStuff.BigSquareRoot;


public class Problem80 
{
	/*
	 * It is well known that if the square root of a natural number is not an integer, then it is irrational. 
	 * The decimal expansion of such square roots is infinite without any repeating pattern at all.
	 * 
	 * The square root of two is 1.41421356237309504880..., and the digital sum of the first one hundred decimal digits is 475.
	 * 
	 * For the first one hundred natural numbers, 
	 * find the total of the digital sums of the first one hundred decimal digits for all the irrational square roots.
	 * 
	 */
	
	public static void main(String[] args) 
	{
		BigSquareRoot sqrtGenerator = new BigSquareRoot();
		sqrtGenerator.setScale(110);
		
		int sum = 0;
		for(int i=2; i<100; i++)
		{
			sum += (int) Math.sqrt((double)i);
			String decPart = sqrtGenerator.get(BigInteger.valueOf(i)).toString().substring(2);
			for(int j=0; j<99; j++)
				sum += (decPart.charAt(j) - 0x30);
		}
		sum = sum - 9 - 8 - 7 - 6 - 5 - 4 - 3 - 2;
		System.out.println(sum);
	}
}



import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import MyStuff.PermutationGenerator;

public class Problem171 {
	/*
	 * For a positive integer n, let f(n) be the sum of the squares 
	 * of the digits (in base 10) of n, e.g.
	 * 
	 * f(3) = 3^2 = 9,
	 * f(25) = 2^2 + 5^2 = 4 + 25 = 29,
	 * f(442) = 4^2 + 4^2 + 2^2 = 16 + 16 + 4 = 36
	 * 
	 * Find the last nine digits of the sum of all n, 0 < n < 10^20, 
	 * such that f(n) is a perfect square.
	 */

	private static final int N = 20;
	//private static long result = 0L;
	private static final int[] squares = getSquares();
	private static final Set<Integer> perfectSquares = getPerfectSquares(N);
	private static List<int[]> possibleCounters = new LinkedList<int[]>();
	private static Set<List<Integer>> numbers = new HashSet<List<Integer>>();
	private static BigInteger modulo = BigInteger.valueOf((long) 1e9);
	private static List<Long> bruteForceNumbers = new LinkedList<Long>();

	private static BigInteger[] factorials = getFactorials(N);

	public static void main(String[] args) {
		//long resultBrute = bruteForce(N);

		computePossibleCounters();
		BigInteger result = BigInteger.ZERO;
		long counter = 0L;

		PermutationGenerator perm = new PermutationGenerator(10, true);
		while (perm.hasMore()) {
			int[] permutation = perm.getNext();

			if ((counter % 10000L) == 0L) {
				System.out.println(counter);
			}
			counter++;

			for (int[] counters : possibleCounters) {
				BigInteger divisor = getDivisor(counters);

				List<Integer> number = getNumber(permutation, counters);
				int sumOfSquaredDigits = getSumOfSquaredDigits(number);

				//Collections.sort(number);
				if (perfectSquares.contains(sumOfSquaredDigits)
						&& !numbers.contains(number)) {
					numbers.add(number);
					BigInteger sumOfNumbers = getSumOfNumbers(number);
					BigInteger addend = sumOfNumbers.divide(divisor);
					result = (result.add(addend)).mod(modulo);
					///.intValue();
				}
			}
		}
		//computeResult(digitCounters);
		System.out.println("RESULT = " + result);
		//System.out.println("RESULT BRUTE = " + resultBrute);
		//System.out.println("SIZE = " + numbers.size());

		//		Set<Long> numbersNormal = new HashSet<Long>();
		//		PermutationGenerator permgen = new PermutationGenerator(N, true);
		//		while (permgen.hasMore()) {
		//			int[] permutation = permgen.getNext();
		//			for (List<Integer> list : numbers) {
		//				Long number = getNumber(permutation, list);
		//				numbersNormal.add(number);
		//			}
		//		}

		//		List<Long> normal = new LinkedList<Long>(numbersNormal);
		//		Collections.sort(normal);
		//		System.out.println("SIZE BRUTE = " + bruteForceNumbers.size());
		//		System.out.println("SIZE = " + normal.size());

		//		List<Long> diff = new LinkedList<Long>(bruteForceNumbers);
		//		diff.removeAll(normal);
		//
		//		int fsd = 23;
	}

	private static Long getNumber(int[] permutation, List<Integer> list) {
		StringBuffer s = new StringBuffer("");

		for (int element : permutation) {
			s.append(list.get(element));
		}

		return Long.parseLong(s.toString());
	}

	private static BigInteger[] getFactorials(int n) {
		BigInteger[] factorials = new BigInteger[n + 1];

		for (int i = 0; i <= n; i++) {
			factorials[i] = getFactorial(i);
		}

		return factorials;
	}

	private static BigInteger getDivisor(int[] counters) {
		BigInteger divisor = BigInteger.ONE;

		for (int c : counters) {
			if (c > 1) {
				divisor = divisor.multiply(factorials[c]);
			}
		}

		return divisor;
	}

	private static BigInteger getSumOfNumbers(List<Integer> number) {
		//List<Integer> trimmed = trimTrailingZeros(number);
		int size = number.size();
		if (size == 0) {
			return BigInteger.ZERO;
		}

		//long modulo = 1000000000L;
		BigInteger factorial = factorials[size - 1];//getFactorial(size - 1);
		BigInteger value = BigInteger.ZERO;

		for (int digit : number) {
			BigInteger temp = BigInteger.valueOf(digit).multiply(factorial);
			value = value.add(temp);
		}

		BigInteger result = BigInteger.ZERO;
		int limit = size > 9 ? 9 : size;

		for (int i = 0; i < limit; i++) {
			long multiplier = (long) Math.pow(10.0, i);
			result = result.add(BigInteger.valueOf(multiplier).multiply(value));
		}

		return result;
	}

	private static BigInteger getFactorial(int n) {
		if (n == 0) {
			return BigInteger.ONE;
		}

		BigInteger factorial = BigInteger.ONE;
		for (int i = 2; i <= n; i++) {
			factorial = factorial.multiply(BigInteger.valueOf(i));
		}

		return factorial;
	}

	private static long bruteForce(int n) {
		long max = (long) Math.pow(10.0, n);
		long result = 0;

		for (long i = 1; i < max; i++) {
			String s = Long.toString(i);
			int sumOfSquaredDigits = getSumOfSquaredDigits(s);

			if (MyStuff.MyMath.isSquareNumber(sumOfSquaredDigits)) {
				bruteForceNumbers.add(i);
				result += i % 1000000000L;
			}
		}

		return result;
	}

	private static int getSumOfSquaredDigits(String s) {
		int sum = 0;
		int length = s.length();

		for (int i = 0; i < length; i++) {
			int digit = s.charAt(i) - '0';
			sum += (digit * digit);
		}

		return sum;
	}

	private static int getLast9DigitsOfFactorial(int n) {
		if (n == 0) {
			return 1;
		}

		int modulo = 1000000000;
		int factorial = 1;

		for (int i = 2; i <= n; i++) {
			factorial = (factorial * i) % modulo;
		}

		return factorial;
	}

	private static List<Integer> getNumber(int[] permutation, int[] counters) {
		int[] permutatedCounters = new int[counters.length];
		List<Integer> number = new ArrayList<Integer>(10);

		for (int i = 0; i < permutatedCounters.length; i++) {
			permutatedCounters[i] = counters[permutation[i]];

			for (int j = 0; j < permutatedCounters[i]; j++) {
				number.add(i);
			}
		}

		return number;
	}

	private static void computePossibleCounters() {
		for (int i = 1; i <= N; i++) {
			int[] v = new int[10];
			v[0] = i;
			addCounters(N - i, i, v, 1);
		}
	}

	private static void addCounters(int n, int max, int[] counters, int from) {
		if (from == counters.length) {
			if (getSum(counters) == N) {
				possibleCounters.add(getCopy(counters));
			}
			return;
		}

		if (getSum(counters) == N) {
			possibleCounters.add(getCopy(counters));
			counters[from] = 0;
			return;
		}

		for (int i = 1; i <= max; i++) {
			counters[from] = i;
			addCounters(n - i, i, counters, from + 1);
			counters[from] = 0;
		}
	}

	private static int[] getCopy(int[] values) {
		int[] copy = new int[values.length];

		for (int i = 0; i < copy.length; i++) {
			copy[i] = values[i];
		}

		return copy;
	}

	private static Set<Integer> getPerfectSquares(int n) {
		Set<Integer> perfectSquares = new HashSet<Integer>();
		int maxSquare = n * 9 * 9;

		for (int i = 1;; i++) {
			int square = i * i;
			if (square > maxSquare) {
				break;
			}

			perfectSquares.add(square);
		}

		return perfectSquares;
	}

	private static int[] getSquares() {
		int[] squares = new int[10];

		for (int i = 0; i < squares.length; i++) {
			squares[i] = i * i;
		}

		return squares;
	}

	private static int getSumOfSquaredDigits(List<Integer> number) {
		int sum = 0;

		for (int element : number) {
			sum += element * element;
		}

		return sum;
	}

	private static int getSum(int[] values) {
		int sum = 0;

		for (int v : values) {
			sum += v;
		}

		return sum;
	}
}

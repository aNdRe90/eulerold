

public class Problem120 {
	/*
	 * Let r be the remainder when (a-1)^n + (a+1)^n is divided by a^2.
	 * 
	 * For example, if a = 7 and n = 3, then r = 42: 6^3 + 8^3 = 728 = 42 mod 49. 
	 * And as n varies, so too will r, but for a = 7 it turns out that rmax = 42.
	 * 
	 * For 3 <= a <= 1000, find sum rmax.
	 */
	
	/*
	 * SOLUTION:
	 * 
	 * According to newtons equation:
	 * (x+y)^n = (n 0)*x^n*y^0 + (n 1)*x^(n-1)*y^1 + (n 2)*x^(n-2)*y^2 + ... + (n n)*x^0*y^n
	 * 
	 * we get (a+1)^n + (a-1)^n =
	 * = 2*(n 0)*a^n+ 2*(n 2)*a^(n-2) + 2*(n 4)*a^(n-4) + ...
	 *  			 [if n%2==0]
	 *  														+ 2*(n n-2)*a^2 + 2(n n)*a^0
	 *  			 [else]
	 *  														+ 2*(n n-1)*a^1
	 *  
	 *  so [ (a+1)^n + (a-1)^n ] mod a^2 = 
	 * 				 [if n%2==0]
	 * 						= 2(n n)a^0 = 2
	 *  			 [else]
	 *  					= 2(n n-1)*a^1 = 2an
	 *  
	 *  Furthermore:
	 *  2an mod a^2 = [ (2a mod a^2)*(n mod a^2) ] mod a^2 = [2a * (n mod a^2)] mod a^2
	 *  
	 *  n can vary then from 1 to a^2 - 1.
	 *  
	 *  So we just need to find for given "a" such "n" where 1<=n<=a^2  
	 *  that 2an mod a^2 is biggest value. This value would be the biggest remainder for "a".
	 */
	public static void main(String[] args) {
		final int minA = 3;
		final int maxA = 1000;
		long sum = 0L;
		
		for(int a=minA; a<=maxA; a++) {
			System.out.println(a);
			sum += getBiggestRemainder(a);
		}
		
		System.out.println(sum);
	}

	private static long getBiggestRemainder(int a) {
		int a2 = a*a;
		int nMax = a*a - 1;
		int maxRemainder = Integer.MIN_VALUE;
		
		for(int n=1; n<nMax; n++) {
			int remainder = (2*a*n) % a2;
			if(remainder>maxRemainder)
				maxRemainder = remainder;
		}
		
		return maxRemainder;
	}
}

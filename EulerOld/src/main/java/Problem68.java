import java.util.LinkedList;

import MyStuff.PermutationGenerator;

public class Problem68 {
	/*
	 * Consider the following "magic" 3-gon ring, filled with the numbers 1 to 6, and each line adding to nine.
	 * 
	 * Working clockwise, and starting from the group of three with the numerically 
	 * lowest external node (4,3,2 in this example), each solution can be described uniquely. 
	 * For example, the above solution can be described by the set: 4,3,2; 6,2,1; 5,1,3.
	 * 
	 * It is possible to complete the ring with four different totals: 9, 10, 11, and 12. There are eight solutions in total.
	 * Total	Solution Set
	9	4,2,3; 5,3,1; 6,1,2
	9	4,3,2; 6,2,1; 5,1,3
	10	2,3,5; 4,5,1; 6,1,3
	10	2,5,3; 6,3,1; 4,1,5
	11	1,4,6; 3,6,2; 5,2,4
	11	1,6,4; 5,4,2; 3,2,6
	12	1,5,6; 2,6,4; 3,4,5
	12	1,6,5; 3,5,4; 2,4,6

	By concatenating each group it is possible to form 9-digit strings; 
	the maximum string for a 3-gon ring is 432621513.
	
	Using the numbers 1 to 10, and depending on arrangements, it is possible to form 16- and 17-digit strings. 
	What is the maximum 16-digit string for a "magic" 5-gon ring?
	
	[screen Problem68.png]
	 */
	public static void main(String[] args) {
		GonRing ring = new GonRing();
		final int N = 10;
		PermutationGenerator permutationGenerator = new PermutationGenerator(N, false);
		LinkedList<String> solutionSet16 = new LinkedList<String>();
		
		while(permutationGenerator.hasMore()) {
			if(ring.isCompletedWithSameTotals(permutationGenerator.getNext())) {
				int[] solutionSet = ring.getSolutionSet();
				StringBuffer solutionStringBuffer = new StringBuffer("");
				
				for(int i : solutionSet)
					solutionStringBuffer.append(i);
				
				String solutionString = solutionStringBuffer.toString();
				if(solutionString.length()==16)
					solutionSet16.add(solutionString);
			}
		}
		
		long max = Long.MIN_VALUE;
		for(String s : solutionSet16) {
			long current = Long.parseLong(s);
			if(max<current)
				max = current;
		}
		
		System.out.println(max);
	}
}

class GonRing {	
	private int[][] threes = new int[5][3];
	
	public int[] getSolutionSet() {
		int startIndex = getLowestExternalValueIndex();		
		LinkedList<Integer> solutionSet = new LinkedList<Integer>();
		
		for(int i=0; i<5; i++) {
			solutionSet.add(threes[startIndex][0]);
			solutionSet.add(threes[startIndex][1]);
			solutionSet.add(threes[startIndex][2]);
			
			startIndex++;
			if(startIndex==5)
				startIndex = 0;
		}
		
		int[] ret = new int[solutionSet.size()];
		int k = 0;
		for(int i : solutionSet)
			ret[k++] = i;
		
		return ret;
	}
	
	private int getLowestExternalValueIndex() {
		int lowestExternal = threes[0][0];
		int lowestIndex = 0;
		
		for(int i=1; i<5; i++)
			if(threes[i][0]<lowestExternal) {
				lowestExternal = threes[i][0];
				lowestIndex = i;
			}
		
		return lowestIndex;
	}
	
	public boolean isCompletedWithSameTotals(int[] values) {
		if(values.length!=10)
			return false;
		
		setThrees(values);
		
		int sum = sumFromThreeNodes(threes[0]);
		for(int i=1; i<5; i++)
			if(sumFromThreeNodes(threes[i])!=sum)
				return false;
		
		return true;
	}
	
	private int sumFromThreeNodes(int[] v) {
		if(v.length!=3)
			return -1;
		
		return v[0]+v[1]+v[2];
	}
	
	private void setThrees(int j, int v1, int v2, int v3) {
		threes[j][0] = v1;
		threes[j][1] = v2;
		threes[j][2] = v3;
	}
	
	private void setThrees(int[] values) {
		if(values.length!=10)
			throw new IllegalArgumentException("ERROR");
		
		//numbers from top nodes of ring
				//L1 = 1,3,5
				//L2 = 2,5,8
				//L3 = 9,8,7
				//L4 = 10,7,4
				//L5 = 6,4,3
		setThrees(0, values[0], values[2], values[4]);
		setThrees(1, values[1], values[4], values[7]);
		setThrees(2, values[8], values[7], values[6]);
		setThrees(3, values[9], values[6], values[3]);
		setThrees(4, values[5], values[3], values[2]);
	}
}

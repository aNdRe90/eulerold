import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.StringTokenizer;


public class Problem54
{
	/*
	 * In the card game poker, a hand consists of five cards and are ranked, from lowest to highest, in the following way:

    High Card: Highest value card.
    One Pair: Two cards of the same value.
    Two Pairs: Two different pairs.
    Three of a Kind: Three cards of the same value.
    Straight: All cards are consecutive values.
    Flush: All cards of the same suit.
    Full House: Three of a kind and a pair.
    Four of a Kind: Four cards of the same value.
    Straight Flush: All cards are consecutive values of same suit.
    Royal Flush: Ten, Jack, Queen, King, Ace, in same suit.

	The cards are valued in the order:
	2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King, Ace.

	If two players have the same ranked hands then the rank made up of the highest 
	value wins; for example, a pair of eights beats a pair of fives (see example 1 below). 
	But if two ranks tie, for example, both players have a pair of queens, then highest cards 
	in each hand are compared (see example 4 below); if the highest cards tie 
	then the next highest cards are compared, and so on.
	
	Consider the following five hands dealt to two players:
	Hand	 	Player 1	 		Player 2	 		Winner
	1	 		5H 5C 6S 7S KD 		2C 3S 8S 8D TD
				Pair of Fives		Pair of Eights		Player 2
				
	2	 	5D 8C 9S JS AC			2C 5C 7D 8S QH
			Highest card Ace		Highest card Queen	Player 1
	 	
	3	 	2D 9C AS AH AC			3D 6D 7D TD QD
			Three Aces				Flush with Diamonds
	 	
	4	 	4D 6S 9H QH QC			3D 6D 7H QD QS
			Pair of Queens			Pair of Queens
			Highest card Nine		Highest card Seven	Player 1
	 	
	5	 	2H 2D 4C 4D 4S			3C 3D 3S 9S 9D
			Full House				Full House
			With Three Fours		With Three Threes	Player 1	 	

	The file, poker.txt, contains one-thousand random hands dealt to two players. 
	Each line of the file contains ten cards (separated by a single space): 
	the first five are Player 1's cards and the last five are Player 2's cards. 
	You can assume that all hands are valid (no invalid characters or repeated cards), 
	each player's hand is in no specific order, and in each hand there is a clear winner.
	
	How many hands does Player 1 win?	 */

	public static void main(String[] args) 
	{
		Card[] test = new Card[5];
		char[] testValues = { 'J', 'K', 'J', 'K', 'J' };
		char[] testColours = { 'S', 'D', 'H', 'D', 'C'  };
		for(int i=0; i<5; i++)
		{
			test[i] = new Card(testValues[i], testColours[i]);
		}
		Poker.areFullHouse(test);
		
		readHands();
		int hands = player2.size();
		Card[] first = null;
		Card[] second = null;
		
		int firstIsWinner = 0;
		for(int i=0; i<hands; i++)
		{
			first = player1.get(i);
			second = player2.get(i);
			if(Poker.getWinner(first, second)==1)
				firstIsWinner++;
		}
		//Card[] f = new Card[5];
		//Card[] s = new Card[5];
		
		System.out.println(firstIsWinner);
	}
	
	public static int pokerWinner(Card[] first, Card[] second)
	{
		
		return 0;    //0 if first is the winner, 1 if second is the winner, -1 if draw
	}	
	
	public static void readHands()
	{
		BufferedReader f = null;
		
		try
		{
			f = new BufferedReader(new FileReader("poker.txt"));
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
		String line = null;
		do
		{
			try 
			{
				line = f.readLine();
			} 
			catch(IOException ee) { ee.printStackTrace(); }
			
			if(line==null)
				break;
			
			StringTokenizer st = new StringTokenizer(line, " ");
			
			Card[] cards1 = new Card[5];
			Card[] cards2 = new Card[5];
			for(int i=0; i<5; i++)
			{
				String currentCard = st.nextToken();
				cards1[i] = new Card(currentCard.charAt(0), currentCard.charAt(1));
			}
			for(int i=0; i<5; i++)
			{
				String currentCard = st.nextToken();
				cards2[i] = new Card(currentCard.charAt(0), currentCard.charAt(1));
			}
			player1.add(cards1);
			player2.add(cards2);
		}
		while(true);
	}
	
	private static ArrayList<Card[]> player1 = new ArrayList<Card[]>(1000);
	private static ArrayList<Card[]> player2 = new ArrayList<Card[]>(1000);
}

class Card
{
	public Card(char val, char col)
	{
		value = val;
		color = col;
	}
	
	public boolean equals(Object other)
	{
		Card otherCard = (Card) other;
		return color==otherCard.color && value==otherCard.value;
	}
	public char color;
	public char value;
	
	public String toString()
	{
		return ""+value+color;
	}
}

class Poker
{
	//1 if first is the winner, 2 if second, 0 if draw [not possible in this problem]
	public static int getWinner(Card[] first, Card[] second)
	{
		int rankFirst = 0;
		int rankSecond = 0;
		
		char[] eliminate1 = null;
		if(areRoyalFlush(first))
			rankFirst = 9;
		else if(areStraightFlush(first))
			rankFirst = 8;
		else if((eliminate1 = areFourOfSameKind(first))!=null)
			rankFirst = 7;
		else if((eliminate1 = areFullHouse(first))!=null)
			rankFirst = 6;
		else if(areFlush(first))
			rankFirst = 5;
		else if(areStraight(first))
			rankFirst = 4;
		else if((eliminate1 = areThreeOfSameKind(first))!=null)
			rankFirst = 3;
		else if((eliminate1 = areTwoPairs(first))!=null)
			rankFirst = 2;
		else if((eliminate1 = areOnePair(first))!=null)
			rankFirst = 1;
		
		char[] eliminate2 = null;
		if(areRoyalFlush(second))
			rankSecond = 9;
		else if(areStraightFlush(second))
			rankSecond = 8;
		else if((eliminate2 = areFourOfSameKind(second))!=null)
			rankSecond = 7;
		else if((eliminate2 = areFullHouse(second))!=null)
			rankSecond = 6;
		else if(areFlush(second))
			rankSecond = 5;
		else if(areStraight(second))
			rankSecond = 4;
		else if((eliminate2 = areThreeOfSameKind(second))!=null)
			rankSecond = 3;
		else if((eliminate2 = areTwoPairs(second))!=null)
			rankSecond = 2;
		else if((eliminate2 = areOnePair(second))!=null)
			rankSecond = 1;
		
		//if(rankFirst==9 || rankSecond==9)
		//{
		//	int gg = 0;
		//}
		
		if(rankFirst>rankSecond)
			return 1;
		else if(rankFirst<rankSecond)
			return 2;
		else
		{
			if(rankFirst==0)
			{
				int highest1 = 0;
				int highest2 = 0;
				do
				{
					highest1 = getHighestValue(first);
					highest2 = getHighestValue(second);						
				}
				while(highest1==highest2);
				
				if(highest1>highest2)
					return 1;
				else
					return 2;
			}
			if(rankFirst==1)
			{
				int firstV = getValue(eliminate1[0]);
				int secondV = getValue(eliminate2[0]);
				if(firstV>secondV)
					return 1;
				else if(firstV<secondV)
					return 2;
				else
				{
					for(int i=0; i<first.length; i++)
					{
						if(first[i].value==eliminate1[0])
							first[i].value = 0;
					}
					for(int i=0; i<second.length; i++)
					{
						if(second[i].value==eliminate2[0])
							second[i].value = 0;
					}
					int highest1 = 0;
					int highest2 = 0;
					do
					{
						highest1 = getHighestValue(first);
						highest2 = getHighestValue(second);						
					}
					while(highest1==highest2);
					
					if(highest1>highest2)
						return 1;
					else
						return 2;
				}
			}
			if(rankFirst==2)
			{
				int firstV = getValue(eliminate1[0]);
				int secondV = getValue(eliminate2[0]);
				if(firstV>secondV)
					return 1;
				else if(firstV<secondV)
					return 2;
				else
				{
					for(int i=0; i<first.length; i++)
					{
						if(first[i].value==eliminate1[0] || first[i].value==eliminate1[1])
							first[i].value = 0;
					}
					for(int i=0; i<second.length; i++)
					{
						if(second[i].value==eliminate2[0] || second[i].value==eliminate2[1] )
							second[i].value = 0;
					}
					int highest1 = 0;
					int highest2 = 0;
					do
					{
						highest1 = getHighestValue(first);
						highest2 = getHighestValue(second);						
					}
					while(highest1==highest2);
					
					if(highest1>highest2)
						return 1;
					else
						return 2;
				}
			}
			if(rankFirst==3)
			{
				int firstV = getValue(eliminate1[0]);
				int secondV = getValue(eliminate2[0]);
				if(firstV>secondV)
					return 1;
				else
					return 2;
			}
			if(rankFirst==4)
			{
				int highest1 = getHighestValue(first);
				int highest2 = getHighestValue(second);		
				
				if(highest1>highest2)
					return 1;
				else
					return 2;
			}
			if(rankFirst==5)
			{
				int highest1 = 0;
				int highest2 = 0;
				do
				{
					highest1 = getHighestValue(first);
					highest2 = getHighestValue(second);						
				}
				while(highest1==highest2);
				
				if(highest1>highest2)
					return 1;
				else
					return 2;
			}
			if(rankFirst==6)
			{
				int firstV = getValue(eliminate1[0]);
				int secondV = getValue(eliminate2[0]);
				if(firstV>secondV)
					return 1;
				else //if(firstV<secondV)
					return 2;
			}
			if(rankFirst==7)
			{
				int highest1 = 0;
				int highest2 = 0;
				do
				{
					highest1 = getHighestValue(first);
					highest2 = getHighestValue(second);						
				}
				while(highest1==highest2);
				
				if(highest1>highest2)
					return 1;
				else
					return 2;
			}
			if(rankFirst==8)
			{
				int highest1 = getHighestValue(first);
				int highest2 = getHighestValue(second);		
				
				if(highest1>highest2)
					return 1;
				else
					return 2;
			}
			
			return -1;
		}
	}	
	
	public static char getValue(char c)
	{
		if(c=='T')
			c = '9'+1;
		else if(c=='J')
			c = '9'+2;
		else if(c=='Q')
			c = '9'+3;
		else if(c=='K')
			c = '9'+4;
		else if(c=='A')
			c = '9'+5;
		
		return c;
	}
	
	public static int getHighestValue(Card[] set)
	{
		int highest = 0;
		int index = 0;
		for(int i=0; i<set.length; i++)
		{
			int temp = getValue(set[i].value) - 0x30;
			if(temp>highest)
			{
				highest = temp;
				index = i;
			}
		}		
		//if(index==-1)
		//{
		//	int a = 0;
		//}
		set[index].value = 0;
		return highest;
	}
	
	public static boolean areFlush(Card[] cards)
	{
		char color = cards[0].color;
		
		for(int i=1; i<cards.length; i++)
			if(cards[i].color!=color)
				return false;
		
		return true;
	}
	
	public static char[] areOnePair(Card[] cards)
	{
		if(cards.length<2)
			return null;
		
		char pairChar = 'x';
		
		HashSet<Character> differentValues = new HashSet<Character>();
		for(int i=0; i<cards.length; i++)
			differentValues.add(new Character(cards[i].value));
		
		if(differentValues.size()!=(cards.length-1))
			return null;
		
		int pairs = 0;
		Iterator<Character> iter = differentValues.iterator();
		while(iter.hasNext())
		{
			pairs = 0;
			char cardValue = iter.next().charValue();
			int counter = 0;
			for(int j=0; j<cards.length; j++)
			{
				if(cardValue==cards[j].value)
					counter++;
			}
			if(counter==2)
			{
				pairs++;	
				if(pairs>1)
					continue;
				else
					pairChar = cardValue;
			}
		}
		
		if(pairChar=='x')
			return null;
		else
		{
			char[] r = new char[1];
			r[0] = pairChar;
			return r;
		}
	}
	
	public static char[] areTwoPairs(Card[] cards)
	{
		if(cards.length<2)
			return null;
		
		char[] pairChar = new char[2];
		pairChar[0] = 'x';
		pairChar[1] = 'x';
		
		HashSet<Character> differentValues = new HashSet<Character>();
		for(int i=0; i<cards.length; i++)
			differentValues.add(new Character(cards[i].value));
		
		if(differentValues.size()!=(cards.length-2))
			return null;
		
		int pairs = 0;
		Iterator<Character> iter = differentValues.iterator();
		while(iter.hasNext())
		{
			//pairs = 0;
			char cardValue = iter.next().charValue();
			int counter = 0;
			for(int j=0; j<cards.length; j++)
			{
				if(cardValue==cards[j].value)
					counter++;
			}
			if(counter==2)
			{
				pairs++;	
				if(pairs>2)
					continue;
				else if(pairs==1)
					pairChar[0] = cardValue;
				else if(pairs==2)
					pairChar[1] = cardValue;
			}
		}
		
		if(pairs==2)
		{
			return pairChar;
		}
		else
			return null;
	}
	
	public static char[] areThreeOfSameKind(Card[] cards)
	{
		if(cards.length<3)
			return null;
		
		HashSet<Character> differentValues = new HashSet<Character>();
		for(int i=0; i<cards.length; i++)
			differentValues.add(new Character(cards[i].value));
		
		//if(differentValues.size()!=(cards.length-2))
		//	return null;
		
		Iterator<Character> iter = differentValues.iterator();
		while(iter.hasNext())
		{
			char cardValue = iter.next().charValue();
			int counter = 0;
			for(int j=0; j<cards.length; j++)
			{
				if(cardValue==cards[j].value)
					counter++;
			}
			if(counter==3)
			{
				char[] r = new char[1];
				r[0] = cardValue;
				return r;
			}
		}
		
		return null;
	}
	
	public static char[] areFourOfSameKind(Card[] cards)
	{
		if(cards.length<4)
			return null;
		
		HashSet<Character> differentValues = new HashSet<Character>();
		for(int i=0; i<cards.length; i++)
			differentValues.add(new Character(cards[i].value));
		
		if(differentValues.size()!=(cards.length-3))
			return null;
		
		Iterator<Character> iter = differentValues.iterator();
		while(iter.hasNext())
		{
			char cardValue = iter.next().charValue();
			int counter = 0;
			for(int j=0; j<cards.length; j++)
			{
				if(cardValue==cards[j].value)
					counter++;
			}
			if(counter==4)
			{
				char[] r = new char[1];
				r[0] = cardValue;
				return r;
			}
		}
		
		return null;
	}
	
	public static char[] areFullHouse(Card[] cards)
	{
		char[] pairChar = new char[2];
		char[] first = areThreeOfSameKind(cards);
		if(first!=null)
		{
			char[] second = new char[2];
			for(int i=0, j=0; i<cards.length; i++)
			{
				if(cards[i].value!=first[0])
					second[j++] = cards[i].value;
			}
			if(second[0]==second[1])
			{
				pairChar[0] = first[0];
				pairChar[1] = second[0];
				return  pairChar;
			}
			else
				return null;
		}
		else
			return null;		
	}
	
	public static boolean areStraight(Card[] cards)
	{
		if(cards.length<1)
			return false;
		
		HashSet<Character> differentValues = new HashSet<Character>();
		for(int i=0; i<cards.length; i++)
			differentValues.add(new Character(cards[i].value));
		
		if(differentValues.size()!=cards.length)
			return false;
		
		LinkedList<Character> vals = new LinkedList<Character>();
		for(int i=0; i<cards.length; i++)
		{
			vals.add(new Character(getValue(cards[i].value)));
		}
		Collections.sort(vals);
		Iterator<Character> iter = vals.iterator();
		
		char previous = 'x';
		if(iter.hasNext())
			previous = iter.next().charValue();
		
		while(iter.hasNext())
		{
			char current = iter.next().charValue();;
			if(current!=(previous+1))
				return false;
			else
				previous = current;
		}
		
		return true;
	}
	
	public static boolean areStraightFlush(Card[] cards)
	{
		return areStraight(cards) && areFlush(cards);
	}
	
	public static boolean areRoyalFlush(Card[] cards)
	{
		if(cards.length!=5 || !areFlush(cards))
			return false;
		
		LinkedList<Character> vals = new LinkedList<Character>();
		for(int i=0; i<cards.length; i++)
		{
			char c = cards[i].value;
			if(c=='T')
				c = '9'+1;
			else if(c=='J')
				c = '9'+2;
			else if(c=='Q')
				c = '9'+3;
			else if(c=='K')
				c = '9'+4;
			else if(c=='A')
				c = '9'+5;
			
			vals.add(new Character(c));
		}
		Collections.sort(vals);
		Iterator<Character> iter = vals.iterator();
		
		char previous = 'x';
		if(iter.hasNext())
			previous = iter.next().charValue();
		
		if(previous!=('9'+2))
			return false;
		else
		{
			while(iter.hasNext())
			{
				char current = iter.next().charValue();;
				if(current!=(previous+1))
					return false;
				else
					previous = current;
			}
		}
		
		return true;
	}
}


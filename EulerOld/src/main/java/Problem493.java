import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

import MyStuff.BaseNumber;
import MyStuff.VariationNumber;
import static MyStuff.MyMath.*;


public class Problem493 {
	/*
	 * 70 colored balls are placed in an urn, 10 for each of the seven rainbow colors.
	 * What is the expected number of distinct colors in 20 randomly picked balls?
	 * 
	 * Give your answer with nine digits after the decimal point (a.bcdefghij).
	 */
	private static final int scale = 100;
	private static final int balls = 70;
	private static final int colours = 7;
	private static final int ballsPerColour = 10;
	private static final int ballsPicked = 20;
	
	public static void main(String[] args) {
		BigDecimal result = BigDecimal.ZERO;
		
		for(int differentColours = 2; differentColours<=colours; differentColours++) {
			BigInteger possibilities = getPossibleNumberOfChoosingDifferentColours(differentColours);
			BigDecimal dfiferentColoursBig = new BigDecimal(Integer.toString(differentColours));
			
			result = result.add(dfiferentColoursBig.multiply(new BigDecimal(possibilities)));
		}

		BigInteger allPossibilities = binomialCoefficient(balls, ballsPicked);
		result = result.divide(new BigDecimal(allPossibilities), scale, RoundingMode.HALF_UP);
		System.out.println("RESULT = " + result.setScale(9, RoundingMode.DOWN));
	}
	private static BigInteger getPossibleNumberOfChoosingDifferentColours(int differentColours) {
		BigInteger possibilities = BigInteger.ONE;
		VariationNumber variationNumber = new VariationNumber(getBases(differentColours));
		
		while(!variationNumber.isOverflow()) {
			int[] colourPicksDistribution = getColourPicksDistribution(variationNumber);
			if(isValidDistribution(colourPicksDistribution)) {
				possibilities = possibilities.add(getPossibleNumberOfChoosingForDistribution(colourPicksDistribution));
			}
			
			variationNumber.increment();
		}
		
		return possibilities.multiply(binomialCoefficient(colours, differentColours));
	}
	private static BigInteger getPossibleNumberOfChoosingForDistribution(
			int[] colourPicksDistribution) {
		BigInteger possibilities = BigInteger.ONE;
		
		for(int value : colourPicksDistribution) {
			possibilities = possibilities.multiply(binomialCoefficient(ballsPerColour, value));
		}
		
		return possibilities;
	}
	private static boolean isValidDistribution(int[] colourPicksDistribution) {
		int sum = 0;
		
		for(int value : colourPicksDistribution) {
			sum += value;
		}
		
		return sum == ballsPicked;
	}
	private static int[] getColourPicksDistribution(
			VariationNumber variationNumber) {
		int[] variationValues = variationNumber.getValues();
		
		for(int i=0; i<variationValues.length; i++) {
			variationValues[i]++;
		}
		
		return variationValues;
	}
	private static int[] getBases(int differentColours) {
		int[] bases = new int[differentColours];
		
		for(int i=0; i<differentColours; i++) {
			bases[i] = ballsPerColour;
		}
		
		return bases;
	}
}

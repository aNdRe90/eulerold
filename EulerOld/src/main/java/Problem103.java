

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import MyStuff.CombinationGenerator;
import MyStuff.VariationNumber;

public class Problem103 {
	/*
	 * Let S(A) represent the sum of elements in set A of size n. 
	 * We shall call it a special sum set if for any two non-empty disjoint subsets, 
	 * B and C, the following properties are true:
	 * 
	 * S(B) != S(C); that is, sums of subsets cannot be equal.
	 * If B contains more elements than C then S(B) > S(C).
	 * 
	 * If S(A) is minimised for a given n, we shall call it an optimum special sum set. 
	 * The first five optimum special sum sets are given below.
	 * 
	 * n = 1: {1}
	 * n = 2: {1, 2}
	 * n = 3: {2, 3, 4}
	 * n = 4: {3, 5, 6, 7}
	 * n = 5: {6, 9, 11, 12, 13}
	 * 
	 * It seems that for a given optimum set, A = {a1, a2, ... , an}, 
	 * the next optimum set is of the form B = {b, a1+b, a2+b, ... ,an+b}, 
	 * where b is the "middle" element on the previous row.
	 * 
	 * By applying this "rule" we would expect the optimum set for n = 6 to be 
	 * A = {11, 17, 20, 22, 23, 24}, with S(A) = 117. 
	 * However, this is not the optimum set, as we have merely applied an algorithm 
	 * to provide a near optimum set. The optimum set for n = 6 is A = {11, 18, 19, 20, 22, 25}, 
	 * with S(A) = 115 and corresponding set string: 111819202225.
	 * 
	 * Given that A is an optimum special sum set for n = 7, find its set string.
	 * 
	 * NOTE: This problem is related to problems 105 and 106.
	 * 
	 */
	public static void main(String[] args) {
		//mozna brac w dowolne podzbiory rozlaczne, nie tylko podzbior i dopelnienie
		//
		int[] set = { 11, 18, 19, 20, 22, 25 };
		int[] nearOptimal7set = getNearOptimalSetAfter(set);	
		
		int delta = 3;
		List<int[]> setsToCheck = getSetsToCheck(nearOptimal7set, delta);
		
		int smallestSum = Integer.MAX_VALUE;
		String resultString = null;
		
		int counter = 0;
		int size = setsToCheck.size();
		for(int[] s : setsToCheck) {
			if(counter%1000==0)
				System.out.println(counter+"/"+size);
			
			counter++;
			
			if(isSpecialSumSet(s)) {
				int sum = getSumOfElements(s);
				if(sum<smallestSum) {
					smallestSum = sum;
					Arrays.sort(s);
					resultString = getSetString(s);
				}
			}
		}
		
		System.out.println(resultString);
	}
	
	private static List<int[]> getSetsToCheck(int[] nearOptimal, int delta) {
		List<int[]> sets = new LinkedList<int[]>();		
		List<int[]> valuesOnPosition = getPossibleValuesOnPosition(nearOptimal, delta);
		int[] bases = new int[nearOptimal.length];
		for(int i=0; i<bases.length; i++)
			bases[i] = 2*delta+1;
		
		VariationNumber n = new VariationNumber(bases);
		while(!n.isOverflow()) {
			int[] choices = n.getValues();
			int[] set = new int[choices.length];
			
			for(int i=0; i<choices.length; i++)
				set[i] = valuesOnPosition.get(i)[choices[i]];
			
			sets.add(set);
			n.increment();				
		}
		
		return sets;
	}

	private static List<int[]> getPossibleValuesOnPosition(int[] nearOptimal, int delta) {
		List<int[]> values = new ArrayList<int[]>(nearOptimal.length);
		
		for(int i=0; i<nearOptimal.length; i++) {
			int[] v = new int[2*delta+1];
			
			for(int j=-delta, k=0; j<=delta; j++, k++)
				v[k] = nearOptimal[i]+j;
			
			values.add(v);
		}
		
		return values;
	}

	private static int[] getNearOptimalSetAfter(int[] set) {
		int[] next = new int[set.length+1];		
		next[0] = getMiddleElement(set);
		
		for(int i=0; i<set.length; i++)
			next[i+1] = next[0] + set[i];
		
		return next;
	}

	private static boolean isSpecialSumSet(int[] set) {		
		for(int i=1; i<=set.length/2; i++) {
			CombinationGenerator combGenA = new CombinationGenerator(set.length, i);
			
			while(combGenA.hasMore()) {
				List<Integer> positionsToCombinateFrom = getPositionsToCombinateFrom(set.length);
				int[] combinationA = getCombinationFrom(combGenA.getNext(), positionsToCombinateFrom);
				positionsToCombinateFrom = removeElementsFromPositions(positionsToCombinateFrom, combinationA);
				
				for(int j=1; (j+i)<=set.length; j++) {
					CombinationGenerator combGenB = new CombinationGenerator(set.length - i, j);
					while(combGenB.hasMore()) {
						int[] combinationB = getCombinationFrom(combGenB.getNext(), positionsToCombinateFrom);
						
						int[] subsetA = getSubsetOfElements(set, combinationA);
						int[] subsetB = getSubsetOfElements(set, combinationB);
						
						if(!specialSumSubsetsPropertiesTrue(subsetA, subsetB))
							return false;
					}
				}
			}
		}
		
		return true;		
	}
	
	private static int[] getCombinationFrom(int[] combination, 
											List<Integer> positions) {
		int[] combs = combination.clone();
		
		for(int i=0; i<combs.length; i++)
			combs[i] = positions.get(combination[i]);
		
		return combs;
	}

	private static List<Integer> removeElementsFromPositions(
			List<Integer> positions, int[] combination) {
		
		for(int i=0; i<combination.length; i++) {
			if(positions.contains(combination[i])) {
				positions.remove(positions.indexOf(combination[i]));
			}
		}
			
		return positions;
	}

	private static List<Integer> getPositionsToCombinateFrom(int length) {
		List<Integer> positions = new ArrayList<Integer>(length);
		
		for(int i=0; i<length; i++) 
			positions.add(i);
		
		return positions;
	}

	private static String getSetString(int[] set) {
		StringBuffer buffer = new StringBuffer("");
		for(int i : set)
			buffer.append(i);
		
		return buffer.toString();
	}
	
	private static int getMiddleElement(int[] set) {
		return set[set.length/2];
	}

	private static boolean specialSumSubsetsPropertiesTrue(int[] A, int[] B) {		
		int sumA = getSumOfElements(A);
		int sumB = getSumOfElements(B);
		
		return (A.length==B.length && sumA!=sumB) || (A.length>B.length && sumA>sumB)
				|| (B.length>A.length && sumB>sumA);
	}

	private static int getSumOfElements(int[] a) {
		int sum = 0;
		for(int i : a)
			sum += i;
		
		return sum;
	}

	private static int[] getSubsetOfElements(int[] set, int[] combination) {
		int[] subset = new int[combination.length];
		
		for(int i=0; i<subset.length; i++)
			subset[i] = set[combination[i]];
			
		return subset;
	}
}


public class Problem27 
{
	/*
	 * Euler published the remarkable quadratic formula:

	n^2 + n + 41

	It turns out that the formula will produce 40 primes for the consecutive values n = 0 to 39. 
	However, when n = 40, 40^2 + 40 + 41 = 40(40 + 1) + 41 is divisible by 41, 
	and certainly when n = 41, 41^2 + 41 + 41 is clearly divisible by 41.

	Using computers, the incredible formula  n^2 - 79n + 1601 was discovered, 
	which produces 80 primes for the consecutive values n = 0 to 79. 
	The product of the coefficients, -79 and 1601, is -126479.

	Considering quadratics of the form:

    	n^2 + an + b, where |a| < 1000 and |b| < 1000

    	where |n| is the modulus/absolute value of n
    	e.g. |11| = 11 and |-4| = 4
	
	Find the product of the coefficients, a and b, for the quadratic expression 
	that produces the maximum number of primes for consecutive values of n, starting with n = 0.

	 */
	public static void main(String[] args) 
	{
		int maxPrimes = 0;
		int maxA = 0;
		int maxB = 0;
		
		for(int a=-999; a<1000; a++)
			for(int b=-999; b<1000; b++)
			{
				int primes = getNumberOfPrimesForFormula(a,b);
				if(primes>maxPrimes)
				{
					maxPrimes = primes;
					maxA = a;
					maxB = b;
				}
			}
		
		long product = maxA*maxB;
		System.out.println(product);
	}
	
	public static int getNumberOfPrimesForFormula(int a, int b)  //n^2 +an +b
	{
		int primes = 0;
		long currentVal = 0L;
		for(long n=0; ; n++)
		{
			currentVal = (long)(n*(n+a)+b);
			if(MyMath.isPrime(currentVal))
				primes++;
			else
				break;
		}
		
		return primes;
	}
}

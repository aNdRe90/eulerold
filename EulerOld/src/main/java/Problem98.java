import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import MyStuff.CombinationGenerator;
import MyStuff.PermutationGenerator;


public class Problem98 {
	/*
	 *By replacing each of the letters in the word CARE with 1, 2, 9, and 6 respectively, 
	 *we form a square number: 1296 = 362. 
	 *What is remarkable is that, by using the same digital substitutions, the anagram, 
	 *RACE, also forms a square number: 9216 = 962. 
	 *We shall call CARE (and RACE) a square anagram word pair and specify further that leading zeroes 
	 *are not permitted, neither may a different letter have the same digital value as another letter.
	 *
	 *Using words.txt (right click and 'Save Link/Target As...'), a 16K text file containing 
	 *nearly two-thousand common English words, find all the square anagram word pairs 
	 *(a palindromic word is NOT considered to be an anagram of itself).
	 *
	 *What is the largest square number formed by any member of such a pair?
	 *
	 *NOTE: All anagrams formed must be contained in the given text file. 
	 */
	public static void main(String[] args) {
		String filename = "words.txt";
		List<String> words = getWordsFromFile(filename);		
		List<String[]> anagramPairs = getAnagramPairs(words);
		
		Collections.sort(anagramPairs, new Comparator<String[]>() {
				@Override
				public int compare(String[] a, String[] b) {
					return b[0].length() - a[0].length();
				} 
			});
		
		int longestWordLength = getGreatestLengthOfWordInPairs(anagramPairs);
		squares = getSquaresUpToLengthOf(longestWordLength);
		
		long biggestSquare = Long.MIN_VALUE;
		int biggestSquareLength = 0;
		
		for(String[] pair : anagramPairs) {
			long biggestSquareToBe = getBiggestSquareFromWordsIfSqaureAnagramPair(pair[0], pair[1]);
			int biggestSquareToBeLength = Long.toString(biggestSquareToBe).length();
			
			if(biggestSquareLength>0 && biggestSquareToBeLength<biggestSquareLength)
				break;
			
			if(biggestSquareToBe>biggestSquare) {
				biggestSquare = biggestSquareToBe;
				biggestSquareLength = Long.toString(biggestSquare).length();
			}
		}
		
		System.out.println(biggestSquare);
	}

	private static long getBiggestSquareFromWordsIfSqaureAnagramPair(String a, String b) {
		char[] differentLetters = getDifferentLettersFrom(a);
		int wordLength = a.length();
		
		List<Long> squaresNeeded = getSqauresOfLengthXwithYDifferentDigits(
											wordLength, differentLetters.length);
		
		if(squaresNeeded.size()<2)
			return Long.MIN_VALUE;
		
		for(long square : squaresNeeded) {
			Map<Character, Integer> values = getMappedValuesForSqaureAndWord(square, a, wordLength);
			long bValue = getNumberForWordByValues(b, values);
			if(squares.get(wordLength).contains(bValue))
				return square>bValue ? square : bValue;
		}
		
		return Long.MIN_VALUE;
	}

	private static Map<Character, Integer> getMappedValuesForSqaureAndWord(long square, String a, int length) {
		Map<Character, Integer> values = new HashMap<Character, Integer>(length);
		String squareString = Long.toString(square);
		
		for(int i=0; i<length; i++)
			values.put(a.charAt(i), squareString.charAt(i) - 0x30);
		
		return values;
	}

	private static List<Long> getSqauresOfLengthXwithYDifferentDigits(int length, int differentDigits) {
		return getSqauresWithDifferentDigits(squares.get(length), differentDigits);
	}

	private static List<Long> getSqauresWithDifferentDigits(
			LinkedList<Long> allSquares, int differentDigits) {
		LinkedList<Long> neededSquares = new LinkedList<Long>();
		
		for(long square : allSquares) {
			if(containsGivenDifferentDigits(square, differentDigits))
				neededSquares.add(square);
		}
		
		return neededSquares;
	}

	private static boolean containsGivenDifferentDigits(long square, int differentDigits) {
		String squareString = Long.toString(square);
		int length = squareString.length();
		Set<Character> digits = new HashSet<Character>(length);
		
		for(int i=0; i<length; i++)
			digits.add(squareString.charAt(i));
		
		return digits.size()==differentDigits;
	}

	private static long getNumberForWordByValues(String s, Map<Character, Integer> values) {
		StringBuffer numberBuffer = new StringBuffer("");
		int length = s.length();
		
		for(int i=0; i<length; i++)
			numberBuffer.append(values.get(s.charAt(i)));
		
		return Long.parseLong(numberBuffer.toString());
	}

	private static char[] getDifferentLettersFrom(String s) {
		int length = s.length();
		Set<Character> letters = new HashSet<Character>(length);

		for(int i=0; i<length; i++)
			letters.add(s.charAt(i));
		
		char[] lettersArray = new char[letters.size()];
		
		int i=0;
		for(char letter : letters)
			lettersArray[i++] = letter;
		
		return lettersArray;
	}

	private static List<LinkedList<Long>> getSquaresUpToLengthOf(int longest) {
		List<LinkedList<Long>> squares = new ArrayList<LinkedList<Long>>(longest+1);
		squares.add(null);
		for(int i=0; i<longest; i++)
			squares.add(new LinkedList<Long>());
		
		for(long i=1L; ;i++) {
			long square = i*i;
			int squareLength = Long.toString(square).length();
			
			if(squareLength>longest)
				break;
			
			squares.get(squareLength).add(square);							
		}
		
		return squares;
	}

	private static int getGreatestLengthOfWordInPairs(List<String[]> anagramPairs) {
		int longest = 0;
		
		for(String[] pair : anagramPairs) {
			int currentLength = pair[0].length();
			if(currentLength>longest)
				longest = currentLength;
		}
		
		return longest;
			
	}

	private static List<String[]> getAnagramPairs(List<String> words) {
		List<String[]> pairs = new LinkedList<String[]>();
		CombinationGenerator combGen = new CombinationGenerator(words.size(), 2);
		
		while(combGen.hasMore()) {
			int[] combination = combGen.getNext();
			String word1 = words.get(combination[0]);
			String word2 = words.get(combination[1]);
			
			if(isAnagramPair(word1, word2)) {
				String[] pair = new String[2];
				pair[0] = word1;
				pair[1] = word2;
				
				pairs.add(pair);
			}
		}
		
		return pairs;
	}

	private static boolean isAnagramPair(String word1, String word2) {
		if(word1.length()!=word2.length())
			return false;
		
		List<Character> chars1 = getSortedCharactersFromWord(word1);
		List<Character> chars2 = getSortedCharactersFromWord(word2);
		
		return containsSameCharacters(chars1, chars2);
	}

	private static boolean containsSameCharacters(List<Character> chars1, List<Character> chars2) {
		int size = chars1.size();
		
		for(int i=0; i<size; i++)
			if(chars1.get(i)!=chars2.get(i))
				return false;
		
		return true;
	}

	private static List<Character> getSortedCharactersFromWord(String word) {
		List<Character> chars = new LinkedList<Character>();
		int length = word.length();
		
		for(int i=0; i<length; i++)
			chars.add(word.charAt(i));
		
		Collections.sort(chars);
		
		return chars;
	}

	private static List<String> getWordsFromFile(String filename) {
		String fileContent = getFileContente(filename);
		StringTokenizer tokenizer = new StringTokenizer(fileContent, ",");
		
		List<String> words = new ArrayList<String>(2000);
		while(tokenizer.hasMoreTokens()) {
			String word = tokenizer.nextToken();
			words.add(word.substring(1, word.length()-1));
		}
		
		return words;
	}

	private static String getFileContente(String filename) {
		BufferedReader file = null;
		String content = "";
		try	{
			file = new BufferedReader(new FileReader(filename));
			content = file.readLine();
		}
		catch(IOException e) { e.printStackTrace(); }
		
		return content;
	}

	private static List<LinkedList<Long>> squares;
}



import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

public class Problem115 {
	/*
	 * NOTE: This is a more difficult version of problem 114.
	 * 
	 * A row measuring n units in length has red blocks with a minimum 
	 * length of m units placed on it, such that any two red blocks 
	 * (which are allowed to be different lengths) are separated by at 
	 * least one black square.
	 * 
	 * Let the fill-count function, F(m, n), represent the number of ways 
	 * that a row can be filled.
	 * 
	 * For example, F(3, 29) = 673135 and F(3, 30) = 1089155.
	 * 
	 * That is, for m = 3, it can be seen that n = 30 is the smallest value 
	 * for which the fill-count function first exceeds one million.
	 * 
	 * In the same way, for m = 10, it can be verified that F(10, 56) = 880711 
	 * and F(10, 57) = 1148904, so n = 57 is the least value for which the fill-count 
	 * function first exceeds one million.
	 * 
	 * For m = 50, find the least value of n for which the fill-count function 
	 * first exceeds one million.
	 */
	public static void main(String[] args) {
		BigInteger million = new BigInteger("1000000");
		
		for(int i=minBlockLength; ;i++) {
			solutions.put(i, new HashMap<Integer, BigInteger>());
			BigInteger solution = getSolutionForRow(i);
			
			//if(i%100==0)
			//	System.out.println(i);
			
			if(solution.compareTo(million)>0) {
				System.out.println("SOLUTION = "+i);
				break;
			}
		}
	}

	private static BigInteger getSolutionForRow(int rowLength) {
		BigInteger preResult = getPreResult(rowLength, -1);
		if(preResult!=null)
			return preResult;
		
		BigInteger result = BigInteger.ONE;		
		if(rowLength<minBlockLength)
			return result;		
		
		for(int blockLength=minBlockLength; blockLength<=rowLength; blockLength++) {
			result = result.add(getSolutionForRowAndBlockLength(rowLength, blockLength));
		}
		
		solutions.get(rowLength).put(-1, result);
		return result;
	}

	private static BigInteger getSolutionForRowAndBlockLength(int rowLength, int blockLength) {
		BigInteger preResult = getPreResult(rowLength, blockLength);
		if(preResult!=null)
			return preResult;
		
		BigInteger result = BigInteger.ZERO;
		int maxStartIndex = rowLength - blockLength;
		
		for(int startIndex=0; startIndex<=maxStartIndex; startIndex++) {
			result = result.add(BigInteger.ONE.multiply(getSolutionForRow(maxStartIndex-startIndex-1)));
		}
		
		solutions.get(rowLength).put(blockLength, result);
		return result;
	}
	
	
	private static BigInteger getPreResult(int rowLength, int blockLength) {
		BigInteger preResult = null;
		
		Map<Integer, BigInteger> temp = solutions.get(rowLength);
		if(temp!=null)
			preResult = temp.get(blockLength);
			
		return preResult;
	}

	private static final int minBlockLength = 50;
	private static Map<Integer, Map<Integer, BigInteger>> solutions 
				= new HashMap<Integer, Map<Integer, BigInteger>>();
}

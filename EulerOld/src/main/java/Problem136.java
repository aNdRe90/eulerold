

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

public class Problem136 {
	/*
	 * The positive integers, x, y, and z, are consecutive terms 
	 * of an arithmetic progression. Given that n is a positive integer, 
	 * the equation, x2 - y2 - z2 = n, 
	 * has exactly one solution when n = 20:
	 * 
	 * 13^2 - 10^2 - 7^2 = 20
	 * 
	 * In fact there are twenty-five values of n below one hundred 
	 * for which the equation has a unique solution.
	 * 
	 * How many values of n less than fifty million 
	 * have exactly one solution?
	 */
	
	/*
	 * SOLUTION: Look at SOLUTION to problem 135.
	 * 
	 * Ugly and runs in almost an hour, but works:)
	 */
	public static void main(String[] args) {
		final long N = 50000000L;
		final long delta = 1000000L;
		final int distinctSolutions = 1;
		int result = 0;	
		
		for(long from=1, to=delta-1L; to<N; from=to+1L, to+=delta) {
			Map<Long, LinkedList<Long>> divisors 
					= getDivisorsForValues(from, to);			

			for(long n=from; n<=to; n++) {
				System.out.println("Solving from " + n + "... " + n);
				
				Set<Solution> solutions = new HashSet<Solution>();			
				LinkedList<Long> divisorsN = divisors.get(n);		
				
				if(divisorsN.size()==1) {
					if( (n+1L)%4L==0L) {
						result++;
					}
					continue;
				}
				
				for(long divisor : divisorsN) {
					long numerator = (divisor*divisor + n);
					long denominator = 4L*divisor;
					if(numerator%denominator==0L) {
						long r = numerator/denominator;
						if(divisor>r) {
							solutions.add(new Solution(divisor - r, r));
						}					
					}
				}
				
				if(solutions.size()==distinctSolutions) {
					result++;
				}
			}
		}
		
		
		
		System.out.println(result);
	}
	
	private static Map<Long, LinkedList<Long>> getDivisorsForValues(
										long from, long to) {
		Map<Long, LinkedList<Long>> divisors 
				= new HashMap<Long, LinkedList<Long>>((int)(to - from));
		
		for(long n=from; n<=to; n++) {
			divisors.put(n, new LinkedList<Long>());
		}
		
		for(long n=1; n<=to; n++) {
			if(n%1000==0) {
				System.out.println(
						"Computing divisor from " + from + "... " + n);
			}
			
			long minC = (long) Math.ceil((double)from/(double)n);
			for(long c=minC; ;c++) {
				long value = c*n;
				long minDivisor 
					= (long) Math.ceil((Math.sqrt(value) / 2.0)) + 1;
				
				if(value > to) {
					break;
				}
				
				if(n>=minDivisor) {
					divisors.get(value).add(n);
				}				
			}
		}
		System.out.println(divisors.size());
		return divisors;
	}
	
	private static class Solution {
		private long a0;
		private long r;
		
		public Solution(long a0, long r) {
			this.a0 = a0;
			this.r = r;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (int)a0;
			result = prime * result + (int)r;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Solution other = (Solution) obj;
			if (a0 != other.a0)
				return false;
			if (r != other.r)
				return false;
			return true;
		}
		
		@Override
		public String toString() {
			return "[a0 = " + a0 + ", r = " + r + "]";
		}
	}
}
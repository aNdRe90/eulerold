
public class Problem24 
{
	/*
	 * A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation of the digits 1, 2, 3 and 4. If all of the permutations are listed numerically or alphabetically, we call it lexicographic order. The lexicographic permutations of 0, 1 and 2 are:

							012   021   102   120   201   210

		What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
	*/
	public static void main(String[] args) 
	{
		int whichLexicographicPermutation = 1000000;
		int forward = whichLexicographicPermutation - 1;
		
		forward = 3;
		StringBuffer perm = new StringBuffer("0231");
		int len = perm.length();
		
		int previousJump = -1;
		int repeats = 0;
		int leftIndex = -1;
		int rightIndex = -1;
		
		for(int j=1; forward>0; j++)
		{
			for(int i=9; i>0; i--)
				if(facts[i]<=forward)
				{
					forward -= facts[i];	
					
					rightIndex = len-i;
					if(previousJump!=facts[i])
					{						
						leftIndex = rightIndex - 1;
						repeats = 0;
					}
					else
						rightIndex += (++repeats);
					
					previousJump = facts[i];
					char temp = perm.charAt(rightIndex);
					perm.setCharAt(rightIndex, perm.charAt(leftIndex));
					perm.setCharAt(leftIndex, temp);
					break;
				}
		}	
		
		System.out.println(perm.toString());
	}
	
	private static int[] facts = { 1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880 };
}

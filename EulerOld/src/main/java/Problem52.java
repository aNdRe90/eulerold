
public class Problem52 
{
	/*
	 * It can be seen that the number, 125874, and its double, 251748, 
	 * contain exactly the same digits, but in a different order.
	 * 
	 * Find the smallest positive integer, x, 
	 * such that 2x, 3x, 4x, 5x, and 6x, contain the same digits.
	 */
	
	public static void main(String[] args) 
	{
		long smallest = 0L;
		for(long i=1; ;i++)
		{
			if(MyMath.areOfSameDigits(i, 2*i) && MyMath.areOfSameDigits(i, 3*i)
			&& MyMath.areOfSameDigits(i, 4*i) && MyMath.areOfSameDigits(i, 5*i)
			&& MyMath.areOfSameDigits(i, 6*i))
			{
				smallest = i;
				break;
			}
		}
		System.out.println(smallest);
	}
}
